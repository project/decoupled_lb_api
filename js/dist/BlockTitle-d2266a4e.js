var v = Object.defineProperty;
var b = (t, e, r) => e in t ? v(t, e, { enumerable: !0, configurable: !0, writable: !0, value: r }) : t[e] = r;
var l = (t, e, r) => (b(t, typeof e != "symbol" ? e + "" : e, r), r);
import { jsx as n, jsxs as i, Fragment as P } from "react/jsx-runtime";
import { createContext as c, useState as u, useContext as d } from "react";
import { s as g, b as y, a4 as F } from "./ErrorHelpers-a6fb9f99.js";
const m = c({
  resolved: {},
  promises: {},
  updatePlugins: () => {
  }
}), M = ({
  children: t,
  plugins: e
}) => {
  const [r, o] = u({
    promises: e,
    resolved: {}
  });
  return /* @__PURE__ */ n(
    m.Provider,
    {
      value: { ...r, updatePlugins: o },
      children: t
    }
  );
}, h = c({ promises: {}, resolved: {}, updatePlugins: () => {
} }), N = ({
  children: t,
  plugins: e
}) => {
  const [r, o] = u({
    promises: e,
    resolved: {}
  });
  return /* @__PURE__ */ n(h.Provider, { value: { ...r, updatePlugins: o }, children: t });
}, _ = ({
  fieldName: t,
  formatter: e
}) => /* @__PURE__ */ i("div", { className: "fallback", children: [
  "The ",
  t,
  " field is using the ",
  e.type,
  " formatter that doesn't have a React version. So is being rendered by a fallback."
] }), S = "fallback", k = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  Formatter: _,
  id: S
}, Symbol.toStringTag, { value: "Module" })), j = (t, e = !1) => {
  const r = d(m);
  if (t && t in r.resolved)
    return r.resolved[t];
  try {
    g(t, r.promises).then(
      (o) => {
        r.resolved[t] = o, r.updatePlugins(r);
      }
    );
  } catch (o) {
    if (e)
      return k;
    throw o;
  }
  return null;
}, E = ({
  itemValues: t,
  formatter: e,
  fieldName: r
}) => {
  const o = j(e.type, !0);
  return o ? /* @__PURE__ */ n(
    o.Formatter,
    {
      itemValues: t,
      formatter: e,
      fieldName: r
    }
  ) : /* @__PURE__ */ n("div", { children: "...Loading" });
}, x = ({
  fieldName: t,
  widget: e
}) => /* @__PURE__ */ i("div", { className: "fallback", children: [
  "The ",
  t,
  " field is using the ",
  e.type,
  " widget that doesn't have a React version. So is being rendered by a fallback."
] }), T = "fallback", W = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  Widget: x,
  id: T
}, Symbol.toStringTag, { value: "Module" })), C = (t, e = !1) => {
  const r = d(h);
  if (t && t in r.resolved)
    return r.resolved[t];
  try {
    g(t, r.promises).then(
      (o) => {
        r.resolved[t] = o, r.updatePlugins(r);
      }
    );
  } catch (o) {
    if (e)
      return W;
    throw o;
  }
  return null;
}, R = ({
  itemValues: t,
  widget: e,
  fieldName: r,
  update: o
}) => {
  const s = C(e.type, !0);
  return s ? /* @__PURE__ */ n(
    s.Widget,
    {
      itemValues: t,
      widget: e,
      fieldName: r,
      update: o
    }
  ) : null;
};
class L {
  constructor(e) {
    l(this, "data");
    this.data = e;
  }
  getComponent(e) {
    return e in this.data.components ? this.data.components[e] : !1;
  }
  getSortedComponents() {
    return Object.entries(this.data.components).sort(([, e], [, r]) => e.weight - r.weight).reduce(
      (e, [r, o]) => ({ ...e, [r]: o }),
      {}
    );
  }
}
const O = ({
  label: t,
  label_display: e
}) => {
  if (e)
    return /* @__PURE__ */ n("h2", { children: t });
}, $ = (t) => {
  const { uuid: e, configuration: r } = t, { label: o, label_display: s } = r, p = y(), f = (a) => {
    p(
      F({
        componentId: e,
        values: {
          ...t,
          configuration: {
            ...t.configuration,
            label: a
          }
        }
      })
    );
  };
  if (s)
    return /* @__PURE__ */ i(P, { children: [
      /* @__PURE__ */ n("label", { htmlFor: `label-${e}`, hidden: !0, children: "Block label" }),
      /* @__PURE__ */ n(
        "input",
        {
          className: "as-h2",
          name: "label",
          id: `label-${e}`,
          value: o || "",
          onChange: (a) => f(a.currentTarget.value)
        }
      )
    ] });
}, q = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  Edit: $,
  Preview: O
}, Symbol.toStringTag, { value: "Module" }));
export {
  q as B,
  L as D,
  $ as E,
  M as F,
  O as P,
  N as W,
  E as a,
  R as b,
  j as c,
  C as u
};
