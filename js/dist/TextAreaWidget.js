import { jsx as s } from "react/jsx-runtime";
import { F as n } from "./Field-d8905e3d.js";
import { E as p } from "./Editor-4eaaaeec.js";
import "react";
import "./_commonjsHelpers-10dfc225.js";
const u = ({ itemValues: t, update: i }) => /* @__PURE__ */ s(
  n,
  {
    items: t.map((o, r) => ({
      content: /* @__PURE__ */ s(
        p,
        {
          data: o.value,
          id: r,
          onChange: (c, m) => {
            const a = m.getData(), e = [...t];
            e.splice(r, 1, {
              value: a,
              processed: a,
              format: o.format
            }), i(e);
          }
        }
      )
    }))
  }
), v = "text_textarea";
export {
  u as Widget,
  v as id
};
