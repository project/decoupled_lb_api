import { jsxs as d, jsx as t } from "react/jsx-runtime";
import { useMemo as y } from "react";
import { ad as p, Q as v, b as k, a4 as h, P as m } from "./ErrorHelpers-a6fb9f99.js";
import { P as z, a as _, E as D, b as C, D as b } from "./BlockTitle-d2266a4e.js";
import "react-dom";
import "./_commonjsHelpers-10dfc225.js";
const I = (i) => {
  const [, s] = i.configuration.id.split(":"), n = y(
    () => ({
      entityType: "block_content",
      bundle: s,
      viewMode: i.configuration.view_mode_uuid,
      type: "view"
    }),
    [s, i.configuration.view_mode_uuid]
  ), {
    error: a,
    data: o,
    isLoading: l
  } = p(n);
  if (a)
    return /* @__PURE__ */ d("div", { children: [
      "An error occurred fetching display settings",
      " ",
      v(a, "Could not load entity view display")
    ] });
  if (l)
    return /* @__PURE__ */ d("div", { children: [
      "loading view display...In preview ",
      i.uuid
    ] });
  if (o) {
    const c = new b(o).getSortedComponents();
    return /* @__PURE__ */ d("div", { children: [
      /* @__PURE__ */ t(z, { ...i.configuration }),
      Object.entries(c).filter(
        ([e]) => Array.isArray(i.configuration.block_serialized[e]) && i.configuration.block_serialized[e].length
      ).map(([e, r]) => /* @__PURE__ */ t(
        _,
        {
          fieldName: e,
          itemValues: i.configuration.block_serialized[e],
          formatter: r
        },
        e
      ))
    ] });
  }
}, M = (i) => {
  const [, s] = i.configuration.id.split(":"), n = k(), a = y(
    () => ({
      entityType: "block_content",
      bundle: s,
      // See \Drupal\layout_builder\Plugin\Block\InlineBlock::processBlockForm
      viewMode: i.configuration.form_mode_uuid,
      type: "form"
    }),
    [s, i.configuration.form_mode_uuid]
  ), {
    error: o,
    data: l,
    isLoading: u
  } = p(a);
  if (o)
    return /* @__PURE__ */ d("div", { children: [
      "An error occurred fetching display settings",
      " ",
      v(o, "Could not load entity form display")
    ] });
  if (u)
    return /* @__PURE__ */ d("div", { children: [
      "loading form display...In preview ",
      i.uuid
    ] });
  if (l) {
    const e = new b(l).getSortedComponents();
    return /* @__PURE__ */ d("div", { children: [
      /* @__PURE__ */ t(D, { ...i }),
      Object.entries(e).map(([r, f]) => {
        const g = /* @__PURE__ */ t(
          C,
          {
            fieldName: r,
            update: (w) => {
              n(
                h({
                  componentId: i.uuid,
                  values: {
                    ...i,
                    configuration: {
                      ...i.configuration,
                      block_serialized: {
                        ...i.configuration.block_serialized,
                        [r]: w
                      }
                    }
                  }
                })
              );
            },
            itemValues: i.configuration.block_serialized[r],
            widget: f
          },
          r
        );
        return g || /* @__PURE__ */ t(
          _,
          {
            fieldName: r,
            itemValues: i.configuration.block_serialized[r],
            formatter: f
          },
          r
        );
      })
    ] });
  }
  return /* @__PURE__ */ t(I, { ...i });
}, N = m, V = "inline_block";
export {
  M as Edit,
  I as Preview,
  N as Settings,
  V as id
};
