import { jsx as o } from "react/jsx-runtime";
import { F as m } from "./Field-d8905e3d.js";
import "react";
const s = ({
  itemValues: t
}) => /* @__PURE__ */ o(
  m,
  {
    items: t.map((r) => ({
      content: r.value
    }))
  }
), p = "string";
export {
  s as Formatter,
  p as id
};
