import { jsx as E, jsxs as Z, Fragment as Za } from "react/jsx-runtime";
import N, { createContext as wn, useState as Ae, useLayoutEffect as En, useEffect as ie, useMemo as ae, useContext as Je, useReducer as _a, useRef as O } from "react";
import { am as Ht, an as $t, ao as ei, ap as ri, aq as ti, P as ni, s as ai, b as Ar, u as F, H as dt, a3 as ii, ab as oi, M as qt, ar as se, aa as Ue, q as ue, ae as pt, aj as li, Q as Pn, p as Ge, c as vt, ak as si, I as An, o as Bn, a as Rn, i as ft, af as On, e as ui, as as hr, J as tt, at as He, a9 as be, a1 as ci, a0 as di, _ as pi, Y as vi, Z as fi, D as Vt, ag as Tn, r as gi, a2 as mi } from "./ErrorHelpers-a6fb9f99.js";
import bi, { unstable_batchedUpdates as hi } from "react-dom";
import { g as yi } from "./_commonjsHelpers-10dfc225.js";
function S() {
  return S = Object.assign ? Object.assign.bind() : function(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = arguments[r];
      for (var n in t)
        Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
    }
    return e;
  }, S.apply(this, arguments);
}
function yr(e, r) {
  if (e == null)
    return {};
  var t = {}, n = Object.keys(e), i, a;
  for (a = 0; a < n.length; a++)
    i = n[a], !(r.indexOf(i) >= 0) && (t[i] = e[i]);
  return t;
}
const Nn = wn({
  resolved: {},
  promises: {},
  updatePlugins: () => {
  }
}), _c = ({
  children: e,
  plugins: r
}) => {
  const [t, n] = Ae({
    promises: r,
    resolved: {}
  });
  return /* @__PURE__ */ E(Nn.Provider, { value: { ...t, updatePlugins: n }, children: e });
};
function nt(e, r) {
  return nt = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function(n, i) {
    return n.__proto__ = i, n;
  }, nt(e, r);
}
function Mn(e, r) {
  e.prototype = Object.create(r.prototype), e.prototype.constructor = e, nt(e, r);
}
var Ln = /* @__PURE__ */ N.createContext(null);
function Di(e) {
  e();
}
var Fn = Di, Ii = function(r) {
  return Fn = r;
}, xi = function() {
  return Fn;
};
function Si() {
  var e = xi(), r = null, t = null;
  return {
    clear: function() {
      r = null, t = null;
    },
    notify: function() {
      e(function() {
        for (var i = r; i; )
          i.callback(), i = i.next;
      });
    },
    get: function() {
      for (var i = [], a = r; a; )
        i.push(a), a = a.next;
      return i;
    },
    subscribe: function(i) {
      var a = !0, o = t = {
        callback: i,
        next: null,
        prev: t
      };
      return o.prev ? o.prev.next = o : r = o, function() {
        !a || r === null || (a = !1, o.next ? o.next.prev = o.prev : t = o.prev, o.prev ? o.prev.next = o.next : r = o.next);
      };
    }
  };
}
var jt = {
  notify: function() {
  },
  get: function() {
    return [];
  }
};
function Gn(e, r) {
  var t, n = jt;
  function i(c) {
    return s(), n.subscribe(c);
  }
  function a() {
    n.notify();
  }
  function o() {
    p.onStateChange && p.onStateChange();
  }
  function l() {
    return !!t;
  }
  function s() {
    t || (t = r ? r.addNestedSub(o) : e.subscribe(o), n = Si());
  }
  function d() {
    t && (t(), t = void 0, n.clear(), n = jt);
  }
  var p = {
    addNestedSub: i,
    notifyNestedSubs: a,
    handleChangeWrapper: o,
    isSubscribed: l,
    trySubscribe: s,
    tryUnsubscribe: d,
    getListeners: function() {
      return n;
    }
  };
  return p;
}
var kn = typeof window < "u" && typeof window.document < "u" && typeof window.document.createElement < "u" ? En : ie;
function Ci(e) {
  var r = e.store, t = e.context, n = e.children, i = ae(function() {
    var l = Gn(r);
    return {
      store: r,
      subscription: l
    };
  }, [r]), a = ae(function() {
    return r.getState();
  }, [r]);
  kn(function() {
    var l = i.subscription;
    return l.onStateChange = l.notifyNestedSubs, l.trySubscribe(), a !== r.getState() && l.notifyNestedSubs(), function() {
      l.tryUnsubscribe(), l.onStateChange = null;
    };
  }, [i, a]);
  var o = t || Ln;
  return /* @__PURE__ */ N.createElement(o.Provider, {
    value: i
  }, n);
}
var Wn = { exports: {} }, T = {};
/** @license React v17.0.2
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Br = 60103, Rr = 60106, Xe = 60107, Qe = 60108, Ze = 60114, _e = 60109, er = 60110, rr = 60112, tr = 60113, gt = 60120, nr = 60115, ar = 60116, Un = 60121, Hn = 60122, $n = 60117, qn = 60129, Vn = 60131;
if (typeof Symbol == "function" && Symbol.for) {
  var U = Symbol.for;
  Br = U("react.element"), Rr = U("react.portal"), Xe = U("react.fragment"), Qe = U("react.strict_mode"), Ze = U("react.profiler"), _e = U("react.provider"), er = U("react.context"), rr = U("react.forward_ref"), tr = U("react.suspense"), gt = U("react.suspense_list"), nr = U("react.memo"), ar = U("react.lazy"), Un = U("react.block"), Hn = U("react.server.block"), $n = U("react.fundamental"), qn = U("react.debug_trace_mode"), Vn = U("react.legacy_hidden");
}
function ne(e) {
  if (typeof e == "object" && e !== null) {
    var r = e.$$typeof;
    switch (r) {
      case Br:
        switch (e = e.type, e) {
          case Xe:
          case Ze:
          case Qe:
          case tr:
          case gt:
            return e;
          default:
            switch (e = e && e.$$typeof, e) {
              case er:
              case rr:
              case ar:
              case nr:
              case _e:
                return e;
              default:
                return r;
            }
        }
      case Rr:
        return r;
    }
  }
}
var wi = _e, Ei = Br, Pi = rr, Ai = Xe, Bi = ar, Ri = nr, Oi = Rr, Ti = Ze, Ni = Qe, Mi = tr;
T.ContextConsumer = er;
T.ContextProvider = wi;
T.Element = Ei;
T.ForwardRef = Pi;
T.Fragment = Ai;
T.Lazy = Bi;
T.Memo = Ri;
T.Portal = Oi;
T.Profiler = Ti;
T.StrictMode = Ni;
T.Suspense = Mi;
T.isAsyncMode = function() {
  return !1;
};
T.isConcurrentMode = function() {
  return !1;
};
T.isContextConsumer = function(e) {
  return ne(e) === er;
};
T.isContextProvider = function(e) {
  return ne(e) === _e;
};
T.isElement = function(e) {
  return typeof e == "object" && e !== null && e.$$typeof === Br;
};
T.isForwardRef = function(e) {
  return ne(e) === rr;
};
T.isFragment = function(e) {
  return ne(e) === Xe;
};
T.isLazy = function(e) {
  return ne(e) === ar;
};
T.isMemo = function(e) {
  return ne(e) === nr;
};
T.isPortal = function(e) {
  return ne(e) === Rr;
};
T.isProfiler = function(e) {
  return ne(e) === Ze;
};
T.isStrictMode = function(e) {
  return ne(e) === Qe;
};
T.isSuspense = function(e) {
  return ne(e) === tr;
};
T.isValidElementType = function(e) {
  return typeof e == "string" || typeof e == "function" || e === Xe || e === Ze || e === qn || e === Qe || e === tr || e === gt || e === Vn || typeof e == "object" && e !== null && (e.$$typeof === ar || e.$$typeof === nr || e.$$typeof === _e || e.$$typeof === er || e.$$typeof === rr || e.$$typeof === $n || e.$$typeof === Un || e[0] === Hn);
};
T.typeOf = ne;
Wn.exports = T;
var Li = Wn.exports, Fi = ["getDisplayName", "methodName", "renderCountProp", "shouldHandleStateChanges", "storeKey", "withRef", "forwardRef", "context"], Gi = ["reactReduxForwardedRef"], ki = [], Wi = [null, null];
function Ui(e, r) {
  var t = e[1];
  return [r.payload, t + 1];
}
function zt(e, r, t) {
  kn(function() {
    return e.apply(void 0, r);
  }, t);
}
function Hi(e, r, t, n, i, a, o) {
  e.current = n, r.current = i, t.current = !1, a.current && (a.current = null, o());
}
function $i(e, r, t, n, i, a, o, l, s, d) {
  if (e) {
    var p = !1, c = null, u = function() {
      if (!p) {
        var g = r.getState(), b, m;
        try {
          b = n(g, i.current);
        } catch (y) {
          m = y, c = y;
        }
        m || (c = null), b === a.current ? o.current || s() : (a.current = b, l.current = b, o.current = !0, d({
          type: "STORE_UPDATED",
          payload: {
            error: m
          }
        }));
      }
    };
    t.onStateChange = u, t.trySubscribe(), u();
    var v = function() {
      if (p = !0, t.tryUnsubscribe(), t.onStateChange = null, c)
        throw c;
    };
    return v;
  }
}
var qi = function() {
  return [null, 0];
};
function Vi(e, r) {
  r === void 0 && (r = {});
  var t = r, n = t.getDisplayName, i = n === void 0 ? function(I) {
    return "ConnectAdvanced(" + I + ")";
  } : n, a = t.methodName, o = a === void 0 ? "connectAdvanced" : a, l = t.renderCountProp, s = l === void 0 ? void 0 : l, d = t.shouldHandleStateChanges, p = d === void 0 ? !0 : d, c = t.storeKey, u = c === void 0 ? "store" : c;
  t.withRef;
  var v = t.forwardRef, f = v === void 0 ? !1 : v, g = t.context, b = g === void 0 ? Ln : g, m = yr(t, Fi), y = b;
  return function(D) {
    var w = D.displayName || D.name || "Component", C = i(w), B = S({}, m, {
      getDisplayName: i,
      methodName: o,
      renderCountProp: s,
      shouldHandleStateChanges: p,
      storeKey: u,
      displayName: C,
      wrappedComponentName: w,
      WrappedComponent: D
    }), M = m.pure;
    function P(G) {
      return e(G.dispatch, B);
    }
    var L = M ? ae : function(G) {
      return G();
    };
    function R(G) {
      var J = ae(function() {
        var Ne = G.reactReduxForwardedRef, qr = yr(G, Gi);
        return [G.context, Ne, qr];
      }, [G]), X = J[0], De = J[1], oe = J[2], Ie = ae(function() {
        return X && X.Consumer && Li.isContextConsumer(/* @__PURE__ */ N.createElement(X.Consumer, null)) ? X : y;
      }, [X, y]), re = Je(Ie), ge = !!G.store && !!G.store.getState && !!G.store.dispatch;
      re && re.store;
      var q = ge ? G.store : re.store, Te = ae(function() {
        return P(q);
      }, [q]), lr = ae(function() {
        if (!p)
          return Wi;
        var Ne = Gn(q, ge ? null : re.subscription), qr = Ne.notifyNestedSubs.bind(Ne);
        return [Ne, qr];
      }, [q, ge, re]), le = lr[0], sr = lr[1], ur = ae(function() {
        return ge ? re : S({}, re, {
          subscription: le
        });
      }, [ge, re, le]), cr = _a(Ui, ki, qi), kr = cr[0], xe = kr[0], Wr = cr[1];
      if (xe && xe.error)
        throw xe.error;
      var Wt = O(), Ur = O(oe), dr = O(), Ut = O(!1), Hr = L(function() {
        return dr.current && oe === Ur.current ? dr.current : Te(q.getState(), oe);
      }, [q, xe, oe]);
      zt(Hi, [Ur, Wt, Ut, oe, Hr, dr, sr]), zt($i, [p, q, le, Te, Ur, Wt, Ut, dr, sr, Wr], [q, le, Te]);
      var $r = ae(function() {
        return /* @__PURE__ */ N.createElement(D, S({}, Hr, {
          ref: De
        }));
      }, [De, D, Hr]), Qa = ae(function() {
        return p ? /* @__PURE__ */ N.createElement(Ie.Provider, {
          value: ur
        }, $r) : $r;
      }, [Ie, $r, ur]);
      return Qa;
    }
    var K = M ? N.memo(R) : R;
    if (K.WrappedComponent = D, K.displayName = R.displayName = C, f) {
      var Y = N.forwardRef(function(J, X) {
        return /* @__PURE__ */ N.createElement(K, S({}, J, {
          reactReduxForwardedRef: X
        }));
      });
      return Y.displayName = C, Y.WrappedComponent = D, Ht(Y, D);
    }
    return Ht(K, D);
  };
}
function Kt(e, r) {
  return e === r ? e !== 0 || r !== 0 || 1 / e === 1 / r : e !== e && r !== r;
}
function Vr(e, r) {
  if (Kt(e, r))
    return !0;
  if (typeof e != "object" || e === null || typeof r != "object" || r === null)
    return !1;
  var t = Object.keys(e), n = Object.keys(r);
  if (t.length !== n.length)
    return !1;
  for (var i = 0; i < t.length; i++)
    if (!Object.prototype.hasOwnProperty.call(r, t[i]) || !Kt(e[t[i]], r[t[i]]))
      return !1;
  return !0;
}
function ji(e, r) {
  var t = {}, n = function(o) {
    var l = e[o];
    typeof l == "function" && (t[o] = function() {
      return r(l.apply(void 0, arguments));
    });
  };
  for (var i in e)
    n(i);
  return t;
}
function mt(e) {
  return function(t, n) {
    var i = e(t, n);
    function a() {
      return i;
    }
    return a.dependsOnOwnProps = !1, a;
  };
}
function Yt(e) {
  return e.dependsOnOwnProps !== null && e.dependsOnOwnProps !== void 0 ? !!e.dependsOnOwnProps : e.length !== 1;
}
function jn(e, r) {
  return function(n, i) {
    i.displayName;
    var a = function(l, s) {
      return a.dependsOnOwnProps ? a.mapToProps(l, s) : a.mapToProps(l);
    };
    return a.dependsOnOwnProps = !0, a.mapToProps = function(l, s) {
      a.mapToProps = e, a.dependsOnOwnProps = Yt(e);
      var d = a(l, s);
      return typeof d == "function" && (a.mapToProps = d, a.dependsOnOwnProps = Yt(d), d = a(l, s)), d;
    }, a;
  };
}
function zi(e) {
  return typeof e == "function" ? jn(e) : void 0;
}
function Ki(e) {
  return e ? void 0 : mt(function(r) {
    return {
      dispatch: r
    };
  });
}
function Yi(e) {
  return e && typeof e == "object" ? mt(function(r) {
    return ji(e, r);
  }) : void 0;
}
const Ji = [zi, Ki, Yi];
function Xi(e) {
  return typeof e == "function" ? jn(e) : void 0;
}
function Qi(e) {
  return e ? void 0 : mt(function() {
    return {};
  });
}
const Zi = [Xi, Qi];
function _i(e, r, t) {
  return S({}, t, e, r);
}
function eo(e) {
  return function(t, n) {
    n.displayName;
    var i = n.pure, a = n.areMergedPropsEqual, o = !1, l;
    return function(d, p, c) {
      var u = e(d, p, c);
      return o ? (!i || !a(u, l)) && (l = u) : (o = !0, l = u), l;
    };
  };
}
function ro(e) {
  return typeof e == "function" ? eo(e) : void 0;
}
function to(e) {
  return e ? void 0 : function() {
    return _i;
  };
}
const no = [ro, to];
var ao = ["initMapStateToProps", "initMapDispatchToProps", "initMergeProps"];
function io(e, r, t, n) {
  return function(a, o) {
    return t(e(a, o), r(n, o), o);
  };
}
function oo(e, r, t, n, i) {
  var a = i.areStatesEqual, o = i.areOwnPropsEqual, l = i.areStatePropsEqual, s = !1, d, p, c, u, v;
  function f(I, D) {
    return d = I, p = D, c = e(d, p), u = r(n, p), v = t(c, u, p), s = !0, v;
  }
  function g() {
    return c = e(d, p), r.dependsOnOwnProps && (u = r(n, p)), v = t(c, u, p), v;
  }
  function b() {
    return e.dependsOnOwnProps && (c = e(d, p)), r.dependsOnOwnProps && (u = r(n, p)), v = t(c, u, p), v;
  }
  function m() {
    var I = e(d, p), D = !l(I, c);
    return c = I, D && (v = t(c, u, p)), v;
  }
  function y(I, D) {
    var w = !o(D, p), C = !a(I, d, D, p);
    return d = I, p = D, w && C ? g() : w ? b() : C ? m() : v;
  }
  return function(D, w) {
    return s ? y(D, w) : f(D, w);
  };
}
function lo(e, r) {
  var t = r.initMapStateToProps, n = r.initMapDispatchToProps, i = r.initMergeProps, a = yr(r, ao), o = t(e, a), l = n(e, a), s = i(e, a), d = a.pure ? oo : io;
  return d(o, l, s, e, a);
}
var so = ["pure", "areStatesEqual", "areOwnPropsEqual", "areStatePropsEqual", "areMergedPropsEqual"];
function jr(e, r, t) {
  for (var n = r.length - 1; n >= 0; n--) {
    var i = r[n](e);
    if (i)
      return i;
  }
  return function(a, o) {
    throw new Error("Invalid value of type " + typeof e + " for " + t + " argument when connecting component " + o.wrappedComponentName + ".");
  };
}
function uo(e, r) {
  return e === r;
}
function co(e) {
  var r = e === void 0 ? {} : e, t = r.connectHOC, n = t === void 0 ? Vi : t, i = r.mapStateToPropsFactories, a = i === void 0 ? Zi : i, o = r.mapDispatchToPropsFactories, l = o === void 0 ? Ji : o, s = r.mergePropsFactories, d = s === void 0 ? no : s, p = r.selectorFactory, c = p === void 0 ? lo : p;
  return function(v, f, g, b) {
    b === void 0 && (b = {});
    var m = b, y = m.pure, I = y === void 0 ? !0 : y, D = m.areStatesEqual, w = D === void 0 ? uo : D, C = m.areOwnPropsEqual, B = C === void 0 ? Vr : C, M = m.areStatePropsEqual, P = M === void 0 ? Vr : M, L = m.areMergedPropsEqual, R = L === void 0 ? Vr : L, K = yr(m, so), Y = jr(v, a, "mapStateToProps"), G = jr(f, l, "mapDispatchToProps"), J = jr(g, d, "mergeProps");
    return n(c, S({
      // used in error messages
      methodName: "connect",
      // used to compute Connect's displayName from the wrapped component's displayName.
      getDisplayName: function(De) {
        return "Connect(" + De + ")";
      },
      // if mapStateToProps is falsy, the Connect component doesn't subscribe to store state changes
      shouldHandleStateChanges: !!v,
      // passed through to selectorFactory
      initMapStateToProps: Y,
      initMapDispatchToProps: G,
      initMergeProps: J,
      pure: I,
      areStatesEqual: w,
      areOwnPropsEqual: B,
      areStatePropsEqual: P,
      areMergedPropsEqual: R
    }, K));
  };
}
const zn = /* @__PURE__ */ co();
Ii(hi);
function po(e, r) {
  if (e.length !== r.length)
    return !1;
  for (var t = 0; t < e.length; t++)
    if (e[t] !== r[t])
      return !1;
  return !0;
}
function Kn(e, r) {
  var t = Ae(function() {
    return {
      inputs: r,
      result: e()
    };
  })[0], n = O(!0), i = O(t), a = n.current || !!(r && i.current.inputs && po(r, i.current.inputs)), o = a ? i.current : {
    inputs: r,
    result: e()
  };
  return ie(function() {
    n.current = !1, i.current = o;
  }, [o]), o.result;
}
function vo(e, r) {
  return Kn(function() {
    return e;
  }, r);
}
var A = Kn, x = vo, fo = !0, zr = "Invariant failed";
function go(e, r) {
  if (!e) {
    if (fo)
      throw new Error(zr);
    var t = typeof r == "function" ? r() : r, n = t ? "".concat(zr, ": ").concat(t) : zr;
    throw new Error(n);
  }
}
var te = function(r) {
  var t = r.top, n = r.right, i = r.bottom, a = r.left, o = n - a, l = i - t, s = {
    top: t,
    right: n,
    bottom: i,
    left: a,
    width: o,
    height: l,
    x: a,
    y: t,
    center: {
      x: (n + a) / 2,
      y: (i + t) / 2
    }
  };
  return s;
}, bt = function(r, t) {
  return {
    top: r.top - t.top,
    left: r.left - t.left,
    bottom: r.bottom + t.bottom,
    right: r.right + t.right
  };
}, Jt = function(r, t) {
  return {
    top: r.top + t.top,
    left: r.left + t.left,
    bottom: r.bottom - t.bottom,
    right: r.right - t.right
  };
}, mo = function(r, t) {
  return {
    top: r.top + t.y,
    left: r.left + t.x,
    bottom: r.bottom + t.y,
    right: r.right + t.x
  };
}, Kr = {
  top: 0,
  right: 0,
  bottom: 0,
  left: 0
}, ht = function(r) {
  var t = r.borderBox, n = r.margin, i = n === void 0 ? Kr : n, a = r.border, o = a === void 0 ? Kr : a, l = r.padding, s = l === void 0 ? Kr : l, d = te(bt(t, i)), p = te(Jt(t, o)), c = te(Jt(p, s));
  return {
    marginBox: d,
    borderBox: te(t),
    paddingBox: p,
    contentBox: c,
    margin: i,
    border: o,
    padding: s
  };
}, Q = function(r) {
  var t = r.slice(0, -2), n = r.slice(-2);
  if (n !== "px")
    return 0;
  var i = Number(t);
  return isNaN(i) && go(!1), i;
}, bo = function() {
  return {
    x: window.pageXOffset,
    y: window.pageYOffset
  };
}, Dr = function(r, t) {
  var n = r.borderBox, i = r.border, a = r.margin, o = r.padding, l = mo(n, t);
  return ht({
    borderBox: l,
    border: i,
    margin: a,
    padding: o
  });
}, Ir = function(r, t) {
  return t === void 0 && (t = bo()), Dr(r, t);
}, Yn = function(r, t) {
  var n = {
    top: Q(t.marginTop),
    right: Q(t.marginRight),
    bottom: Q(t.marginBottom),
    left: Q(t.marginLeft)
  }, i = {
    top: Q(t.paddingTop),
    right: Q(t.paddingRight),
    bottom: Q(t.paddingBottom),
    left: Q(t.paddingLeft)
  }, a = {
    top: Q(t.borderTopWidth),
    right: Q(t.borderRightWidth),
    bottom: Q(t.borderBottomWidth),
    left: Q(t.borderLeftWidth)
  };
  return ht({
    borderBox: r,
    margin: n,
    padding: i,
    border: a
  });
}, Jn = function(r) {
  var t = r.getBoundingClientRect(), n = window.getComputedStyle(r);
  return Yn(t, n);
}, Xt = Number.isNaN || function(r) {
  return typeof r == "number" && r !== r;
};
function ho(e, r) {
  return !!(e === r || Xt(e) && Xt(r));
}
function yo(e, r) {
  if (e.length !== r.length)
    return !1;
  for (var t = 0; t < e.length; t++)
    if (!ho(e[t], r[t]))
      return !1;
  return !0;
}
function k(e, r) {
  r === void 0 && (r = yo);
  var t, n = [], i, a = !1;
  function o() {
    for (var l = [], s = 0; s < arguments.length; s++)
      l[s] = arguments[s];
    return a && t === this && r(l, n) || (i = e.apply(this, l), a = !0, t = this, n = l), i;
  }
  return o;
}
var Do = function(r) {
  var t = [], n = null, i = function() {
    for (var o = arguments.length, l = new Array(o), s = 0; s < o; s++)
      l[s] = arguments[s];
    t = l, !n && (n = requestAnimationFrame(function() {
      n = null, r.apply(void 0, t);
    }));
  };
  return i.cancel = function() {
    n && (cancelAnimationFrame(n), n = null);
  }, i;
};
const $e = Do;
function Xn(e, r) {
}
Xn.bind(null, "warn");
Xn.bind(null, "error");
function ce() {
}
function Io(e, r) {
  return S({}, e, {}, r);
}
function _(e, r, t) {
  var n = r.map(function(i) {
    var a = Io(t, i.options);
    return e.addEventListener(i.eventName, i.fn, a), function() {
      e.removeEventListener(i.eventName, i.fn, a);
    };
  });
  return function() {
    n.forEach(function(a) {
      a();
    });
  };
}
var xo = "Invariant failed";
function xr(e) {
  this.message = e;
}
xr.prototype.toString = function() {
  return this.message;
};
function h(e, r) {
  if (!e)
    throw new xr(xo);
}
var So = function(e) {
  Mn(r, e);
  function r() {
    for (var n, i = arguments.length, a = new Array(i), o = 0; o < i; o++)
      a[o] = arguments[o];
    return n = e.call.apply(e, [this].concat(a)) || this, n.callbacks = null, n.unbind = ce, n.onWindowError = function(l) {
      var s = n.getCallbacks();
      s.isDragging() && s.tryAbort();
      var d = l.error;
      d instanceof xr && l.preventDefault();
    }, n.getCallbacks = function() {
      if (!n.callbacks)
        throw new Error("Unable to find AppCallbacks in <ErrorBoundary/>");
      return n.callbacks;
    }, n.setCallbacks = function(l) {
      n.callbacks = l;
    }, n;
  }
  var t = r.prototype;
  return t.componentDidMount = function() {
    this.unbind = _(window, [{
      eventName: "error",
      fn: this.onWindowError
    }]);
  }, t.componentDidCatch = function(i) {
    if (i instanceof xr) {
      this.setState({});
      return;
    }
    throw i;
  }, t.componentWillUnmount = function() {
    this.unbind();
  }, t.render = function() {
    return this.props.children(this.setCallbacks);
  }, r;
}(N.Component), Co = `
  Press space bar to start a drag.
  When dragging you can use the arrow keys to move the item around and escape to cancel.
  Some screen readers may require you to be in focus mode or to use your pass through key
`, Sr = function(r) {
  return r + 1;
}, wo = function(r) {
  return `
  You have lifted an item in position ` + Sr(r.source.index) + `
`;
}, Qn = function(r, t) {
  var n = r.droppableId === t.droppableId, i = Sr(r.index), a = Sr(t.index);
  return n ? `
      You have moved the item from position ` + i + `
      to position ` + a + `
    ` : `
    You have moved the item from position ` + i + `
    in list ` + r.droppableId + `
    to list ` + t.droppableId + `
    in position ` + a + `
  `;
}, Zn = function(r, t, n) {
  var i = t.droppableId === n.droppableId;
  return i ? `
      The item ` + r + `
      has been combined with ` + n.draggableId : `
      The item ` + r + `
      in list ` + t.droppableId + `
      has been combined with ` + n.draggableId + `
      in list ` + n.droppableId + `
    `;
}, Eo = function(r) {
  var t = r.destination;
  if (t)
    return Qn(r.source, t);
  var n = r.combine;
  return n ? Zn(r.draggableId, r.source, n) : "You are over an area that cannot be dropped on";
}, Qt = function(r) {
  return `
  The item has returned to its starting position
  of ` + Sr(r.index) + `
`;
}, Po = function(r) {
  if (r.reason === "CANCEL")
    return `
      Movement cancelled.
      ` + Qt(r.source) + `
    `;
  var t = r.destination, n = r.combine;
  return t ? `
      You have dropped the item.
      ` + Qn(r.source, t) + `
    ` : n ? `
      You have dropped the item.
      ` + Zn(r.draggableId, r.source, n) + `
    ` : `
    The item has been dropped while not over a drop area.
    ` + Qt(r.source) + `
  `;
}, br = {
  dragHandleUsageInstructions: Co,
  onDragStart: wo,
  onDragUpdate: Eo,
  onDragEnd: Po
}, W = {
  x: 0,
  y: 0
}, H = function(r, t) {
  return {
    x: r.x + t.x,
    y: r.y + t.y
  };
}, V = function(r, t) {
  return {
    x: r.x - t.x,
    y: r.y - t.y
  };
}, de = function(r, t) {
  return r.x === t.x && r.y === t.y;
}, Be = function(r) {
  return {
    x: r.x !== 0 ? -r.x : 0,
    y: r.y !== 0 ? -r.y : 0
  };
}, ye = function(r, t, n) {
  var i;
  return n === void 0 && (n = 0), i = {}, i[r] = t, i[r === "x" ? "y" : "x"] = n, i;
}, qe = function(r, t) {
  return Math.sqrt(Math.pow(t.x - r.x, 2) + Math.pow(t.y - r.y, 2));
}, Zt = function(r, t) {
  return Math.min.apply(Math, t.map(function(n) {
    return qe(r, n);
  }));
}, _n = function(r) {
  return function(t) {
    return {
      x: r(t.x),
      y: r(t.y)
    };
  };
}, Ao = function(e, r) {
  var t = te({
    top: Math.max(r.top, e.top),
    right: Math.min(r.right, e.right),
    bottom: Math.min(r.bottom, e.bottom),
    left: Math.max(r.left, e.left)
  });
  return t.width <= 0 || t.height <= 0 ? null : t;
}, ir = function(r, t) {
  return {
    top: r.top + t.y,
    left: r.left + t.x,
    bottom: r.bottom + t.y,
    right: r.right + t.x
  };
}, _t = function(r) {
  return [{
    x: r.left,
    y: r.top
  }, {
    x: r.right,
    y: r.top
  }, {
    x: r.left,
    y: r.bottom
  }, {
    x: r.right,
    y: r.bottom
  }];
}, Bo = {
  top: 0,
  right: 0,
  bottom: 0,
  left: 0
}, Ro = function(r, t) {
  return t ? ir(r, t.scroll.diff.displacement) : r;
}, Oo = function(r, t, n) {
  if (n && n.increasedBy) {
    var i;
    return S({}, r, (i = {}, i[t.end] = r[t.end] + n.increasedBy[t.line], i));
  }
  return r;
}, To = function(r, t) {
  return t && t.shouldClipSubject ? Ao(t.pageMarginBox, r) : te(r);
}, Ce = function(e) {
  var r = e.page, t = e.withPlaceholder, n = e.axis, i = e.frame, a = Ro(r.marginBox, i), o = Oo(a, n, t), l = To(o, i);
  return {
    page: r,
    withPlaceholder: t,
    active: l
  };
}, yt = function(e, r) {
  e.frame || h(!1);
  var t = e.frame, n = V(r, t.scroll.initial), i = Be(n), a = S({}, t, {
    scroll: {
      initial: t.scroll.initial,
      current: r,
      diff: {
        value: n,
        displacement: i
      },
      max: t.scroll.max
    }
  }), o = Ce({
    page: e.subject.page,
    withPlaceholder: e.subject.withPlaceholder,
    axis: e.axis,
    frame: a
  }), l = S({}, e, {
    frame: a,
    subject: o
  });
  return l;
};
function Cr(e) {
  return Object.values ? Object.values(e) : Object.keys(e).map(function(r) {
    return e[r];
  });
}
function Dt(e, r) {
  if (e.findIndex)
    return e.findIndex(r);
  for (var t = 0; t < e.length; t++)
    if (r(e[t]))
      return t;
  return -1;
}
function fe(e, r) {
  if (e.find)
    return e.find(r);
  var t = Dt(e, r);
  if (t !== -1)
    return e[t];
}
function ea(e) {
  return Array.prototype.slice.call(e);
}
var ra = k(function(e) {
  return e.reduce(function(r, t) {
    return r[t.descriptor.id] = t, r;
  }, {});
}), ta = k(function(e) {
  return e.reduce(function(r, t) {
    return r[t.descriptor.id] = t, r;
  }, {});
}), Or = k(function(e) {
  return Cr(e);
}), No = k(function(e) {
  return Cr(e);
}), Re = k(function(e, r) {
  var t = No(r).filter(function(n) {
    return e === n.descriptor.droppableId;
  }).sort(function(n, i) {
    return n.descriptor.index - i.descriptor.index;
  });
  return t;
});
function It(e) {
  return e.at && e.at.type === "REORDER" ? e.at.destination : null;
}
function Tr(e) {
  return e.at && e.at.type === "COMBINE" ? e.at.combine : null;
}
var Nr = k(function(e, r) {
  return r.filter(function(t) {
    return t.descriptor.id !== e.descriptor.id;
  });
}), Mo = function(e) {
  var r = e.isMovingForward, t = e.draggable, n = e.destination, i = e.insideDestination, a = e.previousImpact;
  if (!n.isCombineEnabled)
    return null;
  var o = It(a);
  if (!o)
    return null;
  function l(g) {
    var b = {
      type: "COMBINE",
      combine: {
        draggableId: g,
        droppableId: n.descriptor.id
      }
    };
    return S({}, a, {
      at: b
    });
  }
  var s = a.displaced.all, d = s.length ? s[0] : null;
  if (r)
    return d ? l(d) : null;
  var p = Nr(t, i);
  if (!d) {
    if (!p.length)
      return null;
    var c = p[p.length - 1];
    return l(c.descriptor.id);
  }
  var u = Dt(p, function(g) {
    return g.descriptor.id === d;
  });
  u === -1 && h(!1);
  var v = u - 1;
  if (v < 0)
    return null;
  var f = p[v];
  return l(f.descriptor.id);
}, Oe = function(e, r) {
  return e.descriptor.droppableId === r.descriptor.id;
}, na = {
  point: W,
  value: 0
}, Ve = {
  invisible: {},
  visible: {},
  all: []
}, Lo = {
  displaced: Ve,
  displacedBy: na,
  at: null
}, ee = function(e, r) {
  return function(t) {
    return e <= t && t <= r;
  };
}, aa = function(e) {
  var r = ee(e.top, e.bottom), t = ee(e.left, e.right);
  return function(n) {
    var i = r(n.top) && r(n.bottom) && t(n.left) && t(n.right);
    if (i)
      return !0;
    var a = r(n.top) || r(n.bottom), o = t(n.left) || t(n.right), l = a && o;
    if (l)
      return !0;
    var s = n.top < e.top && n.bottom > e.bottom, d = n.left < e.left && n.right > e.right, p = s && d;
    if (p)
      return !0;
    var c = s && o || d && a;
    return c;
  };
}, Fo = function(e) {
  var r = ee(e.top, e.bottom), t = ee(e.left, e.right);
  return function(n) {
    var i = r(n.top) && r(n.bottom) && t(n.left) && t(n.right);
    return i;
  };
}, xt = {
  direction: "vertical",
  line: "y",
  crossAxisLine: "x",
  start: "top",
  end: "bottom",
  size: "height",
  crossAxisStart: "left",
  crossAxisEnd: "right",
  crossAxisSize: "width"
}, ia = {
  direction: "horizontal",
  line: "x",
  crossAxisLine: "y",
  start: "left",
  end: "right",
  size: "width",
  crossAxisStart: "top",
  crossAxisEnd: "bottom",
  crossAxisSize: "height"
}, Go = function(e) {
  return function(r) {
    var t = ee(r.top, r.bottom), n = ee(r.left, r.right);
    return function(i) {
      return e === xt ? t(i.top) && t(i.bottom) : n(i.left) && n(i.right);
    };
  };
}, ko = function(r, t) {
  var n = t.frame ? t.frame.scroll.diff.displacement : W;
  return ir(r, n);
}, Wo = function(r, t, n) {
  return t.subject.active ? n(t.subject.active)(r) : !1;
}, Uo = function(r, t, n) {
  return n(t)(r);
}, St = function(r) {
  var t = r.target, n = r.destination, i = r.viewport, a = r.withDroppableDisplacement, o = r.isVisibleThroughFrameFn, l = a ? ko(t, n) : t;
  return Wo(l, n, o) && Uo(l, i, o);
}, Ho = function(r) {
  return St(S({}, r, {
    isVisibleThroughFrameFn: aa
  }));
}, oa = function(r) {
  return St(S({}, r, {
    isVisibleThroughFrameFn: Fo
  }));
}, $o = function(r) {
  return St(S({}, r, {
    isVisibleThroughFrameFn: Go(r.destination.axis)
  }));
}, qo = function(r, t, n) {
  if (typeof n == "boolean")
    return n;
  if (!t)
    return !0;
  var i = t.invisible, a = t.visible;
  if (i[r])
    return !1;
  var o = a[r];
  return o ? o.shouldAnimate : !0;
};
function Vo(e, r) {
  var t = e.page.marginBox, n = {
    top: r.point.y,
    right: 0,
    bottom: 0,
    left: r.point.x
  };
  return te(bt(t, n));
}
function je(e) {
  var r = e.afterDragging, t = e.destination, n = e.displacedBy, i = e.viewport, a = e.forceShouldAnimate, o = e.last;
  return r.reduce(function(s, d) {
    var p = Vo(d, n), c = d.descriptor.id;
    s.all.push(c);
    var u = Ho({
      target: p,
      destination: t,
      viewport: i,
      withDroppableDisplacement: !0
    });
    if (!u)
      return s.invisible[d.descriptor.id] = !0, s;
    var v = qo(c, o, a), f = {
      draggableId: c,
      shouldAnimate: v
    };
    return s.visible[c] = f, s;
  }, {
    all: [],
    visible: {},
    invisible: {}
  });
}
function jo(e, r) {
  if (!e.length)
    return 0;
  var t = e[e.length - 1].descriptor.index;
  return r.inHomeList ? t : t + 1;
}
function en(e) {
  var r = e.insideDestination, t = e.inHomeList, n = e.displacedBy, i = e.destination, a = jo(r, {
    inHomeList: t
  });
  return {
    displaced: Ve,
    displacedBy: n,
    at: {
      type: "REORDER",
      destination: {
        droppableId: i.descriptor.id,
        index: a
      }
    }
  };
}
function wr(e) {
  var r = e.draggable, t = e.insideDestination, n = e.destination, i = e.viewport, a = e.displacedBy, o = e.last, l = e.index, s = e.forceShouldAnimate, d = Oe(r, n);
  if (l == null)
    return en({
      insideDestination: t,
      inHomeList: d,
      displacedBy: a,
      destination: n
    });
  var p = fe(t, function(g) {
    return g.descriptor.index === l;
  });
  if (!p)
    return en({
      insideDestination: t,
      inHomeList: d,
      displacedBy: a,
      destination: n
    });
  var c = Nr(r, t), u = t.indexOf(p), v = c.slice(u), f = je({
    afterDragging: v,
    destination: n,
    displacedBy: a,
    last: o,
    viewport: i.frame,
    forceShouldAnimate: s
  });
  return {
    displaced: f,
    displacedBy: a,
    at: {
      type: "REORDER",
      destination: {
        droppableId: n.descriptor.id,
        index: l
      }
    }
  };
}
function ve(e, r) {
  return !!r.effected[e];
}
var zo = function(e) {
  var r = e.isMovingForward, t = e.destination, n = e.draggables, i = e.combine, a = e.afterCritical;
  if (!t.isCombineEnabled)
    return null;
  var o = i.draggableId, l = n[o], s = l.descriptor.index, d = ve(o, a);
  return d ? r ? s : s - 1 : r ? s + 1 : s;
}, Ko = function(e) {
  var r = e.isMovingForward, t = e.isInHomeList, n = e.insideDestination, i = e.location;
  if (!n.length)
    return null;
  var a = i.index, o = r ? a + 1 : a - 1, l = n[0].descriptor.index, s = n[n.length - 1].descriptor.index, d = t ? s : s + 1;
  return o < l || o > d ? null : o;
}, Yo = function(e) {
  var r = e.isMovingForward, t = e.isInHomeList, n = e.draggable, i = e.draggables, a = e.destination, o = e.insideDestination, l = e.previousImpact, s = e.viewport, d = e.afterCritical, p = l.at;
  if (p || h(!1), p.type === "REORDER") {
    var c = Ko({
      isMovingForward: r,
      isInHomeList: t,
      location: p.destination,
      insideDestination: o
    });
    return c == null ? null : wr({
      draggable: n,
      insideDestination: o,
      destination: a,
      viewport: s,
      last: l.displaced,
      displacedBy: l.displacedBy,
      index: c
    });
  }
  var u = zo({
    isMovingForward: r,
    destination: a,
    displaced: l.displaced,
    draggables: i,
    combine: p.combine,
    afterCritical: d
  });
  return u == null ? null : wr({
    draggable: n,
    insideDestination: o,
    destination: a,
    viewport: s,
    last: l.displaced,
    displacedBy: l.displacedBy,
    index: u
  });
}, Jo = function(e) {
  var r = e.displaced, t = e.afterCritical, n = e.combineWith, i = e.displacedBy, a = !!(r.visible[n] || r.invisible[n]);
  return ve(n, t) ? a ? W : Be(i.point) : a ? i.point : W;
}, Xo = function(e) {
  var r = e.afterCritical, t = e.impact, n = e.draggables, i = Tr(t);
  i || h(!1);
  var a = i.draggableId, o = n[a].page.borderBox.center, l = Jo({
    displaced: t.displaced,
    afterCritical: r,
    combineWith: a,
    displacedBy: t.displacedBy
  });
  return H(o, l);
}, la = function(r, t) {
  return t.margin[r.start] + t.borderBox[r.size] / 2;
}, Qo = function(r, t) {
  return t.margin[r.end] + t.borderBox[r.size] / 2;
}, Ct = function(r, t, n) {
  return t[r.crossAxisStart] + n.margin[r.crossAxisStart] + n.borderBox[r.crossAxisSize] / 2;
}, rn = function(r) {
  var t = r.axis, n = r.moveRelativeTo, i = r.isMoving;
  return ye(t.line, n.marginBox[t.end] + la(t, i), Ct(t, n.marginBox, i));
}, tn = function(r) {
  var t = r.axis, n = r.moveRelativeTo, i = r.isMoving;
  return ye(t.line, n.marginBox[t.start] - Qo(t, i), Ct(t, n.marginBox, i));
}, Zo = function(r) {
  var t = r.axis, n = r.moveInto, i = r.isMoving;
  return ye(t.line, n.contentBox[t.start] + la(t, i), Ct(t, n.contentBox, i));
}, _o = function(e) {
  var r = e.impact, t = e.draggable, n = e.draggables, i = e.droppable, a = e.afterCritical, o = Re(i.descriptor.id, n), l = t.page, s = i.axis;
  if (!o.length)
    return Zo({
      axis: s,
      moveInto: i.page,
      isMoving: l
    });
  var d = r.displaced, p = r.displacedBy, c = d.all[0];
  if (c) {
    var u = n[c];
    if (ve(c, a))
      return tn({
        axis: s,
        moveRelativeTo: u.page,
        isMoving: l
      });
    var v = Dr(u.page, p.point);
    return tn({
      axis: s,
      moveRelativeTo: v,
      isMoving: l
    });
  }
  var f = o[o.length - 1];
  if (f.descriptor.id === t.descriptor.id)
    return l.borderBox.center;
  if (ve(f.descriptor.id, a)) {
    var g = Dr(f.page, Be(a.displacedBy.point));
    return rn({
      axis: s,
      moveRelativeTo: g,
      isMoving: l
    });
  }
  return rn({
    axis: s,
    moveRelativeTo: f.page,
    isMoving: l
  });
}, at = function(e, r) {
  var t = e.frame;
  return t ? H(r, t.scroll.diff.displacement) : r;
}, el = function(r) {
  var t = r.impact, n = r.draggable, i = r.droppable, a = r.draggables, o = r.afterCritical, l = n.page.borderBox.center, s = t.at;
  return !i || !s ? l : s.type === "REORDER" ? _o({
    impact: t,
    draggable: n,
    draggables: a,
    droppable: i,
    afterCritical: o
  }) : Xo({
    impact: t,
    draggables: a,
    afterCritical: o
  });
}, Mr = function(e) {
  var r = el(e), t = e.droppable, n = t ? at(t, r) : r;
  return n;
}, sa = function(e, r) {
  var t = V(r, e.scroll.initial), n = Be(t), i = te({
    top: r.y,
    bottom: r.y + e.frame.height,
    left: r.x,
    right: r.x + e.frame.width
  }), a = {
    frame: i,
    scroll: {
      initial: e.scroll.initial,
      max: e.scroll.max,
      current: r,
      diff: {
        value: t,
        displacement: n
      }
    }
  };
  return a;
};
function nn(e, r) {
  return e.map(function(t) {
    return r[t];
  });
}
function rl(e, r) {
  for (var t = 0; t < r.length; t++) {
    var n = r[t].visible[e];
    if (n)
      return n;
  }
  return null;
}
var tl = function(e) {
  var r = e.impact, t = e.viewport, n = e.destination, i = e.draggables, a = e.maxScrollChange, o = sa(t, H(t.scroll.current, a)), l = n.frame ? yt(n, H(n.frame.scroll.current, a)) : n, s = r.displaced, d = je({
    afterDragging: nn(s.all, i),
    destination: n,
    displacedBy: r.displacedBy,
    viewport: o.frame,
    last: s,
    forceShouldAnimate: !1
  }), p = je({
    afterDragging: nn(s.all, i),
    destination: l,
    displacedBy: r.displacedBy,
    viewport: t.frame,
    last: s,
    forceShouldAnimate: !1
  }), c = {}, u = {}, v = [s, d, p];
  s.all.forEach(function(g) {
    var b = rl(g, v);
    if (b) {
      u[g] = b;
      return;
    }
    c[g] = !0;
  });
  var f = S({}, r, {
    displaced: {
      all: s.all,
      invisible: c,
      visible: u
    }
  });
  return f;
}, nl = function(e, r) {
  return H(e.scroll.diff.displacement, r);
}, wt = function(e) {
  var r = e.pageBorderBoxCenter, t = e.draggable, n = e.viewport, i = nl(n, r), a = V(i, t.page.borderBox.center);
  return H(t.client.borderBox.center, a);
}, ua = function(e) {
  var r = e.draggable, t = e.destination, n = e.newPageBorderBoxCenter, i = e.viewport, a = e.withDroppableDisplacement, o = e.onlyOnMainAxis, l = o === void 0 ? !1 : o, s = V(n, r.page.borderBox.center), d = ir(r.page.borderBox, s), p = {
    target: d,
    destination: t,
    withDroppableDisplacement: a,
    viewport: i
  };
  return l ? $o(p) : oa(p);
}, al = function(e) {
  var r = e.isMovingForward, t = e.draggable, n = e.destination, i = e.draggables, a = e.previousImpact, o = e.viewport, l = e.previousPageBorderBoxCenter, s = e.previousClientSelection, d = e.afterCritical;
  if (!n.isEnabled)
    return null;
  var p = Re(n.descriptor.id, i), c = Oe(t, n), u = Mo({
    isMovingForward: r,
    draggable: t,
    destination: n,
    insideDestination: p,
    previousImpact: a
  }) || Yo({
    isMovingForward: r,
    isInHomeList: c,
    draggable: t,
    draggables: i,
    destination: n,
    insideDestination: p,
    previousImpact: a,
    viewport: o,
    afterCritical: d
  });
  if (!u)
    return null;
  var v = Mr({
    impact: u,
    draggable: t,
    droppable: n,
    draggables: i,
    afterCritical: d
  }), f = ua({
    draggable: t,
    destination: n,
    newPageBorderBoxCenter: v,
    viewport: o.frame,
    withDroppableDisplacement: !1,
    onlyOnMainAxis: !0
  });
  if (f) {
    var g = wt({
      pageBorderBoxCenter: v,
      draggable: t,
      viewport: o
    });
    return {
      clientSelection: g,
      impact: u,
      scrollJumpRequest: null
    };
  }
  var b = V(v, l), m = tl({
    impact: u,
    viewport: o,
    destination: n,
    draggables: i,
    maxScrollChange: b
  });
  return {
    clientSelection: s,
    impact: m,
    scrollJumpRequest: b
  };
}, $ = function(r) {
  var t = r.subject.active;
  return t || h(!1), t;
}, il = function(e) {
  var r = e.isMovingForward, t = e.pageBorderBoxCenter, n = e.source, i = e.droppables, a = e.viewport, o = n.subject.active;
  if (!o)
    return null;
  var l = n.axis, s = ee(o[l.start], o[l.end]), d = Or(i).filter(function(c) {
    return c !== n;
  }).filter(function(c) {
    return c.isEnabled;
  }).filter(function(c) {
    return !!c.subject.active;
  }).filter(function(c) {
    return aa(a.frame)($(c));
  }).filter(function(c) {
    var u = $(c);
    return r ? o[l.crossAxisEnd] < u[l.crossAxisEnd] : u[l.crossAxisStart] < o[l.crossAxisStart];
  }).filter(function(c) {
    var u = $(c), v = ee(u[l.start], u[l.end]);
    return s(u[l.start]) || s(u[l.end]) || v(o[l.start]) || v(o[l.end]);
  }).sort(function(c, u) {
    var v = $(c)[l.crossAxisStart], f = $(u)[l.crossAxisStart];
    return r ? v - f : f - v;
  }).filter(function(c, u, v) {
    return $(c)[l.crossAxisStart] === $(v[0])[l.crossAxisStart];
  });
  if (!d.length)
    return null;
  if (d.length === 1)
    return d[0];
  var p = d.filter(function(c) {
    var u = ee($(c)[l.start], $(c)[l.end]);
    return u(t[l.line]);
  });
  return p.length === 1 ? p[0] : p.length > 1 ? p.sort(function(c, u) {
    return $(c)[l.start] - $(u)[l.start];
  })[0] : d.sort(function(c, u) {
    var v = Zt(t, _t($(c))), f = Zt(t, _t($(u)));
    return v !== f ? v - f : $(c)[l.start] - $(u)[l.start];
  })[0];
}, an = function(r, t) {
  var n = r.page.borderBox.center;
  return ve(r.descriptor.id, t) ? V(n, t.displacedBy.point) : n;
}, ol = function(r, t) {
  var n = r.page.borderBox;
  return ve(r.descriptor.id, t) ? ir(n, Be(t.displacedBy.point)) : n;
}, ll = function(e) {
  var r = e.pageBorderBoxCenter, t = e.viewport, n = e.destination, i = e.insideDestination, a = e.afterCritical, o = i.filter(function(l) {
    return oa({
      target: ol(l, a),
      destination: n,
      viewport: t.frame,
      withDroppableDisplacement: !0
    });
  }).sort(function(l, s) {
    var d = qe(r, at(n, an(l, a))), p = qe(r, at(n, an(s, a)));
    return d < p ? -1 : p < d ? 1 : l.descriptor.index - s.descriptor.index;
  });
  return o[0] || null;
}, or = k(function(r, t) {
  var n = t[r.line];
  return {
    value: n,
    point: ye(r.line, n)
  };
}), sl = function(r, t, n) {
  var i = r.axis;
  if (r.descriptor.mode === "virtual")
    return ye(i.line, t[i.line]);
  var a = r.subject.page.contentBox[i.size], o = Re(r.descriptor.id, n), l = o.reduce(function(p, c) {
    return p + c.client.marginBox[i.size];
  }, 0), s = l + t[i.line], d = s - a;
  return d <= 0 ? null : ye(i.line, d);
}, ca = function(r, t) {
  return S({}, r, {
    scroll: S({}, r.scroll, {
      max: t
    })
  });
}, da = function(r, t, n) {
  var i = r.frame;
  Oe(t, r) && h(!1), r.subject.withPlaceholder && h(!1);
  var a = or(r.axis, t.displaceBy).point, o = sl(r, a, n), l = {
    placeholderSize: a,
    increasedBy: o,
    oldFrameMaxScroll: r.frame ? r.frame.scroll.max : null
  };
  if (!i) {
    var s = Ce({
      page: r.subject.page,
      withPlaceholder: l,
      axis: r.axis,
      frame: r.frame
    });
    return S({}, r, {
      subject: s
    });
  }
  var d = o ? H(i.scroll.max, o) : i.scroll.max, p = ca(i, d), c = Ce({
    page: r.subject.page,
    withPlaceholder: l,
    axis: r.axis,
    frame: p
  });
  return S({}, r, {
    subject: c,
    frame: p
  });
}, ul = function(r) {
  var t = r.subject.withPlaceholder;
  t || h(!1);
  var n = r.frame;
  if (!n) {
    var i = Ce({
      page: r.subject.page,
      axis: r.axis,
      frame: null,
      withPlaceholder: null
    });
    return S({}, r, {
      subject: i
    });
  }
  var a = t.oldFrameMaxScroll;
  a || h(!1);
  var o = ca(n, a), l = Ce({
    page: r.subject.page,
    axis: r.axis,
    frame: o,
    withPlaceholder: null
  });
  return S({}, r, {
    subject: l,
    frame: o
  });
}, cl = function(e) {
  var r = e.previousPageBorderBoxCenter, t = e.moveRelativeTo, n = e.insideDestination, i = e.draggable, a = e.draggables, o = e.destination, l = e.viewport, s = e.afterCritical;
  if (!t) {
    if (n.length)
      return null;
    var d = {
      displaced: Ve,
      displacedBy: na,
      at: {
        type: "REORDER",
        destination: {
          droppableId: o.descriptor.id,
          index: 0
        }
      }
    }, p = Mr({
      impact: d,
      draggable: i,
      droppable: o,
      draggables: a,
      afterCritical: s
    }), c = Oe(i, o) ? o : da(o, i, a), u = ua({
      draggable: i,
      destination: c,
      newPageBorderBoxCenter: p,
      viewport: l.frame,
      withDroppableDisplacement: !1,
      onlyOnMainAxis: !0
    });
    return u ? d : null;
  }
  var v = r[o.axis.line] <= t.page.borderBox.center[o.axis.line], f = function() {
    var b = t.descriptor.index;
    return t.descriptor.id === i.descriptor.id || v ? b : b + 1;
  }(), g = or(o.axis, i.displaceBy);
  return wr({
    draggable: i,
    insideDestination: n,
    destination: o,
    viewport: l,
    displacedBy: g,
    last: Ve,
    index: f
  });
}, dl = function(e) {
  var r = e.isMovingForward, t = e.previousPageBorderBoxCenter, n = e.draggable, i = e.isOver, a = e.draggables, o = e.droppables, l = e.viewport, s = e.afterCritical, d = il({
    isMovingForward: r,
    pageBorderBoxCenter: t,
    source: i,
    droppables: o,
    viewport: l
  });
  if (!d)
    return null;
  var p = Re(d.descriptor.id, a), c = ll({
    pageBorderBoxCenter: t,
    viewport: l,
    destination: d,
    insideDestination: p,
    afterCritical: s
  }), u = cl({
    previousPageBorderBoxCenter: t,
    destination: d,
    draggable: n,
    draggables: a,
    moveRelativeTo: c,
    insideDestination: p,
    viewport: l,
    afterCritical: s
  });
  if (!u)
    return null;
  var v = Mr({
    impact: u,
    draggable: n,
    droppable: d,
    draggables: a,
    afterCritical: s
  }), f = wt({
    pageBorderBoxCenter: v,
    draggable: n,
    viewport: l
  });
  return {
    clientSelection: f,
    impact: u,
    scrollJumpRequest: null
  };
}, j = function(e) {
  var r = e.at;
  return r ? r.type === "REORDER" ? r.destination.droppableId : r.combine.droppableId : null;
}, pl = function(r, t) {
  var n = j(r);
  return n ? t[n] : null;
}, vl = function(e) {
  var r = e.state, t = e.type, n = pl(r.impact, r.dimensions.droppables), i = !!n, a = r.dimensions.droppables[r.critical.droppable.id], o = n || a, l = o.axis.direction, s = l === "vertical" && (t === "MOVE_UP" || t === "MOVE_DOWN") || l === "horizontal" && (t === "MOVE_LEFT" || t === "MOVE_RIGHT");
  if (s && !i)
    return null;
  var d = t === "MOVE_DOWN" || t === "MOVE_RIGHT", p = r.dimensions.draggables[r.critical.draggable.id], c = r.current.page.borderBoxCenter, u = r.dimensions, v = u.draggables, f = u.droppables;
  return s ? al({
    isMovingForward: d,
    previousPageBorderBoxCenter: c,
    draggable: p,
    destination: o,
    draggables: v,
    viewport: r.viewport,
    previousClientSelection: r.current.client.selection,
    previousImpact: r.impact,
    afterCritical: r.afterCritical
  }) : dl({
    isMovingForward: d,
    previousPageBorderBoxCenter: c,
    draggable: p,
    isOver: o,
    draggables: v,
    droppables: f,
    viewport: r.viewport,
    afterCritical: r.afterCritical
  });
};
function he(e) {
  return e.phase === "DRAGGING" || e.phase === "COLLECTING";
}
function pa(e) {
  var r = ee(e.top, e.bottom), t = ee(e.left, e.right);
  return function(i) {
    return r(i.y) && t(i.x);
  };
}
function fl(e, r) {
  return e.left < r.right && e.right > r.left && e.top < r.bottom && e.bottom > r.top;
}
function gl(e) {
  var r = e.pageBorderBox, t = e.draggable, n = e.candidates, i = t.page.borderBox.center, a = n.map(function(o) {
    var l = o.axis, s = ye(o.axis.line, r.center[l.line], o.page.borderBox.center[l.crossAxisLine]), d = qe(i, s);
    return {
      id: o.descriptor.id,
      distance: d
    };
  }).sort(function(o, l) {
    return l.distance - o.distance;
  });
  return a[0] ? a[0].id : null;
}
function ml(e) {
  var r = e.pageBorderBox, t = e.draggable, n = e.droppables, i = Or(n).filter(function(a) {
    if (!a.isEnabled)
      return !1;
    var o = a.subject.active;
    if (!o || !fl(r, o))
      return !1;
    if (pa(o)(r.center))
      return !0;
    var l = a.axis, s = o.center[l.crossAxisLine], d = r[l.crossAxisStart], p = r[l.crossAxisEnd], c = ee(o[l.crossAxisStart], o[l.crossAxisEnd]), u = c(d), v = c(p);
    return !u && !v ? !0 : u ? d < s : p > s;
  });
  return i.length ? i.length === 1 ? i[0].descriptor.id : gl({
    pageBorderBox: r,
    draggable: t,
    candidates: i
  }) : null;
}
var va = function(r, t) {
  return te(ir(r, t));
}, bl = function(e, r) {
  var t = e.frame;
  return t ? va(r, t.scroll.diff.value) : r;
};
function fa(e) {
  var r = e.displaced, t = e.id;
  return !!(r.visible[t] || r.invisible[t]);
}
function hl(e) {
  var r = e.draggable, t = e.closest, n = e.inHomeList;
  return t ? n && t.descriptor.index > r.descriptor.index ? t.descriptor.index - 1 : t.descriptor.index : null;
}
var yl = function(e) {
  var r = e.pageBorderBoxWithDroppableScroll, t = e.draggable, n = e.destination, i = e.insideDestination, a = e.last, o = e.viewport, l = e.afterCritical, s = n.axis, d = or(n.axis, t.displaceBy), p = d.value, c = r[s.start], u = r[s.end], v = Nr(t, i), f = fe(v, function(b) {
    var m = b.descriptor.id, y = b.page.borderBox.center[s.line], I = ve(m, l), D = fa({
      displaced: a,
      id: m
    });
    return I ? D ? u <= y : c < y - p : D ? u <= y + p : c < y;
  }), g = hl({
    draggable: t,
    closest: f,
    inHomeList: Oe(t, n)
  });
  return wr({
    draggable: t,
    insideDestination: i,
    destination: n,
    viewport: o,
    last: a,
    displacedBy: d,
    index: g
  });
}, Dl = 4, Il = function(e) {
  var r = e.draggable, t = e.pageBorderBoxWithDroppableScroll, n = e.previousImpact, i = e.destination, a = e.insideDestination, o = e.afterCritical;
  if (!i.isCombineEnabled)
    return null;
  var l = i.axis, s = or(i.axis, r.displaceBy), d = s.value, p = t[l.start], c = t[l.end], u = Nr(r, a), v = fe(u, function(g) {
    var b = g.descriptor.id, m = g.page.borderBox, y = m[l.size], I = y / Dl, D = ve(b, o), w = fa({
      displaced: n.displaced,
      id: b
    });
    return D ? w ? c > m[l.start] + I && c < m[l.end] - I : p > m[l.start] - d + I && p < m[l.end] - d - I : w ? c > m[l.start] + d + I && c < m[l.end] + d - I : p > m[l.start] + I && p < m[l.end] - I;
  });
  if (!v)
    return null;
  var f = {
    displacedBy: s,
    displaced: n.displaced,
    at: {
      type: "COMBINE",
      combine: {
        draggableId: v.descriptor.id,
        droppableId: i.descriptor.id
      }
    }
  };
  return f;
}, ga = function(e) {
  var r = e.pageOffset, t = e.draggable, n = e.draggables, i = e.droppables, a = e.previousImpact, o = e.viewport, l = e.afterCritical, s = va(t.page.borderBox, r), d = ml({
    pageBorderBox: s,
    draggable: t,
    droppables: i
  });
  if (!d)
    return Lo;
  var p = i[d], c = Re(p.descriptor.id, n), u = bl(p, s);
  return Il({
    pageBorderBoxWithDroppableScroll: u,
    draggable: t,
    previousImpact: a,
    destination: p,
    insideDestination: c,
    afterCritical: l
  }) || yl({
    pageBorderBoxWithDroppableScroll: u,
    draggable: t,
    destination: p,
    insideDestination: c,
    last: a.displaced,
    viewport: o,
    afterCritical: l
  });
}, Et = function(e, r) {
  var t;
  return S({}, e, (t = {}, t[r.descriptor.id] = r, t));
}, xl = function(r) {
  var t = r.previousImpact, n = r.impact, i = r.droppables, a = j(t), o = j(n);
  if (!a || a === o)
    return i;
  var l = i[a];
  if (!l.subject.withPlaceholder)
    return i;
  var s = ul(l);
  return Et(i, s);
}, Sl = function(e) {
  var r = e.draggable, t = e.draggables, n = e.droppables, i = e.previousImpact, a = e.impact, o = xl({
    previousImpact: i,
    impact: a,
    droppables: n
  }), l = j(a);
  if (!l)
    return o;
  var s = n[l];
  if (Oe(r, s) || s.subject.withPlaceholder)
    return o;
  var d = da(s, r, t);
  return Et(o, d);
}, ke = function(e) {
  var r = e.state, t = e.clientSelection, n = e.dimensions, i = e.viewport, a = e.impact, o = e.scrollJumpRequest, l = i || r.viewport, s = n || r.dimensions, d = t || r.current.client.selection, p = V(d, r.initial.client.selection), c = {
    offset: p,
    selection: d,
    borderBoxCenter: H(r.initial.client.borderBoxCenter, p)
  }, u = {
    selection: H(c.selection, l.scroll.current),
    borderBoxCenter: H(c.borderBoxCenter, l.scroll.current),
    offset: H(c.offset, l.scroll.diff.value)
  }, v = {
    client: c,
    page: u
  };
  if (r.phase === "COLLECTING")
    return S({
      phase: "COLLECTING"
    }, r, {
      dimensions: s,
      viewport: l,
      current: v
    });
  var f = s.draggables[r.critical.draggable.id], g = a || ga({
    pageOffset: u.offset,
    draggable: f,
    draggables: s.draggables,
    droppables: s.droppables,
    previousImpact: r.impact,
    viewport: l,
    afterCritical: r.afterCritical
  }), b = Sl({
    draggable: f,
    impact: g,
    previousImpact: r.impact,
    draggables: s.draggables,
    droppables: s.droppables
  }), m = S({}, r, {
    current: v,
    dimensions: {
      draggables: s.draggables,
      droppables: b
    },
    impact: g,
    viewport: l,
    scrollJumpRequest: o || null,
    forceShouldAnimate: o ? !1 : null
  });
  return m;
};
function Cl(e, r) {
  return e.map(function(t) {
    return r[t];
  });
}
var ma = function(e) {
  var r = e.impact, t = e.viewport, n = e.draggables, i = e.destination, a = e.forceShouldAnimate, o = r.displaced, l = Cl(o.all, n), s = je({
    afterDragging: l,
    destination: i,
    displacedBy: r.displacedBy,
    viewport: t.frame,
    forceShouldAnimate: a,
    last: o
  });
  return S({}, r, {
    displaced: s
  });
}, ba = function(e) {
  var r = e.impact, t = e.draggable, n = e.droppable, i = e.draggables, a = e.viewport, o = e.afterCritical, l = Mr({
    impact: r,
    draggable: t,
    draggables: i,
    droppable: n,
    afterCritical: o
  });
  return wt({
    pageBorderBoxCenter: l,
    draggable: t,
    viewport: a
  });
}, ha = function(e) {
  var r = e.state, t = e.dimensions, n = e.viewport;
  r.movementMode !== "SNAP" && h(!1);
  var i = r.impact, a = n || r.viewport, o = t || r.dimensions, l = o.draggables, s = o.droppables, d = l[r.critical.draggable.id], p = j(i);
  p || h(!1);
  var c = s[p], u = ma({
    impact: i,
    viewport: a,
    destination: c,
    draggables: l
  }), v = ba({
    impact: u,
    draggable: d,
    droppable: c,
    draggables: l,
    viewport: a,
    afterCritical: r.afterCritical
  });
  return ke({
    impact: u,
    clientSelection: v,
    state: r,
    dimensions: o,
    viewport: a
  });
}, wl = function(e) {
  return {
    index: e.index,
    droppableId: e.droppableId
  };
}, ya = function(e) {
  var r = e.draggable, t = e.home, n = e.draggables, i = e.viewport, a = or(t.axis, r.displaceBy), o = Re(t.descriptor.id, n), l = o.indexOf(r);
  l === -1 && h(!1);
  var s = o.slice(l + 1), d = s.reduce(function(v, f) {
    return v[f.descriptor.id] = !0, v;
  }, {}), p = {
    inVirtualList: t.descriptor.mode === "virtual",
    displacedBy: a,
    effected: d
  }, c = je({
    afterDragging: s,
    destination: t,
    displacedBy: a,
    last: null,
    viewport: i.frame,
    forceShouldAnimate: !1
  }), u = {
    displaced: c,
    displacedBy: a,
    at: {
      type: "REORDER",
      destination: wl(r.descriptor)
    }
  };
  return {
    impact: u,
    afterCritical: p
  };
}, El = function(e, r) {
  return {
    draggables: e.draggables,
    droppables: Et(e.droppables, r)
  };
}, Pl = function(e) {
  var r = e.draggable, t = e.offset, n = e.initialWindowScroll, i = Dr(r.client, t), a = Ir(i, n), o = S({}, r, {
    placeholder: S({}, r.placeholder, {
      client: i
    }),
    client: i,
    page: a
  });
  return o;
}, Al = function(e) {
  var r = e.frame;
  return r || h(!1), r;
}, Bl = function(e) {
  var r = e.additions, t = e.updatedDroppables, n = e.viewport, i = n.scroll.diff.value;
  return r.map(function(a) {
    var o = a.descriptor.droppableId, l = t[o], s = Al(l), d = s.scroll.diff.value, p = H(i, d), c = Pl({
      draggable: a,
      offset: p,
      initialWindowScroll: n.scroll.initial
    });
    return c;
  });
}, Rl = function(e) {
  var r = e.state, t = e.published, n = t.modified.map(function(I) {
    var D = r.dimensions.droppables[I.droppableId], w = yt(D, I.scroll);
    return w;
  }), i = S({}, r.dimensions.droppables, {}, ra(n)), a = ta(Bl({
    additions: t.additions,
    updatedDroppables: i,
    viewport: r.viewport
  })), o = S({}, r.dimensions.draggables, {}, a);
  t.removals.forEach(function(I) {
    delete o[I];
  });
  var l = {
    droppables: i,
    draggables: o
  }, s = j(r.impact), d = s ? l.droppables[s] : null, p = l.draggables[r.critical.draggable.id], c = l.droppables[r.critical.droppable.id], u = ya({
    draggable: p,
    home: c,
    draggables: o,
    viewport: r.viewport
  }), v = u.impact, f = u.afterCritical, g = d && d.isCombineEnabled ? r.impact : v, b = ga({
    pageOffset: r.current.page.offset,
    draggable: l.draggables[r.critical.draggable.id],
    draggables: l.draggables,
    droppables: l.droppables,
    previousImpact: g,
    viewport: r.viewport,
    afterCritical: f
  }), m = S({
    phase: "DRAGGING"
  }, r, {
    phase: "DRAGGING",
    impact: b,
    onLiftImpact: v,
    dimensions: l,
    afterCritical: f,
    forceShouldAnimate: !1
  });
  if (r.phase === "COLLECTING")
    return m;
  var y = S({
    phase: "DROP_PENDING"
  }, m, {
    phase: "DROP_PENDING",
    reason: r.reason,
    isWaiting: !1
  });
  return y;
}, it = function(r) {
  return r.movementMode === "SNAP";
}, Yr = function(r, t, n) {
  var i = El(r.dimensions, t);
  return !it(r) || n ? ke({
    state: r,
    dimensions: i
  }) : ha({
    state: r,
    dimensions: i
  });
};
function Jr(e) {
  return e.isDragging && e.movementMode === "SNAP" ? S({
    phase: "DRAGGING"
  }, e, {
    scrollJumpRequest: null
  }) : e;
}
var on = {
  phase: "IDLE",
  completed: null,
  shouldFlush: !1
}, Ol = function(e, r) {
  if (e === void 0 && (e = on), r.type === "FLUSH")
    return S({}, on, {
      shouldFlush: !0
    });
  if (r.type === "INITIAL_PUBLISH") {
    e.phase !== "IDLE" && h(!1);
    var t = r.payload, n = t.critical, i = t.clientSelection, a = t.viewport, o = t.dimensions, l = t.movementMode, s = o.draggables[n.draggable.id], d = o.droppables[n.droppable.id], p = {
      selection: i,
      borderBoxCenter: s.client.borderBox.center,
      offset: W
    }, c = {
      client: p,
      page: {
        selection: H(p.selection, a.scroll.initial),
        borderBoxCenter: H(p.selection, a.scroll.initial),
        offset: H(p.selection, a.scroll.diff.value)
      }
    }, u = Or(o.droppables).every(function(Wr) {
      return !Wr.isFixedOnPage;
    }), v = ya({
      draggable: s,
      home: d,
      draggables: o.draggables,
      viewport: a
    }), f = v.impact, g = v.afterCritical, b = {
      phase: "DRAGGING",
      isDragging: !0,
      critical: n,
      movementMode: l,
      dimensions: o,
      initial: c,
      current: c,
      isWindowScrollAllowed: u,
      impact: f,
      afterCritical: g,
      onLiftImpact: f,
      viewport: a,
      scrollJumpRequest: null,
      forceShouldAnimate: null
    };
    return b;
  }
  if (r.type === "COLLECTION_STARTING") {
    if (e.phase === "COLLECTING" || e.phase === "DROP_PENDING")
      return e;
    e.phase !== "DRAGGING" && h(!1);
    var m = S({
      phase: "COLLECTING"
    }, e, {
      phase: "COLLECTING"
    });
    return m;
  }
  if (r.type === "PUBLISH_WHILE_DRAGGING")
    return e.phase === "COLLECTING" || e.phase === "DROP_PENDING" || h(!1), Rl({
      state: e,
      published: r.payload
    });
  if (r.type === "MOVE") {
    if (e.phase === "DROP_PENDING")
      return e;
    he(e) || h(!1);
    var y = r.payload.client;
    return de(y, e.current.client.selection) ? e : ke({
      state: e,
      clientSelection: y,
      impact: it(e) ? e.impact : null
    });
  }
  if (r.type === "UPDATE_DROPPABLE_SCROLL") {
    if (e.phase === "DROP_PENDING" || e.phase === "COLLECTING")
      return Jr(e);
    he(e) || h(!1);
    var I = r.payload, D = I.id, w = I.newScroll, C = e.dimensions.droppables[D];
    if (!C)
      return e;
    var B = yt(C, w);
    return Yr(e, B, !1);
  }
  if (r.type === "UPDATE_DROPPABLE_IS_ENABLED") {
    if (e.phase === "DROP_PENDING")
      return e;
    he(e) || h(!1);
    var M = r.payload, P = M.id, L = M.isEnabled, R = e.dimensions.droppables[P];
    R || h(!1), R.isEnabled === L && h(!1);
    var K = S({}, R, {
      isEnabled: L
    });
    return Yr(e, K, !0);
  }
  if (r.type === "UPDATE_DROPPABLE_IS_COMBINE_ENABLED") {
    if (e.phase === "DROP_PENDING")
      return e;
    he(e) || h(!1);
    var Y = r.payload, G = Y.id, J = Y.isCombineEnabled, X = e.dimensions.droppables[G];
    X || h(!1), X.isCombineEnabled === J && h(!1);
    var De = S({}, X, {
      isCombineEnabled: J
    });
    return Yr(e, De, !0);
  }
  if (r.type === "MOVE_BY_WINDOW_SCROLL") {
    if (e.phase === "DROP_PENDING" || e.phase === "DROP_ANIMATING")
      return e;
    he(e) || h(!1), e.isWindowScrollAllowed || h(!1);
    var oe = r.payload.newScroll;
    if (de(e.viewport.scroll.current, oe))
      return Jr(e);
    var Ie = sa(e.viewport, oe);
    return it(e) ? ha({
      state: e,
      viewport: Ie
    }) : ke({
      state: e,
      viewport: Ie
    });
  }
  if (r.type === "UPDATE_VIEWPORT_MAX_SCROLL") {
    if (!he(e))
      return e;
    var re = r.payload.maxScroll;
    if (de(re, e.viewport.scroll.max))
      return e;
    var ge = S({}, e.viewport, {
      scroll: S({}, e.viewport.scroll, {
        max: re
      })
    });
    return S({
      phase: "DRAGGING"
    }, e, {
      viewport: ge
    });
  }
  if (r.type === "MOVE_UP" || r.type === "MOVE_DOWN" || r.type === "MOVE_LEFT" || r.type === "MOVE_RIGHT") {
    if (e.phase === "COLLECTING" || e.phase === "DROP_PENDING")
      return e;
    e.phase !== "DRAGGING" && h(!1);
    var q = vl({
      state: e,
      type: r.type
    });
    return q ? ke({
      state: e,
      impact: q.impact,
      clientSelection: q.clientSelection,
      scrollJumpRequest: q.scrollJumpRequest
    }) : e;
  }
  if (r.type === "DROP_PENDING") {
    var Te = r.payload.reason;
    e.phase !== "COLLECTING" && h(!1);
    var lr = S({
      phase: "DROP_PENDING"
    }, e, {
      phase: "DROP_PENDING",
      isWaiting: !0,
      reason: Te
    });
    return lr;
  }
  if (r.type === "DROP_ANIMATE") {
    var le = r.payload, sr = le.completed, ur = le.dropDuration, cr = le.newHomeClientOffset;
    e.phase === "DRAGGING" || e.phase === "DROP_PENDING" || h(!1);
    var kr = {
      phase: "DROP_ANIMATING",
      completed: sr,
      dropDuration: ur,
      newHomeClientOffset: cr,
      dimensions: e.dimensions
    };
    return kr;
  }
  if (r.type === "DROP_COMPLETE") {
    var xe = r.payload.completed;
    return {
      phase: "IDLE",
      completed: xe,
      shouldFlush: !1
    };
  }
  return e;
}, Tl = function(r) {
  return {
    type: "BEFORE_INITIAL_CAPTURE",
    payload: r
  };
}, Nl = function(r) {
  return {
    type: "LIFT",
    payload: r
  };
}, Ml = function(r) {
  return {
    type: "INITIAL_PUBLISH",
    payload: r
  };
}, Ll = function(r) {
  return {
    type: "PUBLISH_WHILE_DRAGGING",
    payload: r
  };
}, Fl = function() {
  return {
    type: "COLLECTION_STARTING",
    payload: null
  };
}, Gl = function(r) {
  return {
    type: "UPDATE_DROPPABLE_SCROLL",
    payload: r
  };
}, kl = function(r) {
  return {
    type: "UPDATE_DROPPABLE_IS_ENABLED",
    payload: r
  };
}, Wl = function(r) {
  return {
    type: "UPDATE_DROPPABLE_IS_COMBINE_ENABLED",
    payload: r
  };
}, Da = function(r) {
  return {
    type: "MOVE",
    payload: r
  };
}, Ul = function(r) {
  return {
    type: "MOVE_BY_WINDOW_SCROLL",
    payload: r
  };
}, Hl = function(r) {
  return {
    type: "UPDATE_VIEWPORT_MAX_SCROLL",
    payload: r
  };
}, $l = function() {
  return {
    type: "MOVE_UP",
    payload: null
  };
}, ql = function() {
  return {
    type: "MOVE_DOWN",
    payload: null
  };
}, Vl = function() {
  return {
    type: "MOVE_RIGHT",
    payload: null
  };
}, jl = function() {
  return {
    type: "MOVE_LEFT",
    payload: null
  };
}, Pt = function() {
  return {
    type: "FLUSH",
    payload: null
  };
}, zl = function(r) {
  return {
    type: "DROP_ANIMATE",
    payload: r
  };
}, At = function(r) {
  return {
    type: "DROP_COMPLETE",
    payload: r
  };
}, Ia = function(r) {
  return {
    type: "DROP",
    payload: r
  };
}, Kl = function(r) {
  return {
    type: "DROP_PENDING",
    payload: r
  };
}, xa = function() {
  return {
    type: "DROP_ANIMATION_FINISHED",
    payload: null
  };
}, Yl = function(e) {
  return function(r) {
    var t = r.getState, n = r.dispatch;
    return function(i) {
      return function(a) {
        if (a.type !== "LIFT") {
          i(a);
          return;
        }
        var o = a.payload, l = o.id, s = o.clientSelection, d = o.movementMode, p = t();
        p.phase === "DROP_ANIMATING" && n(At({
          completed: p.completed
        })), t().phase !== "IDLE" && h(!1), n(Pt()), n(Tl({
          draggableId: l,
          movementMode: d
        }));
        var c = {
          shouldPublishImmediately: d === "SNAP"
        }, u = {
          draggableId: l,
          scrollOptions: c
        }, v = e.startPublishing(u), f = v.critical, g = v.dimensions, b = v.viewport;
        n(Ml({
          critical: f,
          dimensions: g,
          clientSelection: s,
          movementMode: d,
          viewport: b
        }));
      };
    };
  };
}, Jl = function(e) {
  return function() {
    return function(r) {
      return function(t) {
        t.type === "INITIAL_PUBLISH" && e.dragging(), t.type === "DROP_ANIMATE" && e.dropping(t.payload.completed.result.reason), (t.type === "FLUSH" || t.type === "DROP_COMPLETE") && e.resting(), r(t);
      };
    };
  };
}, Bt = {
  outOfTheWay: "cubic-bezier(0.2, 0, 0, 1)",
  drop: "cubic-bezier(.2,1,.1,1)"
}, ze = {
  opacity: {
    drop: 0,
    combining: 0.7
  },
  scale: {
    drop: 0.75
  }
}, Rt = {
  outOfTheWay: 0.2,
  minDropTime: 0.33,
  maxDropTime: 0.55
}, me = Rt.outOfTheWay + "s " + Bt.outOfTheWay, We = {
  fluid: "opacity " + me,
  snap: "transform " + me + ", opacity " + me,
  drop: function(r) {
    var t = r + "s " + Bt.drop;
    return "transform " + t + ", opacity " + t;
  },
  outOfTheWay: "transform " + me,
  placeholder: "height " + me + ", width " + me + ", margin " + me
}, ln = function(r) {
  return de(r, W) ? null : "translate(" + r.x + "px, " + r.y + "px)";
}, ot = {
  moveTo: ln,
  drop: function(r, t) {
    var n = ln(r);
    return n ? t ? n + " scale(" + ze.scale.drop + ")" : n : null;
  }
}, lt = Rt.minDropTime, Sa = Rt.maxDropTime, Xl = Sa - lt, sn = 1500, Ql = 0.6, Zl = function(e) {
  var r = e.current, t = e.destination, n = e.reason, i = qe(r, t);
  if (i <= 0)
    return lt;
  if (i >= sn)
    return Sa;
  var a = i / sn, o = lt + Xl * a, l = n === "CANCEL" ? o * Ql : o;
  return Number(l.toFixed(2));
}, _l = function(e) {
  var r = e.impact, t = e.draggable, n = e.dimensions, i = e.viewport, a = e.afterCritical, o = n.draggables, l = n.droppables, s = j(r), d = s ? l[s] : null, p = l[t.descriptor.droppableId], c = ba({
    impact: r,
    draggable: t,
    draggables: o,
    afterCritical: a,
    droppable: d || p,
    viewport: i
  }), u = V(c, t.client.borderBox.center);
  return u;
}, es = function(e) {
  var r = e.draggables, t = e.reason, n = e.lastImpact, i = e.home, a = e.viewport, o = e.onLiftImpact;
  if (!n.at || t !== "DROP") {
    var l = ma({
      draggables: r,
      impact: o,
      destination: i,
      viewport: a,
      forceShouldAnimate: !0
    });
    return {
      impact: l,
      didDropInsideDroppable: !1
    };
  }
  if (n.at.type === "REORDER")
    return {
      impact: n,
      didDropInsideDroppable: !0
    };
  var s = S({}, n, {
    displaced: Ve
  });
  return {
    impact: s,
    didDropInsideDroppable: !0
  };
}, rs = function(e) {
  var r = e.getState, t = e.dispatch;
  return function(n) {
    return function(i) {
      if (i.type !== "DROP") {
        n(i);
        return;
      }
      var a = r(), o = i.payload.reason;
      if (a.phase === "COLLECTING") {
        t(Kl({
          reason: o
        }));
        return;
      }
      if (a.phase !== "IDLE") {
        var l = a.phase === "DROP_PENDING" && a.isWaiting;
        l && h(!1), a.phase === "DRAGGING" || a.phase === "DROP_PENDING" || h(!1);
        var s = a.critical, d = a.dimensions, p = d.draggables[a.critical.draggable.id], c = es({
          reason: o,
          lastImpact: a.impact,
          afterCritical: a.afterCritical,
          onLiftImpact: a.onLiftImpact,
          home: a.dimensions.droppables[a.critical.droppable.id],
          viewport: a.viewport,
          draggables: a.dimensions.draggables
        }), u = c.impact, v = c.didDropInsideDroppable, f = v ? It(u) : null, g = v ? Tr(u) : null, b = {
          index: s.draggable.index,
          droppableId: s.droppable.id
        }, m = {
          draggableId: p.descriptor.id,
          type: p.descriptor.type,
          source: b,
          reason: o,
          mode: a.movementMode,
          destination: f,
          combine: g
        }, y = _l({
          impact: u,
          draggable: p,
          dimensions: d,
          viewport: a.viewport,
          afterCritical: a.afterCritical
        }), I = {
          critical: a.critical,
          afterCritical: a.afterCritical,
          result: m,
          impact: u
        }, D = !de(a.current.client.offset, y) || !!m.combine;
        if (!D) {
          t(At({
            completed: I
          }));
          return;
        }
        var w = Zl({
          current: a.current.client.offset,
          destination: y,
          reason: o
        }), C = {
          newHomeClientOffset: y,
          dropDuration: w,
          completed: I
        };
        t(zl(C));
      }
    };
  };
}, Ca = function() {
  return {
    x: window.pageXOffset,
    y: window.pageYOffset
  };
};
function ts(e) {
  return {
    eventName: "scroll",
    options: {
      passive: !0,
      capture: !1
    },
    fn: function(t) {
      t.target !== window && t.target !== window.document || e();
    }
  };
}
function ns(e) {
  var r = e.onWindowScroll;
  function t() {
    r(Ca());
  }
  var n = $e(t), i = ts(n), a = ce;
  function o() {
    return a !== ce;
  }
  function l() {
    o() && h(!1), a = _(window, [i]);
  }
  function s() {
    o() || h(!1), n.cancel(), a(), a = ce;
  }
  return {
    start: l,
    stop: s,
    isActive: o
  };
}
var as = function(r) {
  return r.type === "DROP_COMPLETE" || r.type === "DROP_ANIMATE" || r.type === "FLUSH";
}, is = function(e) {
  var r = ns({
    onWindowScroll: function(n) {
      e.dispatch(Ul({
        newScroll: n
      }));
    }
  });
  return function(t) {
    return function(n) {
      !r.isActive() && n.type === "INITIAL_PUBLISH" && r.start(), r.isActive() && as(n) && r.stop(), t(n);
    };
  };
}, os = function(e) {
  var r = !1, t = !1, n = setTimeout(function() {
    t = !0;
  }), i = function(o) {
    r || t || (r = !0, e(o), clearTimeout(n));
  };
  return i.wasCalled = function() {
    return r;
  }, i;
}, ls = function() {
  var e = [], r = function(a) {
    var o = Dt(e, function(d) {
      return d.timerId === a;
    });
    o === -1 && h(!1);
    var l = e.splice(o, 1), s = l[0];
    s.callback();
  }, t = function(a) {
    var o = setTimeout(function() {
      return r(o);
    }), l = {
      timerId: o,
      callback: a
    };
    e.push(l);
  }, n = function() {
    if (e.length) {
      var a = [].concat(e);
      e.length = 0, a.forEach(function(o) {
        clearTimeout(o.timerId), o.callback();
      });
    }
  };
  return {
    add: t,
    flush: n
  };
}, ss = function(r, t) {
  return r == null && t == null ? !0 : r == null || t == null ? !1 : r.droppableId === t.droppableId && r.index === t.index;
}, us = function(r, t) {
  return r == null && t == null ? !0 : r == null || t == null ? !1 : r.draggableId === t.draggableId && r.droppableId === t.droppableId;
}, cs = function(r, t) {
  if (r === t)
    return !0;
  var n = r.draggable.id === t.draggable.id && r.draggable.droppableId === t.draggable.droppableId && r.draggable.type === t.draggable.type && r.draggable.index === t.draggable.index, i = r.droppable.id === t.droppable.id && r.droppable.type === t.droppable.type;
  return n && i;
}, Me = function(r, t) {
  t();
}, pr = function(r, t) {
  return {
    draggableId: r.draggable.id,
    type: r.droppable.type,
    source: {
      droppableId: r.droppable.id,
      index: r.draggable.index
    },
    mode: t
  };
}, Xr = function(r, t, n, i) {
  if (!r) {
    n(i(t));
    return;
  }
  var a = os(n), o = {
    announce: a
  };
  r(t, o), a.wasCalled() || n(i(t));
}, ds = function(e, r) {
  var t = ls(), n = null, i = function(u, v) {
    n && h(!1), Me("onBeforeCapture", function() {
      var f = e().onBeforeCapture;
      if (f) {
        var g = {
          draggableId: u,
          mode: v
        };
        f(g);
      }
    });
  }, a = function(u, v) {
    n && h(!1), Me("onBeforeDragStart", function() {
      var f = e().onBeforeDragStart;
      f && f(pr(u, v));
    });
  }, o = function(u, v) {
    n && h(!1);
    var f = pr(u, v);
    n = {
      mode: v,
      lastCritical: u,
      lastLocation: f.source,
      lastCombine: null
    }, t.add(function() {
      Me("onDragStart", function() {
        return Xr(e().onDragStart, f, r, br.onDragStart);
      });
    });
  }, l = function(u, v) {
    var f = It(v), g = Tr(v);
    n || h(!1);
    var b = !cs(u, n.lastCritical);
    b && (n.lastCritical = u);
    var m = !ss(n.lastLocation, f);
    m && (n.lastLocation = f);
    var y = !us(n.lastCombine, g);
    if (y && (n.lastCombine = g), !(!b && !m && !y)) {
      var I = S({}, pr(u, n.mode), {
        combine: g,
        destination: f
      });
      t.add(function() {
        Me("onDragUpdate", function() {
          return Xr(e().onDragUpdate, I, r, br.onDragUpdate);
        });
      });
    }
  }, s = function() {
    n || h(!1), t.flush();
  }, d = function(u) {
    n || h(!1), n = null, Me("onDragEnd", function() {
      return Xr(e().onDragEnd, u, r, br.onDragEnd);
    });
  }, p = function() {
    if (n) {
      var u = S({}, pr(n.lastCritical, n.mode), {
        combine: null,
        destination: null,
        reason: "CANCEL"
      });
      d(u);
    }
  };
  return {
    beforeCapture: i,
    beforeStart: a,
    start: o,
    update: l,
    flush: s,
    drop: d,
    abort: p
  };
}, ps = function(e, r) {
  var t = ds(e, r);
  return function(n) {
    return function(i) {
      return function(a) {
        if (a.type === "BEFORE_INITIAL_CAPTURE") {
          t.beforeCapture(a.payload.draggableId, a.payload.movementMode);
          return;
        }
        if (a.type === "INITIAL_PUBLISH") {
          var o = a.payload.critical;
          t.beforeStart(o, a.payload.movementMode), i(a), t.start(o, a.payload.movementMode);
          return;
        }
        if (a.type === "DROP_COMPLETE") {
          var l = a.payload.completed.result;
          t.flush(), i(a), t.drop(l);
          return;
        }
        if (i(a), a.type === "FLUSH") {
          t.abort();
          return;
        }
        var s = n.getState();
        s.phase === "DRAGGING" && t.update(s.critical, s.impact);
      };
    };
  };
}, vs = function(e) {
  return function(r) {
    return function(t) {
      if (t.type !== "DROP_ANIMATION_FINISHED") {
        r(t);
        return;
      }
      var n = e.getState();
      n.phase !== "DROP_ANIMATING" && h(!1), e.dispatch(At({
        completed: n.completed
      }));
    };
  };
}, fs = function(e) {
  var r = null, t = null;
  function n() {
    t && (cancelAnimationFrame(t), t = null), r && (r(), r = null);
  }
  return function(i) {
    return function(a) {
      if ((a.type === "FLUSH" || a.type === "DROP_COMPLETE" || a.type === "DROP_ANIMATION_FINISHED") && n(), i(a), a.type === "DROP_ANIMATE") {
        var o = {
          eventName: "scroll",
          options: {
            capture: !0,
            passive: !1,
            once: !0
          },
          fn: function() {
            var s = e.getState();
            s.phase === "DROP_ANIMATING" && e.dispatch(xa());
          }
        };
        t = requestAnimationFrame(function() {
          t = null, r = _(window, [o]);
        });
      }
    };
  };
}, gs = function(e) {
  return function() {
    return function(r) {
      return function(t) {
        (t.type === "DROP_COMPLETE" || t.type === "FLUSH" || t.type === "DROP_ANIMATE") && e.stopPublishing(), r(t);
      };
    };
  };
}, ms = function(e) {
  var r = !1;
  return function() {
    return function(t) {
      return function(n) {
        if (n.type === "INITIAL_PUBLISH") {
          r = !0, e.tryRecordFocus(n.payload.critical.draggable.id), t(n), e.tryRestoreFocusRecorded();
          return;
        }
        if (t(n), !!r) {
          if (n.type === "FLUSH") {
            r = !1, e.tryRestoreFocusRecorded();
            return;
          }
          if (n.type === "DROP_COMPLETE") {
            r = !1;
            var i = n.payload.completed.result;
            i.combine && e.tryShiftRecord(i.draggableId, i.combine.draggableId), e.tryRestoreFocusRecorded();
          }
        }
      };
    };
  };
}, bs = function(r) {
  return r.type === "DROP_COMPLETE" || r.type === "DROP_ANIMATE" || r.type === "FLUSH";
}, hs = function(e) {
  return function(r) {
    return function(t) {
      return function(n) {
        if (bs(n)) {
          e.stop(), t(n);
          return;
        }
        if (n.type === "INITIAL_PUBLISH") {
          t(n);
          var i = r.getState();
          i.phase !== "DRAGGING" && h(!1), e.start(i);
          return;
        }
        t(n), e.scroll(r.getState());
      };
    };
  };
}, ys = function(e) {
  return function(r) {
    return function(t) {
      if (r(t), t.type === "PUBLISH_WHILE_DRAGGING") {
        var n = e.getState();
        n.phase === "DROP_PENDING" && (n.isWaiting || e.dispatch(Ia({
          reason: n.reason
        })));
      }
    };
  };
}, Ds = ti, Is = function(e) {
  var r = e.dimensionMarshal, t = e.focusMarshal, n = e.styleMarshal, i = e.getResponders, a = e.announce, o = e.autoScroller;
  return ei(Ol, Ds(ri(Jl(n), gs(r), Yl(r), rs, vs, fs, ys, hs(o), is, ms(t), ps(i, a))));
}, Qr = function() {
  return {
    additions: {},
    removals: {},
    modified: {}
  };
};
function xs(e) {
  var r = e.registry, t = e.callbacks, n = Qr(), i = null, a = function() {
    i || (t.collectionStarting(), i = requestAnimationFrame(function() {
      i = null;
      var p = n, c = p.additions, u = p.removals, v = p.modified, f = Object.keys(c).map(function(m) {
        return r.draggable.getById(m).getDimension(W);
      }).sort(function(m, y) {
        return m.descriptor.index - y.descriptor.index;
      }), g = Object.keys(v).map(function(m) {
        var y = r.droppable.getById(m), I = y.callbacks.getScrollWhileDragging();
        return {
          droppableId: m,
          scroll: I
        };
      }), b = {
        additions: f,
        removals: Object.keys(u),
        modified: g
      };
      n = Qr(), t.publish(b);
    }));
  }, o = function(p) {
    var c = p.descriptor.id;
    n.additions[c] = p, n.modified[p.descriptor.droppableId] = !0, n.removals[c] && delete n.removals[c], a();
  }, l = function(p) {
    var c = p.descriptor;
    n.removals[c.id] = !0, n.modified[c.droppableId] = !0, n.additions[c.id] && delete n.additions[c.id], a();
  }, s = function() {
    i && (cancelAnimationFrame(i), i = null, n = Qr());
  };
  return {
    add: o,
    remove: l,
    stop: s
  };
}
var wa = function(e) {
  var r = e.scrollHeight, t = e.scrollWidth, n = e.height, i = e.width, a = V({
    x: t,
    y: r
  }, {
    x: i,
    y: n
  }), o = {
    x: Math.max(0, a.x),
    y: Math.max(0, a.y)
  };
  return o;
}, Ea = function() {
  var e = document.documentElement;
  return e || h(!1), e;
}, Pa = function() {
  var e = Ea(), r = wa({
    scrollHeight: e.scrollHeight,
    scrollWidth: e.scrollWidth,
    width: e.clientWidth,
    height: e.clientHeight
  });
  return r;
}, Ss = function() {
  var e = Ca(), r = Pa(), t = e.y, n = e.x, i = Ea(), a = i.clientWidth, o = i.clientHeight, l = n + a, s = t + o, d = te({
    top: t,
    left: n,
    right: l,
    bottom: s
  }), p = {
    frame: d,
    scroll: {
      initial: e,
      current: e,
      max: r,
      diff: {
        value: W,
        displacement: W
      }
    }
  };
  return p;
}, Cs = function(e) {
  var r = e.critical, t = e.scrollOptions, n = e.registry, i = Ss(), a = i.scroll.current, o = r.droppable, l = n.droppable.getAllByType(o.type).map(function(c) {
    return c.callbacks.getDimensionAndWatchScroll(a, t);
  }), s = n.draggable.getAllByType(r.draggable.type).map(function(c) {
    return c.getDimension(a);
  }), d = {
    draggables: ta(s),
    droppables: ra(l)
  }, p = {
    dimensions: d,
    critical: r,
    viewport: i
  };
  return p;
};
function un(e, r, t) {
  if (t.descriptor.id === r.id || t.descriptor.type !== r.type)
    return !1;
  var n = e.droppable.getById(t.descriptor.droppableId);
  return n.descriptor.mode === "virtual";
}
var ws = function(e, r) {
  var t = null, n = xs({
    callbacks: {
      publish: r.publishWhileDragging,
      collectionStarting: r.collectionStarting
    },
    registry: e
  }), i = function(v, f) {
    e.droppable.exists(v) || h(!1), t && r.updateDroppableIsEnabled({
      id: v,
      isEnabled: f
    });
  }, a = function(v, f) {
    t && (e.droppable.exists(v) || h(!1), r.updateDroppableIsCombineEnabled({
      id: v,
      isCombineEnabled: f
    }));
  }, o = function(v, f) {
    t && (e.droppable.exists(v) || h(!1), r.updateDroppableScroll({
      id: v,
      newScroll: f
    }));
  }, l = function(v, f) {
    t && e.droppable.getById(v).callbacks.scroll(f);
  }, s = function() {
    if (t) {
      n.stop();
      var v = t.critical.droppable;
      e.droppable.getAllByType(v.type).forEach(function(f) {
        return f.callbacks.dragStopped();
      }), t.unsubscribe(), t = null;
    }
  }, d = function(v) {
    t || h(!1);
    var f = t.critical.draggable;
    v.type === "ADDITION" && un(e, f, v.value) && n.add(v.value), v.type === "REMOVAL" && un(e, f, v.value) && n.remove(v.value);
  }, p = function(v) {
    t && h(!1);
    var f = e.draggable.getById(v.draggableId), g = e.droppable.getById(f.descriptor.droppableId), b = {
      draggable: f.descriptor,
      droppable: g.descriptor
    }, m = e.subscribe(d);
    return t = {
      critical: b,
      unsubscribe: m
    }, Cs({
      critical: b,
      registry: e,
      scrollOptions: v.scrollOptions
    });
  }, c = {
    updateDroppableIsEnabled: i,
    updateDroppableIsCombineEnabled: a,
    scrollDroppable: l,
    updateDroppableScroll: o,
    startPublishing: p,
    stopPublishing: s
  };
  return c;
}, Aa = function(e, r) {
  return e.phase === "IDLE" ? !0 : e.phase !== "DROP_ANIMATING" || e.completed.result.draggableId === r ? !1 : e.completed.result.reason === "DROP";
}, Es = function(e) {
  window.scrollBy(e.x, e.y);
}, Ps = k(function(e) {
  return Or(e).filter(function(r) {
    return !(!r.isEnabled || !r.frame);
  });
}), As = function(r, t) {
  var n = fe(Ps(t), function(i) {
    return i.frame || h(!1), pa(i.frame.pageMarginBox)(r);
  });
  return n;
}, Bs = function(e) {
  var r = e.center, t = e.destination, n = e.droppables;
  if (t) {
    var i = n[t];
    return i.frame ? i : null;
  }
  var a = As(r, n);
  return a;
}, pe = {
  startFromPercentage: 0.25,
  maxScrollAtPercentage: 0.05,
  maxPixelScroll: 28,
  ease: function(r) {
    return Math.pow(r, 2);
  },
  durationDampening: {
    stopDampeningAt: 1200,
    accelerateAt: 360
  }
}, Rs = function(e, r) {
  var t = e[r.size] * pe.startFromPercentage, n = e[r.size] * pe.maxScrollAtPercentage, i = {
    startScrollingFrom: t,
    maxScrollValueAt: n
  };
  return i;
}, Ba = function(e) {
  var r = e.startOfRange, t = e.endOfRange, n = e.current, i = t - r;
  if (i === 0)
    return 0;
  var a = n - r, o = a / i;
  return o;
}, Ot = 1, Os = function(e, r) {
  if (e > r.startScrollingFrom)
    return 0;
  if (e <= r.maxScrollValueAt)
    return pe.maxPixelScroll;
  if (e === r.startScrollingFrom)
    return Ot;
  var t = Ba({
    startOfRange: r.maxScrollValueAt,
    endOfRange: r.startScrollingFrom,
    current: e
  }), n = 1 - t, i = pe.maxPixelScroll * pe.ease(n);
  return Math.ceil(i);
}, cn = pe.durationDampening.accelerateAt, dn = pe.durationDampening.stopDampeningAt, Ts = function(e, r) {
  var t = r, n = dn, i = Date.now(), a = i - t;
  if (a >= dn)
    return e;
  if (a < cn)
    return Ot;
  var o = Ba({
    startOfRange: cn,
    endOfRange: n,
    current: a
  }), l = e * pe.ease(o);
  return Math.ceil(l);
}, pn = function(e) {
  var r = e.distanceToEdge, t = e.thresholds, n = e.dragStartTime, i = e.shouldUseTimeDampening, a = Os(r, t);
  return a === 0 ? 0 : i ? Math.max(Ts(a, n), Ot) : a;
}, vn = function(e) {
  var r = e.container, t = e.distanceToEdges, n = e.dragStartTime, i = e.axis, a = e.shouldUseTimeDampening, o = Rs(r, i), l = t[i.end] < t[i.start];
  return l ? pn({
    distanceToEdge: t[i.end],
    thresholds: o,
    dragStartTime: n,
    shouldUseTimeDampening: a
  }) : -1 * pn({
    distanceToEdge: t[i.start],
    thresholds: o,
    dragStartTime: n,
    shouldUseTimeDampening: a
  });
}, Ns = function(e) {
  var r = e.container, t = e.subject, n = e.proposedScroll, i = t.height > r.height, a = t.width > r.width;
  return !a && !i ? n : a && i ? null : {
    x: a ? 0 : n.x,
    y: i ? 0 : n.y
  };
}, Ms = _n(function(e) {
  return e === 0 ? 0 : e;
}), Ra = function(e) {
  var r = e.dragStartTime, t = e.container, n = e.subject, i = e.center, a = e.shouldUseTimeDampening, o = {
    top: i.y - t.top,
    right: t.right - i.x,
    bottom: t.bottom - i.y,
    left: i.x - t.left
  }, l = vn({
    container: t,
    distanceToEdges: o,
    dragStartTime: r,
    axis: xt,
    shouldUseTimeDampening: a
  }), s = vn({
    container: t,
    distanceToEdges: o,
    dragStartTime: r,
    axis: ia,
    shouldUseTimeDampening: a
  }), d = Ms({
    x: s,
    y: l
  });
  if (de(d, W))
    return null;
  var p = Ns({
    container: t,
    subject: n,
    proposedScroll: d
  });
  return p ? de(p, W) ? null : p : null;
}, Ls = _n(function(e) {
  return e === 0 ? 0 : e > 0 ? 1 : -1;
}), Tt = function() {
  var e = function(t, n) {
    return t < 0 ? t : t > n ? t - n : 0;
  };
  return function(r) {
    var t = r.current, n = r.max, i = r.change, a = H(t, i), o = {
      x: e(a.x, n.x),
      y: e(a.y, n.y)
    };
    return de(o, W) ? null : o;
  };
}(), Oa = function(r) {
  var t = r.max, n = r.current, i = r.change, a = {
    x: Math.max(n.x, t.x),
    y: Math.max(n.y, t.y)
  }, o = Ls(i), l = Tt({
    max: a,
    current: n,
    change: o
  });
  return !l || o.x !== 0 && l.x === 0 || o.y !== 0 && l.y === 0;
}, Nt = function(r, t) {
  return Oa({
    current: r.scroll.current,
    max: r.scroll.max,
    change: t
  });
}, Fs = function(r, t) {
  if (!Nt(r, t))
    return null;
  var n = r.scroll.max, i = r.scroll.current;
  return Tt({
    current: i,
    max: n,
    change: t
  });
}, Mt = function(r, t) {
  var n = r.frame;
  return n ? Oa({
    current: n.scroll.current,
    max: n.scroll.max,
    change: t
  }) : !1;
}, Gs = function(r, t) {
  var n = r.frame;
  return !n || !Mt(r, t) ? null : Tt({
    current: n.scroll.current,
    max: n.scroll.max,
    change: t
  });
}, ks = function(e) {
  var r = e.viewport, t = e.subject, n = e.center, i = e.dragStartTime, a = e.shouldUseTimeDampening, o = Ra({
    dragStartTime: i,
    container: r.frame,
    subject: t,
    center: n,
    shouldUseTimeDampening: a
  });
  return o && Nt(r, o) ? o : null;
}, Ws = function(e) {
  var r = e.droppable, t = e.subject, n = e.center, i = e.dragStartTime, a = e.shouldUseTimeDampening, o = r.frame;
  if (!o)
    return null;
  var l = Ra({
    dragStartTime: i,
    container: o.pageMarginBox,
    subject: t,
    center: n,
    shouldUseTimeDampening: a
  });
  return l && Mt(r, l) ? l : null;
}, fn = function(e) {
  var r = e.state, t = e.dragStartTime, n = e.shouldUseTimeDampening, i = e.scrollWindow, a = e.scrollDroppable, o = r.current.page.borderBoxCenter, l = r.dimensions.draggables[r.critical.draggable.id], s = l.page.marginBox;
  if (r.isWindowScrollAllowed) {
    var d = r.viewport, p = ks({
      dragStartTime: t,
      viewport: d,
      subject: s,
      center: o,
      shouldUseTimeDampening: n
    });
    if (p) {
      i(p);
      return;
    }
  }
  var c = Bs({
    center: o,
    destination: j(r.impact),
    droppables: r.dimensions.droppables
  });
  if (c) {
    var u = Ws({
      dragStartTime: t,
      droppable: c,
      subject: s,
      center: o,
      shouldUseTimeDampening: n
    });
    u && a(c.descriptor.id, u);
  }
}, Us = function(e) {
  var r = e.scrollWindow, t = e.scrollDroppable, n = $e(r), i = $e(t), a = null, o = function(p) {
    a || h(!1);
    var c = a, u = c.shouldUseTimeDampening, v = c.dragStartTime;
    fn({
      state: p,
      scrollWindow: n,
      scrollDroppable: i,
      dragStartTime: v,
      shouldUseTimeDampening: u
    });
  }, l = function(p) {
    a && h(!1);
    var c = Date.now(), u = !1, v = function() {
      u = !0;
    };
    fn({
      state: p,
      dragStartTime: 0,
      shouldUseTimeDampening: !1,
      scrollWindow: v,
      scrollDroppable: v
    }), a = {
      dragStartTime: c,
      shouldUseTimeDampening: u
    }, u && o(p);
  }, s = function() {
    a && (n.cancel(), i.cancel(), a = null);
  };
  return {
    start: l,
    stop: s,
    scroll: o
  };
}, Hs = function(e) {
  var r = e.move, t = e.scrollDroppable, n = e.scrollWindow, i = function(d, p) {
    var c = H(d.current.client.selection, p);
    r({
      client: c
    });
  }, a = function(d, p) {
    if (!Mt(d, p))
      return p;
    var c = Gs(d, p);
    if (!c)
      return t(d.descriptor.id, p), null;
    var u = V(p, c);
    t(d.descriptor.id, u);
    var v = V(p, u);
    return v;
  }, o = function(d, p, c) {
    if (!d || !Nt(p, c))
      return c;
    var u = Fs(p, c);
    if (!u)
      return n(c), null;
    var v = V(c, u);
    n(v);
    var f = V(c, v);
    return f;
  }, l = function(d) {
    var p = d.scrollJumpRequest;
    if (p) {
      var c = j(d.impact);
      c || h(!1);
      var u = a(d.dimensions.droppables[c], p);
      if (u) {
        var v = d.viewport, f = o(d.isWindowScrollAllowed, v, u);
        f && i(d, f);
      }
    }
  };
  return l;
}, $s = function(e) {
  var r = e.scrollDroppable, t = e.scrollWindow, n = e.move, i = Us({
    scrollWindow: t,
    scrollDroppable: r
  }), a = Hs({
    move: n,
    scrollWindow: t,
    scrollDroppable: r
  }), o = function(d) {
    if (d.phase === "DRAGGING") {
      if (d.movementMode === "FLUID") {
        i.scroll(d);
        return;
      }
      d.scrollJumpRequest && a(d);
    }
  }, l = {
    scroll: o,
    start: i.start,
    stop: i.stop
  };
  return l;
}, we = "data-rbd", Ee = function() {
  var e = we + "-drag-handle";
  return {
    base: e,
    draggableId: e + "-draggable-id",
    contextId: e + "-context-id"
  };
}(), st = function() {
  var e = we + "-draggable";
  return {
    base: e,
    contextId: e + "-context-id",
    id: e + "-id"
  };
}(), qs = function() {
  var e = we + "-droppable";
  return {
    base: e,
    contextId: e + "-context-id",
    id: e + "-id"
  };
}(), gn = {
  contextId: we + "-scroll-container-context-id"
}, Vs = function(r) {
  return function(t) {
    return "[" + t + '="' + r + '"]';
  };
}, Le = function(r, t) {
  return r.map(function(n) {
    var i = n.styles[t];
    return i ? n.selector + " { " + i + " }" : "";
  }).join(" ");
}, js = "pointer-events: none;", zs = function(e) {
  var r = Vs(e), t = function() {
    var l = `
      cursor: -webkit-grab;
      cursor: grab;
    `;
    return {
      selector: r(Ee.contextId),
      styles: {
        always: `
          -webkit-touch-callout: none;
          -webkit-tap-highlight-color: rgba(0,0,0,0);
          touch-action: manipulation;
        `,
        resting: l,
        dragging: js,
        dropAnimating: l
      }
    };
  }(), n = function() {
    var l = `
      transition: ` + We.outOfTheWay + `;
    `;
    return {
      selector: r(st.contextId),
      styles: {
        dragging: l,
        dropAnimating: l,
        userCancel: l
      }
    };
  }(), i = {
    selector: r(qs.contextId),
    styles: {
      always: "overflow-anchor: none;"
    }
  }, a = {
    selector: "body",
    styles: {
      dragging: `
        cursor: grabbing;
        cursor: -webkit-grabbing;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        overflow-anchor: none;
      `
    }
  }, o = [n, t, i, a];
  return {
    always: Le(o, "always"),
    resting: Le(o, "resting"),
    dragging: Le(o, "dragging"),
    dropAnimating: Le(o, "dropAnimating"),
    userCancel: Le(o, "userCancel")
  };
}, z = typeof window < "u" && typeof window.document < "u" && typeof window.document.createElement < "u" ? En : ie, Zr = function() {
  var r = document.querySelector("head");
  return r || h(!1), r;
}, mn = function(r) {
  var t = document.createElement("style");
  return r && t.setAttribute("nonce", r), t.type = "text/css", t;
};
function Ks(e, r) {
  var t = A(function() {
    return zs(e);
  }, [e]), n = O(null), i = O(null), a = x(k(function(c) {
    var u = i.current;
    u || h(!1), u.textContent = c;
  }), []), o = x(function(c) {
    var u = n.current;
    u || h(!1), u.textContent = c;
  }, []);
  z(function() {
    !n.current && !i.current || h(!1);
    var c = mn(r), u = mn(r);
    return n.current = c, i.current = u, c.setAttribute(we + "-always", e), u.setAttribute(we + "-dynamic", e), Zr().appendChild(c), Zr().appendChild(u), o(t.always), a(t.resting), function() {
      var v = function(g) {
        var b = g.current;
        b || h(!1), Zr().removeChild(b), g.current = null;
      };
      v(n), v(i);
    };
  }, [r, o, a, t.always, t.resting, e]);
  var l = x(function() {
    return a(t.dragging);
  }, [a, t.dragging]), s = x(function(c) {
    if (c === "DROP") {
      a(t.dropAnimating);
      return;
    }
    a(t.userCancel);
  }, [a, t.dropAnimating, t.userCancel]), d = x(function() {
    i.current && a(t.resting);
  }, [a, t.resting]), p = A(function() {
    return {
      dragging: l,
      dropping: s,
      resting: d
    };
  }, [l, s, d]);
  return p;
}
var Ta = function(e) {
  return e && e.ownerDocument ? e.ownerDocument.defaultView : window;
};
function Lr(e) {
  return e instanceof Ta(e).HTMLElement;
}
function Ys(e, r) {
  var t = "[" + Ee.contextId + '="' + e + '"]', n = ea(document.querySelectorAll(t));
  if (!n.length)
    return null;
  var i = fe(n, function(a) {
    return a.getAttribute(Ee.draggableId) === r;
  });
  return !i || !Lr(i) ? null : i;
}
function Js(e) {
  var r = O({}), t = O(null), n = O(null), i = O(!1), a = x(function(u, v) {
    var f = {
      id: u,
      focus: v
    };
    return r.current[u] = f, function() {
      var b = r.current, m = b[u];
      m !== f && delete b[u];
    };
  }, []), o = x(function(u) {
    var v = Ys(e, u);
    v && v !== document.activeElement && v.focus();
  }, [e]), l = x(function(u, v) {
    t.current === u && (t.current = v);
  }, []), s = x(function() {
    n.current || i.current && (n.current = requestAnimationFrame(function() {
      n.current = null;
      var u = t.current;
      u && o(u);
    }));
  }, [o]), d = x(function(u) {
    t.current = null;
    var v = document.activeElement;
    v && v.getAttribute(Ee.draggableId) === u && (t.current = u);
  }, []);
  z(function() {
    return i.current = !0, function() {
      i.current = !1;
      var u = n.current;
      u && cancelAnimationFrame(u);
    };
  }, []);
  var p = A(function() {
    return {
      register: a,
      tryRecordFocus: d,
      tryRestoreFocusRecorded: s,
      tryShiftRecord: l
    };
  }, [a, d, s, l]);
  return p;
}
function Xs() {
  var e = {
    draggables: {},
    droppables: {}
  }, r = [];
  function t(c) {
    return r.push(c), function() {
      var v = r.indexOf(c);
      v !== -1 && r.splice(v, 1);
    };
  }
  function n(c) {
    r.length && r.forEach(function(u) {
      return u(c);
    });
  }
  function i(c) {
    return e.draggables[c] || null;
  }
  function a(c) {
    var u = i(c);
    return u || h(!1), u;
  }
  var o = {
    register: function(u) {
      e.draggables[u.descriptor.id] = u, n({
        type: "ADDITION",
        value: u
      });
    },
    update: function(u, v) {
      var f = e.draggables[v.descriptor.id];
      f && f.uniqueId === u.uniqueId && (delete e.draggables[v.descriptor.id], e.draggables[u.descriptor.id] = u);
    },
    unregister: function(u) {
      var v = u.descriptor.id, f = i(v);
      f && u.uniqueId === f.uniqueId && (delete e.draggables[v], n({
        type: "REMOVAL",
        value: u
      }));
    },
    getById: a,
    findById: i,
    exists: function(u) {
      return !!i(u);
    },
    getAllByType: function(u) {
      return Cr(e.draggables).filter(function(v) {
        return v.descriptor.type === u;
      });
    }
  };
  function l(c) {
    return e.droppables[c] || null;
  }
  function s(c) {
    var u = l(c);
    return u || h(!1), u;
  }
  var d = {
    register: function(u) {
      e.droppables[u.descriptor.id] = u;
    },
    unregister: function(u) {
      var v = l(u.descriptor.id);
      v && u.uniqueId === v.uniqueId && delete e.droppables[u.descriptor.id];
    },
    getById: s,
    findById: l,
    exists: function(u) {
      return !!l(u);
    },
    getAllByType: function(u) {
      return Cr(e.droppables).filter(function(v) {
        return v.descriptor.type === u;
      });
    }
  };
  function p() {
    e.draggables = {}, e.droppables = {}, r.length = 0;
  }
  return {
    draggable: o,
    droppable: d,
    subscribe: t,
    clean: p
  };
}
function Qs() {
  var e = A(Xs, []);
  return ie(function() {
    return function() {
      requestAnimationFrame(e.clean);
    };
  }, [e]), e;
}
var Lt = N.createContext(null), Er = function() {
  var e = document.body;
  return e || h(!1), e;
}, Zs = {
  position: "absolute",
  width: "1px",
  height: "1px",
  margin: "-1px",
  border: "0",
  padding: "0",
  overflow: "hidden",
  clip: "rect(0 0 0 0)",
  "clip-path": "inset(100%)"
}, _s = function(r) {
  return "rbd-announcement-" + r;
};
function eu(e) {
  var r = A(function() {
    return _s(e);
  }, [e]), t = O(null);
  ie(function() {
    var a = document.createElement("div");
    return t.current = a, a.id = r, a.setAttribute("aria-live", "assertive"), a.setAttribute("aria-atomic", "true"), S(a.style, Zs), Er().appendChild(a), function() {
      setTimeout(function() {
        var s = Er();
        s.contains(a) && s.removeChild(a), a === t.current && (t.current = null);
      });
    };
  }, [r]);
  var n = x(function(i) {
    var a = t.current;
    if (a) {
      a.textContent = i;
      return;
    }
  }, []);
  return n;
}
var ru = 0, tu = {
  separator: "::"
};
function Ft(e, r) {
  return r === void 0 && (r = tu), A(function() {
    return "" + e + r.separator + ru++;
  }, [r.separator, e]);
}
function nu(e) {
  var r = e.contextId, t = e.uniqueId;
  return "rbd-hidden-text-" + r + "-" + t;
}
function au(e) {
  var r = e.contextId, t = e.text, n = Ft("hidden-text", {
    separator: "-"
  }), i = A(function() {
    return nu({
      contextId: r,
      uniqueId: n
    });
  }, [n, r]);
  return ie(function() {
    var o = document.createElement("div");
    return o.id = i, o.textContent = t, o.style.display = "none", Er().appendChild(o), function() {
      var s = Er();
      s.contains(o) && s.removeChild(o);
    };
  }, [i, t]), i;
}
var Fr = N.createContext(null);
function Na(e) {
  var r = O(e);
  return ie(function() {
    r.current = e;
  }), r;
}
function iu() {
  var e = null;
  function r() {
    return !!e;
  }
  function t(o) {
    return o === e;
  }
  function n(o) {
    e && h(!1);
    var l = {
      abandon: o
    };
    return e = l, l;
  }
  function i() {
    e || h(!1), e = null;
  }
  function a() {
    e && (e.abandon(), i());
  }
  return {
    isClaimed: r,
    isActive: t,
    claim: n,
    release: i,
    tryAbandon: a
  };
}
var ou = 9, lu = 13, Gt = 27, Ma = 32, su = 33, uu = 34, cu = 35, du = 36, pu = 37, vu = 38, fu = 39, gu = 40, vr, mu = (vr = {}, vr[lu] = !0, vr[ou] = !0, vr), La = function(e) {
  mu[e.keyCode] && e.preventDefault();
}, Gr = function() {
  var e = "visibilitychange";
  if (typeof document > "u")
    return e;
  var r = [e, "ms" + e, "webkit" + e, "moz" + e, "o" + e], t = fe(r, function(n) {
    return "on" + n in document;
  });
  return t || e;
}(), Fa = 0, bn = 5;
function bu(e, r) {
  return Math.abs(r.x - e.x) >= bn || Math.abs(r.y - e.y) >= bn;
}
var hn = {
  type: "IDLE"
};
function hu(e) {
  var r = e.cancel, t = e.completed, n = e.getPhase, i = e.setPhase;
  return [{
    eventName: "mousemove",
    fn: function(o) {
      var l = o.button, s = o.clientX, d = o.clientY;
      if (l === Fa) {
        var p = {
          x: s,
          y: d
        }, c = n();
        if (c.type === "DRAGGING") {
          o.preventDefault(), c.actions.move(p);
          return;
        }
        c.type !== "PENDING" && h(!1);
        var u = c.point;
        if (bu(u, p)) {
          o.preventDefault();
          var v = c.actions.fluidLift(p);
          i({
            type: "DRAGGING",
            actions: v
          });
        }
      }
    }
  }, {
    eventName: "mouseup",
    fn: function(o) {
      var l = n();
      if (l.type !== "DRAGGING") {
        r();
        return;
      }
      o.preventDefault(), l.actions.drop({
        shouldBlockNextClick: !0
      }), t();
    }
  }, {
    eventName: "mousedown",
    fn: function(o) {
      n().type === "DRAGGING" && o.preventDefault(), r();
    }
  }, {
    eventName: "keydown",
    fn: function(o) {
      var l = n();
      if (l.type === "PENDING") {
        r();
        return;
      }
      if (o.keyCode === Gt) {
        o.preventDefault(), r();
        return;
      }
      La(o);
    }
  }, {
    eventName: "resize",
    fn: r
  }, {
    eventName: "scroll",
    options: {
      passive: !0,
      capture: !1
    },
    fn: function() {
      n().type === "PENDING" && r();
    }
  }, {
    eventName: "webkitmouseforcedown",
    fn: function(o) {
      var l = n();
      if (l.type === "IDLE" && h(!1), l.actions.shouldRespectForcePress()) {
        r();
        return;
      }
      o.preventDefault();
    }
  }, {
    eventName: Gr,
    fn: r
  }];
}
function yu(e) {
  var r = O(hn), t = O(ce), n = A(function() {
    return {
      eventName: "mousedown",
      fn: function(c) {
        if (!c.defaultPrevented && c.button === Fa && !(c.ctrlKey || c.metaKey || c.shiftKey || c.altKey)) {
          var u = e.findClosestDraggableId(c);
          if (u) {
            var v = e.tryGetLock(u, o, {
              sourceEvent: c
            });
            if (v) {
              c.preventDefault();
              var f = {
                x: c.clientX,
                y: c.clientY
              };
              t.current(), d(v, f);
            }
          }
        }
      }
    };
  }, [e]), i = A(function() {
    return {
      eventName: "webkitmouseforcewillbegin",
      fn: function(c) {
        if (!c.defaultPrevented) {
          var u = e.findClosestDraggableId(c);
          if (u) {
            var v = e.findOptionsForDraggable(u);
            v && (v.shouldRespectForcePress || e.canGetLock(u) && c.preventDefault());
          }
        }
      }
    };
  }, [e]), a = x(function() {
    var c = {
      passive: !1,
      capture: !0
    };
    t.current = _(window, [i, n], c);
  }, [i, n]), o = x(function() {
    var p = r.current;
    p.type !== "IDLE" && (r.current = hn, t.current(), a());
  }, [a]), l = x(function() {
    var p = r.current;
    o(), p.type === "DRAGGING" && p.actions.cancel({
      shouldBlockNextClick: !0
    }), p.type === "PENDING" && p.actions.abort();
  }, [o]), s = x(function() {
    var c = {
      capture: !0,
      passive: !1
    }, u = hu({
      cancel: l,
      completed: o,
      getPhase: function() {
        return r.current;
      },
      setPhase: function(f) {
        r.current = f;
      }
    });
    t.current = _(window, u, c);
  }, [l, o]), d = x(function(c, u) {
    r.current.type !== "IDLE" && h(!1), r.current = {
      type: "PENDING",
      point: u,
      actions: c
    }, s();
  }, [s]);
  z(function() {
    return a(), function() {
      t.current();
    };
  }, [a]);
}
var Se;
function Du() {
}
var Iu = (Se = {}, Se[uu] = !0, Se[su] = !0, Se[du] = !0, Se[cu] = !0, Se);
function xu(e, r) {
  function t() {
    r(), e.cancel();
  }
  function n() {
    r(), e.drop();
  }
  return [{
    eventName: "keydown",
    fn: function(a) {
      if (a.keyCode === Gt) {
        a.preventDefault(), t();
        return;
      }
      if (a.keyCode === Ma) {
        a.preventDefault(), n();
        return;
      }
      if (a.keyCode === gu) {
        a.preventDefault(), e.moveDown();
        return;
      }
      if (a.keyCode === vu) {
        a.preventDefault(), e.moveUp();
        return;
      }
      if (a.keyCode === fu) {
        a.preventDefault(), e.moveRight();
        return;
      }
      if (a.keyCode === pu) {
        a.preventDefault(), e.moveLeft();
        return;
      }
      if (Iu[a.keyCode]) {
        a.preventDefault();
        return;
      }
      La(a);
    }
  }, {
    eventName: "mousedown",
    fn: t
  }, {
    eventName: "mouseup",
    fn: t
  }, {
    eventName: "click",
    fn: t
  }, {
    eventName: "touchstart",
    fn: t
  }, {
    eventName: "resize",
    fn: t
  }, {
    eventName: "wheel",
    fn: t,
    options: {
      passive: !0
    }
  }, {
    eventName: Gr,
    fn: t
  }];
}
function Su(e) {
  var r = O(Du), t = A(function() {
    return {
      eventName: "keydown",
      fn: function(a) {
        if (a.defaultPrevented || a.keyCode !== Ma)
          return;
        var o = e.findClosestDraggableId(a);
        if (!o)
          return;
        var l = e.tryGetLock(o, p, {
          sourceEvent: a
        });
        if (!l)
          return;
        a.preventDefault();
        var s = !0, d = l.snapLift();
        r.current();
        function p() {
          s || h(!1), s = !1, r.current(), n();
        }
        r.current = _(window, xu(d, p), {
          capture: !0,
          passive: !1
        });
      }
    };
  }, [e]), n = x(function() {
    var a = {
      passive: !1,
      capture: !0
    };
    r.current = _(window, [t], a);
  }, [t]);
  z(function() {
    return n(), function() {
      r.current();
    };
  }, [n]);
}
var _r = {
  type: "IDLE"
}, Cu = 120, wu = 0.15;
function Eu(e) {
  var r = e.cancel, t = e.getPhase;
  return [{
    eventName: "orientationchange",
    fn: r
  }, {
    eventName: "resize",
    fn: r
  }, {
    eventName: "contextmenu",
    fn: function(i) {
      i.preventDefault();
    }
  }, {
    eventName: "keydown",
    fn: function(i) {
      if (t().type !== "DRAGGING") {
        r();
        return;
      }
      i.keyCode === Gt && i.preventDefault(), r();
    }
  }, {
    eventName: Gr,
    fn: r
  }];
}
function Pu(e) {
  var r = e.cancel, t = e.completed, n = e.getPhase;
  return [{
    eventName: "touchmove",
    options: {
      capture: !1
    },
    fn: function(a) {
      var o = n();
      if (o.type !== "DRAGGING") {
        r();
        return;
      }
      o.hasMoved = !0;
      var l = a.touches[0], s = l.clientX, d = l.clientY, p = {
        x: s,
        y: d
      };
      a.preventDefault(), o.actions.move(p);
    }
  }, {
    eventName: "touchend",
    fn: function(a) {
      var o = n();
      if (o.type !== "DRAGGING") {
        r();
        return;
      }
      a.preventDefault(), o.actions.drop({
        shouldBlockNextClick: !0
      }), t();
    }
  }, {
    eventName: "touchcancel",
    fn: function(a) {
      if (n().type !== "DRAGGING") {
        r();
        return;
      }
      a.preventDefault(), r();
    }
  }, {
    eventName: "touchforcechange",
    fn: function(a) {
      var o = n();
      o.type === "IDLE" && h(!1);
      var l = a.touches[0];
      if (l) {
        var s = l.force >= wu;
        if (s) {
          var d = o.actions.shouldRespectForcePress();
          if (o.type === "PENDING") {
            d && r();
            return;
          }
          if (d) {
            if (o.hasMoved) {
              a.preventDefault();
              return;
            }
            r();
            return;
          }
          a.preventDefault();
        }
      }
    }
  }, {
    eventName: Gr,
    fn: r
  }];
}
function Au(e) {
  var r = O(_r), t = O(ce), n = x(function() {
    return r.current;
  }, []), i = x(function(v) {
    r.current = v;
  }, []), a = A(function() {
    return {
      eventName: "touchstart",
      fn: function(v) {
        if (!v.defaultPrevented) {
          var f = e.findClosestDraggableId(v);
          if (f) {
            var g = e.tryGetLock(f, l, {
              sourceEvent: v
            });
            if (g) {
              var b = v.touches[0], m = b.clientX, y = b.clientY, I = {
                x: m,
                y
              };
              t.current(), c(g, I);
            }
          }
        }
      }
    };
  }, [e]), o = x(function() {
    var v = {
      capture: !0,
      passive: !1
    };
    t.current = _(window, [a], v);
  }, [a]), l = x(function() {
    var u = r.current;
    u.type !== "IDLE" && (u.type === "PENDING" && clearTimeout(u.longPressTimerId), i(_r), t.current(), o());
  }, [o, i]), s = x(function() {
    var u = r.current;
    l(), u.type === "DRAGGING" && u.actions.cancel({
      shouldBlockNextClick: !0
    }), u.type === "PENDING" && u.actions.abort();
  }, [l]), d = x(function() {
    var v = {
      capture: !0,
      passive: !1
    }, f = {
      cancel: s,
      completed: l,
      getPhase: n
    }, g = _(window, Pu(f), v), b = _(window, Eu(f), v);
    t.current = function() {
      g(), b();
    };
  }, [s, n, l]), p = x(function() {
    var v = n();
    v.type !== "PENDING" && h(!1);
    var f = v.actions.fluidLift(v.point);
    i({
      type: "DRAGGING",
      actions: f,
      hasMoved: !1
    });
  }, [n, i]), c = x(function(v, f) {
    n().type !== "IDLE" && h(!1);
    var g = setTimeout(p, Cu);
    i({
      type: "PENDING",
      point: f,
      actions: v,
      longPressTimerId: g
    }), d();
  }, [d, n, i, p]);
  z(function() {
    return o(), function() {
      t.current();
      var f = n();
      f.type === "PENDING" && (clearTimeout(f.longPressTimerId), i(_r));
    };
  }, [n, o, i]), z(function() {
    var v = _(window, [{
      eventName: "touchmove",
      fn: function() {
      },
      options: {
        capture: !1,
        passive: !1
      }
    }]);
    return v;
  }, []);
}
var Bu = {
  input: !0,
  button: !0,
  textarea: !0,
  select: !0,
  option: !0,
  optgroup: !0,
  video: !0,
  audio: !0
};
function Ga(e, r) {
  if (r == null)
    return !1;
  var t = !!Bu[r.tagName.toLowerCase()];
  if (t)
    return !0;
  var n = r.getAttribute("contenteditable");
  return n === "true" || n === "" ? !0 : r === e ? !1 : Ga(e, r.parentElement);
}
function Ru(e, r) {
  var t = r.target;
  return Lr(t) ? Ga(e, t) : !1;
}
var Ou = function(e) {
  return te(e.getBoundingClientRect()).center;
};
function Tu(e) {
  return e instanceof Ta(e).Element;
}
var Nu = function() {
  var e = "matches";
  if (typeof document > "u")
    return e;
  var r = [e, "msMatchesSelector", "webkitMatchesSelector"], t = fe(r, function(n) {
    return n in Element.prototype;
  });
  return t || e;
}();
function ka(e, r) {
  return e == null ? null : e[Nu](r) ? e : ka(e.parentElement, r);
}
function Mu(e, r) {
  return e.closest ? e.closest(r) : ka(e, r);
}
function Lu(e) {
  return "[" + Ee.contextId + '="' + e + '"]';
}
function Fu(e, r) {
  var t = r.target;
  if (!Tu(t))
    return null;
  var n = Lu(e), i = Mu(t, n);
  return !i || !Lr(i) ? null : i;
}
function Gu(e, r) {
  var t = Fu(e, r);
  return t ? t.getAttribute(Ee.draggableId) : null;
}
function ku(e, r) {
  var t = "[" + st.contextId + '="' + e + '"]', n = ea(document.querySelectorAll(t)), i = fe(n, function(a) {
    return a.getAttribute(st.id) === r;
  });
  return !i || !Lr(i) ? null : i;
}
function Wu(e) {
  e.preventDefault();
}
function fr(e) {
  var r = e.expected, t = e.phase, n = e.isLockActive;
  return e.shouldWarn, !(!n() || r !== t);
}
function Wa(e) {
  var r = e.lockAPI, t = e.store, n = e.registry, i = e.draggableId;
  if (r.isClaimed())
    return !1;
  var a = n.draggable.findById(i);
  return !(!a || !a.options.isEnabled || !Aa(t.getState(), i));
}
function Uu(e) {
  var r = e.lockAPI, t = e.contextId, n = e.store, i = e.registry, a = e.draggableId, o = e.forceSensorStop, l = e.sourceEvent, s = Wa({
    lockAPI: r,
    store: n,
    registry: i,
    draggableId: a
  });
  if (!s)
    return null;
  var d = i.draggable.getById(a), p = ku(t, d.descriptor.id);
  if (!p || l && !d.options.canDragInteractiveElements && Ru(p, l))
    return null;
  var c = r.claim(o || ce), u = "PRE_DRAG";
  function v() {
    return d.options.shouldRespectForcePress;
  }
  function f() {
    return r.isActive(c);
  }
  function g(C, B) {
    fr({
      expected: C,
      phase: u,
      isLockActive: f,
      shouldWarn: !0
    }) && n.dispatch(B());
  }
  var b = g.bind(null, "DRAGGING");
  function m(C) {
    function B() {
      r.release(), u = "COMPLETED";
    }
    u !== "PRE_DRAG" && (B(), u !== "PRE_DRAG" && h(!1)), n.dispatch(Nl(C.liftActionArgs)), u = "DRAGGING";
    function M(P, L) {
      if (L === void 0 && (L = {
        shouldBlockNextClick: !1
      }), C.cleanup(), L.shouldBlockNextClick) {
        var R = _(window, [{
          eventName: "click",
          fn: Wu,
          options: {
            once: !0,
            passive: !1,
            capture: !0
          }
        }]);
        setTimeout(R);
      }
      B(), n.dispatch(Ia({
        reason: P
      }));
    }
    return S({
      isActive: function() {
        return fr({
          expected: "DRAGGING",
          phase: u,
          isLockActive: f,
          shouldWarn: !1
        });
      },
      shouldRespectForcePress: v,
      drop: function(L) {
        return M("DROP", L);
      },
      cancel: function(L) {
        return M("CANCEL", L);
      }
    }, C.actions);
  }
  function y(C) {
    var B = $e(function(P) {
      b(function() {
        return Da({
          client: P
        });
      });
    }), M = m({
      liftActionArgs: {
        id: a,
        clientSelection: C,
        movementMode: "FLUID"
      },
      cleanup: function() {
        return B.cancel();
      },
      actions: {
        move: B
      }
    });
    return S({}, M, {
      move: B
    });
  }
  function I() {
    var C = {
      moveUp: function() {
        return b($l);
      },
      moveRight: function() {
        return b(Vl);
      },
      moveDown: function() {
        return b(ql);
      },
      moveLeft: function() {
        return b(jl);
      }
    };
    return m({
      liftActionArgs: {
        id: a,
        clientSelection: Ou(p),
        movementMode: "SNAP"
      },
      cleanup: ce,
      actions: C
    });
  }
  function D() {
    var C = fr({
      expected: "PRE_DRAG",
      phase: u,
      isLockActive: f,
      shouldWarn: !0
    });
    C && r.release();
  }
  var w = {
    isActive: function() {
      return fr({
        expected: "PRE_DRAG",
        phase: u,
        isLockActive: f,
        shouldWarn: !1
      });
    },
    shouldRespectForcePress: v,
    fluidLift: y,
    snapLift: I,
    abort: D
  };
  return w;
}
var Hu = [yu, Su, Au];
function $u(e) {
  var r = e.contextId, t = e.store, n = e.registry, i = e.customSensors, a = e.enableDefaultSensors, o = [].concat(a ? Hu : [], i || []), l = Ae(function() {
    return iu();
  })[0], s = x(function(y, I) {
    y.isDragging && !I.isDragging && l.tryAbandon();
  }, [l]);
  z(function() {
    var y = t.getState(), I = t.subscribe(function() {
      var D = t.getState();
      s(y, D), y = D;
    });
    return I;
  }, [l, t, s]), z(function() {
    return l.tryAbandon;
  }, [l.tryAbandon]);
  for (var d = x(function(m) {
    return Wa({
      lockAPI: l,
      registry: n,
      store: t,
      draggableId: m
    });
  }, [l, n, t]), p = x(function(m, y, I) {
    return Uu({
      lockAPI: l,
      registry: n,
      contextId: r,
      store: t,
      draggableId: m,
      forceSensorStop: y,
      sourceEvent: I && I.sourceEvent ? I.sourceEvent : null
    });
  }, [r, l, n, t]), c = x(function(m) {
    return Gu(r, m);
  }, [r]), u = x(function(m) {
    var y = n.draggable.findById(m);
    return y ? y.options : null;
  }, [n.draggable]), v = x(function() {
    l.isClaimed() && (l.tryAbandon(), t.getState().phase !== "IDLE" && t.dispatch(Pt()));
  }, [l, t]), f = x(l.isClaimed, [l]), g = A(function() {
    return {
      canGetLock: d,
      tryGetLock: p,
      findClosestDraggableId: c,
      findOptionsForDraggable: u,
      tryReleaseLock: v,
      isLockClaimed: f
    };
  }, [d, p, c, u, v, f]), b = 0; b < o.length; b++)
    o[b](g);
}
var qu = function(r) {
  return {
    onBeforeCapture: r.onBeforeCapture,
    onBeforeDragStart: r.onBeforeDragStart,
    onDragStart: r.onDragStart,
    onDragEnd: r.onDragEnd,
    onDragUpdate: r.onDragUpdate
  };
};
function Fe(e) {
  return e.current || h(!1), e.current;
}
function Vu(e) {
  var r = e.contextId, t = e.setCallbacks, n = e.sensors, i = e.nonce, a = e.dragHandleUsageInstructions, o = O(null), l = Na(e), s = x(function() {
    return qu(l.current);
  }, [l]), d = eu(r), p = au({
    contextId: r,
    text: a
  }), c = Ks(r, i), u = x(function(P) {
    Fe(o).dispatch(P);
  }, []), v = A(function() {
    return $t({
      publishWhileDragging: Ll,
      updateDroppableScroll: Gl,
      updateDroppableIsEnabled: kl,
      updateDroppableIsCombineEnabled: Wl,
      collectionStarting: Fl
    }, u);
  }, [u]), f = Qs(), g = A(function() {
    return ws(f, v);
  }, [f, v]), b = A(function() {
    return $s(S({
      scrollWindow: Es,
      scrollDroppable: g.scrollDroppable
    }, $t({
      move: Da
    }, u)));
  }, [g.scrollDroppable, u]), m = Js(r), y = A(function() {
    return Is({
      announce: d,
      autoScroller: b,
      dimensionMarshal: g,
      focusMarshal: m,
      getResponders: s,
      styleMarshal: c
    });
  }, [d, b, g, m, s, c]);
  o.current = y;
  var I = x(function() {
    var P = Fe(o), L = P.getState();
    L.phase !== "IDLE" && P.dispatch(Pt());
  }, []), D = x(function() {
    var P = Fe(o).getState();
    return P.isDragging || P.phase === "DROP_ANIMATING";
  }, []), w = A(function() {
    return {
      isDragging: D,
      tryAbort: I
    };
  }, [D, I]);
  t(w);
  var C = x(function(P) {
    return Aa(Fe(o).getState(), P);
  }, []), B = x(function() {
    return he(Fe(o).getState());
  }, []), M = A(function() {
    return {
      marshal: g,
      focus: m,
      contextId: r,
      canLift: C,
      isMovementAllowed: B,
      dragHandleUsageInstructionsId: p,
      registry: f
    };
  }, [r, g, p, m, C, B, f]);
  return $u({
    contextId: r,
    store: y,
    registry: f,
    customSensors: n,
    enableDefaultSensors: e.enableDefaultSensors !== !1
  }), ie(function() {
    return I;
  }, [I]), N.createElement(Fr.Provider, {
    value: M
  }, N.createElement(Ci, {
    context: Lt,
    store: y
  }, e.children));
}
var ju = 0;
function zu() {
  return A(function() {
    return "" + ju++;
  }, []);
}
function Ku(e) {
  var r = zu(), t = e.dragHandleUsageInstructions || br.dragHandleUsageInstructions;
  return N.createElement(So, null, function(n) {
    return N.createElement(Vu, {
      nonce: e.nonce,
      contextId: r,
      setCallbacks: n,
      dragHandleUsageInstructions: t,
      enableDefaultSensors: e.enableDefaultSensors,
      sensors: e.sensors,
      onBeforeCapture: e.onBeforeCapture,
      onBeforeDragStart: e.onBeforeDragStart,
      onDragStart: e.onDragStart,
      onDragUpdate: e.onDragUpdate,
      onDragEnd: e.onDragEnd
    }, e.children);
  });
}
var Ua = function(r) {
  return function(t) {
    return r === t;
  };
}, Yu = Ua("scroll"), Ju = Ua("auto"), yn = function(r, t) {
  return t(r.overflowX) || t(r.overflowY);
}, Xu = function(r) {
  var t = window.getComputedStyle(r), n = {
    overflowX: t.overflowX,
    overflowY: t.overflowY
  };
  return yn(n, Yu) || yn(n, Ju);
}, Qu = function() {
  return !1;
}, Zu = function e(r) {
  return r == null ? null : r === document.body ? Qu() ? r : null : r === document.documentElement ? null : Xu(r) ? r : e(r.parentElement);
}, ut = function(e) {
  return {
    x: e.scrollLeft,
    y: e.scrollTop
  };
}, _u = function e(r) {
  if (!r)
    return !1;
  var t = window.getComputedStyle(r);
  return t.position === "fixed" ? !0 : e(r.parentElement);
}, ec = function(e) {
  var r = Zu(e), t = _u(e);
  return {
    closestScrollable: r,
    isFixedOnPage: t
  };
}, rc = function(e) {
  var r = e.descriptor, t = e.isEnabled, n = e.isCombineEnabled, i = e.isFixedOnPage, a = e.direction, o = e.client, l = e.page, s = e.closest, d = function() {
    if (!s)
      return null;
    var v = s.scrollSize, f = s.client, g = wa({
      scrollHeight: v.scrollHeight,
      scrollWidth: v.scrollWidth,
      height: f.paddingBox.height,
      width: f.paddingBox.width
    });
    return {
      pageMarginBox: s.page.marginBox,
      frameClient: f,
      scrollSize: v,
      shouldClipSubject: s.shouldClipSubject,
      scroll: {
        initial: s.scroll,
        current: s.scroll,
        max: g,
        diff: {
          value: W,
          displacement: W
        }
      }
    };
  }(), p = a === "vertical" ? xt : ia, c = Ce({
    page: l,
    withPlaceholder: null,
    axis: p,
    frame: d
  }), u = {
    descriptor: r,
    isCombineEnabled: n,
    isFixedOnPage: i,
    axis: p,
    isEnabled: t,
    client: o,
    page: l,
    frame: d,
    subject: c
  };
  return u;
}, tc = function(r, t) {
  var n = Jn(r);
  if (!t || r !== t)
    return n;
  var i = n.paddingBox.top - t.scrollTop, a = n.paddingBox.left - t.scrollLeft, o = i + t.scrollHeight, l = a + t.scrollWidth, s = {
    top: i,
    right: l,
    bottom: o,
    left: a
  }, d = bt(s, n.border), p = ht({
    borderBox: d,
    margin: n.margin,
    border: n.border,
    padding: n.padding
  });
  return p;
}, nc = function(e) {
  var r = e.ref, t = e.descriptor, n = e.env, i = e.windowScroll, a = e.direction, o = e.isDropDisabled, l = e.isCombineEnabled, s = e.shouldClipSubject, d = n.closestScrollable, p = tc(r, d), c = Ir(p, i), u = function() {
    if (!d)
      return null;
    var f = Jn(d), g = {
      scrollHeight: d.scrollHeight,
      scrollWidth: d.scrollWidth
    };
    return {
      client: f,
      page: Ir(f, i),
      scroll: ut(d),
      scrollSize: g,
      shouldClipSubject: s
    };
  }(), v = rc({
    descriptor: t,
    isEnabled: !o,
    isCombineEnabled: l,
    isFixedOnPage: n.isFixedOnPage,
    direction: a,
    client: p,
    page: c,
    closest: u
  });
  return v;
}, ac = {
  passive: !1
}, ic = {
  passive: !0
}, Dn = function(e) {
  return e.shouldPublishImmediately ? ac : ic;
};
function Pr(e) {
  var r = Je(e);
  return r || h(!1), r;
}
var gr = function(r) {
  return r && r.env.closestScrollable || null;
};
function oc(e) {
  var r = O(null), t = Pr(Fr), n = Ft("droppable"), i = t.registry, a = t.marshal, o = Na(e), l = A(function() {
    return {
      id: e.droppableId,
      type: e.type,
      mode: e.mode
    };
  }, [e.droppableId, e.mode, e.type]), s = O(l), d = A(function() {
    return k(function(D, w) {
      r.current || h(!1);
      var C = {
        x: D,
        y: w
      };
      a.updateDroppableScroll(l.id, C);
    });
  }, [l.id, a]), p = x(function() {
    var D = r.current;
    return !D || !D.env.closestScrollable ? W : ut(D.env.closestScrollable);
  }, []), c = x(function() {
    var D = p();
    d(D.x, D.y);
  }, [p, d]), u = A(function() {
    return $e(c);
  }, [c]), v = x(function() {
    var D = r.current, w = gr(D);
    D && w || h(!1);
    var C = D.scrollOptions;
    if (C.shouldPublishImmediately) {
      c();
      return;
    }
    u();
  }, [u, c]), f = x(function(D, w) {
    r.current && h(!1);
    var C = o.current, B = C.getDroppableRef();
    B || h(!1);
    var M = ec(B), P = {
      ref: B,
      descriptor: l,
      env: M,
      scrollOptions: w
    };
    r.current = P;
    var L = nc({
      ref: B,
      descriptor: l,
      env: M,
      windowScroll: D,
      direction: C.direction,
      isDropDisabled: C.isDropDisabled,
      isCombineEnabled: C.isCombineEnabled,
      shouldClipSubject: !C.ignoreContainerClipping
    }), R = M.closestScrollable;
    return R && (R.setAttribute(gn.contextId, t.contextId), R.addEventListener("scroll", v, Dn(P.scrollOptions))), L;
  }, [t.contextId, l, v, o]), g = x(function() {
    var D = r.current, w = gr(D);
    return D && w || h(!1), ut(w);
  }, []), b = x(function() {
    var D = r.current;
    D || h(!1);
    var w = gr(D);
    r.current = null, w && (u.cancel(), w.removeAttribute(gn.contextId), w.removeEventListener("scroll", v, Dn(D.scrollOptions)));
  }, [v, u]), m = x(function(D) {
    var w = r.current;
    w || h(!1);
    var C = gr(w);
    C || h(!1), C.scrollTop += D.y, C.scrollLeft += D.x;
  }, []), y = A(function() {
    return {
      getDimensionAndWatchScroll: f,
      getScrollWhileDragging: g,
      dragStopped: b,
      scroll: m
    };
  }, [b, f, g, m]), I = A(function() {
    return {
      uniqueId: n,
      descriptor: l,
      callbacks: y
    };
  }, [y, l, n]);
  z(function() {
    return s.current = I.descriptor, i.droppable.register(I), function() {
      r.current && b(), i.droppable.unregister(I);
    };
  }, [y, l, b, I, a, i.droppable]), z(function() {
    r.current && a.updateDroppableIsEnabled(s.current.id, !e.isDropDisabled);
  }, [e.isDropDisabled, a]), z(function() {
    r.current && a.updateDroppableIsCombineEnabled(s.current.id, e.isCombineEnabled);
  }, [e.isCombineEnabled, a]);
}
function et() {
}
var In = {
  width: 0,
  height: 0,
  margin: Bo
}, lc = function(r) {
  var t = r.isAnimatingOpenOnMount, n = r.placeholder, i = r.animate;
  return t || i === "close" ? In : {
    height: n.client.borderBox.height,
    width: n.client.borderBox.width,
    margin: n.client.margin
  };
}, sc = function(r) {
  var t = r.isAnimatingOpenOnMount, n = r.placeholder, i = r.animate, a = lc({
    isAnimatingOpenOnMount: t,
    placeholder: n,
    animate: i
  });
  return {
    display: n.display,
    boxSizing: "border-box",
    width: a.width,
    height: a.height,
    marginTop: a.margin.top,
    marginRight: a.margin.right,
    marginBottom: a.margin.bottom,
    marginLeft: a.margin.left,
    flexShrink: "0",
    flexGrow: "0",
    pointerEvents: "none",
    transition: i !== "none" ? We.placeholder : null
  };
};
function uc(e) {
  var r = O(null), t = x(function() {
    r.current && (clearTimeout(r.current), r.current = null);
  }, []), n = e.animate, i = e.onTransitionEnd, a = e.onClose, o = e.contextId, l = Ae(e.animate === "open"), s = l[0], d = l[1];
  ie(function() {
    return s ? n !== "open" ? (t(), d(!1), et) : r.current ? et : (r.current = setTimeout(function() {
      r.current = null, d(!1);
    }), t) : et;
  }, [n, s, t]);
  var p = x(function(u) {
    u.propertyName === "height" && (i(), n === "close" && a());
  }, [n, a, i]), c = sc({
    isAnimatingOpenOnMount: s,
    animate: e.animate,
    placeholder: e.placeholder
  });
  return N.createElement(e.placeholder.tagName, {
    style: c,
    "data-rbd-placeholder-context-id": o,
    onTransitionEnd: p,
    ref: e.innerRef
  });
}
var cc = N.memo(uc), kt = N.createContext(null), dc = function(e) {
  Mn(r, e);
  function r() {
    for (var n, i = arguments.length, a = new Array(i), o = 0; o < i; o++)
      a[o] = arguments[o];
    return n = e.call.apply(e, [this].concat(a)) || this, n.state = {
      isVisible: !!n.props.on,
      data: n.props.on,
      animate: n.props.shouldAnimate && n.props.on ? "open" : "none"
    }, n.onClose = function() {
      n.state.animate === "close" && n.setState({
        isVisible: !1
      });
    }, n;
  }
  r.getDerivedStateFromProps = function(i, a) {
    return i.shouldAnimate ? i.on ? {
      isVisible: !0,
      data: i.on,
      animate: "open"
    } : a.isVisible ? {
      isVisible: !0,
      data: a.data,
      animate: "close"
    } : {
      isVisible: !1,
      animate: "close",
      data: null
    } : {
      isVisible: !!i.on,
      data: i.on,
      animate: "none"
    };
  };
  var t = r.prototype;
  return t.render = function() {
    if (!this.state.isVisible)
      return null;
    var i = {
      onClose: this.onClose,
      data: this.state.data,
      animate: this.state.animate
    };
    return this.props.children(i);
  }, r;
}(N.PureComponent), xn = {
  dragging: 5e3,
  dropAnimating: 4500
}, pc = function(r, t) {
  return t ? We.drop(t.duration) : r ? We.snap : We.fluid;
}, vc = function(r, t) {
  return r ? t ? ze.opacity.drop : ze.opacity.combining : null;
}, fc = function(r) {
  return r.forceShouldAnimate != null ? r.forceShouldAnimate : r.mode === "SNAP";
};
function gc(e) {
  var r = e.dimension, t = r.client, n = e.offset, i = e.combineWith, a = e.dropping, o = !!i, l = fc(e), s = !!a, d = s ? ot.drop(n, o) : ot.moveTo(n), p = {
    position: "fixed",
    top: t.marginBox.top,
    left: t.marginBox.left,
    boxSizing: "border-box",
    width: t.borderBox.width,
    height: t.borderBox.height,
    transition: pc(l, a),
    transform: d,
    opacity: vc(o, s),
    zIndex: s ? xn.dropAnimating : xn.dragging,
    pointerEvents: "none"
  };
  return p;
}
function mc(e) {
  return {
    transform: ot.moveTo(e.offset),
    transition: e.shouldAnimateDisplacement ? null : "none"
  };
}
function bc(e) {
  return e.type === "DRAGGING" ? gc(e) : mc(e);
}
function hc(e, r, t) {
  t === void 0 && (t = W);
  var n = window.getComputedStyle(r), i = r.getBoundingClientRect(), a = Yn(i, n), o = Ir(a, t), l = {
    client: a,
    tagName: r.tagName.toLowerCase(),
    display: n.display
  }, s = {
    x: a.marginBox.width,
    y: a.marginBox.height
  }, d = {
    descriptor: e,
    placeholder: l,
    displaceBy: s,
    client: a,
    page: o
  };
  return d;
}
function yc(e) {
  var r = Ft("draggable"), t = e.descriptor, n = e.registry, i = e.getDraggableRef, a = e.canDragInteractiveElements, o = e.shouldRespectForcePress, l = e.isEnabled, s = A(function() {
    return {
      canDragInteractiveElements: a,
      shouldRespectForcePress: o,
      isEnabled: l
    };
  }, [a, l, o]), d = x(function(v) {
    var f = i();
    return f || h(!1), hc(t, f, v);
  }, [t, i]), p = A(function() {
    return {
      uniqueId: r,
      descriptor: t,
      options: s,
      getDimension: d
    };
  }, [t, d, s, r]), c = O(p), u = O(!0);
  z(function() {
    return n.draggable.register(c.current), function() {
      return n.draggable.unregister(c.current);
    };
  }, [n.draggable]), z(function() {
    if (u.current) {
      u.current = !1;
      return;
    }
    var v = c.current;
    c.current = p, n.draggable.update(p, v);
  }, [p, n.draggable]);
}
function Dc(e) {
  e.preventDefault();
}
function Ic(e) {
  var r = O(null), t = x(function(P) {
    r.current = P;
  }, []), n = x(function() {
    return r.current;
  }, []), i = Pr(Fr), a = i.contextId, o = i.dragHandleUsageInstructionsId, l = i.registry, s = Pr(kt), d = s.type, p = s.droppableId, c = A(function() {
    return {
      id: e.draggableId,
      index: e.index,
      type: d,
      droppableId: p
    };
  }, [e.draggableId, e.index, d, p]), u = e.children, v = e.draggableId, f = e.isEnabled, g = e.shouldRespectForcePress, b = e.canDragInteractiveElements, m = e.isClone, y = e.mapped, I = e.dropAnimationFinished;
  if (!m) {
    var D = A(function() {
      return {
        descriptor: c,
        registry: l,
        getDraggableRef: n,
        canDragInteractiveElements: b,
        shouldRespectForcePress: g,
        isEnabled: f
      };
    }, [c, l, n, b, g, f]);
    yc(D);
  }
  var w = A(function() {
    return f ? {
      tabIndex: 0,
      role: "button",
      "aria-describedby": o,
      "data-rbd-drag-handle-draggable-id": v,
      "data-rbd-drag-handle-context-id": a,
      draggable: !1,
      onDragStart: Dc
    } : null;
  }, [a, o, v, f]), C = x(function(P) {
    y.type === "DRAGGING" && y.dropping && P.propertyName === "transform" && I();
  }, [I, y]), B = A(function() {
    var P = bc(y), L = y.type === "DRAGGING" && y.dropping ? C : null, R = {
      innerRef: t,
      draggableProps: {
        "data-rbd-draggable-context-id": a,
        "data-rbd-draggable-id": v,
        style: P,
        onTransitionEnd: L
      },
      dragHandleProps: w
    };
    return R;
  }, [a, w, v, y, C, t]), M = A(function() {
    return {
      draggableId: c.id,
      type: c.type,
      source: {
        index: c.index,
        droppableId: c.droppableId
      }
    };
  }, [c.droppableId, c.id, c.index, c.type]);
  return u(B, y.snapshot, M);
}
var Ha = function(e, r) {
  return e === r;
}, $a = function(e) {
  var r = e.combine, t = e.destination;
  return t ? t.droppableId : r ? r.droppableId : null;
}, xc = function(r) {
  return r.combine ? r.combine.draggableId : null;
}, Sc = function(r) {
  return r.at && r.at.type === "COMBINE" ? r.at.combine.draggableId : null;
};
function Cc() {
  var e = k(function(i, a) {
    return {
      x: i,
      y: a
    };
  }), r = k(function(i, a, o, l, s) {
    return {
      isDragging: !0,
      isClone: a,
      isDropAnimating: !!s,
      dropAnimation: s,
      mode: i,
      draggingOver: o,
      combineWith: l,
      combineTargetFor: null
    };
  }), t = k(function(i, a, o, l, s, d, p) {
    return {
      mapped: {
        type: "DRAGGING",
        dropping: null,
        draggingOver: s,
        combineWith: d,
        mode: a,
        offset: i,
        dimension: o,
        forceShouldAnimate: p,
        snapshot: r(a, l, s, d, null)
      }
    };
  }), n = function(a, o) {
    if (a.isDragging) {
      if (a.critical.draggable.id !== o.draggableId)
        return null;
      var l = a.current.client.offset, s = a.dimensions.draggables[o.draggableId], d = j(a.impact), p = Sc(a.impact), c = a.forceShouldAnimate;
      return t(e(l.x, l.y), a.movementMode, s, o.isClone, d, p, c);
    }
    if (a.phase === "DROP_ANIMATING") {
      var u = a.completed;
      if (u.result.draggableId !== o.draggableId)
        return null;
      var v = o.isClone, f = a.dimensions.draggables[o.draggableId], g = u.result, b = g.mode, m = $a(g), y = xc(g), I = a.dropDuration, D = {
        duration: I,
        curve: Bt.drop,
        moveTo: a.newHomeClientOffset,
        opacity: y ? ze.opacity.drop : null,
        scale: y ? ze.scale.drop : null
      };
      return {
        mapped: {
          type: "DRAGGING",
          offset: a.newHomeClientOffset,
          dimension: f,
          dropping: D,
          draggingOver: m,
          combineWith: y,
          mode: b,
          forceShouldAnimate: null,
          snapshot: r(b, v, m, y, D)
        }
      };
    }
    return null;
  };
  return n;
}
function qa(e) {
  return {
    isDragging: !1,
    isDropAnimating: !1,
    isClone: !1,
    dropAnimation: null,
    mode: null,
    draggingOver: null,
    combineTargetFor: e,
    combineWith: null
  };
}
var wc = {
  mapped: {
    type: "SECONDARY",
    offset: W,
    combineTargetFor: null,
    shouldAnimateDisplacement: !0,
    snapshot: qa(null)
  }
};
function Ec() {
  var e = k(function(o, l) {
    return {
      x: o,
      y: l
    };
  }), r = k(qa), t = k(function(o, l, s) {
    return l === void 0 && (l = null), {
      mapped: {
        type: "SECONDARY",
        offset: o,
        combineTargetFor: l,
        shouldAnimateDisplacement: s,
        snapshot: r(l)
      }
    };
  }), n = function(l) {
    return l ? t(W, l, !0) : null;
  }, i = function(l, s, d, p) {
    var c = d.displaced.visible[l], u = !!(p.inVirtualList && p.effected[l]), v = Tr(d), f = v && v.draggableId === l ? s : null;
    if (!c) {
      if (!u)
        return n(f);
      if (d.displaced.invisible[l])
        return null;
      var g = Be(p.displacedBy.point), b = e(g.x, g.y);
      return t(b, f, !0);
    }
    if (u)
      return n(f);
    var m = d.displacedBy.point, y = e(m.x, m.y);
    return t(y, f, c.shouldAnimate);
  }, a = function(l, s) {
    if (l.isDragging)
      return l.critical.draggable.id === s.draggableId ? null : i(s.draggableId, l.critical.draggable.id, l.impact, l.afterCritical);
    if (l.phase === "DROP_ANIMATING") {
      var d = l.completed;
      return d.result.draggableId === s.draggableId ? null : i(s.draggableId, d.result.draggableId, d.impact, d.afterCritical);
    }
    return null;
  };
  return a;
}
var Pc = function() {
  var r = Cc(), t = Ec(), n = function(a, o) {
    return r(a, o) || t(a, o) || wc;
  };
  return n;
}, Ac = {
  dropAnimationFinished: xa
}, Bc = zn(Pc, Ac, null, {
  context: Lt,
  pure: !0,
  areStatePropsEqual: Ha
})(Ic);
function Va(e) {
  var r = Pr(kt), t = r.isUsingCloneFor;
  return t === e.draggableId && !e.isClone ? null : N.createElement(Bc, e);
}
function Pe(e) {
  var r = typeof e.isDragDisabled == "boolean" ? !e.isDragDisabled : !0, t = !!e.disableInteractiveElementBlocking, n = !!e.shouldRespectForcePress;
  return N.createElement(Va, S({}, e, {
    isClone: !1,
    isEnabled: r,
    canDragInteractiveElements: t,
    shouldRespectForcePress: n
  }));
}
function Rc(e) {
  var r = Je(Fr);
  r || h(!1);
  var t = r.contextId, n = r.isMovementAllowed, i = O(null), a = O(null), o = e.children, l = e.droppableId, s = e.type, d = e.mode, p = e.direction, c = e.ignoreContainerClipping, u = e.isDropDisabled, v = e.isCombineEnabled, f = e.snapshot, g = e.useClone, b = e.updateViewportMaxScroll, m = e.getContainerForClone, y = x(function() {
    return i.current;
  }, []), I = x(function(R) {
    i.current = R;
  }, []);
  x(function() {
    return a.current;
  }, []);
  var D = x(function(R) {
    a.current = R;
  }, []), w = x(function() {
    n() && b({
      maxScroll: Pa()
    });
  }, [n, b]);
  oc({
    droppableId: l,
    type: s,
    mode: d,
    direction: p,
    isDropDisabled: u,
    isCombineEnabled: v,
    ignoreContainerClipping: c,
    getDroppableRef: y
  });
  var C = N.createElement(dc, {
    on: e.placeholder,
    shouldAnimate: e.shouldAnimatePlaceholder
  }, function(R) {
    var K = R.onClose, Y = R.data, G = R.animate;
    return N.createElement(cc, {
      placeholder: Y,
      onClose: K,
      innerRef: D,
      animate: G,
      contextId: t,
      onTransitionEnd: w
    });
  }), B = A(function() {
    return {
      innerRef: I,
      placeholder: C,
      droppableProps: {
        "data-rbd-droppable-id": l,
        "data-rbd-droppable-context-id": t
      }
    };
  }, [t, l, C, I]), M = g ? g.dragging.draggableId : null, P = A(function() {
    return {
      droppableId: l,
      type: s,
      isUsingCloneFor: M
    };
  }, [l, M, s]);
  function L() {
    if (!g)
      return null;
    var R = g.dragging, K = g.render, Y = N.createElement(Va, {
      draggableId: R.draggableId,
      index: R.source.index,
      isClone: !0,
      isEnabled: !0,
      shouldRespectForcePress: !1,
      canDragInteractiveElements: !0
    }, function(G, J) {
      return K(G, J, R);
    });
    return bi.createPortal(Y, m());
  }
  return N.createElement(kt.Provider, {
    value: P
  }, o(B, f), L());
}
var rt = function(r, t) {
  return r === t.droppable.type;
}, Sn = function(r, t) {
  return t.draggables[r.draggable.id];
}, Oc = function() {
  var r = {
    placeholder: null,
    shouldAnimatePlaceholder: !0,
    snapshot: {
      isDraggingOver: !1,
      draggingOverWith: null,
      draggingFromThisWith: null,
      isUsingPlaceholder: !1
    },
    useClone: null
  }, t = S({}, r, {
    shouldAnimatePlaceholder: !1
  }), n = k(function(o) {
    return {
      draggableId: o.id,
      type: o.type,
      source: {
        index: o.index,
        droppableId: o.droppableId
      }
    };
  }), i = k(function(o, l, s, d, p, c) {
    var u = p.descriptor.id, v = p.descriptor.droppableId === o;
    if (v) {
      var f = c ? {
        render: c,
        dragging: n(p.descriptor)
      } : null, g = {
        isDraggingOver: s,
        draggingOverWith: s ? u : null,
        draggingFromThisWith: u,
        isUsingPlaceholder: !0
      };
      return {
        placeholder: p.placeholder,
        shouldAnimatePlaceholder: !1,
        snapshot: g,
        useClone: f
      };
    }
    if (!l)
      return t;
    if (!d)
      return r;
    var b = {
      isDraggingOver: s,
      draggingOverWith: u,
      draggingFromThisWith: null,
      isUsingPlaceholder: !0
    };
    return {
      placeholder: p.placeholder,
      shouldAnimatePlaceholder: !0,
      snapshot: b,
      useClone: null
    };
  }), a = function(l, s) {
    var d = s.droppableId, p = s.type, c = !s.isDropDisabled, u = s.renderClone;
    if (l.isDragging) {
      var v = l.critical;
      if (!rt(p, v))
        return t;
      var f = Sn(v, l.dimensions), g = j(l.impact) === d;
      return i(d, c, g, g, f, u);
    }
    if (l.phase === "DROP_ANIMATING") {
      var b = l.completed;
      if (!rt(p, b.critical))
        return t;
      var m = Sn(b.critical, l.dimensions);
      return i(d, c, $a(b.result) === d, j(b.impact) === d, m, u);
    }
    if (l.phase === "IDLE" && l.completed && !l.shouldFlush) {
      var y = l.completed;
      if (!rt(p, y.critical))
        return t;
      var I = j(y.impact) === d, D = !!(y.impact.at && y.impact.at.type === "COMBINE"), w = y.critical.droppable.id === d;
      return I ? D ? r : t : w ? r : t;
    }
    return t;
  };
  return a;
}, Tc = {
  updateViewportMaxScroll: Hl
};
function Nc() {
  return document.body || h(!1), document.body;
}
var Mc = {
  mode: "standard",
  type: "DEFAULT",
  direction: "vertical",
  isDropDisabled: !1,
  isCombineEnabled: !1,
  ignoreContainerClipping: !1,
  renderClone: null,
  getContainerForClone: Nc
}, ja = zn(Oc, Tc, null, {
  context: Lt,
  pure: !0,
  areStatePropsEqual: Ha
})(Rc);
ja.defaultProps = Mc;
const za = ({ fallback: e }) => /* @__PURE__ */ E("div", { dangerouslySetInnerHTML: { __html: e || "" } }), Lc = za, Fc = ni, Gc = "fallback", kc = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  Edit: za,
  Preview: Lc,
  Settings: Fc,
  id: Gc
}, Symbol.toStringTag, { value: "Module" })), Wc = (e, r = !1) => {
  const t = Je(Nn);
  if (e === null)
    return null;
  if (e && e in t.resolved)
    return t.resolved[e];
  try {
    ai(e, t.promises).then(
      (n) => {
        t.resolved[e] = n, t.updatePlugins(t);
      }
    );
  } catch (n) {
    if (r)
      return kc;
    throw n;
  }
  return null;
};
var Ka = { exports: {} };
/*!
	Copyright (c) 2018 Jed Watson.
	Licensed under the MIT License (MIT), see
	http://jedwatson.github.io/classnames
*/
(function(e) {
  (function() {
    var r = {}.hasOwnProperty;
    function t() {
      for (var n = [], i = 0; i < arguments.length; i++) {
        var a = arguments[i];
        if (a) {
          var o = typeof a;
          if (o === "string" || o === "number")
            n.push(a);
          else if (Array.isArray(a)) {
            if (a.length) {
              var l = t.apply(null, a);
              l && n.push(l);
            }
          } else if (o === "object") {
            if (a.toString !== Object.prototype.toString && !a.toString.toString().includes("[native code]")) {
              n.push(a.toString());
              continue;
            }
            for (var s in a)
              r.call(a, s) && a[s] && n.push(s);
          }
        }
      }
      return n.join(" ");
    }
    e.exports ? (t.default = t, e.exports = t) : window.classNames = t;
  })();
})(Ka);
var Uc = Ka.exports;
const Ke = /* @__PURE__ */ yi(Uc);
const Hc = ({
  uuid: e,
  configuration: r,
  regionId: t,
  weight: n,
  additional: i,
  index: a,
  fallback: o
}) => {
  const l = Wc(r.id, !0), s = Ar(), d = F(dt), p = F((g) => ii(g, t)), c = F(oi);
  if (!l)
    return /* @__PURE__ */ E("div", { children: "...Loading" });
  const u = (g) => {
    if (g.preventDefault(), d === e) {
      s(Ue({ uuid: e, mode: qt }));
      return;
    }
    s(Ue({ uuid: e }));
  }, v = (g) => {
    g.key === "Enter" && u(g);
  }, f = r.label || `Block ${a} in ${p.region}`;
  return d === e && c === qt ? /* @__PURE__ */ E(Pe, { draggableId: `${se}:${e}`, index: a, children: (g) => /* @__PURE__ */ E(
    "div",
    {
      className: Ke({
        "dlb__component--active": d === e
      }),
      "data-block": !0,
      ...g.draggableProps,
      ...g.dragHandleProps,
      "aria-label": f,
      ref: g.innerRef,
      "data-uuid": r.label,
      children: /* @__PURE__ */ E(
        l.Edit,
        {
          uuid: e,
          fallback: o,
          configuration: r,
          regionId: t,
          weight: n,
          additional: i
        }
      )
    }
  ) }) : /* @__PURE__ */ E(Pe, { draggableId: `${se}:${e}`, index: a, children: (g) => /* @__PURE__ */ Z(
    "div",
    {
      className: Ke("block", "block--previewing", {
        "dlb__component--active": d === e
      }),
      ...g.draggableProps,
      ...g.dragHandleProps,
      "aria-label": f,
      "aria-roledescription": "Draggable block",
      "aria-describedby": `${e}-description`,
      ref: g.innerRef,
      "data-uuid": r.label,
      onKeyDown: v,
      onClick: u,
      children: [
        /* @__PURE__ */ E(
          l.Preview,
          {
            uuid: e,
            fallback: o,
            configuration: r,
            regionId: t,
            weight: n,
            additional: i
          }
        ),
        /* @__PURE__ */ E("span", { hidden: !0, id: `${e}-description`, children: "Press space bar to start a drag. Press ENTER to enter edit mode. When dragging you can use the arrow keys to move the item around and escape to cancel. Ensure your screen reader is in focus mode or forms mode" })
      ]
    }
  ) });
}, Ye = ({ children: e, ...r }) => {
  const [t, n] = Ae(!1);
  return ie(() => {
    const i = requestAnimationFrame(() => n(!0));
    return () => {
      cancelAnimationFrame(i), n(!1);
    };
  }, []), t ? /* @__PURE__ */ E(ja, { ...r, children: e }) : null;
};
const $c = ({
  label: e,
  id: r,
  index: t,
  icon: n,
  icon_is_url: i
}) => /* @__PURE__ */ E(Pe, { draggableId: `${ue}:${r}`, index: t, children: (a) => /* @__PURE__ */ Z(
  "div",
  {
    className: Ke("dlb__insert-block", "dlb__insert-item", {
      "dlb__insert-block--no-icon": !n
    }),
    ref: a.innerRef,
    ...a.draggableProps,
    ...a.dragHandleProps,
    children: [
      i && n && /* @__PURE__ */ E("img", { src: n, alt: `Icon for ${e}` }),
      !i && n && /* @__PURE__ */ E("span", { dangerouslySetInnerHTML: { __html: n } }),
      e
    ]
  }
) }), Ya = "block-picker", ed = () => {
  const { error: e, isLoading: r } = pt(), t = F((n) => li(n));
  if (r)
    return /* @__PURE__ */ E("div", { children: "...loading" });
  if (e)
    return /* @__PURE__ */ E("div", { children: Pn(e, "Could not fetch block list") });
  if (t) {
    let n = 0;
    return /* @__PURE__ */ E(
      Ye,
      {
        droppableId: Ya,
        isDropDisabled: !0,
        children: (i) => /* @__PURE__ */ Z("div", { ...i.droppableProps, ref: i.innerRef, children: [
          Object.entries(
            t.reduce(
              (a, o) => ({
                ...a,
                [o.category]: [
                  ...a[o.category] || [],
                  o
                ]
              }),
              {}
            )
          ).map(([a, o], l) => /* @__PURE__ */ Z("div", { children: [
            /* @__PURE__ */ E("h3", { children: a }),
            o.map((s) => /* @__PURE__ */ E($c, { index: n++, ...s }, s.id))
          ] }, l)),
          /* @__PURE__ */ E("div", { hidden: !0, children: i.placeholder })
        ] })
      }
    );
  }
}, qc = ({
  id: e,
  index: r,
  icon: t,
  icon_is_url: n,
  label: i
}) => /* @__PURE__ */ E(Pe, { draggableId: `${Ge}:${e}`, index: r, children: (a) => /* @__PURE__ */ Z(
  "div",
  {
    className: "dlb__insert-section dlb__insert-item",
    ref: a.innerRef,
    ...a.draggableProps,
    ...a.dragHandleProps,
    children: [
      n && t && /* @__PURE__ */ E("img", { src: t, alt: `Icon for ${i}` }),
      !n && t && /* @__PURE__ */ E("span", { dangerouslySetInnerHTML: { __html: t } }),
      i
    ]
  }
) }), Ja = "section-picker", rd = () => {
  const { error: e, isLoading: r } = vt(), t = F((n) => si(n));
  if (r)
    return /* @__PURE__ */ E("div", { children: "...loading" });
  if (e)
    return /* @__PURE__ */ E("div", { children: Pn(e, "Could not fetch section list") });
  if (t.length)
    return /* @__PURE__ */ E(
      Ye,
      {
        droppableId: Ja,
        isDropDisabled: !0,
        children: (n) => /* @__PURE__ */ Z("div", { ...n.droppableProps, ref: n.innerRef, children: [
          /* @__PURE__ */ E("h3", { children: "Layouts" }),
          t.map((i, a) => /* @__PURE__ */ E(qc, { index: a, ...i }, i.id)),
          n.placeholder
        ] })
      }
    );
};
const ct = "outline", td = () => {
  const e = F(An), r = F(Bn), t = F(Rn), n = Ar(), i = F(ft);
  vt();
  const a = F(
    (s) => On(s)
  ), o = F(dt), l = F(ui);
  return /* @__PURE__ */ E(
    Ye,
    {
      droppableId: ct,
      isDropDisabled: i === null || i.type !== hr,
      children: (s) => /* @__PURE__ */ Z(
        "div",
        {
          className: "dlb__outline",
          ...s.droppableProps,
          ref: s.innerRef,
          children: [
            Object.entries(r).map(
              ([d, p], c) => /* @__PURE__ */ E(
                Pe,
                {
                  draggableId: `${hr}:${p.id}`,
                  index: c,
                  children: (u) => /* @__PURE__ */ Z(
                    "div",
                    {
                      className: Ke("dlb__outline-section", {
                        "dlb__outline-section--selected": p.id === l
                      }),
                      ref: u.innerRef,
                      ...u.draggableProps,
                      children: [
                        /* @__PURE__ */ E(
                          "div",
                          {
                            className: "dlb__outline-drag",
                            ...u.dragHandleProps,
                            children: "↕️"
                          }
                        ),
                        /* @__PURE__ */ E("button", { children: p.layout_settings.label || tt("Section @section", {
                          "@section": `${p.weight + 1}`
                        }) }),
                        Object.entries(t).filter(([, v]) => v.section === d).map(([v, f]) => /* @__PURE__ */ E(
                          Ye,
                          {
                            droppableId: `${v}:outline`,
                            isDropDisabled: i === null || i.type !== He,
                            children: (g) => {
                              var b;
                              return /* @__PURE__ */ Z(
                                "div",
                                {
                                  className: "dlb__outline-region",
                                  ...g.droppableProps,
                                  ref: g.innerRef,
                                  children: [
                                    /* @__PURE__ */ E("button", { children: ((b = a[p.layout_id]) == null ? void 0 : b.region_labels[f.region]) || f.region }),
                                    Object.entries(e).filter(
                                      ([, m]) => m.regionId === v
                                    ).map(
                                      ([m, y], I) => /* @__PURE__ */ E(
                                        Pe,
                                        {
                                          draggableId: `${He}:${m}`,
                                          index: I,
                                          children: (D) => /* @__PURE__ */ Z(
                                            "div",
                                            {
                                              className: Ke(
                                                "dlb__outline-component",
                                                {
                                                  "dlb__outline-component--selected": m === o
                                                }
                                              ),
                                              ref: D.innerRef,
                                              ...D.draggableProps,
                                              children: [
                                                /* @__PURE__ */ E(
                                                  "div",
                                                  {
                                                    className: "dlb__outline-drag",
                                                    ...D.dragHandleProps,
                                                    children: "↕️"
                                                  }
                                                ),
                                                /* @__PURE__ */ E(
                                                  "button",
                                                  {
                                                    onClick: () => n(
                                                      Ue({
                                                        uuid: m
                                                      })
                                                    ),
                                                    children: y.configuration.label || tt("Component @componentId", {
                                                      "@componentId": m
                                                    })
                                                  }
                                                )
                                              ]
                                            }
                                          )
                                        },
                                        m
                                      )
                                    ),
                                    g.placeholder
                                  ]
                                },
                                v
                              );
                            }
                          },
                          v
                        ))
                      ]
                    },
                    d
                  )
                },
                d
              )
            ),
            s.placeholder
          ]
        }
      )
    }
  );
}, Vc = (e, r, t, n, i) => {
  const { destination: a, source: o } = e;
  if (t(be(null)), !a)
    return;
  const l = () => {
    r && t(Ue({ uuid: r }));
  };
  if (o.droppableId === Ya && n !== null && n.type === ue) {
    if (a.droppableId === mr) {
      t(
        ci({
          at: a.index,
          blockInfo: n.info,
          sectionInfo: i[n.info.default_layout]
        })
      ), t(be(null));
      return;
    }
    t(di({ at: a, blockInfo: n.info })), t(be(null));
    return;
  }
  if (o.droppableId === Ja && n !== null && n.type === Ge && a.droppableId === mr) {
    t(
      pi({
        at: a.index,
        sectionInfo: n.info
      })
    ), t(be(null));
    return;
  }
  if (o.droppableId === mr && a.droppableId === mr || o.droppableId === ct && a.droppableId === ct) {
    t(
      vi({ from: o.index, to: a.index })
    ), t(be(null)), l();
    return;
  }
  if (n !== null && (n.type === se || n.type === He)) {
    const s = n.info.uuid, [d] = o.droppableId.split(":"), [p] = a.droppableId.split(":");
    t(
      fi({
        from: { ...o, droppableId: d },
        to: { ...a, droppableId: p },
        componentUuid: s
      })
    ), t(be(null)), l();
    return;
  }
}, jc = (e, r, t, n) => {
  const i = e.split(":"), a = i.shift();
  if (!a || ![
    Vt,
    se,
    ue,
    Ge,
    He,
    hr
  ].includes(a))
    return null;
  const o = i.join(":");
  if (a === Vt || a === hr)
    return {
      type: a,
      info: n.sections[o]
    };
  if (a === se || a === He)
    return {
      type: a,
      info: n.components[o]
    };
  let l;
  return a === ue && (l = r[o] ?? null) ? { type: ue, info: l } : a === Ge && (l = t[o] ?? null) ? { type: Ge, info: l } : null;
}, zc = (e, r, t, n, i) => {
  e(Ue({ uuid: null })), e(
    be(
      jc(r.draggableId, t, n, i)
    )
  );
}, Xa = wn({
  isOverRegion: !1,
  setIsOverRegion: () => {
  }
}), mr = "layout", nd = ({ children: e }) => {
  const r = Ar(), t = F(dt), n = F(ft), [i, a] = Ae(!1);
  pt(), vt();
  const o = F((p) => Tn(p)), l = F(
    (p) => On(p)
  ), s = F(gi), d = {
    isOverRegion: i,
    setIsOverRegion: a
  };
  return /* @__PURE__ */ E(Xa.Provider, { value: d, children: /* @__PURE__ */ E(
    Ku,
    {
      onDragEnd: (p) => {
        Vc(
          p,
          t,
          r,
          n,
          l
        );
      },
      onBeforeCapture: (p) => {
        zc(
          r,
          p,
          o,
          l,
          s.layout
        );
      },
      onDragUpdate: () => {
      },
      children: e
    }
  ) });
}, Kc = () => Je(Xa), Cn = (e, r, t, n) => {
  var a;
  return (((a = n[e]) == null ? void 0 : a.section_restrictions) || []).filter(
    (o) => o.layout_id === t && o.regions.includes(r)
  ).length > 0;
}, ad = ({
  regionId: e,
  as: r = "div",
  ...t
}) => {
  const n = F(An), i = Object.entries(n).map(([, u]) => u).filter((u) => u.regionId === e), a = F(Rn), o = F(Bn), l = e in a ? o[a[e].section].layout_id : null;
  pt();
  const s = F(Tn), d = F(ft), p = !l || d === null || ![se, ue].includes(d.type) || d.type === ue && !Cn(
    d.info.id,
    a[e].region,
    l,
    s
  ) || d.type === se && !Cn(
    d.info.configuration.id,
    a[e].region,
    l,
    s
  ), c = Kc();
  return /* @__PURE__ */ E(
    Ye,
    {
      droppableId: `${e}`,
      ignoreContainerClipping: !0,
      isDropDisabled: p,
      children: (u, v) => /* @__PURE__ */ Z(
        r,
        {
          "data-region": !0,
          "data-drop": d && [se, ue].includes(d.type) ? p ? "not-allowed" : v != null && v.isDraggingOver ? "allowed-over" : "allowed" : null,
          onMouseOver: (f) => {
            f.preventDefault(), c.setIsOverRegion(!0);
          },
          onMouseOut: (f) => {
            f.preventDefault(), c.setIsOverRegion(!1);
          },
          ...u.droppableProps,
          ...t,
          ref: u.innerRef,
          children: [
            i.map((f, g) => /* @__PURE__ */ E(
              Hc,
              {
                ...f,
                index: g,
                regionId: e
              },
              f.uuid
            )),
            u.placeholder
          ]
        }
      )
    }
  );
}, id = (e) => {
  const { id: r, layout_settings: t } = e, n = `label-${r}`, i = Ar(), a = (o) => {
    i(
      mi({
        sectionId: r,
        layoutSettings: { label: o.currentTarget.value }
      })
    );
  };
  return /* @__PURE__ */ E(Za, { children: /* @__PURE__ */ Z("div", { className: "form-item", children: [
    /* @__PURE__ */ E("label", { htmlFor: n, children: tt("Label") }),
    /* @__PURE__ */ E(
      "input",
      {
        onChange: a,
        type: "text",
        value: t.label || "",
        id: n
      }
    )
  ] }) });
};
export {
  ed as B,
  id as D,
  td as O,
  Pe as P,
  ad as R,
  rd as S,
  Ye as a,
  mr as b,
  Ke as c,
  Wc as d,
  _c as e,
  nd as f,
  Hc as g,
  Kc as u
};
