import { jsx as r } from "react/jsx-runtime";
import { F as o } from "./Field-d8905e3d.js";
import "react";
const m = ({ itemValues: t, fieldName: e }) => /* @__PURE__ */ r(
  o,
  {
    multiple: !0,
    items: t.map((i) => ({
      content: i.value,
      attributes: {
        contentEditable: !0,
        tabIndex: 0,
        role: "textbox",
        "aria-label": `Field: ${e}`
      }
    }))
  }
), d = "string_textfield";
export {
  m as Widget,
  d as id
};
