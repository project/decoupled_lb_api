import { jsx as a, jsxs as p, Fragment as g } from "react/jsx-runtime";
import { g as ee, R as te, s as se, u as r, a as z, b as h, c as K, d as Q, n as ne, e as A, D as N, f as ae, h as V, i as x, j as k, S as oe, l as ie, k as y, m as Y, o as O, p as I, q as w, r as re, t as le, v as D, w as v, x as E, y as ce, M as L, z as de, A as F, B as $, C as j, E as G, F as T, G as ue, H as R, I as q, J as M, K as P, L as B, N as be, O as pe } from "./ErrorHelpers-a6fb9f99.js";
import { P as lt, a0 as ct, a1 as dt, _ as ut, Q as bt, a6 as pt, T as St, U as gt, V as ft, X as ht, W as mt, Z as _t, a5 as yt, Y as vt, $ as Et, aj as Tt, ak as Ct, al as kt, ai as Lt, ag as Pt, af as Bt, ab as Nt, a3 as Dt, ah as At, a9 as Ot, aa as Rt, ac as xt, a7 as It, a8 as wt, a4 as Ft, a2 as $t, ae as jt, ad as Gt } from "./ErrorHelpers-a6fb9f99.js";
import { R as Se, D as ge, P as fe, u as he, c as f, a as me, b as _e, B as ye, S as ve, O as Ee, d as Te, e as Ce, f as ke } from "./DefaultLayoutSettings-b131a39e.js";
import { g as Wt } from "./DefaultLayoutSettings-b131a39e.js";
import * as m from "react";
import { createContext as Le, useState as Pe, useContext as Be, useEffect as W } from "react";
import { F as Ne, W as De, B as Ae } from "./BlockTitle-d2266a4e.js";
import { D as zt, a as Kt, b as Qt, c as Vt, u as Yt } from "./BlockTitle-d2266a4e.js";
import { F as Ht } from "./Field-d8905e3d.js";
import { E as Xt } from "./Editor-4eaaaeec.js";
import "react-dom";
import "./_commonjsHelpers-10dfc225.js";
function Oe() {
  const e = ee();
  let t = null, s = null;
  return {
    clear() {
      t = null, s = null;
    },
    notify() {
      e(() => {
        let o = t;
        for (; o; )
          o.callback(), o = o.next;
      });
    },
    get() {
      let o = [], n = t;
      for (; n; )
        o.push(n), n = n.next;
      return o;
    },
    subscribe(o) {
      let n = !0, i = s = {
        callback: o,
        next: null,
        prev: s
      };
      return i.prev ? i.prev.next = i : t = i, function() {
        !n || t === null || (n = !1, i.next ? i.next.prev = i.prev : s = i.prev, i.prev ? i.prev.next = i.next : t = i.next);
      };
    }
  };
}
const U = {
  notify() {
  },
  get: () => []
};
function Re(e, t) {
  let s, o = U;
  function n(S) {
    return u(), o.subscribe(S);
  }
  function i() {
    o.notify();
  }
  function l() {
    c.onStateChange && c.onStateChange();
  }
  function b() {
    return !!s;
  }
  function u() {
    s || (s = t ? t.addNestedSub(l) : e.subscribe(l), o = Oe());
  }
  function d() {
    s && (s(), s = void 0, o.clear(), o = U);
  }
  const c = {
    addNestedSub: n,
    notifyNestedSubs: i,
    handleChangeWrapper: l,
    isSubscribed: b,
    trySubscribe: u,
    tryUnsubscribe: d,
    getListeners: () => o
  };
  return c;
}
const xe = typeof window < "u" && typeof window.document < "u" && typeof window.document.createElement < "u", Ie = xe ? m.useLayoutEffect : m.useEffect;
function we({
  store: e,
  context: t,
  children: s,
  serverState: o,
  stabilityCheck: n = "once",
  noopCheck: i = "once"
}) {
  const l = m.useMemo(() => {
    const d = Re(e);
    return {
      store: e,
      subscription: d,
      getServerState: o ? () => o : void 0,
      stabilityCheck: n,
      noopCheck: i
    };
  }, [e, o, n, i]), b = m.useMemo(() => e.getState(), [e]);
  Ie(() => {
    const {
      subscription: d
    } = l;
    return d.onStateChange = d.notifyNestedSubs, d.trySubscribe(), b !== e.getState() && d.notifyNestedSubs(), () => {
      d.tryUnsubscribe(), d.onStateChange = void 0;
    };
  }, [l, b]);
  const u = t || te;
  return /* @__PURE__ */ m.createElement(u.Provider, {
    value: l
  }, s);
}
const H = Le({
  resolved: {},
  promises: {},
  updatePlugins: () => {
  }
}), Fe = ({
  children: e,
  plugins: t
}) => {
  const [s, o] = Pe({
    promises: t,
    resolved: {}
  });
  return /* @__PURE__ */ a(H.Provider, { value: { ...s, updatePlugins: o }, children: e });
}, $e = ({
  section: e,
  sectionProps: t,
  regions: s
}) => /* @__PURE__ */ p("div", { ...t, className: `section--${e.id}`, children: [
  "The section with weight ",
  e.weight,
  " is using the layout plugin",
  " ",
  e.layout_id,
  " but there is no React component for that layout. Using fallback instead.",
  Object.keys(s).map((o) => /* @__PURE__ */ a(Se, { regionId: s[o].id }, o))
] }), je = ge, Ge = "fallback", Me = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  Preview: $e,
  Settings: je,
  id: Ge
}, Symbol.toStringTag, { value: "Module" })), J = (e, t = !1) => {
  const s = Be(H);
  if (e === null)
    return null;
  if (e && e in s.resolved)
    return s.resolved[e];
  try {
    se(e, s.promises).then((o) => {
      s.resolved[e] = o, s.updatePlugins(s);
    });
  } catch (o) {
    if (t)
      return Me;
    throw o;
  }
  return null;
};
const We = ({ index: e, ...t }) => {
  const s = r(z), o = h();
  K();
  const n = r(
    (c) => Q(c, t.layout_id)
  ), i = Object.entries(s).map(([, c]) => c).filter((c) => c.section === t.id).reduce((c, S) => ({ ...c, [S.region]: S }), {}), l = Object.keys(
    n ? n.region_labels : {}
  ).reduce((c, S) => {
    const _ = ne();
    return {
      ...c,
      [S]: {
        id: _,
        region: S,
        sectionId: t.id
      }
    };
  }, {}), b = J(t.layout_id, !0), u = r(A);
  if (!b)
    return /* @__PURE__ */ a("div", { children: "...Loading" });
  const d = t.layout_settings.label || `Section ${e + 1}`;
  return /* @__PURE__ */ a(fe, { draggableId: `${N}:${t.id}`, index: e, children: (c) => /* @__PURE__ */ p(g, { children: [
    /* @__PURE__ */ a(
      b.Preview,
      {
        section: t,
        regions: { ...l, ...i },
        "aria-label": t.layout_settings.label,
        sectionProps: {
          "aria-label": d,
          "aria-roledescription": "Draggable section",
          ...c.draggableProps,
          ...c.dragHandleProps,
          "aria-describedby": `${t.id}-description`,
          onClick: () => o(ae(t.id)),
          "data-selected": u === t.id,
          ref: c.innerRef
        }
      }
    ),
    /* @__PURE__ */ a("span", { hidden: !0, id: `${t.id}-description`, children: "Press space bar to start a drag. Press ENTER to enter edit mode. When dragging you can use the arrow keys to move the item around and escape to cancel. Ensure your screen reader is in focus mode or forms mode" })
  ] }) });
}, Ue = ({
  baseUrl: e,
  sectionStorage: t,
  sectionStorageType: s
}) => {
  const o = h(), n = r(V), i = r(x), { startSidebarOpen: l, endSidebarOpen: b } = r(k);
  W(() => {
    n === oe && o(ie({ baseUrl: e, sectionStorage: t, sectionStorageType: s }));
  }, [n, o, e, t, s]), W(() => {
    if (n === y && i === null) {
      const S = setTimeout(() => {
        o(Y({ baseUrl: e, sectionStorage: t, sectionStorageType: s }));
      }, 5e3);
      return () => clearTimeout(S);
    }
  }, [
    n,
    o,
    e,
    t,
    s,
    i
  ]);
  const u = he(), d = r(x), c = r(O);
  return /* @__PURE__ */ a(
    "div",
    {
      className: f("dlb__layout", {
        "dlb__layout--start-sidebar-open": l,
        "dlb__layout--end-sidebar-open": b
      }),
      children: /* @__PURE__ */ a(
        me,
        {
          droppableId: _e,
          isDropDisabled: d === null || u.isOverRegion || ![N, I, w].includes(
            d.type
          ),
          children: (S) => /* @__PURE__ */ p(
            "div",
            {
              ...S.droppableProps,
              ref: S.innerRef,
              "data-section-drop": d !== null && [w, I, N].includes(
                d.type
              ),
              children: [
                Object.keys(c).map((_, Z) => /* @__PURE__ */ a(We, { ...c[_], index: Z }, _)),
                S.placeholder
              ]
            }
          )
        }
      )
    }
  );
};
const ze = ({ title: e }) => {
  const { endSidebarOpen: t, startSidebarOpen: s, mode: o } = r(k), n = r(V), { sectionStorageType: i, sectionStorage: l } = r(re), b = r(le), u = h();
  return /* @__PURE__ */ p("div", { className: "dlb__topbar", children: [
    /* @__PURE__ */ a(
      "button",
      {
        onClick: () => u(D(v)),
        "aria-pressed": s,
        "aria-controls": "dlb-sidebar-start",
        children: "➕"
      }
    ),
    /* @__PURE__ */ a(
      "button",
      {
        onClick: () => u(D(E)),
        "aria-pressed": s,
        "aria-controls": "dlb-sidebar-start",
        children: "📂"
      }
    ),
    /* @__PURE__ */ a(
      "button",
      {
        onClick: () => u(ce(o === L ? de : L)),
        children: o === L ? "🖱️" : "✏️"
      }
    ),
    /* @__PURE__ */ a("h1", { children: e }),
    /* @__PURE__ */ p(
      "div",
      {
        className: f("state", {
          "state--loading": n === F,
          "state--error": n === $,
          "state--dirty": n === y,
          "state--clean": n === j,
          "state--saving": n === G
        }),
        children: [
          n === F && "Loading",
          n === $ && "An error occurred",
          n === y && "Changes to be saved",
          n === j && "All changes saved",
          n === G && "Saving changes"
        ]
      }
    ),
    n === y && /* @__PURE__ */ a(
      "button",
      {
        onClick: () => u(
          Y({
            baseUrl: b,
            sectionStorage: l,
            sectionStorageType: i
          })
        ),
        children: "💾"
      }
    ),
    /* @__PURE__ */ a(
      "button",
      {
        onClick: () => u(T(!0)),
        "aria-pressed": t,
        "aria-controls": "dlb-sidebar-end",
        children: "⚙️"
      }
    )
  ] });
};
const X = ({
  tabs: e,
  children: t,
  update: s
}) => {
  const o = (n) => (i) => {
    i.preventDefault(), s(n);
  };
  return /* @__PURE__ */ p("div", { className: "dlb-tabset", children: [
    /* @__PURE__ */ a("nav", { className: "dlb-tabset__nav", children: /* @__PURE__ */ a("ul", { children: e.map((n) => /* @__PURE__ */ a("li", { children: /* @__PURE__ */ a(
      "button",
      {
        "aria-pressed": n.active,
        title: n.title,
        onClick: o(n.id),
        "data-tab-id": n.id,
        children: n.label
      }
    ) }, n.id)) }) }),
    /* @__PURE__ */ a("div", { className: "dlb-tabset__pane", children: t })
  ] });
}, C = ({ handler: e }) => {
  const t = h();
  return /* @__PURE__ */ a("div", { className: "dlb__sidebar-actions", children: /* @__PURE__ */ a("button", { onClick: (o) => {
    o.preventDefault(), t(e(!1));
  }, children: "✖️" }) });
};
const Ke = () => /* @__PURE__ */ p(g, { children: [
  /* @__PURE__ */ a(ye, {}),
  /* @__PURE__ */ a(ve, {})
] }), Qe = () => {
  const { startSidebarOpen: e, activeStartSidebarTab: t } = r(k), s = h(), o = (n) => {
    s(D(n));
  };
  return /* @__PURE__ */ a(
    "div",
    {
      id: "dlb-sidebar-start",
      className: f("dlb__sidebar", "dlb__sidebar--start", {
        "dlb__sidebar--open": e
      }),
      children: e && /* @__PURE__ */ p(g, { children: [
        /* @__PURE__ */ a(C, { handler: ue }),
        /* @__PURE__ */ p(
          X,
          {
            tabs: [
              {
                label: "Insert",
                title: "Drag new components into the page",
                active: t === v,
                id: v
              },
              {
                label: "Outline",
                title: "View the outline of your built layout",
                active: t === E,
                id: E
              }
            ],
            update: o,
            children: [
              t === E && /* @__PURE__ */ a(Ee, {}),
              t === v && /* @__PURE__ */ a(Ke, {})
            ]
          }
        )
      ] })
    }
  );
};
const Ve = () => {
  const e = r(R), t = r(q), s = Te(
    e ? t[e].configuration.id : null,
    !0
  );
  if (!(!e || !s || !s.Settings))
    return /* @__PURE__ */ a(s.Settings, { ...t[e] });
}, Ye = () => {
  const e = r(A), t = r(O), s = J(
    e ? t[e].layout_id : null,
    !0
  );
  if (!(!e || !s || !s.Settings))
    return /* @__PURE__ */ a(s.Settings, { ...t[e] });
}, qe = () => {
  const { endSidebarOpen: e, activeEndSidebarTab: t } = r(k), s = h(), o = (u) => {
    s(be(u));
  }, n = /* @__PURE__ */ a(Ve, {}), i = /* @__PURE__ */ a(Ye, {}), l = r(R), b = r(A);
  return !l && !b ? /* @__PURE__ */ a(
    "div",
    {
      id: "dlb-sidebar-end",
      className: f("dlb__sidebar", "dlb__sidebar--end", {
        "dlb__sidebar--open": e
      }),
      children: e && /* @__PURE__ */ p(g, { children: [
        /* @__PURE__ */ a(C, { handler: T }),
        /* @__PURE__ */ a("div", { children: M("Select a section or block to configure") })
      ] })
    }
  ) : !i && !n ? /* @__PURE__ */ a(
    "div",
    {
      id: "dlb-sidebar-end",
      className: f("dlb__sidebar", "dlb__sidebar--end", {
        "dlb__sidebar--open": e
      }),
      children: e && /* @__PURE__ */ p(g, { children: [
        /* @__PURE__ */ a(C, { handler: T }),
        /* @__PURE__ */ a("div", { children: M(
          "The selected block and section do not have configuration options"
        ) })
      ] })
    }
  ) : /* @__PURE__ */ a(
    "div",
    {
      id: "dlb-sidebar-end",
      className: f("dlb__sidebar", "dlb__sidebar--end", {
        "dlb__sidebar--open": e
      }),
      children: e && /* @__PURE__ */ p(g, { children: [
        /* @__PURE__ */ a(C, { handler: T }),
        /* @__PURE__ */ p(
          X,
          {
            tabs: [
              {
                label: "Layout",
                title: "Edit layout settings",
                active: t === P,
                id: P
              },
              {
                label: "Component",
                title: "Edit component settings",
                active: t === B,
                id: B
              }
            ],
            update: o,
            children: [
              t === P && i,
              t === B && n
            ]
          }
        )
      ] })
    }
  );
};
const He = () => {
  const e = r(R), t = r(O), s = r(q), o = r(z), n = e ? s[e] : null, i = n ? o[n.regionId] : null, l = i ? t[i.section] : null;
  K();
  const b = r(
    (d) => Q(d, (l == null ? void 0 : l.layout_id) || "_none")
  );
  if (!e || !l || !i || !n)
    return /* @__PURE__ */ a("footer", { className: "dlb__footer", children: "Click or press tab to select an item" });
  const u = `${l.layout_settings.label || `Section ${l.weight + 1}`} > ${(b == null ? void 0 : b.region_labels[i.region]) ?? i.region} > ${n.configuration.label || `Component ${n.uuid}`}`;
  return /* @__PURE__ */ a("footer", { className: "dlb__footer", children: u });
};
const at = ({
  blockPlugins: e,
  layoutPlugins: t,
  formatterPlugins: s,
  widgetPlugins: o,
  title: n,
  ...i
}) => /* @__PURE__ */ a(we, { store: pe(i), children: /* @__PURE__ */ a(Ce, { plugins: e, children: /* @__PURE__ */ a(Fe, { plugins: t, children: /* @__PURE__ */ a(Ne, { plugins: s, children: /* @__PURE__ */ a(De, { plugins: o, children: /* @__PURE__ */ p("div", { className: "dlb", children: [
  /* @__PURE__ */ a(ze, { title: n }),
  /* @__PURE__ */ p(ke, { children: [
    /* @__PURE__ */ a(Qe, {}),
    /* @__PURE__ */ a(Ue, { ...i }),
    /* @__PURE__ */ a(qe, {})
  ] }),
  /* @__PURE__ */ a(He, {})
] }) }) }) }) }) }), ot = Ae;
export {
  Wt as Block,
  ot as BlockTitle,
  lt as DefaultBlockSettings,
  ge as DefaultLayoutSettings,
  zt as Display,
  Xt as Editor,
  Ht as Field,
  Kt as Formatter,
  at as LayoutEditor,
  Se as Region,
  Qt as Widget,
  ct as addNewBlock,
  dt as addNewBlockAndSection,
  ut as addNewSection,
  bt as decodeError,
  pt as denormalizeLayout,
  St as drupalSlice,
  gt as layoutSlice,
  ie as loadLayout,
  ft as loadLayoutStart,
  ht as loadOrSaveLayoutFailure,
  mt as loadOrSaveLayoutSuccess,
  _t as moveBlock,
  yt as normalizeLayout,
  vt as reorderLayoutSections,
  Y as saveLayout,
  Et as saveLayoutStart,
  x as selectActiveDrag,
  Tt as selectAllBlocks,
  Ct as selectAllSections,
  le as selectBaseUrl,
  kt as selectBlockById,
  Lt as selectBlocksResult,
  q as selectComponents,
  Pt as selectKeyedBlocks,
  Bt as selectKeyedSections,
  re as selectLayout,
  O as selectLayoutSections,
  V as selectLayoutState,
  Nt as selectMode,
  Dt as selectRegion,
  z as selectRegions,
  Q as selectSectionById,
  At as selectSectionsResult,
  R as selectSelectedComponent,
  A as selectSelectedSection,
  k as selectorUiState,
  Ot as setActiveDrag,
  be as setActiveSidebarEndPane,
  D as setActiveSidebarStartPane,
  T as setEndSidebarOpen,
  ce as setMode,
  Rt as setSelectedComponent,
  ae as setSelectedSection,
  ue as setStartSidebarOpen,
  xt as setUnsavedChanges,
  It as sortComponentsByWeight,
  pe as storeFactory,
  wt as uiSlice,
  Ft as updateBlock,
  $t as updateLayoutSettings,
  Te as useBlockPlugin,
  h as useDispatch,
  Vt as useFormatterPlugin,
  jt as useGetBlockListQuery,
  Gt as useGetDisplayPluginQuery,
  K as useGetSectionListQuery,
  J as useLayoutPlugin,
  r as useSelector,
  Yt as useWidgetPlugin
};
