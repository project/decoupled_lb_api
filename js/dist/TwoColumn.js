import { jsxs as n, jsx as i } from "react/jsx-runtime";
import { R as s, D as a } from "./DefaultLayoutSettings-b131a39e.js";
import "react";
import "./ErrorHelpers-a6fb9f99.js";
import "react-dom";
import "./_commonjsHelpers-10dfc225.js";
const d = ({
  section: o,
  sectionProps: e,
  regions: t
}) => /* @__PURE__ */ n(
  "div",
  {
    ...e,
    className: `section section--${o.layout_id} layout layout--twocol-section layout--twocol-section--${o.layout_settings.column_widths}`,
    children: [
      /* @__PURE__ */ i(
        s,
        {
          regionId: t.first.id,
          className: "layout__region layout__region--first"
        }
      ),
      /* @__PURE__ */ i(
        s,
        {
          regionId: t.second.id,
          as: "aside",
          className: "layout__region layout__region--second"
        }
      )
    ]
  }
), y = a, g = "layout_twocol_section";
export {
  d as Preview,
  y as Settings,
  g as id
};
