var Ka = Object.defineProperty;
var Ua = (e, t, r) => t in e ? Ka(e, t, { enumerable: !0, configurable: !0, writable: !0, value: r }) : e[t] = r;
var nn = (e, t, r) => (Ua(e, typeof t != "symbol" ? t + "" : t, r), r);
import { jsxs as cr, Fragment as Ba, jsx as ft } from "react/jsx-runtime";
import * as dt from "react";
import Gn, { useContext as Wa, useRef as Ae, useCallback as We, useDebugValue as Tr, useMemo as ue, useState as an, useEffect as Te, useLayoutEffect as Va } from "react";
import { unstable_batchedUpdates as Jn } from "react-dom";
import { g as Ha } from "./_commonjsHelpers-10dfc225.js";
var Xn = { exports: {} }, Yn = {};
/**
 * @license React
 * use-sync-external-store-shim.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Le = Gn;
function Ga(e, t) {
  return e === t && (e !== 0 || 1 / e === 1 / t) || e !== e && t !== t;
}
var Ja = typeof Object.is == "function" ? Object.is : Ga, Xa = Le.useState, Ya = Le.useEffect, Za = Le.useLayoutEffect, ei = Le.useDebugValue;
function ti(e, t) {
  var r = t(), n = Xa({ inst: { value: r, getSnapshot: t } }), a = n[0].inst, i = n[1];
  return Za(function() {
    a.value = r, a.getSnapshot = t, lr(a) && i({ inst: a });
  }, [e, r, t]), Ya(function() {
    return lr(a) && i({ inst: a }), e(function() {
      lr(a) && i({ inst: a });
    });
  }, [e]), ei(r), r;
}
function lr(e) {
  var t = e.getSnapshot;
  e = e.value;
  try {
    var r = t();
    return !Ja(e, r);
  } catch {
    return !0;
  }
}
function ri(e, t) {
  return t();
}
var ni = typeof window > "u" || typeof window.document > "u" || typeof window.document.createElement > "u" ? ri : ti;
Yn.useSyncExternalStore = Le.useSyncExternalStore !== void 0 ? Le.useSyncExternalStore : ni;
Xn.exports = Yn;
var ai = Xn.exports, Zn = { exports: {} }, ea = {};
/**
 * @license React
 * use-sync-external-store-shim/with-selector.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var $t = Gn, ii = ai;
function oi(e, t) {
  return e === t && (e !== 0 || 1 / e === 1 / t) || e !== e && t !== t;
}
var ui = typeof Object.is == "function" ? Object.is : oi, si = ii.useSyncExternalStore, ci = $t.useRef, li = $t.useEffect, fi = $t.useMemo, di = $t.useDebugValue;
ea.useSyncExternalStoreWithSelector = function(e, t, r, n, a) {
  var i = ci(null);
  if (i.current === null) {
    var s = { hasValue: !1, value: null };
    i.current = s;
  } else
    s = i.current;
  i = fi(function() {
    function u(l) {
      if (!o) {
        if (o = !0, f = l, l = n(l), a !== void 0 && s.hasValue) {
          var d = s.value;
          if (a(d, l))
            return g = d;
        }
        return g = l;
      }
      if (d = g, ui(f, l))
        return d;
      var p = n(l);
      return a !== void 0 && a(d, p) ? d : (f = l, g = p);
    }
    var o = !1, f, g, h = r === void 0 ? null : r;
    return [function() {
      return u(t());
    }, h === null ? void 0 : function() {
      return u(h());
    }];
  }, [t, r, n, a]);
  var c = si(e, i[0], i[1]);
  return li(function() {
    s.hasValue = !0, s.value = c;
  }, [c]), di(c), c;
};
Zn.exports = ea;
var vi = Zn.exports;
function pi(e) {
  e();
}
let ta = pi;
const yi = (e) => ta = e, Ls = () => ta, on = Symbol.for("react-redux-context"), un = typeof globalThis < "u" ? globalThis : (
  /* fall back to a per-module scope (pre-8.1 behaviour) if `globalThis` is not available */
  {}
);
function hi() {
  var e;
  if (!dt.createContext)
    return {};
  const t = (e = un[on]) != null ? e : un[on] = /* @__PURE__ */ new Map();
  let r = t.get(dt.createContext);
  return r || (r = dt.createContext(null), t.set(dt.createContext, r)), r;
}
const $e = /* @__PURE__ */ hi();
function jr(e = $e) {
  return function() {
    return Wa(e);
  };
}
const ra = /* @__PURE__ */ jr(), gi = () => {
  throw new Error("uSES not initialized!");
};
let na = gi;
const mi = (e) => {
  na = e;
}, bi = (e, t) => e === t;
function Si(e = $e) {
  const t = e === $e ? ra : jr(e);
  return function(n, a = {}) {
    const {
      equalityFn: i = bi,
      stabilityCheck: s = void 0,
      noopCheck: c = void 0
    } = typeof a == "function" ? {
      equalityFn: a
    } : a, {
      store: u,
      subscription: o,
      getServerState: f,
      stabilityCheck: g,
      noopCheck: h
    } = t();
    Ae(!0);
    const l = We({
      [n.name](p) {
        return n(p);
      }
    }[n.name], [n, g, s]), d = na(o.addNestedSub, u.getState, f || u.getState, l, i);
    return Tr(d), d;
  };
}
const aa = /* @__PURE__ */ Si();
var ia = { exports: {} }, L = {};
/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var X = typeof Symbol == "function" && Symbol.for, _r = X ? Symbol.for("react.element") : 60103, Nr = X ? Symbol.for("react.portal") : 60106, Dt = X ? Symbol.for("react.fragment") : 60107, Qt = X ? Symbol.for("react.strict_mode") : 60108, jt = X ? Symbol.for("react.profiler") : 60114, _t = X ? Symbol.for("react.provider") : 60109, Nt = X ? Symbol.for("react.context") : 60110, Lr = X ? Symbol.for("react.async_mode") : 60111, Lt = X ? Symbol.for("react.concurrent_mode") : 60111, Ft = X ? Symbol.for("react.forward_ref") : 60112, zt = X ? Symbol.for("react.suspense") : 60113, Oi = X ? Symbol.for("react.suspense_list") : 60120, Kt = X ? Symbol.for("react.memo") : 60115, Ut = X ? Symbol.for("react.lazy") : 60116, wi = X ? Symbol.for("react.block") : 60121, Ai = X ? Symbol.for("react.fundamental") : 60117, Ti = X ? Symbol.for("react.responder") : 60118, Pi = X ? Symbol.for("react.scope") : 60119;
function ae(e) {
  if (typeof e == "object" && e !== null) {
    var t = e.$$typeof;
    switch (t) {
      case _r:
        switch (e = e.type, e) {
          case Lr:
          case Lt:
          case Dt:
          case jt:
          case Qt:
          case zt:
            return e;
          default:
            switch (e = e && e.$$typeof, e) {
              case Nt:
              case Ft:
              case Ut:
              case Kt:
              case _t:
                return e;
              default:
                return t;
            }
        }
      case Nr:
        return t;
    }
  }
}
function oa(e) {
  return ae(e) === Lt;
}
L.AsyncMode = Lr;
L.ConcurrentMode = Lt;
L.ContextConsumer = Nt;
L.ContextProvider = _t;
L.Element = _r;
L.ForwardRef = Ft;
L.Fragment = Dt;
L.Lazy = Ut;
L.Memo = Kt;
L.Portal = Nr;
L.Profiler = jt;
L.StrictMode = Qt;
L.Suspense = zt;
L.isAsyncMode = function(e) {
  return oa(e) || ae(e) === Lr;
};
L.isConcurrentMode = oa;
L.isContextConsumer = function(e) {
  return ae(e) === Nt;
};
L.isContextProvider = function(e) {
  return ae(e) === _t;
};
L.isElement = function(e) {
  return typeof e == "object" && e !== null && e.$$typeof === _r;
};
L.isForwardRef = function(e) {
  return ae(e) === Ft;
};
L.isFragment = function(e) {
  return ae(e) === Dt;
};
L.isLazy = function(e) {
  return ae(e) === Ut;
};
L.isMemo = function(e) {
  return ae(e) === Kt;
};
L.isPortal = function(e) {
  return ae(e) === Nr;
};
L.isProfiler = function(e) {
  return ae(e) === jt;
};
L.isStrictMode = function(e) {
  return ae(e) === Qt;
};
L.isSuspense = function(e) {
  return ae(e) === zt;
};
L.isValidElementType = function(e) {
  return typeof e == "string" || typeof e == "function" || e === Dt || e === Lt || e === jt || e === Qt || e === zt || e === Oi || typeof e == "object" && e !== null && (e.$$typeof === Ut || e.$$typeof === Kt || e.$$typeof === _t || e.$$typeof === Nt || e.$$typeof === Ft || e.$$typeof === Ai || e.$$typeof === Ti || e.$$typeof === Pi || e.$$typeof === wi);
};
L.typeOf = ae;
ia.exports = L;
var Ri = ia.exports, Fr = Ri, Ei = {
  childContextTypes: !0,
  contextType: !0,
  contextTypes: !0,
  defaultProps: !0,
  displayName: !0,
  getDefaultProps: !0,
  getDerivedStateFromError: !0,
  getDerivedStateFromProps: !0,
  mixins: !0,
  propTypes: !0,
  type: !0
}, Ii = {
  name: !0,
  length: !0,
  prototype: !0,
  caller: !0,
  callee: !0,
  arguments: !0,
  arity: !0
}, Mi = {
  $$typeof: !0,
  render: !0,
  defaultProps: !0,
  displayName: !0,
  propTypes: !0
}, ua = {
  $$typeof: !0,
  compare: !0,
  defaultProps: !0,
  displayName: !0,
  propTypes: !0,
  type: !0
}, zr = {};
zr[Fr.ForwardRef] = Mi;
zr[Fr.Memo] = ua;
function sn(e) {
  return Fr.isMemo(e) ? ua : zr[e.$$typeof] || Ei;
}
var ki = Object.defineProperty, Ci = Object.getOwnPropertyNames, cn = Object.getOwnPropertySymbols, xi = Object.getOwnPropertyDescriptor, qi = Object.getPrototypeOf, ln = Object.prototype;
function sa(e, t, r) {
  if (typeof t != "string") {
    if (ln) {
      var n = qi(t);
      n && n !== ln && sa(e, n, r);
    }
    var a = Ci(t);
    cn && (a = a.concat(cn(t)));
    for (var i = sn(e), s = sn(t), c = 0; c < a.length; ++c) {
      var u = a[c];
      if (!Ii[u] && !(r && r[u]) && !(s && s[u]) && !(i && i[u])) {
        var o = xi(t, u);
        try {
          ki(e, u, o);
        } catch {
        }
      }
    }
  }
  return e;
}
var $i = sa;
const Fs = /* @__PURE__ */ Ha($i);
var F = {};
/**
 * @license React
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Kr = Symbol.for("react.element"), Ur = Symbol.for("react.portal"), Bt = Symbol.for("react.fragment"), Wt = Symbol.for("react.strict_mode"), Vt = Symbol.for("react.profiler"), Ht = Symbol.for("react.provider"), Gt = Symbol.for("react.context"), Di = Symbol.for("react.server_context"), Jt = Symbol.for("react.forward_ref"), Xt = Symbol.for("react.suspense"), Yt = Symbol.for("react.suspense_list"), Zt = Symbol.for("react.memo"), er = Symbol.for("react.lazy"), Qi = Symbol.for("react.offscreen"), ca;
ca = Symbol.for("react.module.reference");
function oe(e) {
  if (typeof e == "object" && e !== null) {
    var t = e.$$typeof;
    switch (t) {
      case Kr:
        switch (e = e.type, e) {
          case Bt:
          case Vt:
          case Wt:
          case Xt:
          case Yt:
            return e;
          default:
            switch (e = e && e.$$typeof, e) {
              case Di:
              case Gt:
              case Jt:
              case er:
              case Zt:
              case Ht:
                return e;
              default:
                return t;
            }
        }
      case Ur:
        return t;
    }
  }
}
F.ContextConsumer = Gt;
F.ContextProvider = Ht;
F.Element = Kr;
F.ForwardRef = Jt;
F.Fragment = Bt;
F.Lazy = er;
F.Memo = Zt;
F.Portal = Ur;
F.Profiler = Vt;
F.StrictMode = Wt;
F.Suspense = Xt;
F.SuspenseList = Yt;
F.isAsyncMode = function() {
  return !1;
};
F.isConcurrentMode = function() {
  return !1;
};
F.isContextConsumer = function(e) {
  return oe(e) === Gt;
};
F.isContextProvider = function(e) {
  return oe(e) === Ht;
};
F.isElement = function(e) {
  return typeof e == "object" && e !== null && e.$$typeof === Kr;
};
F.isForwardRef = function(e) {
  return oe(e) === Jt;
};
F.isFragment = function(e) {
  return oe(e) === Bt;
};
F.isLazy = function(e) {
  return oe(e) === er;
};
F.isMemo = function(e) {
  return oe(e) === Zt;
};
F.isPortal = function(e) {
  return oe(e) === Ur;
};
F.isProfiler = function(e) {
  return oe(e) === Vt;
};
F.isStrictMode = function(e) {
  return oe(e) === Wt;
};
F.isSuspense = function(e) {
  return oe(e) === Xt;
};
F.isSuspenseList = function(e) {
  return oe(e) === Yt;
};
F.isValidElementType = function(e) {
  return typeof e == "string" || typeof e == "function" || e === Bt || e === Vt || e === Wt || e === Xt || e === Yt || e === Qi || typeof e == "object" && e !== null && (e.$$typeof === er || e.$$typeof === Zt || e.$$typeof === Ht || e.$$typeof === Gt || e.$$typeof === Jt || e.$$typeof === ca || e.getModuleId !== void 0);
};
F.typeOf = oe;
function fn(e, t) {
  return e === t ? e !== 0 || t !== 0 || 1 / e === 1 / t : e !== e && t !== t;
}
function bt(e, t) {
  if (fn(e, t))
    return !0;
  if (typeof e != "object" || e === null || typeof t != "object" || t === null)
    return !1;
  const r = Object.keys(e), n = Object.keys(t);
  if (r.length !== n.length)
    return !1;
  for (let a = 0; a < r.length; a++)
    if (!Object.prototype.hasOwnProperty.call(t, r[a]) || !fn(e[r[a]], t[r[a]]))
      return !1;
  return !0;
}
function la(e = $e) {
  const t = (
    // @ts-ignore
    e === $e ? ra : (
      // @ts-ignore
      jr(e)
    )
  );
  return function() {
    const {
      store: n
    } = t();
    return n;
  };
}
const fa = /* @__PURE__ */ la();
function ji(e = $e) {
  const t = (
    // @ts-ignore
    e === $e ? fa : la(e)
  );
  return function() {
    return t().dispatch;
  };
}
const da = /* @__PURE__ */ ji();
mi(vi.useSyncExternalStoreWithSelector);
yi(Jn);
function Z(e) {
  for (var t = arguments.length, r = Array(t > 1 ? t - 1 : 0), n = 1; n < t; n++)
    r[n - 1] = arguments[n];
  throw Error("[Immer] minified error nr: " + e + (r.length ? " " + r.map(function(a) {
    return "'" + a + "'";
  }).join(",") : "") + ". Find the full error at: https://bit.ly/3cXEKWf");
}
function re(e) {
  return !!e && !!e[W];
}
function se(e) {
  var t;
  return !!e && (function(r) {
    if (!r || typeof r != "object")
      return !1;
    var n = Object.getPrototypeOf(r);
    if (n === null)
      return !0;
    var a = Object.hasOwnProperty.call(n, "constructor") && n.constructor;
    return a === Object || typeof a == "function" && Function.toString.call(a) === Bi;
  }(e) || Array.isArray(e) || !!e[Ge] || !!(!((t = e.constructor) === null || t === void 0) && t[Ge]) || tr(e) || rr(e));
}
function _i(e) {
  return re(e) || Z(23, e), e[W].t;
}
function Ie(e, t, r) {
  r === void 0 && (r = !1), Me(e) === 0 ? (r ? Object.keys : Ne)(e).forEach(function(n) {
    r && typeof n == "symbol" || t(n, e[n], e);
  }) : e.forEach(function(n, a) {
    return t(a, n, e);
  });
}
function Me(e) {
  var t = e[W];
  return t ? t.i > 3 ? t.i - 4 : t.i : Array.isArray(e) ? 1 : tr(e) ? 2 : rr(e) ? 3 : 0;
}
function Re(e, t) {
  return Me(e) === 2 ? e.has(t) : Object.prototype.hasOwnProperty.call(e, t);
}
function gt(e, t) {
  return Me(e) === 2 ? e.get(t) : e[t];
}
function va(e, t, r) {
  var n = Me(e);
  n === 2 ? e.set(t, r) : n === 3 ? e.add(r) : e[t] = r;
}
function pa(e, t) {
  return e === t ? e !== 0 || 1 / e == 1 / t : e != e && t != t;
}
function tr(e) {
  return Ki && e instanceof Map;
}
function rr(e) {
  return Ui && e instanceof Set;
}
function Ce(e) {
  return e.o || e.t;
}
function Br(e) {
  if (Array.isArray(e))
    return Array.prototype.slice.call(e);
  var t = ga(e);
  delete t[W];
  for (var r = Ne(t), n = 0; n < r.length; n++) {
    var a = r[n], i = t[a];
    i.writable === !1 && (i.writable = !0, i.configurable = !0), (i.get || i.set) && (t[a] = { configurable: !0, writable: !0, enumerable: i.enumerable, value: e[a] });
  }
  return Object.create(Object.getPrototypeOf(e), t);
}
function Wr(e, t) {
  return t === void 0 && (t = !1), Vr(e) || re(e) || !se(e) || (Me(e) > 1 && (e.set = e.add = e.clear = e.delete = Ni), Object.freeze(e), t && Ie(e, function(r, n) {
    return Wr(n, !0);
  }, !0)), e;
}
function Ni() {
  Z(2);
}
function Vr(e) {
  return e == null || typeof e != "object" || Object.isFrozen(e);
}
function ve(e) {
  var t = Ir[e];
  return t || Z(18, e), t;
}
function ya(e, t) {
  Ir[e] || (Ir[e] = t);
}
function Pr() {
  return Ye;
}
function fr(e, t) {
  t && (ve("Patches"), e.u = [], e.s = [], e.v = t);
}
function St(e) {
  Rr(e), e.p.forEach(Li), e.p = null;
}
function Rr(e) {
  e === Ye && (Ye = e.l);
}
function dn(e) {
  return Ye = { p: [], l: Ye, h: e, m: !0, _: 0 };
}
function Li(e) {
  var t = e[W];
  t.i === 0 || t.i === 1 ? t.j() : t.g = !0;
}
function dr(e, t) {
  t._ = t.p.length;
  var r = t.p[0], n = e !== void 0 && e !== r;
  return t.h.O || ve("ES5").S(t, e, n), n ? (r[W].P && (St(t), Z(4)), se(e) && (e = Ot(t, e), t.l || wt(t, e)), t.u && ve("Patches").M(r[W].t, e, t.u, t.s)) : e = Ot(t, r, []), St(t), t.u && t.v(t.u, t.s), e !== Gr ? e : void 0;
}
function Ot(e, t, r) {
  if (Vr(t))
    return t;
  var n = t[W];
  if (!n)
    return Ie(t, function(c, u) {
      return vn(e, n, t, c, u, r);
    }, !0), t;
  if (n.A !== e)
    return t;
  if (!n.P)
    return wt(e, n.t, !0), n.t;
  if (!n.I) {
    n.I = !0, n.A._--;
    var a = n.i === 4 || n.i === 5 ? n.o = Br(n.k) : n.o, i = a, s = !1;
    n.i === 3 && (i = new Set(a), a.clear(), s = !0), Ie(i, function(c, u) {
      return vn(e, n, a, c, u, r, s);
    }), wt(e, a, !1), r && e.u && ve("Patches").N(n, r, e.u, e.s);
  }
  return n.o;
}
function vn(e, t, r, n, a, i, s) {
  if (re(a)) {
    var c = Ot(e, a, i && t && t.i !== 3 && !Re(t.R, n) ? i.concat(n) : void 0);
    if (va(r, n, c), !re(c))
      return;
    e.m = !1;
  } else
    s && r.add(a);
  if (se(a) && !Vr(a)) {
    if (!e.h.D && e._ < 1)
      return;
    Ot(e, a), t && t.A.l || wt(e, a);
  }
}
function wt(e, t, r) {
  r === void 0 && (r = !1), !e.l && e.h.D && e.m && Wr(t, r);
}
function vr(e, t) {
  var r = e[W];
  return (r ? Ce(r) : e)[t];
}
function pn(e, t) {
  if (t in e)
    for (var r = Object.getPrototypeOf(e); r; ) {
      var n = Object.getOwnPropertyDescriptor(r, t);
      if (n)
        return n;
      r = Object.getPrototypeOf(r);
    }
}
function Pe(e) {
  e.P || (e.P = !0, e.l && Pe(e.l));
}
function pr(e) {
  e.o || (e.o = Br(e.t));
}
function Er(e, t, r) {
  var n = tr(t) ? ve("MapSet").F(t, r) : rr(t) ? ve("MapSet").T(t, r) : e.O ? function(a, i) {
    var s = Array.isArray(a), c = { i: s ? 1 : 0, A: i ? i.A : Pr(), P: !1, I: !1, R: {}, l: i, t: a, k: null, o: null, j: null, C: !1 }, u = c, o = Ze;
    s && (u = [c], o = Ve);
    var f = Proxy.revocable(u, o), g = f.revoke, h = f.proxy;
    return c.k = h, c.j = g, h;
  }(t, r) : ve("ES5").J(t, r);
  return (r ? r.A : Pr()).p.push(n), n;
}
function ha(e) {
  return re(e) || Z(22, e), function t(r) {
    if (!se(r))
      return r;
    var n, a = r[W], i = Me(r);
    if (a) {
      if (!a.P && (a.i < 4 || !ve("ES5").K(a)))
        return a.t;
      a.I = !0, n = yn(r, i), a.I = !1;
    } else
      n = yn(r, i);
    return Ie(n, function(s, c) {
      a && gt(a.t, s) === c || va(n, s, t(c));
    }), i === 3 ? new Set(n) : n;
  }(e);
}
function yn(e, t) {
  switch (t) {
    case 2:
      return new Map(e);
    case 3:
      return Array.from(e);
  }
  return Br(e);
}
function Fi() {
  function e(i, s) {
    var c = a[i];
    return c ? c.enumerable = s : a[i] = c = { configurable: !0, enumerable: s, get: function() {
      var u = this[W];
      return Ze.get(u, i);
    }, set: function(u) {
      var o = this[W];
      Ze.set(o, i, u);
    } }, c;
  }
  function t(i) {
    for (var s = i.length - 1; s >= 0; s--) {
      var c = i[s][W];
      if (!c.P)
        switch (c.i) {
          case 5:
            n(c) && Pe(c);
            break;
          case 4:
            r(c) && Pe(c);
        }
    }
  }
  function r(i) {
    for (var s = i.t, c = i.k, u = Ne(c), o = u.length - 1; o >= 0; o--) {
      var f = u[o];
      if (f !== W) {
        var g = s[f];
        if (g === void 0 && !Re(s, f))
          return !0;
        var h = c[f], l = h && h[W];
        if (l ? l.t !== g : !pa(h, g))
          return !0;
      }
    }
    var d = !!s[W];
    return u.length !== Ne(s).length + (d ? 0 : 1);
  }
  function n(i) {
    var s = i.k;
    if (s.length !== i.t.length)
      return !0;
    var c = Object.getOwnPropertyDescriptor(s, s.length - 1);
    if (c && !c.get)
      return !0;
    for (var u = 0; u < s.length; u++)
      if (!s.hasOwnProperty(u))
        return !0;
    return !1;
  }
  var a = {};
  ya("ES5", { J: function(i, s) {
    var c = Array.isArray(i), u = function(f, g) {
      if (f) {
        for (var h = Array(g.length), l = 0; l < g.length; l++)
          Object.defineProperty(h, "" + l, e(l, !0));
        return h;
      }
      var d = ga(g);
      delete d[W];
      for (var p = Ne(d), S = 0; S < p.length; S++) {
        var A = p[S];
        d[A] = e(A, f || !!d[A].enumerable);
      }
      return Object.create(Object.getPrototypeOf(g), d);
    }(c, i), o = { i: c ? 5 : 4, A: s ? s.A : Pr(), P: !1, I: !1, R: {}, l: s, t: i, k: u, o: null, g: !1, C: !1 };
    return Object.defineProperty(u, W, { value: o, writable: !0 }), u;
  }, S: function(i, s, c) {
    c ? re(s) && s[W].A === i && t(i.p) : (i.u && function u(o) {
      if (o && typeof o == "object") {
        var f = o[W];
        if (f) {
          var g = f.t, h = f.k, l = f.R, d = f.i;
          if (d === 4)
            Ie(h, function(v) {
              v !== W && (g[v] !== void 0 || Re(g, v) ? l[v] || u(h[v]) : (l[v] = !0, Pe(f)));
            }), Ie(g, function(v) {
              h[v] !== void 0 || Re(h, v) || (l[v] = !1, Pe(f));
            });
          else if (d === 5) {
            if (n(f) && (Pe(f), l.length = !0), h.length < g.length)
              for (var p = h.length; p < g.length; p++)
                l[p] = !1;
            else
              for (var S = g.length; S < h.length; S++)
                l[S] = !0;
            for (var A = Math.min(h.length, g.length), b = 0; b < A; b++)
              h.hasOwnProperty(b) || (l[b] = !0), l[b] === void 0 && u(h[b]);
          }
        }
      }
    }(i.p[0]), t(i.p));
  }, K: function(i) {
    return i.i === 4 ? r(i) : n(i);
  } });
}
function zi() {
  function e(n) {
    if (!se(n))
      return n;
    if (Array.isArray(n))
      return n.map(e);
    if (tr(n))
      return new Map(Array.from(n.entries()).map(function(s) {
        return [s[0], e(s[1])];
      }));
    if (rr(n))
      return new Set(Array.from(n).map(e));
    var a = Object.create(Object.getPrototypeOf(n));
    for (var i in n)
      a[i] = e(n[i]);
    return Re(n, Ge) && (a[Ge] = n[Ge]), a;
  }
  function t(n) {
    return re(n) ? e(n) : n;
  }
  var r = "add";
  ya("Patches", { $: function(n, a) {
    return a.forEach(function(i) {
      for (var s = i.path, c = i.op, u = n, o = 0; o < s.length - 1; o++) {
        var f = Me(u), g = s[o];
        typeof g != "string" && typeof g != "number" && (g = "" + g), f !== 0 && f !== 1 || g !== "__proto__" && g !== "constructor" || Z(24), typeof u == "function" && g === "prototype" && Z(24), typeof (u = gt(u, g)) != "object" && Z(15, s.join("/"));
      }
      var h = Me(u), l = e(i.value), d = s[s.length - 1];
      switch (c) {
        case "replace":
          switch (h) {
            case 2:
              return u.set(d, l);
            case 3:
              Z(16);
            default:
              return u[d] = l;
          }
        case r:
          switch (h) {
            case 1:
              return d === "-" ? u.push(l) : u.splice(d, 0, l);
            case 2:
              return u.set(d, l);
            case 3:
              return u.add(l);
            default:
              return u[d] = l;
          }
        case "remove":
          switch (h) {
            case 1:
              return u.splice(d, 1);
            case 2:
              return u.delete(d);
            case 3:
              return u.delete(i.value);
            default:
              return delete u[d];
          }
        default:
          Z(17, c);
      }
    }), n;
  }, N: function(n, a, i, s) {
    switch (n.i) {
      case 0:
      case 4:
      case 2:
        return function(c, u, o, f) {
          var g = c.t, h = c.o;
          Ie(c.R, function(l, d) {
            var p = gt(g, l), S = gt(h, l), A = d ? Re(g, l) ? "replace" : r : "remove";
            if (p !== S || A !== "replace") {
              var b = u.concat(l);
              o.push(A === "remove" ? { op: A, path: b } : { op: A, path: b, value: S }), f.push(A === r ? { op: "remove", path: b } : A === "remove" ? { op: r, path: b, value: t(p) } : { op: "replace", path: b, value: t(p) });
            }
          });
        }(n, a, i, s);
      case 5:
      case 1:
        return function(c, u, o, f) {
          var g = c.t, h = c.R, l = c.o;
          if (l.length < g.length) {
            var d = [l, g];
            g = d[0], l = d[1];
            var p = [f, o];
            o = p[0], f = p[1];
          }
          for (var S = 0; S < g.length; S++)
            if (h[S] && l[S] !== g[S]) {
              var A = u.concat([S]);
              o.push({ op: "replace", path: A, value: t(l[S]) }), f.push({ op: "replace", path: A, value: t(g[S]) });
            }
          for (var b = g.length; b < l.length; b++) {
            var v = u.concat([b]);
            o.push({ op: r, path: v, value: t(l[b]) });
          }
          g.length < l.length && f.push({ op: "replace", path: u.concat(["length"]), value: g.length });
        }(n, a, i, s);
      case 3:
        return function(c, u, o, f) {
          var g = c.t, h = c.o, l = 0;
          g.forEach(function(d) {
            if (!h.has(d)) {
              var p = u.concat([l]);
              o.push({ op: "remove", path: p, value: d }), f.unshift({ op: r, path: p, value: d });
            }
            l++;
          }), l = 0, h.forEach(function(d) {
            if (!g.has(d)) {
              var p = u.concat([l]);
              o.push({ op: r, path: p, value: d }), f.unshift({ op: "remove", path: p, value: d });
            }
            l++;
          });
        }(n, a, i, s);
    }
  }, M: function(n, a, i, s) {
    i.push({ op: "replace", path: [], value: a === Gr ? void 0 : a }), s.push({ op: "replace", path: [], value: n });
  } });
}
var hn, Ye, Hr = typeof Symbol < "u" && typeof Symbol("x") == "symbol", Ki = typeof Map < "u", Ui = typeof Set < "u", gn = typeof Proxy < "u" && Proxy.revocable !== void 0 && typeof Reflect < "u", Gr = Hr ? Symbol.for("immer-nothing") : ((hn = {})["immer-nothing"] = !0, hn), Ge = Hr ? Symbol.for("immer-draftable") : "__$immer_draftable", W = Hr ? Symbol.for("immer-state") : "__$immer_state", Bi = "" + Object.prototype.constructor, Ne = typeof Reflect < "u" && Reflect.ownKeys ? Reflect.ownKeys : Object.getOwnPropertySymbols !== void 0 ? function(e) {
  return Object.getOwnPropertyNames(e).concat(Object.getOwnPropertySymbols(e));
} : Object.getOwnPropertyNames, ga = Object.getOwnPropertyDescriptors || function(e) {
  var t = {};
  return Ne(e).forEach(function(r) {
    t[r] = Object.getOwnPropertyDescriptor(e, r);
  }), t;
}, Ir = {}, Ze = { get: function(e, t) {
  if (t === W)
    return e;
  var r = Ce(e);
  if (!Re(r, t))
    return function(a, i, s) {
      var c, u = pn(i, s);
      return u ? "value" in u ? u.value : (c = u.get) === null || c === void 0 ? void 0 : c.call(a.k) : void 0;
    }(e, r, t);
  var n = r[t];
  return e.I || !se(n) ? n : n === vr(e.t, t) ? (pr(e), e.o[t] = Er(e.A.h, n, e)) : n;
}, has: function(e, t) {
  return t in Ce(e);
}, ownKeys: function(e) {
  return Reflect.ownKeys(Ce(e));
}, set: function(e, t, r) {
  var n = pn(Ce(e), t);
  if (n != null && n.set)
    return n.set.call(e.k, r), !0;
  if (!e.P) {
    var a = vr(Ce(e), t), i = a == null ? void 0 : a[W];
    if (i && i.t === r)
      return e.o[t] = r, e.R[t] = !1, !0;
    if (pa(r, a) && (r !== void 0 || Re(e.t, t)))
      return !0;
    pr(e), Pe(e);
  }
  return e.o[t] === r && (r !== void 0 || t in e.o) || Number.isNaN(r) && Number.isNaN(e.o[t]) || (e.o[t] = r, e.R[t] = !0), !0;
}, deleteProperty: function(e, t) {
  return vr(e.t, t) !== void 0 || t in e.t ? (e.R[t] = !1, pr(e), Pe(e)) : delete e.R[t], e.o && delete e.o[t], !0;
}, getOwnPropertyDescriptor: function(e, t) {
  var r = Ce(e), n = Reflect.getOwnPropertyDescriptor(r, t);
  return n && { writable: !0, configurable: e.i !== 1 || t !== "length", enumerable: n.enumerable, value: r[t] };
}, defineProperty: function() {
  Z(11);
}, getPrototypeOf: function(e) {
  return Object.getPrototypeOf(e.t);
}, setPrototypeOf: function() {
  Z(12);
} }, Ve = {};
Ie(Ze, function(e, t) {
  Ve[e] = function() {
    return arguments[0] = arguments[0][0], t.apply(this, arguments);
  };
}), Ve.deleteProperty = function(e, t) {
  return Ve.set.call(this, e, t, void 0);
}, Ve.set = function(e, t, r) {
  return Ze.set.call(this, e[0], t, r, e[0]);
};
var Wi = function() {
  function e(r) {
    var n = this;
    this.O = gn, this.D = !0, this.produce = function(a, i, s) {
      if (typeof a == "function" && typeof i != "function") {
        var c = i;
        i = a;
        var u = n;
        return function(p) {
          var S = this;
          p === void 0 && (p = c);
          for (var A = arguments.length, b = Array(A > 1 ? A - 1 : 0), v = 1; v < A; v++)
            b[v - 1] = arguments[v];
          return u.produce(p, function(y) {
            var O;
            return (O = i).call.apply(O, [S, y].concat(b));
          });
        };
      }
      var o;
      if (typeof i != "function" && Z(6), s !== void 0 && typeof s != "function" && Z(7), se(a)) {
        var f = dn(n), g = Er(n, a, void 0), h = !0;
        try {
          o = i(g), h = !1;
        } finally {
          h ? St(f) : Rr(f);
        }
        return typeof Promise < "u" && o instanceof Promise ? o.then(function(p) {
          return fr(f, s), dr(p, f);
        }, function(p) {
          throw St(f), p;
        }) : (fr(f, s), dr(o, f));
      }
      if (!a || typeof a != "object") {
        if ((o = i(a)) === void 0 && (o = a), o === Gr && (o = void 0), n.D && Wr(o, !0), s) {
          var l = [], d = [];
          ve("Patches").M(a, o, l, d), s(l, d);
        }
        return o;
      }
      Z(21, a);
    }, this.produceWithPatches = function(a, i) {
      if (typeof a == "function")
        return function(o) {
          for (var f = arguments.length, g = Array(f > 1 ? f - 1 : 0), h = 1; h < f; h++)
            g[h - 1] = arguments[h];
          return n.produceWithPatches(o, function(l) {
            return a.apply(void 0, [l].concat(g));
          });
        };
      var s, c, u = n.produce(a, i, function(o, f) {
        s = o, c = f;
      });
      return typeof Promise < "u" && u instanceof Promise ? u.then(function(o) {
        return [o, s, c];
      }) : [u, s, c];
    }, typeof (r == null ? void 0 : r.useProxies) == "boolean" && this.setUseProxies(r.useProxies), typeof (r == null ? void 0 : r.autoFreeze) == "boolean" && this.setAutoFreeze(r.autoFreeze);
  }
  var t = e.prototype;
  return t.createDraft = function(r) {
    se(r) || Z(8), re(r) && (r = ha(r));
    var n = dn(this), a = Er(this, r, void 0);
    return a[W].C = !0, Rr(n), a;
  }, t.finishDraft = function(r, n) {
    var a = r && r[W], i = a.A;
    return fr(i, n), dr(void 0, i);
  }, t.setAutoFreeze = function(r) {
    this.D = r;
  }, t.setUseProxies = function(r) {
    r && !gn && Z(20), this.O = r;
  }, t.applyPatches = function(r, n) {
    var a;
    for (a = n.length - 1; a >= 0; a--) {
      var i = n[a];
      if (i.path.length === 0 && i.op === "replace") {
        r = i.value;
        break;
      }
    }
    a > -1 && (n = n.slice(a + 1));
    var s = ve("Patches").$;
    return re(r) ? s(r, n) : this.produce(r, function(c) {
      return s(c, n);
    });
  }, e;
}(), ne = new Wi(), Vi = ne.produce, ma = ne.produceWithPatches.bind(ne);
ne.setAutoFreeze.bind(ne);
ne.setUseProxies.bind(ne);
var mn = ne.applyPatches.bind(ne);
ne.createDraft.bind(ne);
ne.finishDraft.bind(ne);
const ze = Vi;
function et(e) {
  "@babel/helpers - typeof";
  return et = typeof Symbol == "function" && typeof Symbol.iterator == "symbol" ? function(t) {
    return typeof t;
  } : function(t) {
    return t && typeof Symbol == "function" && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
  }, et(e);
}
function Hi(e, t) {
  if (et(e) !== "object" || e === null)
    return e;
  var r = e[Symbol.toPrimitive];
  if (r !== void 0) {
    var n = r.call(e, t || "default");
    if (et(n) !== "object")
      return n;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (t === "string" ? String : Number)(e);
}
function Gi(e) {
  var t = Hi(e, "string");
  return et(t) === "symbol" ? t : String(t);
}
function Ji(e, t, r) {
  return t = Gi(t), t in e ? Object.defineProperty(e, t, {
    value: r,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : e[t] = r, e;
}
function bn(e, t) {
  var r = Object.keys(e);
  if (Object.getOwnPropertySymbols) {
    var n = Object.getOwnPropertySymbols(e);
    t && (n = n.filter(function(a) {
      return Object.getOwnPropertyDescriptor(e, a).enumerable;
    })), r.push.apply(r, n);
  }
  return r;
}
function Sn(e) {
  for (var t = 1; t < arguments.length; t++) {
    var r = arguments[t] != null ? arguments[t] : {};
    t % 2 ? bn(Object(r), !0).forEach(function(n) {
      Ji(e, n, r[n]);
    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(r)) : bn(Object(r)).forEach(function(n) {
      Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(r, n));
    });
  }
  return e;
}
function Y(e) {
  return "Minified Redux error #" + e + "; visit https://redux.js.org/Errors?code=" + e + " for the full message or use the non-minified dev environment for full errors. ";
}
var On = function() {
  return typeof Symbol == "function" && Symbol.observable || "@@observable";
}(), yr = function() {
  return Math.random().toString(36).substring(7).split("").join(".");
}, At = {
  INIT: "@@redux/INIT" + yr(),
  REPLACE: "@@redux/REPLACE" + yr(),
  PROBE_UNKNOWN_ACTION: function() {
    return "@@redux/PROBE_UNKNOWN_ACTION" + yr();
  }
};
function Xi(e) {
  if (typeof e != "object" || e === null)
    return !1;
  for (var t = e; Object.getPrototypeOf(t) !== null; )
    t = Object.getPrototypeOf(t);
  return Object.getPrototypeOf(e) === t;
}
function ba(e, t, r) {
  var n;
  if (typeof t == "function" && typeof r == "function" || typeof r == "function" && typeof arguments[3] == "function")
    throw new Error(Y(0));
  if (typeof t == "function" && typeof r > "u" && (r = t, t = void 0), typeof r < "u") {
    if (typeof r != "function")
      throw new Error(Y(1));
    return r(ba)(e, t);
  }
  if (typeof e != "function")
    throw new Error(Y(2));
  var a = e, i = t, s = [], c = s, u = !1;
  function o() {
    c === s && (c = s.slice());
  }
  function f() {
    if (u)
      throw new Error(Y(3));
    return i;
  }
  function g(p) {
    if (typeof p != "function")
      throw new Error(Y(4));
    if (u)
      throw new Error(Y(5));
    var S = !0;
    return o(), c.push(p), function() {
      if (S) {
        if (u)
          throw new Error(Y(6));
        S = !1, o();
        var b = c.indexOf(p);
        c.splice(b, 1), s = null;
      }
    };
  }
  function h(p) {
    if (!Xi(p))
      throw new Error(Y(7));
    if (typeof p.type > "u")
      throw new Error(Y(8));
    if (u)
      throw new Error(Y(9));
    try {
      u = !0, i = a(i, p);
    } finally {
      u = !1;
    }
    for (var S = s = c, A = 0; A < S.length; A++) {
      var b = S[A];
      b();
    }
    return p;
  }
  function l(p) {
    if (typeof p != "function")
      throw new Error(Y(10));
    a = p, h({
      type: At.REPLACE
    });
  }
  function d() {
    var p, S = g;
    return p = {
      /**
       * The minimal observable subscription method.
       * @param {Object} observer Any object that can be used as an observer.
       * The observer object should have a `next` method.
       * @returns {subscription} An object with an `unsubscribe` method that can
       * be used to unsubscribe the observable from the store, and prevent further
       * emission of values from the observable.
       */
      subscribe: function(b) {
        if (typeof b != "object" || b === null)
          throw new Error(Y(11));
        function v() {
          b.next && b.next(f());
        }
        v();
        var y = S(v);
        return {
          unsubscribe: y
        };
      }
    }, p[On] = function() {
      return this;
    }, p;
  }
  return h({
    type: At.INIT
  }), n = {
    dispatch: h,
    subscribe: g,
    getState: f,
    replaceReducer: l
  }, n[On] = d, n;
}
function Yi(e) {
  Object.keys(e).forEach(function(t) {
    var r = e[t], n = r(void 0, {
      type: At.INIT
    });
    if (typeof n > "u")
      throw new Error(Y(12));
    if (typeof r(void 0, {
      type: At.PROBE_UNKNOWN_ACTION()
    }) > "u")
      throw new Error(Y(13));
  });
}
function Jr(e) {
  for (var t = Object.keys(e), r = {}, n = 0; n < t.length; n++) {
    var a = t[n];
    typeof e[a] == "function" && (r[a] = e[a]);
  }
  var i = Object.keys(r), s;
  try {
    Yi(r);
  } catch (c) {
    s = c;
  }
  return function(u, o) {
    if (u === void 0 && (u = {}), s)
      throw s;
    for (var f = !1, g = {}, h = 0; h < i.length; h++) {
      var l = i[h], d = r[l], p = u[l], S = d(p, o);
      if (typeof S > "u")
        throw o && o.type, new Error(Y(14));
      g[l] = S, f = f || S !== p;
    }
    return f = f || i.length !== Object.keys(u).length, f ? g : u;
  };
}
function wn(e, t) {
  return function() {
    return t(e.apply(this, arguments));
  };
}
function zs(e, t) {
  if (typeof e == "function")
    return wn(e, t);
  if (typeof e != "object" || e === null)
    throw new Error(Y(16));
  var r = {};
  for (var n in e) {
    var a = e[n];
    typeof a == "function" && (r[n] = wn(a, t));
  }
  return r;
}
function Tt() {
  for (var e = arguments.length, t = new Array(e), r = 0; r < e; r++)
    t[r] = arguments[r];
  return t.length === 0 ? function(n) {
    return n;
  } : t.length === 1 ? t[0] : t.reduce(function(n, a) {
    return function() {
      return n(a.apply(void 0, arguments));
    };
  });
}
function Zi() {
  for (var e = arguments.length, t = new Array(e), r = 0; r < e; r++)
    t[r] = arguments[r];
  return function(n) {
    return function() {
      var a = n.apply(void 0, arguments), i = function() {
        throw new Error(Y(15));
      }, s = {
        getState: a.getState,
        dispatch: function() {
          return i.apply(void 0, arguments);
        }
      }, c = t.map(function(u) {
        return u(s);
      });
      return i = Tt.apply(void 0, c)(a.dispatch), Sn(Sn({}, a), {}, {
        dispatch: i
      });
    };
  };
}
var Pt = "NOT_FOUND";
function eo(e) {
  var t;
  return {
    get: function(n) {
      return t && e(t.key, n) ? t.value : Pt;
    },
    put: function(n, a) {
      t = {
        key: n,
        value: a
      };
    },
    getEntries: function() {
      return t ? [t] : [];
    },
    clear: function() {
      t = void 0;
    }
  };
}
function to(e, t) {
  var r = [];
  function n(c) {
    var u = r.findIndex(function(f) {
      return t(c, f.key);
    });
    if (u > -1) {
      var o = r[u];
      return u > 0 && (r.splice(u, 1), r.unshift(o)), o.value;
    }
    return Pt;
  }
  function a(c, u) {
    n(c) === Pt && (r.unshift({
      key: c,
      value: u
    }), r.length > e && r.pop());
  }
  function i() {
    return r;
  }
  function s() {
    r = [];
  }
  return {
    get: n,
    put: a,
    getEntries: i,
    clear: s
  };
}
var ro = function(t, r) {
  return t === r;
};
function no(e) {
  return function(r, n) {
    if (r === null || n === null || r.length !== n.length)
      return !1;
    for (var a = r.length, i = 0; i < a; i++)
      if (!e(r[i], n[i]))
        return !1;
    return !0;
  };
}
function Mr(e, t) {
  var r = typeof t == "object" ? t : {
    equalityCheck: t
  }, n = r.equalityCheck, a = n === void 0 ? ro : n, i = r.maxSize, s = i === void 0 ? 1 : i, c = r.resultEqualityCheck, u = no(a), o = s === 1 ? eo(u) : to(s, u);
  function f() {
    var g = o.get(arguments);
    if (g === Pt) {
      if (g = e.apply(null, arguments), c) {
        var h = o.getEntries(), l = h.find(function(d) {
          return c(d.value, g);
        });
        l && (g = l.value);
      }
      o.put(arguments, g);
    }
    return g;
  }
  return f.clearCache = function() {
    return o.clear();
  }, f;
}
function ao(e) {
  var t = Array.isArray(e[0]) ? e[0] : e;
  if (!t.every(function(n) {
    return typeof n == "function";
  })) {
    var r = t.map(function(n) {
      return typeof n == "function" ? "function " + (n.name || "unnamed") + "()" : typeof n;
    }).join(", ");
    throw new Error("createSelector expects all input-selectors to be functions, but received the following types: [" + r + "]");
  }
  return t;
}
function io(e) {
  for (var t = arguments.length, r = new Array(t > 1 ? t - 1 : 0), n = 1; n < t; n++)
    r[n - 1] = arguments[n];
  var a = function() {
    for (var s = arguments.length, c = new Array(s), u = 0; u < s; u++)
      c[u] = arguments[u];
    var o = 0, f, g = {
      memoizeOptions: void 0
    }, h = c.pop();
    if (typeof h == "object" && (g = h, h = c.pop()), typeof h != "function")
      throw new Error("createSelector expects an output function after the inputs, but received: [" + typeof h + "]");
    var l = g, d = l.memoizeOptions, p = d === void 0 ? r : d, S = Array.isArray(p) ? p : [p], A = ao(c), b = e.apply(void 0, [function() {
      return o++, h.apply(null, arguments);
    }].concat(S)), v = e(function() {
      for (var O = [], m = A.length, w = 0; w < m; w++)
        O.push(A[w].apply(null, arguments));
      return f = b.apply(null, O), f;
    });
    return Object.assign(v, {
      resultFunc: h,
      memoizedResultFunc: b,
      dependencies: A,
      lastResult: function() {
        return f;
      },
      recomputations: function() {
        return o;
      },
      resetRecomputations: function() {
        return o = 0;
      }
    }), v;
  };
  return a;
}
var Ee = /* @__PURE__ */ io(Mr);
function Sa(e) {
  var t = function(n) {
    var a = n.dispatch, i = n.getState;
    return function(s) {
      return function(c) {
        return typeof c == "function" ? c(a, i, e) : s(c);
      };
    };
  };
  return t;
}
var Oa = Sa();
Oa.withExtraArgument = Sa;
const An = Oa;
var wa = globalThis && globalThis.__extends || function() {
  var e = function(t, r) {
    return e = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(n, a) {
      n.__proto__ = a;
    } || function(n, a) {
      for (var i in a)
        Object.prototype.hasOwnProperty.call(a, i) && (n[i] = a[i]);
    }, e(t, r);
  };
  return function(t, r) {
    if (typeof r != "function" && r !== null)
      throw new TypeError("Class extends value " + String(r) + " is not a constructor or null");
    e(t, r);
    function n() {
      this.constructor = t;
    }
    t.prototype = r === null ? Object.create(r) : (n.prototype = r.prototype, new n());
  };
}(), oo = globalThis && globalThis.__generator || function(e, t) {
  var r = { label: 0, sent: function() {
    if (i[0] & 1)
      throw i[1];
    return i[1];
  }, trys: [], ops: [] }, n, a, i, s;
  return s = { next: c(0), throw: c(1), return: c(2) }, typeof Symbol == "function" && (s[Symbol.iterator] = function() {
    return this;
  }), s;
  function c(o) {
    return function(f) {
      return u([o, f]);
    };
  }
  function u(o) {
    if (n)
      throw new TypeError("Generator is already executing.");
    for (; r; )
      try {
        if (n = 1, a && (i = o[0] & 2 ? a.return : o[0] ? a.throw || ((i = a.return) && i.call(a), 0) : a.next) && !(i = i.call(a, o[1])).done)
          return i;
        switch (a = 0, i && (o = [o[0] & 2, i.value]), o[0]) {
          case 0:
          case 1:
            i = o;
            break;
          case 4:
            return r.label++, { value: o[1], done: !1 };
          case 5:
            r.label++, a = o[1], o = [0];
            continue;
          case 7:
            o = r.ops.pop(), r.trys.pop();
            continue;
          default:
            if (i = r.trys, !(i = i.length > 0 && i[i.length - 1]) && (o[0] === 6 || o[0] === 2)) {
              r = 0;
              continue;
            }
            if (o[0] === 3 && (!i || o[1] > i[0] && o[1] < i[3])) {
              r.label = o[1];
              break;
            }
            if (o[0] === 6 && r.label < i[1]) {
              r.label = i[1], i = o;
              break;
            }
            if (i && r.label < i[2]) {
              r.label = i[2], r.ops.push(o);
              break;
            }
            i[2] && r.ops.pop(), r.trys.pop();
            continue;
        }
        o = t.call(e, r);
      } catch (f) {
        o = [6, f], a = 0;
      } finally {
        n = i = 0;
      }
    if (o[0] & 5)
      throw o[1];
    return { value: o[0] ? o[1] : void 0, done: !0 };
  }
}, De = globalThis && globalThis.__spreadArray || function(e, t) {
  for (var r = 0, n = t.length, a = e.length; r < n; r++, a++)
    e[a] = t[r];
  return e;
}, uo = Object.defineProperty, so = Object.defineProperties, co = Object.getOwnPropertyDescriptors, Tn = Object.getOwnPropertySymbols, lo = Object.prototype.hasOwnProperty, fo = Object.prototype.propertyIsEnumerable, Pn = function(e, t, r) {
  return t in e ? uo(e, t, { enumerable: !0, configurable: !0, writable: !0, value: r }) : e[t] = r;
}, ee = function(e, t) {
  for (var r in t || (t = {}))
    lo.call(t, r) && Pn(e, r, t[r]);
  if (Tn)
    for (var n = 0, a = Tn(t); n < a.length; n++) {
      var r = a[n];
      fo.call(t, r) && Pn(e, r, t[r]);
    }
  return e;
}, hr = function(e, t) {
  return so(e, co(t));
}, vo = function(e, t, r) {
  return new Promise(function(n, a) {
    var i = function(u) {
      try {
        c(r.next(u));
      } catch (o) {
        a(o);
      }
    }, s = function(u) {
      try {
        c(r.throw(u));
      } catch (o) {
        a(o);
      }
    }, c = function(u) {
      return u.done ? n(u.value) : Promise.resolve(u.value).then(i, s);
    };
    c((r = r.apply(e, t)).next());
  });
}, Oe = function() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  var r = Ee.apply(void 0, e), n = function(a) {
    for (var i = [], s = 1; s < arguments.length; s++)
      i[s - 1] = arguments[s];
    return r.apply(void 0, De([re(a) ? ha(a) : a], i));
  };
  return n;
}, po = typeof window < "u" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : function() {
  if (arguments.length !== 0)
    return typeof arguments[0] == "object" ? Tt : Tt.apply(null, arguments);
};
function be(e) {
  if (typeof e != "object" || e === null)
    return !1;
  var t = Object.getPrototypeOf(e);
  if (t === null)
    return !0;
  for (var r = t; Object.getPrototypeOf(r) !== null; )
    r = Object.getPrototypeOf(r);
  return t === r;
}
var yo = (
  /** @class */
  function(e) {
    wa(t, e);
    function t() {
      for (var r = [], n = 0; n < arguments.length; n++)
        r[n] = arguments[n];
      var a = e.apply(this, r) || this;
      return Object.setPrototypeOf(a, t.prototype), a;
    }
    return Object.defineProperty(t, Symbol.species, {
      get: function() {
        return t;
      },
      enumerable: !1,
      configurable: !0
    }), t.prototype.concat = function() {
      for (var r = [], n = 0; n < arguments.length; n++)
        r[n] = arguments[n];
      return e.prototype.concat.apply(this, r);
    }, t.prototype.prepend = function() {
      for (var r = [], n = 0; n < arguments.length; n++)
        r[n] = arguments[n];
      return r.length === 1 && Array.isArray(r[0]) ? new (t.bind.apply(t, De([void 0], r[0].concat(this))))() : new (t.bind.apply(t, De([void 0], r.concat(this))))();
    }, t;
  }(Array)
), ho = (
  /** @class */
  function(e) {
    wa(t, e);
    function t() {
      for (var r = [], n = 0; n < arguments.length; n++)
        r[n] = arguments[n];
      var a = e.apply(this, r) || this;
      return Object.setPrototypeOf(a, t.prototype), a;
    }
    return Object.defineProperty(t, Symbol.species, {
      get: function() {
        return t;
      },
      enumerable: !1,
      configurable: !0
    }), t.prototype.concat = function() {
      for (var r = [], n = 0; n < arguments.length; n++)
        r[n] = arguments[n];
      return e.prototype.concat.apply(this, r);
    }, t.prototype.prepend = function() {
      for (var r = [], n = 0; n < arguments.length; n++)
        r[n] = arguments[n];
      return r.length === 1 && Array.isArray(r[0]) ? new (t.bind.apply(t, De([void 0], r[0].concat(this))))() : new (t.bind.apply(t, De([void 0], r.concat(this))))();
    }, t;
  }(Array)
);
function kr(e) {
  return se(e) ? ze(e, function() {
  }) : e;
}
function go(e) {
  return typeof e == "boolean";
}
function mo() {
  return function(t) {
    return bo(t);
  };
}
function bo(e) {
  e === void 0 && (e = {});
  var t = e.thunk, r = t === void 0 ? !0 : t;
  e.immutableCheck, e.serializableCheck;
  var n = new yo();
  return r && (go(r) ? n.push(An) : n.push(An.withExtraArgument(r.extraArgument))), n;
}
var So = !0;
function Oo(e) {
  var t = mo(), r = e || {}, n = r.reducer, a = n === void 0 ? void 0 : n, i = r.middleware, s = i === void 0 ? t() : i, c = r.devTools, u = c === void 0 ? !0 : c, o = r.preloadedState, f = o === void 0 ? void 0 : o, g = r.enhancers, h = g === void 0 ? void 0 : g, l;
  if (typeof a == "function")
    l = a;
  else if (be(a))
    l = Jr(a);
  else
    throw new Error('"reducer" is a required argument, and must be a function or an object of functions that can be passed to combineReducers');
  var d = s;
  typeof d == "function" && (d = d(t));
  var p = Zi.apply(void 0, d), S = Tt;
  u && (S = po(ee({
    trace: !So
  }, typeof u == "object" && u)));
  var A = new ho(p), b = A;
  Array.isArray(h) ? b = De([p], h) : typeof h == "function" && (b = h(A));
  var v = S.apply(void 0, b);
  return ba(l, f, v);
}
function te(e, t) {
  function r() {
    for (var n = [], a = 0; a < arguments.length; a++)
      n[a] = arguments[a];
    if (t) {
      var i = t.apply(void 0, n);
      if (!i)
        throw new Error("prepareAction did not return an object");
      return ee(ee({
        type: e,
        payload: i.payload
      }, "meta" in i && { meta: i.meta }), "error" in i && { error: i.error });
    }
    return { type: e, payload: n[0] };
  }
  return r.toString = function() {
    return "" + e;
  }, r.type = e, r.match = function(n) {
    return n.type === e;
  }, r;
}
function wo(e) {
  return be(e) && "type" in e;
}
function Ao(e) {
  return wo(e) && typeof e.type == "string" && Object.keys(e).every(To);
}
function To(e) {
  return ["type", "payload", "error", "meta"].indexOf(e) > -1;
}
function Aa(e) {
  var t = {}, r = [], n, a = {
    addCase: function(i, s) {
      var c = typeof i == "string" ? i : i.type;
      if (c in t)
        throw new Error("addCase cannot be called with two reducers for the same action type");
      return t[c] = s, a;
    },
    addMatcher: function(i, s) {
      return r.push({ matcher: i, reducer: s }), a;
    },
    addDefaultCase: function(i) {
      return n = i, a;
    }
  };
  return e(a), [t, r, n];
}
function Po(e) {
  return typeof e == "function";
}
function Ro(e, t, r, n) {
  r === void 0 && (r = []);
  var a = typeof t == "function" ? Aa(t) : [t, r, n], i = a[0], s = a[1], c = a[2], u;
  if (Po(e))
    u = function() {
      return kr(e());
    };
  else {
    var o = kr(e);
    u = function() {
      return o;
    };
  }
  function f(g, h) {
    g === void 0 && (g = u());
    var l = De([
      i[h.type]
    ], s.filter(function(d) {
      var p = d.matcher;
      return p(h);
    }).map(function(d) {
      var p = d.reducer;
      return p;
    }));
    return l.filter(function(d) {
      return !!d;
    }).length === 0 && (l = [c]), l.reduce(function(d, p) {
      if (p)
        if (re(d)) {
          var S = d, A = p(S, h);
          return A === void 0 ? d : A;
        } else {
          if (se(d))
            return ze(d, function(b) {
              return p(b, h);
            });
          var A = p(d, h);
          if (A === void 0) {
            if (d === null)
              return d;
            throw Error("A case reducer on a non-draftable value must not return undefined");
          }
          return A;
        }
      return d;
    }, g);
  }
  return f.getInitialState = u, f;
}
function Eo(e, t) {
  return e + "/" + t;
}
function ge(e) {
  var t = e.name;
  if (!t)
    throw new Error("`name` is a required option for createSlice");
  typeof process < "u";
  var r = typeof e.initialState == "function" ? e.initialState : kr(e.initialState), n = e.reducers || {}, a = Object.keys(n), i = {}, s = {}, c = {};
  a.forEach(function(f) {
    var g = n[f], h = Eo(t, f), l, d;
    "reducer" in g ? (l = g.reducer, d = g.prepare) : l = g, i[f] = l, s[h] = l, c[f] = d ? te(h, d) : te(h);
  });
  function u() {
    var f = typeof e.extraReducers == "function" ? Aa(e.extraReducers) : [e.extraReducers], g = f[0], h = g === void 0 ? {} : g, l = f[1], d = l === void 0 ? [] : l, p = f[2], S = p === void 0 ? void 0 : p, A = ee(ee({}, h), s);
    return Ro(r, function(b) {
      for (var v in A)
        b.addCase(v, A[v]);
      for (var y = 0, O = d; y < O.length; y++) {
        var m = O[y];
        b.addMatcher(m.matcher, m.reducer);
      }
      S && b.addDefaultCase(S);
    });
  }
  var o;
  return {
    name: t,
    reducer: function(f, g) {
      return o || (o = u()), o(f, g);
    },
    actions: c,
    caseReducers: i,
    getInitialState: function() {
      return o || (o = u()), o.getInitialState();
    }
  };
}
function Io() {
  return {
    ids: [],
    entities: {}
  };
}
function Mo() {
  function e(t) {
    return t === void 0 && (t = {}), Object.assign(Io(), t);
  }
  return { getInitialState: e };
}
function ko() {
  function e(t) {
    var r = function(o) {
      return o.ids;
    }, n = function(o) {
      return o.entities;
    }, a = Oe(r, n, function(o, f) {
      return o.map(function(g) {
        return f[g];
      });
    }), i = function(o, f) {
      return f;
    }, s = function(o, f) {
      return o[f];
    }, c = Oe(r, function(o) {
      return o.length;
    });
    if (!t)
      return {
        selectIds: r,
        selectEntities: n,
        selectAll: a,
        selectTotal: c,
        selectById: Oe(n, i, s)
      };
    var u = Oe(t, n);
    return {
      selectIds: Oe(t, r),
      selectEntities: u,
      selectAll: Oe(t, a),
      selectTotal: Oe(t, c),
      selectById: Oe(u, i, s)
    };
  }
  return { getSelectors: e };
}
function Co(e) {
  var t = G(function(r, n) {
    return e(n);
  });
  return function(n) {
    return t(n, void 0);
  };
}
function G(e) {
  return function(r, n) {
    function a(s) {
      return Ao(s);
    }
    var i = function(s) {
      a(n) ? e(n.payload, s) : e(n, s);
    };
    return re(r) ? (i(r), r) : ze(r, i);
  };
}
function Je(e, t) {
  var r = t(e);
  return r;
}
function qe(e) {
  return Array.isArray(e) || (e = Object.values(e)), e;
}
function Ta(e, t, r) {
  e = qe(e);
  for (var n = [], a = [], i = 0, s = e; i < s.length; i++) {
    var c = s[i], u = Je(c, t);
    u in r.entities ? a.push({ id: u, changes: c }) : n.push(c);
  }
  return [n, a];
}
function Pa(e) {
  function t(d, p) {
    var S = Je(d, e);
    S in p.entities || (p.ids.push(S), p.entities[S] = d);
  }
  function r(d, p) {
    d = qe(d);
    for (var S = 0, A = d; S < A.length; S++) {
      var b = A[S];
      t(b, p);
    }
  }
  function n(d, p) {
    var S = Je(d, e);
    S in p.entities || p.ids.push(S), p.entities[S] = d;
  }
  function a(d, p) {
    d = qe(d);
    for (var S = 0, A = d; S < A.length; S++) {
      var b = A[S];
      n(b, p);
    }
  }
  function i(d, p) {
    d = qe(d), p.ids = [], p.entities = {}, r(d, p);
  }
  function s(d, p) {
    return c([d], p);
  }
  function c(d, p) {
    var S = !1;
    d.forEach(function(A) {
      A in p.entities && (delete p.entities[A], S = !0);
    }), S && (p.ids = p.ids.filter(function(A) {
      return A in p.entities;
    }));
  }
  function u(d) {
    Object.assign(d, {
      ids: [],
      entities: {}
    });
  }
  function o(d, p, S) {
    var A = S.entities[p.id], b = Object.assign({}, A, p.changes), v = Je(b, e), y = v !== p.id;
    return y && (d[p.id] = v, delete S.entities[p.id]), S.entities[v] = b, y;
  }
  function f(d, p) {
    return g([d], p);
  }
  function g(d, p) {
    var S = {}, A = {};
    d.forEach(function(y) {
      y.id in p.entities && (A[y.id] = {
        id: y.id,
        changes: ee(ee({}, A[y.id] ? A[y.id].changes : null), y.changes)
      });
    }), d = Object.values(A);
    var b = d.length > 0;
    if (b) {
      var v = d.filter(function(y) {
        return o(S, y, p);
      }).length > 0;
      v && (p.ids = Object.keys(p.entities));
    }
  }
  function h(d, p) {
    return l([d], p);
  }
  function l(d, p) {
    var S = Ta(d, e, p), A = S[0], b = S[1];
    g(b, p), r(A, p);
  }
  return {
    removeAll: Co(u),
    addOne: G(t),
    addMany: G(r),
    setOne: G(n),
    setMany: G(a),
    setAll: G(i),
    updateOne: G(f),
    updateMany: G(g),
    upsertOne: G(h),
    upsertMany: G(l),
    removeOne: G(s),
    removeMany: G(c)
  };
}
function xo(e, t) {
  var r = Pa(e), n = r.removeOne, a = r.removeMany, i = r.removeAll;
  function s(b, v) {
    return c([b], v);
  }
  function c(b, v) {
    b = qe(b);
    var y = b.filter(function(O) {
      return !(Je(O, e) in v.entities);
    });
    y.length !== 0 && S(y, v);
  }
  function u(b, v) {
    return o([b], v);
  }
  function o(b, v) {
    b = qe(b), b.length !== 0 && S(b, v);
  }
  function f(b, v) {
    b = qe(b), v.entities = {}, v.ids = [], c(b, v);
  }
  function g(b, v) {
    return h([b], v);
  }
  function h(b, v) {
    for (var y = !1, O = 0, m = b; O < m.length; O++) {
      var w = m[O], P = v.entities[w.id];
      if (P) {
        y = !0, Object.assign(P, w.changes);
        var R = e(P);
        w.id !== R && (delete v.entities[w.id], v.entities[R] = P);
      }
    }
    y && A(v);
  }
  function l(b, v) {
    return d([b], v);
  }
  function d(b, v) {
    var y = Ta(b, e, v), O = y[0], m = y[1];
    h(m, v), c(O, v);
  }
  function p(b, v) {
    if (b.length !== v.length)
      return !1;
    for (var y = 0; y < b.length && y < v.length; y++)
      if (b[y] !== v[y])
        return !1;
    return !0;
  }
  function S(b, v) {
    b.forEach(function(y) {
      v.entities[e(y)] = y;
    }), A(v);
  }
  function A(b) {
    var v = Object.values(b.entities);
    v.sort(t);
    var y = v.map(e), O = b.ids;
    p(O, y) || (b.ids = y);
  }
  return {
    removeOne: n,
    removeMany: a,
    removeAll: i,
    addOne: G(s),
    updateOne: G(g),
    upsertOne: G(l),
    setOne: G(u),
    setMany: G(o),
    setAll: G(f),
    addMany: G(c),
    updateMany: G(h),
    upsertMany: G(d)
  };
}
function Ra(e) {
  e === void 0 && (e = {});
  var t = ee({
    sortComparer: !1,
    selectId: function(c) {
      return c.id;
    }
  }, e), r = t.selectId, n = t.sortComparer, a = Mo(), i = ko(), s = n ? xo(r, n) : Pa(r);
  return ee(ee(ee({
    selectId: r,
    sortComparer: n
  }, a), i), s);
}
var qo = "ModuleSymbhasOwnPr-0123456789ABCDEFGHNRVfgctiUvz_KqYTJkLxpZXIjQW", me = function(e) {
  e === void 0 && (e = 21);
  for (var t = "", r = e; r--; )
    t += qo[Math.random() * 64 | 0];
  return t;
}, $o = [
  "name",
  "message",
  "stack",
  "code"
], gr = (
  /** @class */
  function() {
    function e(t, r) {
      this.payload = t, this.meta = r;
    }
    return e;
  }()
), Rn = (
  /** @class */
  function() {
    function e(t, r) {
      this.payload = t, this.meta = r;
    }
    return e;
  }()
), Do = function(e) {
  if (typeof e == "object" && e !== null) {
    for (var t = {}, r = 0, n = $o; r < n.length; r++) {
      var a = n[r];
      typeof e[a] == "string" && (t[a] = e[a]);
    }
    return t;
  }
  return { message: String(e) };
}, En = function() {
  function e(t, r, n) {
    var a = te(t + "/fulfilled", function(o, f, g, h) {
      return {
        payload: o,
        meta: hr(ee({}, h || {}), {
          arg: g,
          requestId: f,
          requestStatus: "fulfilled"
        })
      };
    }), i = te(t + "/pending", function(o, f, g) {
      return {
        payload: void 0,
        meta: hr(ee({}, g || {}), {
          arg: f,
          requestId: o,
          requestStatus: "pending"
        })
      };
    }), s = te(t + "/rejected", function(o, f, g, h, l) {
      return {
        payload: h,
        error: (n && n.serializeError || Do)(o || "Rejected"),
        meta: hr(ee({}, l || {}), {
          arg: g,
          requestId: f,
          rejectedWithValue: !!h,
          requestStatus: "rejected",
          aborted: (o == null ? void 0 : o.name) === "AbortError",
          condition: (o == null ? void 0 : o.name) === "ConditionError"
        })
      };
    }), c = typeof AbortController < "u" ? AbortController : (
      /** @class */
      function() {
        function o() {
          this.signal = {
            aborted: !1,
            addEventListener: function() {
            },
            dispatchEvent: function() {
              return !1;
            },
            onabort: function() {
            },
            removeEventListener: function() {
            },
            reason: void 0,
            throwIfAborted: function() {
            }
          };
        }
        return o.prototype.abort = function() {
        }, o;
      }()
    );
    function u(o) {
      return function(f, g, h) {
        var l = n != null && n.idGenerator ? n.idGenerator(o) : me(), d = new c(), p;
        function S(b) {
          p = b, d.abort();
        }
        var A = function() {
          return vo(this, null, function() {
            var b, v, y, O, m, w, P;
            return oo(this, function(R) {
              switch (R.label) {
                case 0:
                  return R.trys.push([0, 4, , 5]), O = (b = n == null ? void 0 : n.condition) == null ? void 0 : b.call(n, o, { getState: g, extra: h }), jo(O) ? [4, O] : [3, 2];
                case 1:
                  O = R.sent(), R.label = 2;
                case 2:
                  if (O === !1 || d.signal.aborted)
                    throw {
                      name: "ConditionError",
                      message: "Aborted due to condition callback returning false."
                    };
                  return m = new Promise(function(T, E) {
                    return d.signal.addEventListener("abort", function() {
                      return E({
                        name: "AbortError",
                        message: p || "Aborted"
                      });
                    });
                  }), f(i(l, o, (v = n == null ? void 0 : n.getPendingMeta) == null ? void 0 : v.call(n, { requestId: l, arg: o }, { getState: g, extra: h }))), [4, Promise.race([
                    m,
                    Promise.resolve(r(o, {
                      dispatch: f,
                      getState: g,
                      extra: h,
                      requestId: l,
                      signal: d.signal,
                      abort: S,
                      rejectWithValue: function(T, E) {
                        return new gr(T, E);
                      },
                      fulfillWithValue: function(T, E) {
                        return new Rn(T, E);
                      }
                    })).then(function(T) {
                      if (T instanceof gr)
                        throw T;
                      return T instanceof Rn ? a(T.payload, l, o, T.meta) : a(T, l, o);
                    })
                  ])];
                case 3:
                  return y = R.sent(), [3, 5];
                case 4:
                  return w = R.sent(), y = w instanceof gr ? s(null, l, o, w.payload, w.meta) : s(w, l, o), [3, 5];
                case 5:
                  return P = n && !n.dispatchConditionRejection && s.match(y) && y.meta.condition, P || f(y), [2, y];
              }
            });
          });
        }();
        return Object.assign(A, {
          abort: S,
          requestId: l,
          arg: o,
          unwrap: function() {
            return A.then(Qo);
          }
        });
      };
    }
    return Object.assign(u, {
      pending: i,
      rejected: s,
      fulfilled: a,
      typePrefix: t
    });
  }
  return e.withTypes = function() {
    return e;
  }, e;
}();
function Qo(e) {
  if (e.meta && e.meta.rejectedWithValue)
    throw e.payload;
  if (e.error)
    throw e.error;
  return e.payload;
}
function jo(e) {
  return e !== null && typeof e == "object" && typeof e.then == "function";
}
var _o = function(e) {
  return e && typeof e.match == "function";
}, Ea = function(e, t) {
  return _o(e) ? e.match(t) : e(t);
};
function Ke() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return function(r) {
    return e.some(function(n) {
      return Ea(n, r);
    });
  };
}
function Xe() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return function(r) {
    return e.every(function(n) {
      return Ea(n, r);
    });
  };
}
function nr(e, t) {
  if (!e || !e.meta)
    return !1;
  var r = typeof e.meta.requestId == "string", n = t.indexOf(e.meta.requestStatus) > -1;
  return r && n;
}
function ut(e) {
  return typeof e[0] == "function" && "pending" in e[0] && "fulfilled" in e[0] && "rejected" in e[0];
}
function Xr() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return e.length === 0 ? function(r) {
    return nr(r, ["pending"]);
  } : ut(e) ? function(r) {
    var n = e.map(function(i) {
      return i.pending;
    }), a = Ke.apply(void 0, n);
    return a(r);
  } : Xr()(e[0]);
}
function tt() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return e.length === 0 ? function(r) {
    return nr(r, ["rejected"]);
  } : ut(e) ? function(r) {
    var n = e.map(function(i) {
      return i.rejected;
    }), a = Ke.apply(void 0, n);
    return a(r);
  } : tt()(e[0]);
}
function ar() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  var r = function(n) {
    return n && n.meta && n.meta.rejectedWithValue;
  };
  return e.length === 0 ? function(n) {
    var a = Xe(tt.apply(void 0, e), r);
    return a(n);
  } : ut(e) ? function(n) {
    var a = Xe(tt.apply(void 0, e), r);
    return a(n);
  } : ar()(e[0]);
}
function Qe() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return e.length === 0 ? function(r) {
    return nr(r, ["fulfilled"]);
  } : ut(e) ? function(r) {
    var n = e.map(function(i) {
      return i.fulfilled;
    }), a = Ke.apply(void 0, n);
    return a(r);
  } : Qe()(e[0]);
}
function Cr() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return e.length === 0 ? function(r) {
    return nr(r, ["pending", "fulfilled", "rejected"]);
  } : ut(e) ? function(r) {
    for (var n = [], a = 0, i = e; a < i.length; a++) {
      var s = i[a];
      n.push(s.pending, s.rejected, s.fulfilled);
    }
    var c = Ke.apply(void 0, n);
    return c(r);
  } : Cr()(e[0]);
}
var Yr = "listenerMiddleware";
te(Yr + "/add");
te(Yr + "/removeAll");
te(Yr + "/remove");
var He = "RTK_autoBatch", mr = function() {
  return function(e) {
    var t;
    return {
      payload: e,
      meta: (t = {}, t[He] = !0, t)
    };
  };
}, In;
typeof queueMicrotask == "function" && queueMicrotask.bind(typeof window < "u" ? window : typeof global < "u" ? global : globalThis);
Fi();
const st = (e) => ({
  ...e,
  sections: Object.keys(e.sections).sort((t, r) => e.sections[t].weight - e.sections[r].weight).reduce(
    (t, r, n) => ({
      ...t,
      [r]: {
        ...e.sections[r],
        weight: n
      }
    }),
    {}
  ),
  components: Object.keys(e.components).sort((t, r) => e.components[t].weight - e.components[r].weight).reduce(
    (t, r, n) => ({
      ...t,
      [r]: {
        ...e.components[r],
        weight: n
      }
    }),
    {}
  )
}), No = (e) => {
  const t = {
    sections: {},
    regions: {},
    components: {}
  };
  return e.sections.forEach((r, n) => {
    const { decoupled_lb_api: a, ...i } = r.third_party_settings, s = a.uuid;
    t.sections[s] = {
      weight: n,
      id: s,
      ...r,
      third_party_settings: i
    };
    const c = {};
    Object.keys(r.components).forEach((u) => {
      const o = r.components[u];
      if (o.region in c) {
        t.components[u] = {
          ...o,
          regionId: c[o.region]
        };
        return;
      }
      const f = a.region_uuids[o.region] || me();
      c[o.region] = f, t.regions[f] = {
        id: f,
        region: o.region,
        section: s
      }, t.components[u] = {
        ...o,
        regionId: f
      };
    });
  }), st(t);
}, Lo = (e) => ({
  sections: Object.keys(e.sections).sort((r, n) => e.sections[r].weight - e.sections[n].weight).map((r) => {
    const n = Object.entries(e.regions).filter(([c, u]) => u.section === r).map(([c]) => c), { layout_id: a, layout_settings: i, third_party_settings: s } = e.sections[r];
    return {
      layout_id: a,
      layout_settings: i,
      third_party_settings: {
        ...s,
        decoupled_lb_api: {
          uuid: r,
          region_uuids: n.reduce(
            (c, u) => ({
              ...c,
              [e.regions[u].region]: u
            }),
            {}
          )
        }
      },
      components: Object.entries(e.components).filter(
        ([c, u]) => n.includes(u.regionId)
      ).reduce(
        (c, [u, o]) => ({
          ...c,
          [u]: {
            uuid: o.uuid,
            configuration: o.configuration,
            weight: o.weight,
            additional: o.additional,
            region: e.regions[o.regionId].region,
            fallback: o.fallback
          }
        }),
        {}
      )
    };
  })
}), Ia = "idle", Fo = "loading", zo = "error", Ko = "clean", Ma = "saving", je = "dirty", Uo = (e, t) => ({
  ...e,
  layout: No(t.payload),
  state: Ko
}), Bo = (e, t) => {
  const { sections: r } = e.layout, { from: n, to: a } = t.payload, i = Object.keys(r), s = i[n], c = [
    ...i.slice(0, n),
    ...i.slice(n + 1)
  ];
  return c.splice(a, 0, s), {
    ...e,
    state: je,
    layout: {
      ...e.layout,
      sections: c.reduce(
        (u, o, f) => ({
          ...u,
          [o]: {
            ...e.layout.sections[o],
            weight: f
          }
        }),
        {}
      )
    }
  };
}, Wo = (e, t) => {
  const { droppableId: r, index: n } = t.payload.from, { droppableId: a, index: i } = t.payload.to, s = t.payload.componentUuid;
  let c = Object.entries(e.layout.components).map(([, u]) => u).filter((u) => u.regionId === a).map((u) => u.uuid);
  return r === a && (c = [
    ...c.slice(0, n),
    ...c.slice(n + 1)
  ]), c.splice(i, 0, s), {
    ...e,
    state: je,
    layout: st({
      regions: e.layout.regions,
      sections: e.layout.sections,
      components: {
        ...e.layout.components,
        // Update all components in the destination region.
        ...c.reduce(
          (u, o, f) => ({
            ...u,
            [o]: {
              ...e.layout.components[o],
              // Give them a new weight based on the new order.
              weight: f,
              // Update the moved item's region in case of movement between
              // regions. We don't need to limit this to the moved component
              // because at this point we're working only with component IDs
              // that are in this region.
              regionId: a
            }
          }),
          {}
        )
      }
    })
  };
}, Vo = (e, t) => ({
  ...e,
  state: Fo,
  sectionStorage: t.payload.sectionStorage,
  sectionStorageType: t.payload.sectionStorageType
}), Ho = (e, t) => ({
  ...e,
  errors: t.payload,
  state: zo
}), Go = (e, t) => ({
  ...e,
  state: je,
  layout: {
    ...e.layout,
    components: {
      ...e.layout.components,
      [t.payload.componentId]: t.payload.values
    }
  }
}), Jo = (e, t) => {
  const { sections: r } = e.layout, { at: n, sectionInfo: a } = t.payload, i = Object.keys(r), s = me(32);
  i.splice(n, 0, s);
  const c = Object.keys(a.region_labels).reduce(
    (u, o) => {
      const f = me();
      return {
        ...u,
        [f]: { id: f, region: o, section: s }
      };
    },
    {}
  );
  return {
    ...e,
    state: je,
    layout: st({
      ...e.layout,
      regions: {
        ...e.layout.regions,
        ...c
      },
      sections: i.reduce(
        (u, o, f) => ({
          ...u,
          [o]: {
            ...o === s ? {
              layout_id: a.id,
              layout_settings: { label: a.label },
              components: {},
              id: s
            } : e.layout.sections[o],
            weight: f
          }
        }),
        {}
      )
    })
  };
}, Xo = (e, t) => {
  const { blockInfo: r } = t.payload, { droppableId: n, index: a } = t.payload.at, i = me(32), s = {
    additional: {},
    configuration: {
      ...r.default_configuration,
      label: r.label
    },
    weight: 0,
    uuid: i,
    regionId: n
  }, c = Object.entries(e.layout.components).map(([, u]) => u).filter((u) => u.regionId === n).map((u) => u.uuid);
  return c.splice(a, 0, i), {
    ...e,
    state: je,
    layout: st({
      regions: e.layout.regions,
      sections: e.layout.sections,
      components: {
        ...e.layout.components,
        // Update all components in the destination region.
        ...c.reduce(
          (u, o, f) => ({
            ...u,
            [o]: {
              ...o === i ? s : e.layout.components[o],
              // Give them a new weight based on the new order.
              weight: f
            }
          }),
          {}
        )
      }
    })
  };
}, Yo = (e, t) => {
  const { sections: r } = e.layout, { at: n, sectionInfo: a, blockInfo: i } = t.payload, s = Object.keys(r), c = me(32);
  s.splice(n, 0, c);
  const u = Object.keys(
    a.region_labels
  ).reduce((h, l) => {
    const d = me();
    return {
      ...h,
      [d]: { id: d, region: l, section: c }
    };
  }, {}), o = me(32), f = Object.entries(u).filter(([h, l]) => l.region === i.default_region).map(([h]) => h).pop() || "content", g = {
    additional: {},
    configuration: {
      ...i.default_configuration,
      label: i.label
    },
    weight: 0,
    uuid: o,
    regionId: f
  };
  return {
    ...e,
    state: je,
    layout: st({
      ...e.layout,
      regions: {
        ...e.layout.regions,
        ...u
      },
      components: {
        ...e.layout.components,
        [o]: g
      },
      sections: s.reduce(
        (h, l, d) => ({
          ...h,
          [l]: {
            ...l === c ? {
              layout_id: a.id,
              layout_settings: { label: a.label },
              id: c
            } : e.layout.sections[l],
            weight: d
          }
        }),
        {}
      )
    })
  };
}, Zo = (e, t) => ({
  ...e,
  state: je,
  layout: {
    ...e.layout,
    sections: {
      ...e.layout.sections,
      [t.payload.sectionId]: {
        ...e.layout.sections[t.payload.sectionId],
        layout_settings: {
          ...t.payload.layoutSettings
        }
      }
    }
  }
}), eu = (e) => ({
  ...e,
  state: Ma
}), tu = {
  reorderLayoutSections: Bo,
  moveBlock: Wo,
  loadLayoutStart: Vo,
  loadOrSaveLayoutSuccess: Uo,
  loadOrSaveLayoutFailure: Ho,
  updateBlock: Go,
  addNewSection: Jo,
  addNewBlock: Xo,
  addNewBlockAndSection: Yo,
  updateLayoutSettings: Zo,
  saveLayoutStart: eu
};
class ct extends Error {
  constructor(r, n) {
    super(r);
    nn(this, "errors");
    this.errors = n;
  }
  getErrors() {
    return this.errors;
  }
}
const ru = ({
  baseUrl: e,
  sectionStorage: t,
  sectionStorageType: r
}) => fetch(
  `${e}api/v1/layout-builder/layout/${r}/${t}`
).then(async (n) => {
  if (n.ok)
    return (await n.json()).data;
  const a = await n.json();
  throw new ct(n.statusText, a.errors);
}), nu = ({ baseUrl: e, sectionStorage: t, sectionStorageType: r }, n) => fetch(`${e}session/token`).then((a) => a.text()).then((a) => fetch(
  `${e}api/v1/layout-builder/layout/${r}/${t}`,
  {
    method: "PUT",
    body: JSON.stringify({ data: Lo(n.layout) }),
    headers: {
      "X-CSRF-Token": a
    }
  }
).then(async (i) => {
  if (i.ok)
    return (await i.json()).data;
  const s = await i.json();
  throw new ct(i.statusText, s.errors);
})), xr = {
  errors: {},
  sectionStorageType: "defaults",
  sectionStorage: "",
  layout: {
    sections: {},
    components: {},
    regions: {}
  },
  state: Ia
}, _e = "layout", rt = ge({
  name: _e,
  initialState: xr,
  reducers: tu
}), {
  loadLayoutStart: au,
  loadOrSaveLayoutSuccess: ka,
  loadOrSaveLayoutFailure: Rt,
  reorderLayoutSections: Ks,
  moveBlock: Us,
  addNewSection: Bs,
  addNewBlockAndSection: Ws,
  addNewBlock: Vs,
  saveLayoutStart: iu
} = rt.actions, Mn = (e) => rt.actions.updateBlock(e), Hs = (e) => rt.actions.updateLayoutSettings(e), ou = (e) => e[_e], Gs = (e) => e[_e].layout.sections, Js = (e) => e[_e].layout.components, Xs = (e) => e[_e].layout.regions, Ca = (e) => e[_e].state, Ys = (e, t) => e[_e].layout.regions[t] ?? null, Zs = (e) => async (t, r) => {
  if (Ca(r()) === Ia) {
    t(au(e));
    try {
      const a = await ru(e);
      t(ka(a));
    } catch (a) {
      if (a instanceof ct) {
        t(Rt(a.getErrors()));
        return;
      }
      t(
        Rt({
          fetch: { message: String(a), identifier: "fetch" }
        })
      );
    }
  }
}, ec = (e) => async (t, r) => {
  const n = r();
  if (Ca(n) !== Ma) {
    t(iu());
    try {
      const i = await nu(e, ou(n));
      t(ka(i));
    } catch (i) {
      if (i instanceof ct) {
        t(Rt(i.getErrors()));
        return;
      }
      t(
        Rt({
          fetch: { message: String(i), identifier: "fetch" }
        })
      );
    }
  }
}, uu = (e, t) => ({
  ...e,
  endSidebarOpen: t.payload
}), su = (e, t) => ({
  ...e,
  hasUnsavedChanges: t.payload
}), cu = (e, t) => ({
  ...e,
  selectedComponent: t.payload.uuid,
  selectedSection: null,
  mode: t.payload.mode ? t.payload.mode : e.mode
}), lu = (e, t) => ({
  ...e,
  activeDrag: t.payload
}), fu = (e, t) => ({
  ...e,
  startSidebarOpen: t.payload
}), du = (e, t) => ({
  ...e,
  startSidebarOpen: !0,
  activeStartSidebarTab: t.payload
}), vu = (e, t) => ({
  ...e,
  endSidebarOpen: !0,
  activeEndSidebarTab: t.payload
}), pu = (e, t) => ({
  ...e,
  mode: t.payload
}), yu = (e, t) => ({
  ...e,
  selectedSection: t.payload
}), hu = {
  setActiveDrag: lu,
  setUnsavedChanges: su,
  setEndSidebarOpen: uu,
  setMode: pu,
  setStartSidebarOpen: fu,
  setSelectedComponent: cu,
  setActiveSidebarStartPane: du,
  setActiveSidebarEndPane: vu,
  setSelectedSection: yu
}, tc = "edit", gu = "select", mu = "entity", rc = "settings", bu = "insert", nc = "outline", ac = "section", ic = "block", oc = "outline-block", uc = "outline-section", sc = "insert-block", cc = "insert-section", Su = {
  activeStartSidebarTab: bu,
  mode: gu,
  selectedComponent: null,
  selectedSection: null,
  startSidebarOpen: !1,
  primaryAction: {
    label: "Save"
  },
  secondaryActions: [],
  endSidebarOpen: !1,
  activeEndSidebarTab: mu,
  activeDrag: null
}, Ue = "ui", qr = ge({
  name: Ue,
  initialState: Su,
  reducers: hu
}), {
  setUnsavedChanges: lc,
  setEndSidebarOpen: fc,
  setStartSidebarOpen: dc,
  setActiveSidebarStartPane: vc,
  setSelectedComponent: pc,
  setActiveDrag: yc,
  setActiveSidebarEndPane: hc,
  setMode: gc,
  setSelectedSection: mc
} = qr.actions, bc = (e) => e[Ue], Sc = (e) => e[Ue].selectedComponent, Oc = (e) => e[Ue].selectedSection, wc = (e) => e[Ue].mode, Ac = (e) => e[Ue].activeDrag, Ou = (e, t) => ({
  ...e,
  baseUrl: t.payload
}), wu = { setBaseUrl: Ou }, Au = {
  baseUrl: "/"
}, xa = "drupal", $r = ge({
  name: xa,
  initialState: Au,
  reducers: wu
});
$r.actions;
const Tc = (e) => e[xa].baseUrl;
var Et = globalThis && globalThis.__generator || function(e, t) {
  var r = { label: 0, sent: function() {
    if (i[0] & 1)
      throw i[1];
    return i[1];
  }, trys: [], ops: [] }, n, a, i, s;
  return s = { next: c(0), throw: c(1), return: c(2) }, typeof Symbol == "function" && (s[Symbol.iterator] = function() {
    return this;
  }), s;
  function c(o) {
    return function(f) {
      return u([o, f]);
    };
  }
  function u(o) {
    if (n)
      throw new TypeError("Generator is already executing.");
    for (; r; )
      try {
        if (n = 1, a && (i = o[0] & 2 ? a.return : o[0] ? a.throw || ((i = a.return) && i.call(a), 0) : a.next) && !(i = i.call(a, o[1])).done)
          return i;
        switch (a = 0, i && (o = [o[0] & 2, i.value]), o[0]) {
          case 0:
          case 1:
            i = o;
            break;
          case 4:
            return r.label++, { value: o[1], done: !1 };
          case 5:
            r.label++, a = o[1], o = [0];
            continue;
          case 7:
            o = r.ops.pop(), r.trys.pop();
            continue;
          default:
            if (i = r.trys, !(i = i.length > 0 && i[i.length - 1]) && (o[0] === 6 || o[0] === 2)) {
              r = 0;
              continue;
            }
            if (o[0] === 3 && (!i || o[1] > i[0] && o[1] < i[3])) {
              r.label = o[1];
              break;
            }
            if (o[0] === 6 && r.label < i[1]) {
              r.label = i[1], i = o;
              break;
            }
            if (i && r.label < i[2]) {
              r.label = i[2], r.ops.push(o);
              break;
            }
            i[2] && r.ops.pop(), r.trys.pop();
            continue;
        }
        o = t.call(e, r);
      } catch (f) {
        o = [6, f], a = 0;
      } finally {
        n = i = 0;
      }
    if (o[0] & 5)
      throw o[1];
    return { value: o[0] ? o[1] : void 0, done: !0 };
  }
}, It = globalThis && globalThis.__spreadArray || function(e, t) {
  for (var r = 0, n = t.length, a = e.length; r < n; r++, a++)
    e[a] = t[r];
  return e;
}, Tu = Object.defineProperty, Pu = Object.defineProperties, Ru = Object.getOwnPropertyDescriptors, Mt = Object.getOwnPropertySymbols, qa = Object.prototype.hasOwnProperty, $a = Object.prototype.propertyIsEnumerable, kn = function(e, t, r) {
  return t in e ? Tu(e, t, { enumerable: !0, configurable: !0, writable: !0, value: r }) : e[t] = r;
}, J = function(e, t) {
  for (var r in t || (t = {}))
    qa.call(t, r) && kn(e, r, t[r]);
  if (Mt)
    for (var n = 0, a = Mt(t); n < a.length; n++) {
      var r = a[n];
      $a.call(t, r) && kn(e, r, t[r]);
    }
  return e;
}, de = function(e, t) {
  return Pu(e, Ru(t));
}, Cn = function(e, t) {
  var r = {};
  for (var n in e)
    qa.call(e, n) && t.indexOf(n) < 0 && (r[n] = e[n]);
  if (e != null && Mt)
    for (var a = 0, i = Mt(e); a < i.length; a++) {
      var n = i[a];
      t.indexOf(n) < 0 && $a.call(e, n) && (r[n] = e[n]);
    }
  return r;
}, kt = function(e, t, r) {
  return new Promise(function(n, a) {
    var i = function(u) {
      try {
        c(r.next(u));
      } catch (o) {
        a(o);
      }
    }, s = function(u) {
      try {
        c(r.throw(u));
      } catch (o) {
        a(o);
      }
    }, c = function(u) {
      return u.done ? n(u.value) : Promise.resolve(u.value).then(i, s);
    };
    c((r = r.apply(e, t)).next());
  });
}, V;
(function(e) {
  e.uninitialized = "uninitialized", e.pending = "pending", e.fulfilled = "fulfilled", e.rejected = "rejected";
})(V || (V = {}));
function Eu(e) {
  return {
    status: e,
    isUninitialized: e === V.uninitialized,
    isLoading: e === V.pending,
    isSuccess: e === V.fulfilled,
    isError: e === V.rejected
  };
}
function Iu(e) {
  return new RegExp("(^|:)//").test(e);
}
var Mu = function(e) {
  return e.replace(/\/$/, "");
}, ku = function(e) {
  return e.replace(/^\//, "");
};
function Cu(e, t) {
  if (!e)
    return t;
  if (!t)
    return e;
  if (Iu(t))
    return t;
  var r = e.endsWith("/") || !t.startsWith("?") ? "/" : "";
  return e = Mu(e), t = ku(t), "" + e + r + t;
}
var xn = function(e) {
  return [].concat.apply([], e);
};
function xu() {
  return typeof navigator > "u" || navigator.onLine === void 0 ? !0 : navigator.onLine;
}
function qu() {
  return typeof document > "u" ? !0 : document.visibilityState !== "hidden";
}
var qn = be;
function Da(e, t) {
  if (e === t || !(qn(e) && qn(t) || Array.isArray(e) && Array.isArray(t)))
    return t;
  for (var r = Object.keys(t), n = Object.keys(e), a = r.length === n.length, i = Array.isArray(t) ? [] : {}, s = 0, c = r; s < c.length; s++) {
    var u = c[s];
    i[u] = Da(e[u], t[u]), a && (a = e[u] === i[u]);
  }
  return a ? e : i;
}
var $n = function() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return fetch.apply(void 0, e);
}, $u = function(e) {
  return e.status >= 200 && e.status <= 299;
}, Du = function(e) {
  return /ion\/(vnd\.api\+)?json/.test(e.get("content-type") || "");
};
function Dn(e) {
  if (!be(e))
    return e;
  for (var t = J({}, e), r = 0, n = Object.entries(t); r < n.length; r++) {
    var a = n[r], i = a[0], s = a[1];
    s === void 0 && delete t[i];
  }
  return t;
}
function Qu(e) {
  var t = this;
  e === void 0 && (e = {});
  var r = e, n = r.baseUrl, a = r.prepareHeaders, i = a === void 0 ? function(v) {
    return v;
  } : a, s = r.fetchFn, c = s === void 0 ? $n : s, u = r.paramsSerializer, o = r.isJsonContentType, f = o === void 0 ? Du : o, g = r.jsonContentType, h = g === void 0 ? "application/json" : g, l = r.jsonReplacer, d = r.timeout, p = r.responseHandler, S = r.validateStatus, A = Cn(r, [
    "baseUrl",
    "prepareHeaders",
    "fetchFn",
    "paramsSerializer",
    "isJsonContentType",
    "jsonContentType",
    "jsonReplacer",
    "timeout",
    "responseHandler",
    "validateStatus"
  ]);
  return typeof fetch > "u" && c === $n && console.warn("Warning: `fetch` is not available. Please supply a custom `fetchFn` property to use `fetchBaseQuery` on SSR environments."), function(v, y) {
    return kt(t, null, function() {
      var O, m, w, P, R, T, E, I, M, k, C, q, D, x, Q, $, N, U, _, z, j, B, H, ce, ke, ie, ye, K, Se, ir, tn, or, lt, ur, sr, rn;
      return Et(this, function(le) {
        switch (le.label) {
          case 0:
            return O = y.signal, m = y.getState, w = y.extra, P = y.endpoint, R = y.forced, T = y.type, I = typeof v == "string" ? { url: v } : v, M = I.url, k = I.headers, C = k === void 0 ? new Headers(A.headers) : k, q = I.params, D = q === void 0 ? void 0 : q, x = I.responseHandler, Q = x === void 0 ? p ?? "json" : x, $ = I.validateStatus, N = $ === void 0 ? S ?? $u : $, U = I.timeout, _ = U === void 0 ? d : U, z = Cn(I, [
              "url",
              "headers",
              "params",
              "responseHandler",
              "validateStatus",
              "timeout"
            ]), j = J(de(J({}, A), {
              signal: O
            }), z), C = new Headers(Dn(C)), B = j, [4, i(C, {
              getState: m,
              extra: w,
              endpoint: P,
              forced: R,
              type: T
            })];
          case 1:
            B.headers = le.sent() || C, H = function(fe) {
              return typeof fe == "object" && (be(fe) || Array.isArray(fe) || typeof fe.toJSON == "function");
            }, !j.headers.has("content-type") && H(j.body) && j.headers.set("content-type", h), H(j.body) && f(j.headers) && (j.body = JSON.stringify(j.body, l)), D && (ce = ~M.indexOf("?") ? "&" : "?", ke = u ? u(D) : new URLSearchParams(Dn(D)), M += ce + ke), M = Cu(n, M), ie = new Request(M, j), ye = ie.clone(), E = { request: ye }, Se = !1, ir = _ && setTimeout(function() {
              Se = !0, y.abort();
            }, _), le.label = 2;
          case 2:
            return le.trys.push([2, 4, 5, 6]), [4, c(ie)];
          case 3:
            return K = le.sent(), [3, 6];
          case 4:
            return tn = le.sent(), [2, {
              error: {
                status: Se ? "TIMEOUT_ERROR" : "FETCH_ERROR",
                error: String(tn)
              },
              meta: E
            }];
          case 5:
            return ir && clearTimeout(ir), [
              7
              /*endfinally*/
            ];
          case 6:
            or = K.clone(), E.response = or, ur = "", le.label = 7;
          case 7:
            return le.trys.push([7, 9, , 10]), [4, Promise.all([
              b(K, Q).then(function(fe) {
                return lt = fe;
              }, function(fe) {
                return sr = fe;
              }),
              or.text().then(function(fe) {
                return ur = fe;
              }, function() {
              })
            ])];
          case 8:
            if (le.sent(), sr)
              throw sr;
            return [3, 10];
          case 9:
            return rn = le.sent(), [2, {
              error: {
                status: "PARSING_ERROR",
                originalStatus: K.status,
                data: ur,
                error: String(rn)
              },
              meta: E
            }];
          case 10:
            return [2, N(K, lt) ? {
              data: lt,
              meta: E
            } : {
              error: {
                status: K.status,
                data: lt
              },
              meta: E
            }];
        }
      });
    });
  };
  function b(v, y) {
    return kt(this, null, function() {
      var O;
      return Et(this, function(m) {
        switch (m.label) {
          case 0:
            return typeof y == "function" ? [2, y(v)] : (y === "content-type" && (y = f(v.headers) ? "json" : "text"), y !== "json" ? [3, 2] : [4, v.text()]);
          case 1:
            return O = m.sent(), [2, O.length ? JSON.parse(O) : null];
          case 2:
            return [2, v.text()];
        }
      });
    });
  }
}
var Qn = (
  /** @class */
  function() {
    function e(t, r) {
      r === void 0 && (r = void 0), this.value = t, this.meta = r;
    }
    return e;
  }()
), nt = /* @__PURE__ */ te("__rtkq/focused"), Ct = /* @__PURE__ */ te("__rtkq/unfocused"), at = /* @__PURE__ */ te("__rtkq/online"), xt = /* @__PURE__ */ te("__rtkq/offline"), br = !1;
function ju(e, t) {
  function r() {
    var n = function() {
      return e(nt());
    }, a = function() {
      return e(Ct());
    }, i = function() {
      return e(at());
    }, s = function() {
      return e(xt());
    }, c = function() {
      window.document.visibilityState === "visible" ? n() : a();
    };
    br || typeof window < "u" && window.addEventListener && (window.addEventListener("visibilitychange", c, !1), window.addEventListener("focus", n, !1), window.addEventListener("online", i, !1), window.addEventListener("offline", s, !1), br = !0);
    var u = function() {
      window.removeEventListener("focus", n), window.removeEventListener("visibilitychange", c), window.removeEventListener("online", i), window.removeEventListener("offline", s), br = !1;
    };
    return u;
  }
  return t ? t(e, { onFocus: nt, onFocusLost: Ct, onOffline: xt, onOnline: at }) : r();
}
var pe;
(function(e) {
  e.query = "query", e.mutation = "mutation";
})(pe || (pe = {}));
function Qa(e) {
  return e.type === pe.query;
}
function _u(e) {
  return e.type === pe.mutation;
}
function ja(e, t, r, n, a, i) {
  return Nu(e) ? e(t, r, n, a).map(Dr).map(i) : Array.isArray(e) ? e.map(Dr).map(i) : [];
}
function Nu(e) {
  return typeof e == "function";
}
function Dr(e) {
  return typeof e == "string" ? { type: e } : e;
}
function Sr(e) {
  return e != null;
}
var it = Symbol("forceQueryFn"), Qr = function(e) {
  return typeof e[it] == "function";
};
function Lu(e) {
  var t = e.serializeQueryArgs, r = e.queryThunk, n = e.mutationThunk, a = e.api, i = e.context, s = /* @__PURE__ */ new Map(), c = /* @__PURE__ */ new Map(), u = a.internalActions, o = u.unsubscribeQueryResult, f = u.removeMutationResult, g = u.updateSubscriptionOptions;
  return {
    buildInitiateQuery: b,
    buildInitiateMutation: v,
    getRunningQueryThunk: d,
    getRunningMutationThunk: p,
    getRunningQueriesThunk: S,
    getRunningMutationsThunk: A,
    getRunningOperationPromises: l,
    removalWarning: h
  };
  function h() {
    throw new Error(`This method had to be removed due to a conceptual bug in RTK.
       Please see https://github.com/reduxjs/redux-toolkit/pull/2481 for details.
       See https://redux-toolkit.js.org/rtk-query/usage/server-side-rendering for new guidance on SSR.`);
  }
  function l() {
    typeof process < "u";
    var y = function(O) {
      return Array.from(O.values()).flatMap(function(m) {
        return m ? Object.values(m) : [];
      });
    };
    return It(It([], y(s)), y(c)).filter(Sr);
  }
  function d(y, O) {
    return function(m) {
      var w, P = i.endpointDefinitions[y], R = t({
        queryArgs: O,
        endpointDefinition: P,
        endpointName: y
      });
      return (w = s.get(m)) == null ? void 0 : w[R];
    };
  }
  function p(y, O) {
    return function(m) {
      var w;
      return (w = c.get(m)) == null ? void 0 : w[O];
    };
  }
  function S() {
    return function(y) {
      return Object.values(s.get(y) || {}).filter(Sr);
    };
  }
  function A() {
    return function(y) {
      return Object.values(c.get(y) || {}).filter(Sr);
    };
  }
  function b(y, O) {
    var m = function(w, P) {
      var R = P === void 0 ? {} : P, T = R.subscribe, E = T === void 0 ? !0 : T, I = R.forceRefetch, M = R.subscriptionOptions, k = it, C = R[k];
      return function(q, D) {
        var x, Q, $ = t({
          queryArgs: w,
          endpointDefinition: O,
          endpointName: y
        }), N = r((x = {
          type: "query",
          subscribe: E,
          forceRefetch: I,
          subscriptionOptions: M,
          endpointName: y,
          originalArgs: w,
          queryCacheKey: $
        }, x[it] = C, x)), U = a.endpoints[y].select(w), _ = q(N), z = U(D()), j = _.requestId, B = _.abort, H = z.requestId !== j, ce = (Q = s.get(q)) == null ? void 0 : Q[$], ke = function() {
          return U(D());
        }, ie = Object.assign(C ? _.then(ke) : H && !ce ? Promise.resolve(z) : Promise.all([ce, _]).then(ke), {
          arg: w,
          requestId: j,
          subscriptionOptions: M,
          queryCacheKey: $,
          abort: B,
          unwrap: function() {
            return kt(this, null, function() {
              var K;
              return Et(this, function(Se) {
                switch (Se.label) {
                  case 0:
                    return [4, ie];
                  case 1:
                    if (K = Se.sent(), K.isError)
                      throw K.error;
                    return [2, K.data];
                }
              });
            });
          },
          refetch: function() {
            return q(m(w, { subscribe: !1, forceRefetch: !0 }));
          },
          unsubscribe: function() {
            E && q(o({
              queryCacheKey: $,
              requestId: j
            }));
          },
          updateSubscriptionOptions: function(K) {
            ie.subscriptionOptions = K, q(g({
              endpointName: y,
              requestId: j,
              queryCacheKey: $,
              options: K
            }));
          }
        });
        if (!ce && !H && !C) {
          var ye = s.get(q) || {};
          ye[$] = ie, s.set(q, ye), ie.then(function() {
            delete ye[$], Object.keys(ye).length || s.delete(q);
          });
        }
        return ie;
      };
    };
    return m;
  }
  function v(y) {
    return function(O, m) {
      var w = m === void 0 ? {} : m, P = w.track, R = P === void 0 ? !0 : P, T = w.fixedCacheKey;
      return function(E, I) {
        var M = n({
          type: "mutation",
          endpointName: y,
          originalArgs: O,
          track: R,
          fixedCacheKey: T
        }), k = E(M), C = k.requestId, q = k.abort, D = k.unwrap, x = k.unwrap().then(function(U) {
          return { data: U };
        }).catch(function(U) {
          return { error: U };
        }), Q = function() {
          E(f({ requestId: C, fixedCacheKey: T }));
        }, $ = Object.assign(x, {
          arg: k.arg,
          requestId: C,
          abort: q,
          unwrap: D,
          unsubscribe: Q,
          reset: Q
        }), N = c.get(E) || {};
        return c.set(E, N), N[C] = $, $.then(function() {
          delete N[C], Object.keys(N).length || c.delete(E);
        }), T && (N[T] = $, $.then(function() {
          N[T] === $ && (delete N[T], Object.keys(N).length || c.delete(E));
        })), $;
      };
    };
  }
}
function jn(e) {
  return e;
}
function Fu(e) {
  var t = this, r = e.reducerPath, n = e.baseQuery, a = e.context.endpointDefinitions, i = e.serializeQueryArgs, s = e.api, c = function(v, y, O) {
    return function(m) {
      var w = a[v];
      m(s.internalActions.queryResultPatched({
        queryCacheKey: i({
          queryArgs: y,
          endpointDefinition: w,
          endpointName: v
        }),
        patches: O
      }));
    };
  }, u = function(v, y, O) {
    return function(m, w) {
      var P, R, T = s.endpoints[v].select(y)(w()), E = {
        patches: [],
        inversePatches: [],
        undo: function() {
          return m(s.util.patchQueryData(v, y, E.inversePatches));
        }
      };
      if (T.status === V.uninitialized)
        return E;
      if ("data" in T)
        if (se(T.data)) {
          var I = ma(T.data, O), M = I[1], k = I[2];
          (P = E.patches).push.apply(P, M), (R = E.inversePatches).push.apply(R, k);
        } else {
          var C = O(T.data);
          E.patches.push({ op: "replace", path: [], value: C }), E.inversePatches.push({
            op: "replace",
            path: [],
            value: T.data
          });
        }
      return m(s.util.patchQueryData(v, y, E.patches)), E;
    };
  }, o = function(v, y, O) {
    return function(m) {
      var w;
      return m(s.endpoints[v].initiate(y, (w = {
        subscribe: !1,
        forceRefetch: !0
      }, w[it] = function() {
        return {
          data: O
        };
      }, w)));
    };
  }, f = function(v, y) {
    return kt(t, [v, y], function(O, m) {
      var w, P, R, T, E, I, M, k, C, q, D, x, Q, $, N, U, _, z, j = m.signal, B = m.abort, H = m.rejectWithValue, ce = m.fulfillWithValue, ke = m.dispatch, ie = m.getState, ye = m.extra;
      return Et(this, function(K) {
        switch (K.label) {
          case 0:
            w = a[O.endpointName], K.label = 1;
          case 1:
            return K.trys.push([1, 8, , 13]), P = jn, R = void 0, T = {
              signal: j,
              abort: B,
              dispatch: ke,
              getState: ie,
              extra: ye,
              endpoint: O.endpointName,
              type: O.type,
              forced: O.type === "query" ? g(O, ie()) : void 0
            }, E = O.type === "query" ? O[it] : void 0, E ? (R = E(), [3, 6]) : [3, 2];
          case 2:
            return w.query ? [4, n(w.query(O.originalArgs), T, w.extraOptions)] : [3, 4];
          case 3:
            return R = K.sent(), w.transformResponse && (P = w.transformResponse), [3, 6];
          case 4:
            return [4, w.queryFn(O.originalArgs, T, w.extraOptions, function(Se) {
              return n(Se, T, w.extraOptions);
            })];
          case 5:
            R = K.sent(), K.label = 6;
          case 6:
            if (typeof process < "u", R.error)
              throw new Qn(R.error, R.meta);
            return D = ce, [4, P(R.data, R.meta, O.originalArgs)];
          case 7:
            return [2, D.apply(void 0, [K.sent(), (_ = {
              fulfilledTimeStamp: Date.now(),
              baseQueryMeta: R.meta
            }, _[He] = !0, _)])];
          case 8:
            if (x = K.sent(), Q = x, !(Q instanceof Qn))
              return [3, 12];
            $ = jn, w.query && w.transformErrorResponse && ($ = w.transformErrorResponse), K.label = 9;
          case 9:
            return K.trys.push([9, 11, , 12]), N = H, [4, $(Q.value, Q.meta, O.originalArgs)];
          case 10:
            return [2, N.apply(void 0, [K.sent(), (z = { baseQueryMeta: Q.meta }, z[He] = !0, z)])];
          case 11:
            return U = K.sent(), Q = U, [3, 12];
          case 12:
            throw typeof process < "u", console.error(Q), Q;
          case 13:
            return [
              2
              /*return*/
            ];
        }
      });
    });
  };
  function g(v, y) {
    var O, m, w, P, R = (m = (O = y[r]) == null ? void 0 : O.queries) == null ? void 0 : m[v.queryCacheKey], T = (w = y[r]) == null ? void 0 : w.config.refetchOnMountOrArgChange, E = R == null ? void 0 : R.fulfilledTimeStamp, I = (P = v.forceRefetch) != null ? P : v.subscribe && T;
    return I ? I === !0 || (Number(/* @__PURE__ */ new Date()) - Number(E)) / 1e3 >= I : !1;
  }
  var h = En(r + "/executeQuery", f, {
    getPendingMeta: function() {
      var v;
      return v = { startedTimeStamp: Date.now() }, v[He] = !0, v;
    },
    condition: function(v, y) {
      var O = y.getState, m, w, P, R = O(), T = (w = (m = R[r]) == null ? void 0 : m.queries) == null ? void 0 : w[v.queryCacheKey], E = T == null ? void 0 : T.fulfilledTimeStamp, I = v.originalArgs, M = T == null ? void 0 : T.originalArgs, k = a[v.endpointName];
      return Qr(v) ? !0 : (T == null ? void 0 : T.status) === "pending" ? !1 : g(v, R) || Qa(k) && ((P = k == null ? void 0 : k.forceRefetch) != null && P.call(k, {
        currentArg: I,
        previousArg: M,
        endpointState: T,
        state: R
      })) ? !0 : !E;
    },
    dispatchConditionRejection: !0
  }), l = En(r + "/executeMutation", f, {
    getPendingMeta: function() {
      var v;
      return v = { startedTimeStamp: Date.now() }, v[He] = !0, v;
    }
  }), d = function(v) {
    return "force" in v;
  }, p = function(v) {
    return "ifOlderThan" in v;
  }, S = function(v, y, O) {
    return function(m, w) {
      var P = d(O) && O.force, R = p(O) && O.ifOlderThan, T = function(k) {
        return k === void 0 && (k = !0), s.endpoints[v].initiate(y, { forceRefetch: k });
      }, E = s.endpoints[v].select(y)(w());
      if (P)
        m(T());
      else if (R) {
        var I = E == null ? void 0 : E.fulfilledTimeStamp;
        if (!I) {
          m(T());
          return;
        }
        var M = (Number(/* @__PURE__ */ new Date()) - Number(new Date(I))) / 1e3 >= R;
        M && m(T());
      } else
        m(T(!1));
    };
  };
  function A(v) {
    return function(y) {
      var O, m;
      return ((m = (O = y == null ? void 0 : y.meta) == null ? void 0 : O.arg) == null ? void 0 : m.endpointName) === v;
    };
  }
  function b(v, y) {
    return {
      matchPending: Xe(Xr(v), A(y)),
      matchFulfilled: Xe(Qe(v), A(y)),
      matchRejected: Xe(tt(v), A(y))
    };
  }
  return {
    queryThunk: h,
    mutationThunk: l,
    prefetch: S,
    updateQueryData: u,
    upsertQueryData: o,
    patchQueryData: c,
    buildMatchThunkActions: b
  };
}
function _a(e, t, r, n) {
  return ja(r[e.meta.arg.endpointName][t], Qe(e) ? e.payload : void 0, ar(e) ? e.payload : void 0, e.meta.arg.originalArgs, "baseQueryMeta" in e.meta ? e.meta.baseQueryMeta : void 0, n);
}
function vt(e, t, r) {
  var n = e[t];
  n && r(n);
}
function ot(e) {
  var t;
  return (t = "arg" in e ? e.arg.fixedCacheKey : e.fixedCacheKey) != null ? t : e.requestId;
}
function _n(e, t, r) {
  var n = e[ot(t)];
  n && r(n);
}
var Be = {};
function zu(e) {
  var t = e.reducerPath, r = e.queryThunk, n = e.mutationThunk, a = e.context, i = a.endpointDefinitions, s = a.apiUid, c = a.extractRehydrationInfo, u = a.hasRehydrationInfo, o = e.assertTagType, f = e.config, g = te(t + "/resetApiState"), h = ge({
    name: t + "/queries",
    initialState: Be,
    reducers: {
      removeQueryResult: {
        reducer: function(O, m) {
          var w = m.payload.queryCacheKey;
          delete O[w];
        },
        prepare: mr()
      },
      queryResultPatched: function(O, m) {
        var w = m.payload, P = w.queryCacheKey, R = w.patches;
        vt(O, P, function(T) {
          T.data = mn(T.data, R.concat());
        });
      }
    },
    extraReducers: function(O) {
      O.addCase(r.pending, function(m, w) {
        var P = w.meta, R = w.meta.arg, T, E, I = Qr(R);
        (R.subscribe || I) && ((E = m[T = R.queryCacheKey]) != null || (m[T] = {
          status: V.uninitialized,
          endpointName: R.endpointName
        })), vt(m, R.queryCacheKey, function(M) {
          M.status = V.pending, M.requestId = I && M.requestId ? M.requestId : P.requestId, R.originalArgs !== void 0 && (M.originalArgs = R.originalArgs), M.startedTimeStamp = P.startedTimeStamp;
        });
      }).addCase(r.fulfilled, function(m, w) {
        var P = w.meta, R = w.payload;
        vt(m, P.arg.queryCacheKey, function(T) {
          var E;
          if (!(T.requestId !== P.requestId && !Qr(P.arg))) {
            var I = i[P.arg.endpointName].merge;
            if (T.status = V.fulfilled, I)
              if (T.data !== void 0) {
                var M = P.fulfilledTimeStamp, k = P.arg, C = P.baseQueryMeta, q = P.requestId, D = ze(T.data, function(x) {
                  return I(x, R, {
                    arg: k.originalArgs,
                    baseQueryMeta: C,
                    fulfilledTimeStamp: M,
                    requestId: q
                  });
                });
                T.data = D;
              } else
                T.data = R;
            else
              T.data = (E = i[P.arg.endpointName].structuralSharing) == null || E ? Da(re(T.data) ? _i(T.data) : T.data, R) : R;
            delete T.error, T.fulfilledTimeStamp = P.fulfilledTimeStamp;
          }
        });
      }).addCase(r.rejected, function(m, w) {
        var P = w.meta, R = P.condition, T = P.arg, E = P.requestId, I = w.error, M = w.payload;
        vt(m, T.queryCacheKey, function(k) {
          if (!R) {
            if (k.requestId !== E)
              return;
            k.status = V.rejected, k.error = M ?? I;
          }
        });
      }).addMatcher(u, function(m, w) {
        for (var P = c(w).queries, R = 0, T = Object.entries(P); R < T.length; R++) {
          var E = T[R], I = E[0], M = E[1];
          ((M == null ? void 0 : M.status) === V.fulfilled || (M == null ? void 0 : M.status) === V.rejected) && (m[I] = M);
        }
      });
    }
  }), l = ge({
    name: t + "/mutations",
    initialState: Be,
    reducers: {
      removeMutationResult: {
        reducer: function(O, m) {
          var w = m.payload, P = ot(w);
          P in O && delete O[P];
        },
        prepare: mr()
      }
    },
    extraReducers: function(O) {
      O.addCase(n.pending, function(m, w) {
        var P = w.meta, R = w.meta, T = R.requestId, E = R.arg, I = R.startedTimeStamp;
        E.track && (m[ot(P)] = {
          requestId: T,
          status: V.pending,
          endpointName: E.endpointName,
          startedTimeStamp: I
        });
      }).addCase(n.fulfilled, function(m, w) {
        var P = w.payload, R = w.meta;
        R.arg.track && _n(m, R, function(T) {
          T.requestId === R.requestId && (T.status = V.fulfilled, T.data = P, T.fulfilledTimeStamp = R.fulfilledTimeStamp);
        });
      }).addCase(n.rejected, function(m, w) {
        var P = w.payload, R = w.error, T = w.meta;
        T.arg.track && _n(m, T, function(E) {
          E.requestId === T.requestId && (E.status = V.rejected, E.error = P ?? R);
        });
      }).addMatcher(u, function(m, w) {
        for (var P = c(w).mutations, R = 0, T = Object.entries(P); R < T.length; R++) {
          var E = T[R], I = E[0], M = E[1];
          ((M == null ? void 0 : M.status) === V.fulfilled || (M == null ? void 0 : M.status) === V.rejected) && I !== (M == null ? void 0 : M.requestId) && (m[I] = M);
        }
      });
    }
  }), d = ge({
    name: t + "/invalidation",
    initialState: Be,
    reducers: {},
    extraReducers: function(O) {
      O.addCase(h.actions.removeQueryResult, function(m, w) {
        for (var P = w.payload.queryCacheKey, R = 0, T = Object.values(m); R < T.length; R++)
          for (var E = T[R], I = 0, M = Object.values(E); I < M.length; I++) {
            var k = M[I], C = k.indexOf(P);
            C !== -1 && k.splice(C, 1);
          }
      }).addMatcher(u, function(m, w) {
        for (var P, R, T, E, I = c(w).provided, M = 0, k = Object.entries(I); M < k.length; M++)
          for (var C = k[M], q = C[0], D = C[1], x = 0, Q = Object.entries(D); x < Q.length; x++)
            for (var $ = Q[x], N = $[0], U = $[1], _ = (E = (R = (P = m[q]) != null ? P : m[q] = {})[T = N || "__internal_without_id"]) != null ? E : R[T] = [], z = 0, j = U; z < j.length; z++) {
              var B = j[z], H = _.includes(B);
              H || _.push(B);
            }
      }).addMatcher(Ke(Qe(r), ar(r)), function(m, w) {
        for (var P, R, T, E, I = _a(w, "providesTags", i, o), M = w.meta.arg.queryCacheKey, k = 0, C = Object.values(m); k < C.length; k++)
          for (var q = C[k], D = 0, x = Object.values(q); D < x.length; D++) {
            var Q = x[D], $ = Q.indexOf(M);
            $ !== -1 && Q.splice($, 1);
          }
        for (var N = 0, U = I; N < U.length; N++) {
          var _ = U[N], z = _.type, j = _.id, B = (E = (R = (P = m[z]) != null ? P : m[z] = {})[T = j || "__internal_without_id"]) != null ? E : R[T] = [], H = B.includes(M);
          H || B.push(M);
        }
      });
    }
  }), p = ge({
    name: t + "/subscriptions",
    initialState: Be,
    reducers: {
      updateSubscriptionOptions: function(O, m) {
      },
      unsubscribeQueryResult: function(O, m) {
      },
      internal_probeSubscription: function(O, m) {
      }
    }
  }), S = ge({
    name: t + "/internalSubscriptions",
    initialState: Be,
    reducers: {
      subscriptionsUpdated: {
        reducer: function(O, m) {
          return mn(O, m.payload);
        },
        prepare: mr()
      }
    }
  }), A = ge({
    name: t + "/config",
    initialState: J({
      online: xu(),
      focused: qu(),
      middlewareRegistered: !1
    }, f),
    reducers: {
      middlewareRegistered: function(O, m) {
        var w = m.payload;
        O.middlewareRegistered = O.middlewareRegistered === "conflict" || s !== w ? "conflict" : !0;
      }
    },
    extraReducers: function(O) {
      O.addCase(at, function(m) {
        m.online = !0;
      }).addCase(xt, function(m) {
        m.online = !1;
      }).addCase(nt, function(m) {
        m.focused = !0;
      }).addCase(Ct, function(m) {
        m.focused = !1;
      }).addMatcher(u, function(m) {
        return J({}, m);
      });
    }
  }), b = Jr({
    queries: h.reducer,
    mutations: l.reducer,
    provided: d.reducer,
    subscriptions: S.reducer,
    config: A.reducer
  }), v = function(O, m) {
    return b(g.match(m) ? void 0 : O, m);
  }, y = de(J(J(J(J(J({}, A.actions), h.actions), p.actions), S.actions), l.actions), {
    unsubscribeMutationResult: l.actions.removeMutationResult,
    resetApiState: g
  });
  return { reducer: v, actions: y };
}
var xe = /* @__PURE__ */ Symbol.for("RTKQ/skipToken"), Na = {
  status: V.uninitialized
}, Nn = /* @__PURE__ */ ze(Na, function() {
}), Ln = /* @__PURE__ */ ze(Na, function() {
});
function Ku(e) {
  var t = e.serializeQueryArgs, r = e.reducerPath, n = function(f) {
    return Nn;
  }, a = function(f) {
    return Ln;
  };
  return { buildQuerySelector: c, buildMutationSelector: u, selectInvalidatedBy: o };
  function i(f) {
    return J(J({}, f), Eu(f.status));
  }
  function s(f) {
    var g = f[r];
    return g;
  }
  function c(f, g) {
    return function(h) {
      var l = t({
        queryArgs: h,
        endpointDefinition: g,
        endpointName: f
      }), d = function(S) {
        var A, b, v;
        return (v = (b = (A = s(S)) == null ? void 0 : A.queries) == null ? void 0 : b[l]) != null ? v : Nn;
      }, p = h === xe ? n : d;
      return Ee(p, i);
    };
  }
  function u() {
    return function(f) {
      var g, h;
      typeof f == "object" ? h = (g = ot(f)) != null ? g : xe : h = f;
      var l = function(p) {
        var S, A, b;
        return (b = (A = (S = s(p)) == null ? void 0 : S.mutations) == null ? void 0 : A[h]) != null ? b : Ln;
      }, d = h === xe ? a : l;
      return Ee(d, i);
    };
  }
  function o(f, g) {
    for (var h, l = f[r], d = /* @__PURE__ */ new Set(), p = 0, S = g.map(Dr); p < S.length; p++) {
      var A = S[p], b = l.provided[A.type];
      if (b)
        for (var v = (h = A.id !== void 0 ? b[A.id] : xn(Object.values(b))) != null ? h : [], y = 0, O = v; y < O.length; y++) {
          var m = O[y];
          d.add(m);
        }
    }
    return xn(Array.from(d.values()).map(function(w) {
      var P = l.queries[w];
      return P ? [
        {
          queryCacheKey: w,
          endpointName: P.endpointName,
          originalArgs: P.originalArgs
        }
      ] : [];
    }));
  }
}
var pt = WeakMap ? /* @__PURE__ */ new WeakMap() : void 0, Fn = function(e) {
  var t = e.endpointName, r = e.queryArgs, n = "", a = pt == null ? void 0 : pt.get(r);
  if (typeof a == "string")
    n = a;
  else {
    var i = JSON.stringify(r, function(s, c) {
      return be(c) ? Object.keys(c).sort().reduce(function(u, o) {
        return u[o] = c[o], u;
      }, {}) : c;
    });
    be(r) && (pt == null || pt.set(r, i)), n = i;
  }
  return t + "(" + n + ")";
};
function Uu() {
  for (var e = [], t = 0; t < arguments.length; t++)
    e[t] = arguments[t];
  return function(n) {
    var a = Mr(function(f) {
      var g, h;
      return (h = n.extractRehydrationInfo) == null ? void 0 : h.call(n, f, {
        reducerPath: (g = n.reducerPath) != null ? g : "api"
      });
    }), i = de(J({
      reducerPath: "api",
      keepUnusedDataFor: 60,
      refetchOnMountOrArgChange: !1,
      refetchOnFocus: !1,
      refetchOnReconnect: !1
    }, n), {
      extractRehydrationInfo: a,
      serializeQueryArgs: function(f) {
        var g = Fn;
        if ("serializeQueryArgs" in f.endpointDefinition) {
          var h = f.endpointDefinition.serializeQueryArgs;
          g = function(l) {
            var d = h(l);
            return typeof d == "string" ? d : Fn(de(J({}, l), {
              queryArgs: d
            }));
          };
        } else
          n.serializeQueryArgs && (g = n.serializeQueryArgs);
        return g(f);
      },
      tagTypes: It([], n.tagTypes || [])
    }), s = {
      endpointDefinitions: {},
      batch: function(f) {
        f();
      },
      apiUid: me(),
      extractRehydrationInfo: a,
      hasRehydrationInfo: Mr(function(f) {
        return a(f) != null;
      })
    }, c = {
      injectEndpoints: o,
      enhanceEndpoints: function(f) {
        var g = f.addTagTypes, h = f.endpoints;
        if (g)
          for (var l = 0, d = g; l < d.length; l++) {
            var p = d[l];
            i.tagTypes.includes(p) || i.tagTypes.push(p);
          }
        if (h)
          for (var S = 0, A = Object.entries(h); S < A.length; S++) {
            var b = A[S], v = b[0], y = b[1];
            typeof y == "function" ? y(s.endpointDefinitions[v]) : Object.assign(s.endpointDefinitions[v] || {}, y);
          }
        return c;
      }
    }, u = e.map(function(f) {
      return f.init(c, i, s);
    });
    function o(f) {
      for (var g = f.endpoints({
        query: function(y) {
          return de(J({}, y), { type: pe.query });
        },
        mutation: function(y) {
          return de(J({}, y), { type: pe.mutation });
        }
      }), h = 0, l = Object.entries(g); h < l.length; h++) {
        var d = l[h], p = d[0], S = d[1];
        if (!f.overrideExisting && p in s.endpointDefinitions) {
          typeof process < "u";
          continue;
        }
        s.endpointDefinitions[p] = S;
        for (var A = 0, b = u; A < b.length; A++) {
          var v = b[A];
          v.injectEndpoint(p, S);
        }
      }
      return c;
    }
    return c.injectEndpoints({ endpoints: n.endpoints });
  };
}
function Bu(e) {
  for (var t in e)
    return !1;
  return !0;
}
var Wu = 2147483647 / 1e3 - 1, Vu = function(e) {
  var t = e.reducerPath, r = e.api, n = e.context, a = e.internalState, i = r.internalActions, s = i.removeQueryResult, c = i.unsubscribeQueryResult;
  function u(h) {
    var l = a.currentSubscriptions[h];
    return !!l && !Bu(l);
  }
  var o = {}, f = function(h, l, d) {
    var p;
    if (c.match(h)) {
      var S = l.getState()[t], A = h.payload.queryCacheKey;
      g(A, (p = S.queries[A]) == null ? void 0 : p.endpointName, l, S.config);
    }
    if (r.util.resetApiState.match(h))
      for (var b = 0, v = Object.entries(o); b < v.length; b++) {
        var y = v[b], O = y[0], m = y[1];
        m && clearTimeout(m), delete o[O];
      }
    if (n.hasRehydrationInfo(h))
      for (var S = l.getState()[t], w = n.extractRehydrationInfo(h).queries, P = 0, R = Object.entries(w); P < R.length; P++) {
        var T = R[P], A = T[0], E = T[1];
        g(A, E == null ? void 0 : E.endpointName, l, S.config);
      }
  };
  function g(h, l, d, p) {
    var S, A = n.endpointDefinitions[l], b = (S = A == null ? void 0 : A.keepUnusedDataFor) != null ? S : p.keepUnusedDataFor;
    if (b !== 1 / 0) {
      var v = Math.max(0, Math.min(b, Wu));
      if (!u(h)) {
        var y = o[h];
        y && clearTimeout(y), o[h] = setTimeout(function() {
          u(h) || d.dispatch(s({ queryCacheKey: h })), delete o[h];
        }, v * 1e3);
      }
    }
  }
  return f;
}, Hu = function(e) {
  var t = e.reducerPath, r = e.context, n = e.context.endpointDefinitions, a = e.mutationThunk, i = e.api, s = e.assertTagType, c = e.refetchQuery, u = i.internalActions.removeQueryResult, o = Ke(Qe(a), ar(a)), f = function(h, l) {
    o(h) && g(_a(h, "invalidatesTags", n, s), l), i.util.invalidateTags.match(h) && g(ja(h.payload, void 0, void 0, void 0, void 0, s), l);
  };
  function g(h, l) {
    var d = l.getState(), p = d[t], S = i.util.selectInvalidatedBy(d, h);
    r.batch(function() {
      for (var A, b = Array.from(S.values()), v = 0, y = b; v < y.length; v++) {
        var O = y[v].queryCacheKey, m = p.queries[O], w = (A = p.subscriptions[O]) != null ? A : {};
        m && (Object.keys(w).length === 0 ? l.dispatch(u({
          queryCacheKey: O
        })) : m.status !== V.uninitialized && l.dispatch(c(m, O)));
      }
    });
  }
  return f;
}, Gu = function(e) {
  var t = e.reducerPath, r = e.queryThunk, n = e.api, a = e.refetchQuery, i = e.internalState, s = {}, c = function(l, d) {
    (n.internalActions.updateSubscriptionOptions.match(l) || n.internalActions.unsubscribeQueryResult.match(l)) && o(l.payload, d), (r.pending.match(l) || r.rejected.match(l) && l.meta.condition) && o(l.meta.arg, d), (r.fulfilled.match(l) || r.rejected.match(l) && !l.meta.condition) && u(l.meta.arg, d), n.util.resetApiState.match(l) && g();
  };
  function u(l, d) {
    var p = l.queryCacheKey, S = d.getState()[t], A = S.queries[p], b = i.currentSubscriptions[p];
    if (!(!A || A.status === V.uninitialized)) {
      var v = h(b);
      if (Number.isFinite(v)) {
        var y = s[p];
        y != null && y.timeout && (clearTimeout(y.timeout), y.timeout = void 0);
        var O = Date.now() + v, m = s[p] = {
          nextPollTimestamp: O,
          pollingInterval: v,
          timeout: setTimeout(function() {
            m.timeout = void 0, d.dispatch(a(A, p));
          }, v)
        };
      }
    }
  }
  function o(l, d) {
    var p = l.queryCacheKey, S = d.getState()[t], A = S.queries[p], b = i.currentSubscriptions[p];
    if (!(!A || A.status === V.uninitialized)) {
      var v = h(b);
      if (!Number.isFinite(v)) {
        f(p);
        return;
      }
      var y = s[p], O = Date.now() + v;
      (!y || O < y.nextPollTimestamp) && u({ queryCacheKey: p }, d);
    }
  }
  function f(l) {
    var d = s[l];
    d != null && d.timeout && clearTimeout(d.timeout), delete s[l];
  }
  function g() {
    for (var l = 0, d = Object.keys(s); l < d.length; l++) {
      var p = d[l];
      f(p);
    }
  }
  function h(l) {
    l === void 0 && (l = {});
    var d = Number.POSITIVE_INFINITY;
    for (var p in l)
      l[p].pollingInterval && (d = Math.min(l[p].pollingInterval, d));
    return d;
  }
  return c;
}, Ju = function(e) {
  var t = e.reducerPath, r = e.context, n = e.api, a = e.refetchQuery, i = e.internalState, s = n.internalActions.removeQueryResult, c = function(o, f) {
    nt.match(o) && u(f, "refetchOnFocus"), at.match(o) && u(f, "refetchOnReconnect");
  };
  function u(o, f) {
    var g = o.getState()[t], h = g.queries, l = i.currentSubscriptions;
    r.batch(function() {
      for (var d = 0, p = Object.keys(l); d < p.length; d++) {
        var S = p[d], A = h[S], b = l[S];
        if (!(!b || !A)) {
          var v = Object.values(b).some(function(y) {
            return y[f] === !0;
          }) || Object.values(b).every(function(y) {
            return y[f] === void 0;
          }) && g.config[f];
          v && (Object.keys(b).length === 0 ? o.dispatch(s({
            queryCacheKey: S
          })) : A.status !== V.uninitialized && o.dispatch(a(A, S)));
        }
      }
    });
  }
  return c;
}, zn = new Error("Promise never resolved before cacheEntryRemoved."), Xu = function(e) {
  var t = e.api, r = e.reducerPath, n = e.context, a = e.queryThunk, i = e.mutationThunk;
  e.internalState;
  var s = Cr(a), c = Cr(i), u = Qe(a, i), o = {}, f = function(l, d, p) {
    var S = g(l);
    if (a.pending.match(l)) {
      var A = p[r].queries[S], b = d.getState()[r].queries[S];
      !A && b && h(l.meta.arg.endpointName, l.meta.arg.originalArgs, S, d, l.meta.requestId);
    } else if (i.pending.match(l)) {
      var b = d.getState()[r].mutations[S];
      b && h(l.meta.arg.endpointName, l.meta.arg.originalArgs, S, d, l.meta.requestId);
    } else if (u(l)) {
      var v = o[S];
      v != null && v.valueResolved && (v.valueResolved({
        data: l.payload,
        meta: l.meta.baseQueryMeta
      }), delete v.valueResolved);
    } else if (t.internalActions.removeQueryResult.match(l) || t.internalActions.removeMutationResult.match(l)) {
      var v = o[S];
      v && (delete o[S], v.cacheEntryRemoved());
    } else if (t.util.resetApiState.match(l))
      for (var y = 0, O = Object.entries(o); y < O.length; y++) {
        var m = O[y], w = m[0], v = m[1];
        delete o[w], v.cacheEntryRemoved();
      }
  };
  function g(l) {
    return s(l) ? l.meta.arg.queryCacheKey : c(l) ? l.meta.requestId : t.internalActions.removeQueryResult.match(l) ? l.payload.queryCacheKey : t.internalActions.removeMutationResult.match(l) ? ot(l.payload) : "";
  }
  function h(l, d, p, S, A) {
    var b = n.endpointDefinitions[l], v = b == null ? void 0 : b.onCacheEntryAdded;
    if (v) {
      var y = {}, O = new Promise(function(E) {
        y.cacheEntryRemoved = E;
      }), m = Promise.race([
        new Promise(function(E) {
          y.valueResolved = E;
        }),
        O.then(function() {
          throw zn;
        })
      ]);
      m.catch(function() {
      }), o[p] = y;
      var w = t.endpoints[l].select(b.type === pe.query ? d : p), P = S.dispatch(function(E, I, M) {
        return M;
      }), R = de(J({}, S), {
        getCacheEntry: function() {
          return w(S.getState());
        },
        requestId: A,
        extra: P,
        updateCachedData: b.type === pe.query ? function(E) {
          return S.dispatch(t.util.updateQueryData(l, d, E));
        } : void 0,
        cacheDataLoaded: m,
        cacheEntryRemoved: O
      }), T = v(d, R);
      Promise.resolve(T).catch(function(E) {
        if (E !== zn)
          throw E;
      });
    }
  }
  return f;
}, Yu = function(e) {
  var t = e.api, r = e.context, n = e.queryThunk, a = e.mutationThunk, i = Xr(n, a), s = tt(n, a), c = Qe(n, a), u = {}, o = function(f, g) {
    var h, l, d;
    if (i(f)) {
      var p = f.meta, S = p.requestId, A = p.arg, b = A.endpointName, v = A.originalArgs, y = r.endpointDefinitions[b], O = y == null ? void 0 : y.onQueryStarted;
      if (O) {
        var m = {}, w = new Promise(function(C, q) {
          m.resolve = C, m.reject = q;
        });
        w.catch(function() {
        }), u[S] = m;
        var P = t.endpoints[b].select(y.type === pe.query ? v : S), R = g.dispatch(function(C, q, D) {
          return D;
        }), T = de(J({}, g), {
          getCacheEntry: function() {
            return P(g.getState());
          },
          requestId: S,
          extra: R,
          updateCachedData: y.type === pe.query ? function(C) {
            return g.dispatch(t.util.updateQueryData(b, v, C));
          } : void 0,
          queryFulfilled: w
        });
        O(v, T);
      }
    } else if (c(f)) {
      var E = f.meta, S = E.requestId, I = E.baseQueryMeta;
      (h = u[S]) == null || h.resolve({
        data: f.payload,
        meta: I
      }), delete u[S];
    } else if (s(f)) {
      var M = f.meta, S = M.requestId, k = M.rejectedWithValue, I = M.baseQueryMeta;
      (d = u[S]) == null || d.reject({
        error: (l = f.payload) != null ? l : f.error,
        isUnhandledError: !k,
        meta: I
      }), delete u[S];
    }
  };
  return o;
}, Zu = function(e) {
  var t = e.api, r = e.context.apiUid, n = e.reducerPath;
  return function(a, i) {
    var s, c;
    t.util.resetApiState.match(a) && i.dispatch(t.internalActions.middlewareRegistered(r)), typeof process < "u";
  };
}, Kn, es = typeof queueMicrotask == "function" ? queueMicrotask.bind(typeof window < "u" ? window : typeof global < "u" ? global : globalThis) : function(e) {
  return (Kn || (Kn = Promise.resolve())).then(e).catch(function(t) {
    return setTimeout(function() {
      throw t;
    }, 0);
  });
}, ts = function(e) {
  var t = e.api, r = e.queryThunk, n = e.internalState, a = t.reducerPath + "/subscriptions", i = null, s = !1, c = t.internalActions, u = c.updateSubscriptionOptions, o = c.unsubscribeQueryResult, f = function(g, h) {
    var l, d, p, S, A, b, v, y, O;
    if (u.match(h)) {
      var m = h.payload, w = m.queryCacheKey, P = m.requestId, R = m.options;
      return (l = g == null ? void 0 : g[w]) != null && l[P] && (g[w][P] = R), !0;
    }
    if (o.match(h)) {
      var T = h.payload, w = T.queryCacheKey, P = T.requestId;
      return g[w] && delete g[w][P], !0;
    }
    if (t.internalActions.removeQueryResult.match(h))
      return delete g[h.payload.queryCacheKey], !0;
    if (r.pending.match(h)) {
      var E = h.meta, I = E.arg, P = E.requestId;
      if (I.subscribe) {
        var M = (p = g[d = I.queryCacheKey]) != null ? p : g[d] = {};
        return M[P] = (A = (S = I.subscriptionOptions) != null ? S : M[P]) != null ? A : {}, !0;
      }
    }
    if (r.rejected.match(h)) {
      var k = h.meta, C = k.condition, I = k.arg, P = k.requestId;
      if (C && I.subscribe) {
        var M = (v = g[b = I.queryCacheKey]) != null ? v : g[b] = {};
        return M[P] = (O = (y = I.subscriptionOptions) != null ? y : M[P]) != null ? O : {}, !0;
      }
    }
    return !1;
  };
  return function(g, h) {
    var l, d;
    if (i || (i = JSON.parse(JSON.stringify(n.currentSubscriptions))), t.util.resetApiState.match(g))
      return i = n.currentSubscriptions = {}, [!0, !1];
    if (t.internalActions.internal_probeSubscription.match(g)) {
      var p = g.payload, S = p.queryCacheKey, A = p.requestId, b = !!((l = n.currentSubscriptions[S]) != null && l[A]);
      return [!1, b];
    }
    var v = f(n.currentSubscriptions, g);
    if (v) {
      s || (es(function() {
        var w = JSON.parse(JSON.stringify(n.currentSubscriptions)), P = ma(i, function() {
          return w;
        }), R = P[1];
        h.next(t.internalActions.subscriptionsUpdated(R)), i = w, s = !1;
      }), s = !0);
      var y = !!((d = g.type) != null && d.startsWith(a)), O = r.rejected.match(g) && g.meta.condition && !!g.meta.arg.subscribe, m = !y && !O;
      return [m, !1];
    }
    return [!0, !1];
  };
};
function rs(e) {
  var t = e.reducerPath, r = e.queryThunk, n = e.api, a = e.context, i = a.apiUid, s = {
    invalidateTags: te(t + "/invalidateTags")
  }, c = function(g) {
    return !!g && typeof g.type == "string" && g.type.startsWith(t + "/");
  }, u = [
    Zu,
    Vu,
    Hu,
    Gu,
    Xu,
    Yu
  ], o = function(g) {
    var h = !1, l = {
      currentSubscriptions: {}
    }, d = de(J({}, e), {
      internalState: l,
      refetchQuery: f
    }), p = u.map(function(b) {
      return b(d);
    }), S = ts(d), A = Ju(d);
    return function(b) {
      return function(v) {
        h || (h = !0, g.dispatch(n.internalActions.middlewareRegistered(i)));
        var y = de(J({}, g), { next: b }), O = g.getState(), m = S(v, y, O), w = m[0], P = m[1], R;
        if (w ? R = b(v) : R = P, g.getState()[t] && (A(v, y, O), c(v) || a.hasRehydrationInfo(v)))
          for (var T = 0, E = p; T < E.length; T++) {
            var I = E[T];
            I(v, y, O);
          }
        return R;
      };
    };
  };
  return { middleware: o, actions: s };
  function f(g, h, l) {
    return l === void 0 && (l = {}), r(J({
      type: "query",
      endpointName: g.endpointName,
      originalArgs: g.originalArgs,
      subscribe: !1,
      forceRefetch: !0,
      queryCacheKey: h
    }, l));
  }
}
function we(e) {
  for (var t = [], r = 1; r < arguments.length; r++)
    t[r - 1] = arguments[r];
  Object.assign.apply(Object, It([e], t));
}
var Un = /* @__PURE__ */ Symbol(), ns = function() {
  return {
    name: Un,
    init: function(e, t, r) {
      var n = t.baseQuery, a = t.tagTypes, i = t.reducerPath, s = t.serializeQueryArgs, c = t.keepUnusedDataFor, u = t.refetchOnMountOrArgChange, o = t.refetchOnFocus, f = t.refetchOnReconnect;
      zi();
      var g = function(_) {
        return typeof process < "u", _;
      };
      Object.assign(e, {
        reducerPath: i,
        endpoints: {},
        internalActions: {
          onOnline: at,
          onOffline: xt,
          onFocus: nt,
          onFocusLost: Ct
        },
        util: {}
      });
      var h = Fu({
        baseQuery: n,
        reducerPath: i,
        context: r,
        api: e,
        serializeQueryArgs: s
      }), l = h.queryThunk, d = h.mutationThunk, p = h.patchQueryData, S = h.updateQueryData, A = h.upsertQueryData, b = h.prefetch, v = h.buildMatchThunkActions, y = zu({
        context: r,
        queryThunk: l,
        mutationThunk: d,
        reducerPath: i,
        assertTagType: g,
        config: {
          refetchOnFocus: o,
          refetchOnReconnect: f,
          refetchOnMountOrArgChange: u,
          keepUnusedDataFor: c,
          reducerPath: i
        }
      }), O = y.reducer, m = y.actions;
      we(e.util, {
        patchQueryData: p,
        updateQueryData: S,
        upsertQueryData: A,
        prefetch: b,
        resetApiState: m.resetApiState
      }), we(e.internalActions, m);
      var w = rs({
        reducerPath: i,
        context: r,
        queryThunk: l,
        mutationThunk: d,
        api: e,
        assertTagType: g
      }), P = w.middleware, R = w.actions;
      we(e.util, R), we(e, { reducer: O, middleware: P });
      var T = Ku({
        serializeQueryArgs: s,
        reducerPath: i
      }), E = T.buildQuerySelector, I = T.buildMutationSelector, M = T.selectInvalidatedBy;
      we(e.util, { selectInvalidatedBy: M });
      var k = Lu({
        queryThunk: l,
        mutationThunk: d,
        api: e,
        serializeQueryArgs: s,
        context: r
      }), C = k.buildInitiateQuery, q = k.buildInitiateMutation, D = k.getRunningMutationThunk, x = k.getRunningMutationsThunk, Q = k.getRunningQueriesThunk, $ = k.getRunningQueryThunk, N = k.getRunningOperationPromises, U = k.removalWarning;
      return we(e.util, {
        getRunningOperationPromises: N,
        getRunningOperationPromise: U,
        getRunningMutationThunk: D,
        getRunningMutationsThunk: x,
        getRunningQueryThunk: $,
        getRunningQueriesThunk: Q
      }), {
        name: Un,
        injectEndpoint: function(_, z) {
          var j, B, H = e;
          (B = (j = H.endpoints)[_]) != null || (j[_] = {}), Qa(z) ? we(H.endpoints[_], {
            name: _,
            select: E(_, z),
            initiate: C(_, z)
          }, v(l, _)) : _u(z) && we(H.endpoints[_], {
            name: _,
            select: I(),
            initiate: q(_)
          }, v(d, _));
        }
      };
    }
  };
}, as = globalThis && globalThis.__spreadArray || function(e, t) {
  for (var r = 0, n = t.length, a = e.length; r < n; r++, a++)
    e[a] = t[r];
  return e;
}, is = Object.defineProperty, os = Object.defineProperties, us = Object.getOwnPropertyDescriptors, Bn = Object.getOwnPropertySymbols, ss = Object.prototype.hasOwnProperty, cs = Object.prototype.propertyIsEnumerable, Wn = function(e, t, r) {
  return t in e ? is(e, t, { enumerable: !0, configurable: !0, writable: !0, value: r }) : e[t] = r;
}, he = function(e, t) {
  for (var r in t || (t = {}))
    ss.call(t, r) && Wn(e, r, t[r]);
  if (Bn)
    for (var n = 0, a = Bn(t); n < a.length; n++) {
      var r = a[n];
      cs.call(t, r) && Wn(e, r, t[r]);
    }
  return e;
}, mt = function(e, t) {
  return os(e, us(t));
};
function Vn(e, t, r, n) {
  var a = ue(function() {
    return {
      queryArgs: e,
      serialized: typeof e == "object" ? t({ queryArgs: e, endpointDefinition: r, endpointName: n }) : e
    };
  }, [e, t, r, n]), i = Ae(a);
  return Te(function() {
    i.current.serialized !== a.serialized && (i.current = a);
  }, [a]), i.current.serialized === a.serialized ? i.current.queryArgs : e;
}
var Or = Symbol();
function wr(e) {
  var t = Ae(e);
  return Te(function() {
    bt(t.current, e) || (t.current = e);
  }, [e]), bt(t.current, e) ? t.current : e;
}
var yt = WeakMap ? /* @__PURE__ */ new WeakMap() : void 0, ls = function(e) {
  var t = e.endpointName, r = e.queryArgs, n = "", a = yt == null ? void 0 : yt.get(r);
  if (typeof a == "string")
    n = a;
  else {
    var i = JSON.stringify(r, function(s, c) {
      return be(c) ? Object.keys(c).sort().reduce(function(u, o) {
        return u[o] = c[o], u;
      }, {}) : c;
    });
    be(r) && (yt == null || yt.set(r, i)), n = i;
  }
  return t + "(" + n + ")";
}, fs = typeof window < "u" && window.document && window.document.createElement ? Va : Te, ds = function(e) {
  return e;
}, vs = function(e) {
  return e.isUninitialized ? mt(he({}, e), {
    isUninitialized: !1,
    isFetching: !0,
    isLoading: e.data === void 0,
    status: V.pending
  }) : e;
};
function ps(e) {
  var t = e.api, r = e.moduleOptions, n = r.batch, a = r.useDispatch, i = r.useSelector, s = r.useStore, c = r.unstable__sideEffectsInRender, u = e.serializeQueryArgs, o = e.context, f = c ? function(p) {
    return p();
  } : Te;
  return { buildQueryHooks: l, buildMutationHook: d, usePrefetch: h };
  function g(p, S, A) {
    if (S != null && S.endpointName && p.isUninitialized) {
      var b = S.endpointName, v = o.endpointDefinitions[b];
      u({
        queryArgs: S.originalArgs,
        endpointDefinition: v,
        endpointName: b
      }) === u({
        queryArgs: A,
        endpointDefinition: v,
        endpointName: b
      }) && (S = void 0);
    }
    var y = p.isSuccess ? p.data : S == null ? void 0 : S.data;
    y === void 0 && (y = p.data);
    var O = y !== void 0, m = p.isLoading, w = !O && m, P = p.isSuccess || m && O;
    return mt(he({}, p), {
      data: y,
      currentData: p.data,
      isFetching: m,
      isLoading: w,
      isSuccess: P
    });
  }
  function h(p, S) {
    var A = a(), b = wr(S);
    return We(function(v, y) {
      return A(t.util.prefetch(p, v, he(he({}, b), y)));
    }, [p, A, b]);
  }
  function l(p) {
    var S = function(v, y) {
      var O = y === void 0 ? {} : y, m = O.refetchOnReconnect, w = O.refetchOnFocus, P = O.refetchOnMountOrArgChange, R = O.skip, T = R === void 0 ? !1 : R, E = O.pollingInterval, I = E === void 0 ? 0 : E, M = t.endpoints[p].initiate, k = a(), C = Vn(T ? xe : v, ls, o.endpointDefinitions[p], p), q = wr({
        refetchOnReconnect: m,
        refetchOnFocus: w,
        pollingInterval: I
      }), D = Ae(!1), x = Ae(), Q = x.current || {}, $ = Q.queryCacheKey, N = Q.requestId, U = !1;
      if ($ && N) {
        var _ = k(t.internalActions.internal_probeSubscription({
          queryCacheKey: $,
          requestId: N
        }));
        U = !!_;
      }
      var z = !U && D.current;
      return f(function() {
        D.current = U;
      }), f(function() {
        z && (x.current = void 0);
      }, [z]), f(function() {
        var j, B = x.current;
        if (typeof process < "u", C === xe) {
          B == null || B.unsubscribe(), x.current = void 0;
          return;
        }
        var H = (j = x.current) == null ? void 0 : j.subscriptionOptions;
        if (!B || B.arg !== C) {
          B == null || B.unsubscribe();
          var ce = k(M(C, {
            subscriptionOptions: q,
            forceRefetch: P
          }));
          x.current = ce;
        } else
          q !== H && B.updateSubscriptionOptions(q);
      }, [
        k,
        M,
        P,
        C,
        q,
        z
      ]), Te(function() {
        return function() {
          var j;
          (j = x.current) == null || j.unsubscribe(), x.current = void 0;
        };
      }, []), ue(function() {
        return {
          refetch: function() {
            var j;
            if (!x.current)
              throw new Error("Cannot refetch a query that has not been started yet.");
            return (j = x.current) == null ? void 0 : j.refetch();
          }
        };
      }, []);
    }, A = function(v) {
      var y = v === void 0 ? {} : v, O = y.refetchOnReconnect, m = y.refetchOnFocus, w = y.pollingInterval, P = w === void 0 ? 0 : w, R = t.endpoints[p].initiate, T = a(), E = an(Or), I = E[0], M = E[1], k = Ae(), C = wr({
        refetchOnReconnect: O,
        refetchOnFocus: m,
        pollingInterval: P
      });
      f(function() {
        var x, Q, $ = (x = k.current) == null ? void 0 : x.subscriptionOptions;
        C !== $ && ((Q = k.current) == null || Q.updateSubscriptionOptions(C));
      }, [C]);
      var q = Ae(C);
      f(function() {
        q.current = C;
      }, [C]);
      var D = We(function(x, Q) {
        Q === void 0 && (Q = !1);
        var $;
        return n(function() {
          var N;
          (N = k.current) == null || N.unsubscribe(), k.current = $ = T(R(x, {
            subscriptionOptions: q.current,
            forceRefetch: !Q
          })), M(x);
        }), $;
      }, [T, R]);
      return Te(function() {
        return function() {
          var x;
          (x = k == null ? void 0 : k.current) == null || x.unsubscribe();
        };
      }, []), Te(function() {
        I !== Or && !k.current && D(I, !0);
      }, [I, D]), ue(function() {
        return [D, I];
      }, [D, I]);
    }, b = function(v, y) {
      var O = y === void 0 ? {} : y, m = O.skip, w = m === void 0 ? !1 : m, P = O.selectFromResult, R = t.endpoints[p].select, T = Vn(w ? xe : v, u, o.endpointDefinitions[p], p), E = Ae(), I = ue(function() {
        return Ee([
          R(T),
          function(D, x) {
            return x;
          },
          function(D) {
            return T;
          }
        ], g);
      }, [R, T]), M = ue(function() {
        return P ? Ee([I], P) : I;
      }, [I, P]), k = i(function(D) {
        return M(D, E.current);
      }, bt), C = s(), q = I(C.getState(), E.current);
      return fs(function() {
        E.current = q;
      }, [q]), k;
    };
    return {
      useQueryState: b,
      useQuerySubscription: S,
      useLazyQuerySubscription: A,
      useLazyQuery: function(v) {
        var y = A(v), O = y[0], m = y[1], w = b(m, mt(he({}, v), {
          skip: m === Or
        })), P = ue(function() {
          return { lastArg: m };
        }, [m]);
        return ue(function() {
          return [O, w, P];
        }, [O, w, P]);
      },
      useQuery: function(v, y) {
        var O = S(v, y), m = b(v, he({
          selectFromResult: v === xe || y != null && y.skip ? void 0 : vs
        }, y)), w = m.data, P = m.status, R = m.isLoading, T = m.isSuccess, E = m.isError, I = m.error;
        return Tr({ data: w, status: P, isLoading: R, isSuccess: T, isError: E, error: I }), ue(function() {
          return he(he({}, m), O);
        }, [m, O]);
      }
    };
  }
  function d(p) {
    return function(S) {
      var A = S === void 0 ? {} : S, b = A.selectFromResult, v = b === void 0 ? ds : b, y = A.fixedCacheKey, O = t.endpoints[p], m = O.select, w = O.initiate, P = a(), R = an(), T = R[0], E = R[1];
      Te(function() {
        return function() {
          T != null && T.arg.fixedCacheKey || T == null || T.reset();
        };
      }, [T]);
      var I = We(function(B) {
        var H = P(w(B, { fixedCacheKey: y }));
        return E(H), H;
      }, [P, w, y]), M = (T || {}).requestId, k = ue(function() {
        return Ee([m({ fixedCacheKey: y, requestId: T == null ? void 0 : T.requestId })], v);
      }, [m, T, v, y]), C = i(k, bt), q = y == null ? T == null ? void 0 : T.arg.originalArgs : void 0, D = We(function() {
        n(function() {
          T && E(void 0), y && P(t.internalActions.removeMutationResult({
            requestId: M,
            fixedCacheKey: y
          }));
        });
      }, [P, y, T, M]), x = C.endpointName, Q = C.data, $ = C.status, N = C.isLoading, U = C.isSuccess, _ = C.isError, z = C.error;
      Tr({
        endpointName: x,
        data: Q,
        status: $,
        isLoading: N,
        isSuccess: U,
        isError: _,
        error: z
      });
      var j = ue(function() {
        return mt(he({}, C), { originalArgs: q, reset: D });
      }, [C, q, D]);
      return ue(function() {
        return [I, j];
      }, [I, j]);
    };
  }
}
var qt;
(function(e) {
  e.query = "query", e.mutation = "mutation";
})(qt || (qt = {}));
function ys(e) {
  return e.type === qt.query;
}
function hs(e) {
  return e.type === qt.mutation;
}
function Ar(e) {
  return e.replace(e[0], e[0].toUpperCase());
}
function ht(e) {
  for (var t = [], r = 1; r < arguments.length; r++)
    t[r - 1] = arguments[r];
  Object.assign.apply(Object, as([e], t));
}
var gs = /* @__PURE__ */ Symbol(), ms = function(e) {
  var t = e === void 0 ? {} : e, r = t.batch, n = r === void 0 ? Jn : r, a = t.useDispatch, i = a === void 0 ? da : a, s = t.useSelector, c = s === void 0 ? aa : s, u = t.useStore, o = u === void 0 ? fa : u, f = t.unstable__sideEffectsInRender, g = f === void 0 ? !1 : f;
  return {
    name: gs,
    init: function(h, l, d) {
      var p = l.serializeQueryArgs, S = h, A = ps({
        api: h,
        moduleOptions: {
          batch: n,
          useDispatch: i,
          useSelector: c,
          useStore: o,
          unstable__sideEffectsInRender: g
        },
        serializeQueryArgs: p,
        context: d
      }), b = A.buildQueryHooks, v = A.buildMutationHook, y = A.usePrefetch;
      return ht(S, { usePrefetch: y }), ht(d, { batch: n }), {
        injectEndpoint: function(O, m) {
          if (ys(m)) {
            var w = b(O), P = w.useQuery, R = w.useLazyQuery, T = w.useLazyQuerySubscription, E = w.useQueryState, I = w.useQuerySubscription;
            ht(S.endpoints[O], {
              useQuery: P,
              useLazyQuery: R,
              useLazyQuerySubscription: T,
              useQueryState: E,
              useQuerySubscription: I
            }), h["use" + Ar(O) + "Query"] = P, h["useLazy" + Ar(O) + "Query"] = R;
          } else if (hs(m)) {
            var M = v(O);
            ht(S.endpoints[O], {
              useMutation: M
            }), h["use" + Ar(O) + "Mutation"] = M;
          }
        }
      };
    }
  };
}, bs = /* @__PURE__ */ Uu(ns(), ms());
const Ss = (e) => {
  const { baseUrl: t, sectionStorageType: r, sectionStorage: n } = e, a = Qu({
    baseUrl: t
  });
  return async (i, s, c = {}) => {
    const o = (typeof i == "string" ? i : i.url).replace("{section_storage_type}", r).replace("{section_storage}", n), f = typeof i == "string" ? o : { ...i, url: o };
    return a(f, s, c);
  };
}, Os = async (e, t, r) => {
  const n = t.getState(), a = n.drupal.baseUrl;
  return a ? Ss({
    baseUrl: a,
    sectionStorageType: n.layout.sectionStorageType,
    sectionStorage: n.layout.sectionStorage
  })(e, t, r) : {
    error: {
      status: 400,
      statusText: "Bad Request",
      data: "No host found"
    }
  };
}, Zr = Ra({
  selectId: (e) => e.id
}), La = Zr.getInitialState({
  status: "idle",
  error: null
}), en = Ra({
  selectId: (e) => e.id
}), Fa = en.getInitialState({
  status: "idle",
  error: null
}), Fe = bs({
  reducerPath: "drupalApi",
  baseQuery: Os,
  endpoints: (e) => ({
    getBlockList: e.query({
      query: () => "api/v1/layout-builder/blocks/{section_storage_type}/{section_storage}",
      transformResponse: (t) => en.setAll(
        Fa,
        Object.values(t.blocks)
      )
    }),
    getSectionList: e.query({
      query: () => "api/v1/layout-builder/sections/{section_storage_type}/{section_storage}",
      transformResponse: (t) => Zr.setAll(
        La,
        Object.values(t.sections)
      )
    }),
    getDisplayPlugin: e.query({
      query: (t) => {
        const { type: r, viewMode: n } = t;
        return `jsonapi/entity_${r}_display/entity_${r}_display/${n}`;
      },
      transformResponse: (t, r, n) => {
        const { type: a, viewMode: i } = n;
        if (!t.data.id)
          throw new ct("No such display", {
            fetch: {
              message: `Entity ${a} display ${i} does not exist`,
              identifier: "fetch"
            }
          });
        return {
          ...n,
          components: t.data.attributes.content
        };
      }
    })
  })
}), {
  useGetBlockListQuery: Pc,
  useGetSectionListQuery: Rc,
  useGetDisplayPluginQuery: Ec
} = Fe, ws = Fe.endpoints.getSectionList.select(), As = Ee(
  ws,
  (e) => e.data
), {
  selectAll: Ic,
  selectById: Mc,
  selectEntities: kc
} = Zr.getSelectors(
  (e) => As(e) ?? La
), Ts = Fe.endpoints.getBlockList.select(), Ps = Ee(
  Ts,
  (e) => e.data
), {
  selectAll: Cc,
  selectById: xc,
  selectEntities: qc
} = en.getSelectors(
  (e) => Ps(e) ?? Fa
), Rs = {
  [rt.name]: rt.reducer,
  [qr.name]: qr.reducer,
  [$r.name]: $r.reducer,
  [Fe.reducerPath]: Fe.reducer
}, Es = Jr(Rs), $c = (e, t = null) => {
  const r = Oo({
    reducer: Es,
    devTools: !1,
    preloadedState: {
      drupal: { baseUrl: e.baseUrl },
      layout: {
        ...xr,
        layout: {
          ...xr.layout,
          ...t || {}
        },
        sectionStorageType: e.sectionStorageType,
        sectionStorage: e.sectionStorage
      }
    },
    middleware: (n) => n().concat(Fe.middleware)
  });
  return ju(r.dispatch), r;
}, Is = da, Dc = aa, Qc = (e, t) => {
  const r = [e];
  if (e.includes(":")) {
    const [n] = e.split(":");
    r.push(n);
  }
  for (; r.length; ) {
    const n = r.shift();
    if (n && t[n])
      return t[n]();
  }
  throw new Error(
    `There is no such plugin ${e}, valid options are ${Object.keys(
      t
    ).join(", ")}`
  );
}, Ms = {
  t: (e, t) => xs(e, t)
};
let ks = Ms;
const Hn = (e, t = {}) => ks.t(e, t), Cs = (e) => e.toString().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;"), za = (e, t, r) => {
  if (e.length === 0 || (Array.isArray(r) || (r = Object.keys(t || {}), r.sort((i, s) => i.length - s.length)), r.length === 0))
    return e;
  const n = r.pop();
  if (!n)
    return e;
  const a = e.split(n);
  if (r.length)
    for (let i = 0; i < a.length; i++)
      a[i] = za(a[i], t, r.slice(0));
  return a.join(t[n]);
}, xs = (e, t = {}) => {
  const r = {};
  return Object.keys(t).forEach((n) => {
    r[n] = Cs(t[n]);
  }), za(e, r, null);
}, jc = (e) => {
  const { configuration: t, uuid: r } = e, n = `label-display-${r}`, a = `label-${r}`, i = Is(), s = (u) => {
    i(
      Mn({
        componentId: r,
        values: {
          ...e,
          configuration: {
            ...t,
            label_display: u.currentTarget.checked
          }
        }
      })
    );
  }, c = (u) => {
    i(
      Mn({
        componentId: r,
        values: {
          ...e,
          configuration: {
            ...t,
            label: u.currentTarget.value
          }
        }
      })
    );
  };
  return /* @__PURE__ */ cr(Ba, { children: [
    /* @__PURE__ */ cr("div", { className: "form-item", children: [
      /* @__PURE__ */ ft("label", { htmlFor: a, children: Hn("Label") }),
      /* @__PURE__ */ ft(
        "input",
        {
          onChange: c,
          type: "text",
          value: t.label || "",
          id: a
        }
      )
    ] }),
    /* @__PURE__ */ ft("div", { className: "form-item", children: /* @__PURE__ */ cr("label", { htmlFor: n, children: [
      /* @__PURE__ */ ft(
        "input",
        {
          onChange: s,
          type: "checkbox",
          id: n,
          checked: t.label_display
        }
      ),
      Hn("Display label")
    ] }) })
  ] });
}, qs = (e) => typeof e == "object" && e != null && "status" in e, $s = (e) => typeof e == "object" && e != null && "message" in e && typeof e.message == "string", _c = (e, t) => qs(e) ? "error" in e ? e.error : JSON.stringify(e.data) : $s(e) ? e.message : t;
export {
  iu as $,
  Fo as A,
  zo as B,
  Ko as C,
  ac as D,
  Ma as E,
  fc as F,
  dc as G,
  Sc as H,
  Js as I,
  Hn as J,
  mu as K,
  rc as L,
  tc as M,
  hc as N,
  $c as O,
  jc as P,
  _c as Q,
  $e as R,
  Ia as S,
  $r as T,
  rt as U,
  au as V,
  ka as W,
  Rt as X,
  Ks as Y,
  Us as Z,
  Bs as _,
  Xs as a,
  Vs as a0,
  Ws as a1,
  Hs as a2,
  Ys as a3,
  Mn as a4,
  No as a5,
  Lo as a6,
  st as a7,
  qr as a8,
  yc as a9,
  pc as aa,
  wc as ab,
  lc as ac,
  Ec as ad,
  Pc as ae,
  kc as af,
  qc as ag,
  ws as ah,
  Ts as ai,
  Cc as aj,
  Ic as ak,
  xc as al,
  Fs as am,
  zs as an,
  ba as ao,
  Zi as ap,
  Tt as aq,
  ic as ar,
  uc as as,
  oc as at,
  Is as b,
  Rc as c,
  Mc as d,
  Oc as e,
  mc as f,
  Ls as g,
  Ca as h,
  Ac as i,
  bc as j,
  je as k,
  Zs as l,
  ec as m,
  me as n,
  Gs as o,
  cc as p,
  sc as q,
  ou as r,
  Qc as s,
  Tc as t,
  Dc as u,
  vc as v,
  bu as w,
  nc as x,
  gc as y,
  gu as z
};
