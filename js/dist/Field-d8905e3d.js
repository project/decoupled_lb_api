import { jsx as i, jsxs as f } from "react/jsx-runtime";
import { createElement as d } from "react";
const k = ({
  items: e,
  label: o = "",
  labelHidden: a = !0,
  multiple: t = !1,
  attributes: c = {},
  labelAttributes: v = {}
}) => a ? t ? /* @__PURE__ */ i("div", { ...c, children: e.map((r, n) => /* @__PURE__ */ d("div", { ...r.attributes || {}, key: n }, r.content)) }) : e.map((r, n) => /* @__PURE__ */ d("div", { ...c, key: n }, r.content)) : /* @__PURE__ */ f("div", { ...c, children: [
  /* @__PURE__ */ i("div", { ...v, children: o }),
  t && /* @__PURE__ */ i("div", { children: e.map((r, n) => /* @__PURE__ */ d("div", { ...r.attributes || {}, key: n }, r.content)) }),
  !t && e.map((r, n) => /* @__PURE__ */ d("div", { ...r.attributes || {}, key: n }, r.content))
] });
export {
  k as F
};
