import { jsx as o } from "react/jsx-runtime";
import { R as n, D as r } from "./DefaultLayoutSettings-b131a39e.js";
import "react";
import "./ErrorHelpers-a6fb9f99.js";
import "react-dom";
import "./_commonjsHelpers-10dfc225.js";
const d = ({
  section: t,
  sectionProps: i,
  regions: e
}) => /* @__PURE__ */ o("div", { ...i, className: `section--${t.id} section`, children: /* @__PURE__ */ o(n, { regionId: e.content.id, className: "layout__region" }) }), g = r, u = "layout_onecol";
export {
  d as Preview,
  g as Settings,
  u as id
};
