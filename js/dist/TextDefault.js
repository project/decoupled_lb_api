import { jsx as t } from "react/jsx-runtime";
import { F as o } from "./Field-d8905e3d.js";
import "react";
const p = ({ itemValues: e }) => /* @__PURE__ */ t(
  o,
  {
    items: e.map((r) => ({
      // This is sanitized by Drupal.
      content: /* @__PURE__ */ t("span", { dangerouslySetInnerHTML: { __html: r.processed } })
    }))
  }
), a = "text_default";
export {
  p as Formatter,
  a as id
};
