import { jsxs as a, jsx as o } from "react/jsx-runtime";
import { R as i, D as l } from "./DefaultLayoutSettings-b131a39e.js";
import "react";
import "./ErrorHelpers-a6fb9f99.js";
import "react-dom";
import "./_commonjsHelpers-10dfc225.js";
const h = ({
  section: r,
  sectionProps: e,
  regions: t
}) => {
  const d = t.second || t.third || t.fourth || t.fifth;
  return /* @__PURE__ */ a(
    "div",
    {
      ...e,
      className: `section section--${r.layout_id} layout layout--oneplusfourgrid-section`,
      children: [
        t.first && /* @__PURE__ */ o(
          i,
          {
            regionId: t.first.id,
            className: "layout__region layout__region--first"
          }
        ),
        d && /* @__PURE__ */ a("div", { className: "layout__four-grid-group", children: [
          t.second && /* @__PURE__ */ o(
            i,
            {
              regionId: t.second.id,
              className: "layout__region layout__region--second"
            }
          ),
          t.third && /* @__PURE__ */ o(
            i,
            {
              regionId: t.third.id,
              className: "layout__region layout__region--third"
            }
          ),
          t.fourth && /* @__PURE__ */ o(
            i,
            {
              regionId: t.fourth.id,
              className: "layout__region layout__region--fourth"
            }
          ),
          t.fifth && /* @__PURE__ */ o(
            i,
            {
              regionId: t.fifth.id,
              className: "layout__region layout__region--fifth"
            }
          )
        ] })
      ]
    }
  );
}, m = l;
export {
  h as Preview,
  m as Settings
};
