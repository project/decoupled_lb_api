import ReactDOM from "react-dom";
import React from "react";
import { LayoutEditor } from "@drupal/decoupled_lb";

(({ decoupledLb, path }, { t, behaviors }, ReactDOM, React) => {
  behaviors.decoupledLb = {
    attach: (context) => {
      const el = document.querySelector("#layout-builder-decoupled:not(.✅)");
      if (!el) {
        return;
      }
      el.classList.add("✅");
      const pluginsToPromises = (carry, [id, path]) => ({
        ...carry,
        [id]: () => import(path),
      });
      ReactDOM.createRoot(el).render(
        React.createElement(LayoutEditor, {
          blockPlugins: Object.entries(decoupledLb.blockPlugins).reduce(
            pluginsToPromises,
            {},
          ),
          layoutPlugins: Object.entries(decoupledLb.layoutPlugins).reduce(
            pluginsToPromises,
            {},
          ),
          formatterPlugins: Object.entries(decoupledLb.formatterPlugins).reduce(
            pluginsToPromises,
            {},
          ),
          widgetPlugins: Object.entries(decoupledLb.widgetPlugins).reduce(
            pluginsToPromises,
            {},
          ),
          baseUrl: path.baseUrl,
          sectionStorageType: el.dataset.sectionStorageType,
          sectionStorage: el.dataset.sectionStorage,
        }),
      );
    },
  };
})(drupalSettings, Drupal, ReactDOM, React);
