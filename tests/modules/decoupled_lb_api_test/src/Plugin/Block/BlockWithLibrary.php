<?php

namespace Drupal\decoupled_lb_api_test\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a block that attaches a library.
 */
#[Block(
  id: "test_attach_library_block",
  admin_label: new TranslatableMarkup("Test block for attaching libraries."),
)]
class BlockWithLibrary extends BlockBase {

  public function build() {
    return [
      '#markup' => '<p>Test for attaching libraries.</p>',
      '#attached' => [
        'library' => [
          'core/drupal.dialog.ajax',
        ],
      ],
    ];
  }
}
