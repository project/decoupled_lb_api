<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\Core\Url;
use Drupal\Tests\decoupled_lb_api\Traits\OpenApiSchemaTrait;

/**
 * Tests selection controller.
 *
 * @covers \Drupal\decoupled_lb_api\Controller\SelectionController::getSections
 * @group decoupled_lb_api
 */
final class GetSectionsTest extends DecoupledLbApiFunctionalTestBase {

  use OpenApiSchemaTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'decoupled_lb_api_test',
  ];

  /**
   * Tests getting sections.
   */
  public function testGetSections(): void {
    $override_url = Url::fromRoute('decoupled_lb_api.get_sections', [
      'section_storage_type' => 'overrides',
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->editor);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertCount(3, $data['sections']);
    $one_col_expected = [
      'id' => 'layout_onecol',
      'icon_is_url' => FALSE,
      'label' => 'One column',
      'region_labels' => ['content' => 'Content'],
    ];
    $this->assertArrayNotHasKey('layout_twocol_section', $data['sections']);
    $this->assertEquals($one_col_expected, array_diff_key($data['sections']['layout_onecol'], ['icon' => TRUE]));
    $this->assertIsString($data['sections']['layout_onecol']['icon']);
    $this->assertStringContainsString('svg', $data['sections']['layout_onecol']['icon']);
    $this->assertDataCompliesWithApiSpecification($data, 'SectionsResponse');

    // Try to view the defaults.
    $default_url = Url::fromRoute('decoupled_lb_api.get_sections', [
      'section_storage_type' => 'defaults',
      'section_storage' => 'user.user.default',
    ]);
    $this->drupalGet($default_url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->layoutAdmin);
    $this->drupalGet($default_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertCount(3, $data['sections']);
    $this->assertEquals($one_col_expected, array_diff_key($data['sections']['layout_onecol'], ['icon' => TRUE]));
  }

}
