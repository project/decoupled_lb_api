<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Url;
use Drupal\layout_builder\Section;

/**
 * Tests layout controller.
 *
 * @covers \Drupal\decoupled_lb_api\Controller\LayoutController::getLayout()
 * @group decoupled_lb_api
 */
final class GetLayoutTest extends DecoupledLbApiFunctionalTestBase {

  /**
   * Tests getting layout.
   */
  public function testGetLayout(): void {
    $override_url = Url::fromRoute('decoupled_lb_api.get_layout', [
      'section_storage_type' => 'overrides',
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(403);

    // Editor should be able to see per-entity overrides.
    // At this point the entity is just using the default layout, but we should
    // still see the data.
    $this->drupalLogin($this->editor);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    // We don't assert too much here.
    // @see \Drupal\Tests\decoupled_lb_api\Kernel\SectionListSerializationTest
    $this->assertNotEmpty($data['data']['sections']);
    $this->assertNotEmpty($data['data']['sections'][0]['components']);

    // Now let's override the specific entity.
    $section = new Section('layout_onecol');
    $body = $this->getRandomGenerator()->sentences(10);
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $body,
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $component = $this->addInlineBlockToSection($block_content, $section);
    $this->addSectionToEntityLayout($this->user, $section);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertNotEmpty($data['data']['sections']);
    $last_section = end($data['data']['sections']);
    $this->assertArrayHasKey($component->getUuid(), $last_section['components']);

    // Try to view the defaults.
    $default_url = Url::fromRoute('decoupled_lb_api.get_layout', [
      'section_storage_type' => 'defaults',
      'section_storage' => 'user.user.default',
    ]);
    $this->drupalGet($default_url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->layoutAdmin);
    $this->drupalGet($default_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertNotEmpty($data['data']['sections']);
    $this->assertNotEmpty($data['data']['sections'][0]['components']);
  }

}
