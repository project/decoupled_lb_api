<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\layout_builder\Section;
use Drupal\Tests\ApiRequestTrait;

/**
 * Tests put layout operation.
 *
 * @covers \Drupal\decoupled_lb_api\Controller\LayoutController::putLayout
 *
 * @group decoupled_lb_api
 */
final class PutLayoutTest extends DecoupledLbApiFunctionalTestBase {

  use ApiRequestTrait;

  /**
   * Tests put.
   */
  public function testPut(): void {
    $override_url = Url::fromRoute('decoupled_lb_api.put_layout', [
      'section_storage_type' => 'overrides',
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet('/session/token');
    $token = $this->getSession()->getPage()->getContent();
    $response = $this->makeApiRequest('PUT', $override_url, ['headers' => ['X-CSRF-Token' => $token]]);
    $this->assertEquals(403, $response->getStatusCode());

    // Add an override.
    $section = new Section('layout_onecol');
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $this->randomMachineName(),
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $component = $this->addInlineBlockToSection($block_content, $section);
    $this->addSectionToEntityLayout($this->user, $section);
    // Confirm the override exists in the layout.
    $this->assertContains($component->getUuid(), $this->getLayoutComponentUuids($this->user));

    $this->drupalLogin($this->editor);

    // Get a copy of the layout.
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(200);
    $layout_data = $this->getJsonFromResponse();
    $this->assertNotEmpty($layout_data['data']['sections']);

    // Route only works for PUT.
    $response = $this->makeApiRequest('POST', $override_url, []);
    $this->assertEquals(405, $response->getStatusCode());

    // Missing token.
    $response = $this->makeApiRequest('PUT', $override_url, ['json' => $layout_data]);
    $this->assertEquals(403, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'access_denied' => [
          'message' => 'Missing X-CSRF-Token header',
          'identifier' => 'access_denied',
        ],
      ],
    ], $data);

    // Invalid token.
    $response = $this->makeApiRequest('PUT', $override_url, [
      'headers' => ['X-CSRF-Token' => 'this is not a token'],
      'json' => $layout_data,
    ]);
    $this->assertEquals(403, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'access_denied' => [
          'message' => 'Invalid X-CSRF-Token header',
          'identifier' => 'access_denied',
        ],
      ],
    ], $data);

    // Now with valid token.
    $this->drupalGet('/session/token');
    $token = $this->getSession()->getPage()->getContent();

    // Invalid data.
    $response = $this->makeApiRequest('PUT', $override_url, [
      'headers' => ['X-CSRF-Token' => $token],
      'json' => 'yeah not not json',
    ]);
    $this->assertEquals(400, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'request' => [
          'message' => 'Invalid JSON',
          'identifier' => 'request',
        ],
      ],
    ], $data);

    // Missing keys.
    $response = $this->makeApiRequest('PUT', $override_url, [
      'headers' => ['X-CSRF-Token' => $token],
      'json' => ['look' => 'ma', 'no' => 'sections', 'or' => 'data'],
    ]);
    $this->assertEquals(400, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'request' => [
          'message' => "Invalid data structure in JSON, missing 'data' and 'data.sections'",
          'identifier' => 'request',
        ],
      ],
    ], $data);

    // Close but malformed.
    $messed_up = $layout_data;
    unset($messed_up['data']['sections'][0]['components']);
    $response = $this->makeApiRequest('PUT', $override_url, [
      'headers' => ['X-CSRF-Token' => $token],
      'json' => $messed_up,
    ]);
    $this->assertEquals(400, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'components' => [
          'message' => "data.sections.0: Keyword validation failed: Required property 'components' must be present in the object",
          'identifier' => 'components',
        ],
      ],
    ], $data);

    // With edits.
    $deltas = array_keys($layout_data['data']['sections']);
    $last_delta = end($deltas);
    $new_component = '697b305a-4df4-4520-b49b-20d1382c0131';
    $new_content = $this->randomMachineName();
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $new_content,
        'format' => 'plain_text',
      ],
    ]);
    $new_components = $layout_data;
    $new_components['data']['sections'][$last_delta]['components'][$new_component] = [
      'uuid' => $new_component,
      'configuration' => \Drupal::service('serializer')->normalize(\Drupal::service('plugin.manager.block')->createInstance('inline_block:basic', [
        'block_serialized' => serialize($block_content),
      ])),
      'weight' => $component->getWeight() + 1,
      'region' => 'content',
      'additional' => [],
    ];
    $response = $this->makeApiRequest('PUT', $override_url, [
      'headers' => ['X-CSRF-Token' => $token],
      'json' => $new_components,
    ]);
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals(200, $response->getStatusCode(), var_export($data, TRUE));

    // Validate that the overridden component has been removed.
    $last_section = end($data['data']['sections']);
    // Should be in the returned layout (tempstore).
    $this->assertArrayHasKey($new_component, $last_section['components']);
    $this->assertArrayHasKey($component->getUuid(), $last_section['components']);
    // But not saved in the entity yet.
    $component_uuids = $this->getLayoutComponentUuids($this->user);
    $this->assertNotContains($new_component, $component_uuids);
    $this->assertContains($component->getUuid(), $component_uuids);
    $this->assertEquals($new_content, $last_section['components'][$new_component]['configuration']['block_serialized']['body'][0]['value']);
  }

}
