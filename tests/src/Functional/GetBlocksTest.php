<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\Core\Url;
use Drupal\Tests\decoupled_lb_api\Traits\OpenApiSchemaTrait;

/**
 * Tests selection controller.
 *
 * @covers \Drupal\decoupled_lb_api\Controller\SelectionController::getBlocks
 * @group decoupled_lb_api
 */
final class GetBlocksTest extends DecoupledLbApiFunctionalTestBase {

  use OpenApiSchemaTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'decoupled_lb_api_test',
  ];

  /**
   * Tests getting blocks.
   */
  public function testGetBlocks(): void {
    $override_url = Url::fromRoute('decoupled_lb_api.get_blocks', [
      'section_storage_type' => 'overrides',
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->editor);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertDataCompliesWithApiSpecification($data, 'BlocksResponse');
    $this->assertBlockData($data['blocks']);
    $this->assertEquals($this->user->label(), $data['blocks']['field_block:user:user:name']['default_configuration']['field_values'][0]['value']);
    $this->assertEquals('layout_builder.entity', $data['blocks']['field_block:user:user:name']['default_configuration']['context_mapping']['entity']);

    // Try to view the defaults.
    $default_url = Url::fromRoute('decoupled_lb_api.get_blocks', [
      'section_storage_type' => 'defaults',
      'section_storage' => 'user.user.default',
    ]);
    $this->drupalGet($default_url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->layoutAdmin);
    $this->drupalGet($default_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertBlockData($data['blocks']);
    $sample_user = \Drupal::service('layout_builder.sample_entity_generator')->get('user', 'user');
    $this->assertEquals($sample_user->label(), $data['blocks']['field_block:user:user:name']['default_configuration']['field_values'][0]['value']);
  }

  /**
   * Assert block data.
   *
   * @param array $blocks
   *   Blocks from response.
   */
  protected function assertBlockData(array $blocks): void {
    $this->assertNotEmpty($blocks);
    $this->assertArrayHasKey('system_menu_block:main', $blocks);
    $this->assertArrayNotHasKey('broken', $blocks);
    $main_menu = $blocks['system_menu_block:main'];
    $this->assertEquals('Main navigation', $main_menu['label']);
    $this->assertEquals('Menus', $main_menu['category']);
    $this->assertEquals('layout_twocol', $main_menu['default_layout']);
    $this->assertEquals('first', $main_menu['default_region']);
    $this->assertContains([
      'layout_id' => 'layout_onecol',
      'regions' => ['content'],
    ], $main_menu['section_restrictions']);
    $this->assertEquals([
      'level' => 1,
      'depth' => 0,
      'expand_all_items' => FALSE,
      'id' => 'system_menu_block:main',
      'label' => '',
      'label_display' => TRUE,
      'context_mapping' => [],
    ], $main_menu['default_configuration']);
  }

}
