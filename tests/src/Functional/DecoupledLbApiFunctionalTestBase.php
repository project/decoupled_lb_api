<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Component\Serialization\Json;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\decoupled_lb_api\Traits\LayoutBuilderTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\UserInterface;

/**
 * Defines a base class for decoupled_lb_api functional tests.
 */
abstract class DecoupledLbApiFunctionalTestBase extends BrowserTestBase {

  use LayoutBuilderTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'layout_builder',
    'decoupled_lb_api',
    'block_content',
    'user',
    'system',
    'jsonapi',
    'block',
    'contextual',
    'layout_discovery',
    'text',
    'field',
    'filter',
    'serialization',
    'file',
    'field_ui',
  ];

  /**
   * General user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * Layout builder editor.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $editor;

  /**
   * Layout admin.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $layoutAdmin;

  /**
   * Temp store.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected LayoutTempstoreRepositoryInterface $layoutTempstoreRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->layoutTempstoreRepository = \Drupal::service('layout_builder.tempstore_repository');
    BlockContentType::create(['id' => 'basic', 'label' => 'Basic'])->save();
    \block_content_add_body_field('basic');
    $this->enableLayoutBuilderForEntityTypeAndBundle('user', 'user');
    $this->user = $this->createUser(['access content']);
    $this->layoutAdmin = $this->createUser([
      'configure any layout',
    ]);
    $this->editor = $this->createUser([
      'configure all user user layout overrides',
    ]);
  }

  /**
   * Gets JSON from response.
   *
   * @return array
   *   JSON body.
   */
  protected function getJsonFromResponse(): array {
    return Json::decode($this->getTextContent());
  }

}
