<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\layout_builder\Section;
use Drupal\Tests\ApiRequestTrait;

/**
 * Defines a test for reverting layouts.
 *
 * @group decoupled_lb_api
 *
 * @covers \Drupal\decoupled_lb_api\Controller\LayoutController::revert
 */
final class RevertLayoutTest extends DecoupledLbApiFunctionalTestBase {

  use ApiRequestTrait;

  /**
   * Tests revert functionality.
   */
  public function testRevert(): void {
    // Ensure access denied to non-editors.
    $this->drupalLogin($this->user);
    $revert_url = Url::fromRoute('decoupled_lb_api.revert', [
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet('/session/token');
    $token = $this->getSession()->getPage()->getContent();
    $response = $this->makeApiRequest('POST', $revert_url, ['headers' => ['X-CSRF-Token' => $token]]);
    $this->assertEquals(403, $response->getStatusCode());

    // Add an override.
    $section = new Section('layout_onecol');
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $this->randomMachineName(),
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $component = $this->addInlineBlockToSection($block_content, $section);
    $this->addSectionToEntityLayout($this->user, $section);
    // Confirm the override exists in the layout.
    $this->assertContains($component->getUuid(), $this->getLayoutComponentUuids($this->user));

    $this->drupalLogin($this->editor);

    // Get the override URL.
    $this->drupalGet($revert_url);
    // Route only works for POST.
    $this->assertSession()->statusCodeEquals(405);

    // Missing token.
    $response = $this->makeApiRequest('POST', $revert_url, []);
    $this->assertEquals(403, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'access_denied' => [
          'message' => 'Missing X-CSRF-Token header',
          'identifier' => 'access_denied',
        ],
      ],
    ], $data);

    // Invalid token.
    $response = $this->makeApiRequest('POST', $revert_url, ['headers' => ['X-CSRF-Token' => 'this is not a token']]);
    $this->assertEquals(403, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'access_denied' => [
          'message' => 'Invalid X-CSRF-Token header',
          'identifier' => 'access_denied',
        ],
      ],
    ], $data);

    // Now with valid token.
    $this->drupalGet('/session/token');
    $token = $this->getSession()->getPage()->getContent();
    $response = $this->makeApiRequest('POST', $revert_url, ['headers' => ['X-CSRF-Token' => $token]]);
    $this->assertEquals(200, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertNotEmpty($data['data']['sections']);

    // Validate that the overridden component has been removed.
    $last_section = end($data['data']['sections']);
    $this->assertArrayNotHasKey($component->getUuid(), $last_section['components']);
    $this->assertNotContains($component->getUuid(), $this->getLayoutComponentUuids($this->user));
  }

}
