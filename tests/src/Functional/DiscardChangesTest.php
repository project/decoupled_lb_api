<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Functional;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\layout_builder\Section;
use Drupal\Tests\ApiRequestTrait;

/**
 * Defines a test for discarding changes to a layout.
 *
 * @group decoupled_lb_api
 *
 * @covers \Drupal\decoupled_lb_api\Controller\LayoutController::discard
 */
final class DiscardChangesTest extends DecoupledLbApiFunctionalTestBase {

  use ApiRequestTrait;

  /**
   * Tests discard functionality.
   */
  public function testDiscard(): void {
    // Ensure access denied to non-editors.
    $this->drupalLogin($this->user);
    $discard_url = Url::fromRoute('decoupled_lb_api.discard', [
      'section_storage_type' => 'overrides',
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet('/session/token');
    $token = $this->getSession()->getPage()->getContent();
    $response = $this->makeApiRequest('POST', $discard_url, ['headers' => ['X-CSRF-Token' => $token]]);
    $this->assertEquals(403, $response->getStatusCode());

    // Add an override.
    $section = new Section('layout_onecol');
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $this->randomMachineName(),
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $component = $this->addInlineBlockToSection($block_content, $section);
    $storage = $this->getOverridesStorageForEntity($this->user);
    $temp_store = $this->layoutTempstoreRepository->get($storage);
    $temp_store->appendSection($section);
    $this->layoutTempstoreRepository->set($temp_store);
    // Confirm the item doesn't appear in the entity (i.e. is only in the
    // tempstore.
    $this->assertNotContains($component->getUuid(), $this->getLayoutComponentUuids($this->user));

    // Confirm the override exists in the layout in the temp-store.
    $this->drupalLogin($this->editor);
    $override_url = Url::fromRoute('decoupled_lb_api.get_layout', [
      'section_storage_type' => 'overrides',
      'section_storage' => sprintf('user.%d', $this->user->id()),
    ]);
    $this->drupalGet($override_url);
    $this->assertSession()->statusCodeEquals(200);
    $data = $this->getJsonFromResponse();
    $this->assertNotEmpty($data['data']['sections']);
    $last_section = end($data['data']['sections']);
    $this->assertArrayHasKey($component->getUuid(), $last_section['components']);

    // Get the override URL.
    $this->drupalGet($discard_url);
    // Route only works for POST.
    $this->assertSession()->statusCodeEquals(405);

    // Missing token.
    $response = $this->makeApiRequest('POST', $discard_url, []);
    $this->assertEquals(403, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'access_denied' => [
          'message' => 'Missing X-CSRF-Token header',
          'identifier' => 'access_denied',
        ],
      ],
    ], $data);

    // Invalid token.
    $response = $this->makeApiRequest('POST', $discard_url, ['headers' => ['X-CSRF-Token' => 'this is not a token']]);
    $this->assertEquals(403, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertEquals([
      'errors' => [
        'access_denied' => [
          'message' => 'Invalid X-CSRF-Token header',
          'identifier' => 'access_denied',
        ],
      ],
    ], $data);

    // Now with valid token.
    $this->drupalGet('/session/token');
    $token = $this->getSession()->getPage()->getContent();
    $response = $this->makeApiRequest('POST', $discard_url, ['headers' => ['X-CSRF-Token' => $token]]);
    $this->assertEquals(200, $response->getStatusCode());
    $data = Json::decode((string) $response->getBody());
    $this->assertNotEmpty($data['data']['sections']);
    $last_section = end($data['data']['sections']);
    $this->assertArrayNotHasKey($component->getUuid(), $last_section['components']);
  }

}
