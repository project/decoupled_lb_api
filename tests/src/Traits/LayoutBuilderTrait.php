<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Traits;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Field\LayoutSectionItemList;
use Drupal\layout_builder\OverridesSectionStorageInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;

/**
 * Defines a trait for various layout builder operations.
 */
trait LayoutBuilderTrait {

  /**
   * Add an inline block into a section.
   *
   * @param \Drupal\block_content\BlockContentInterface $inline_block
   *   Block.
   * @param \Drupal\layout_builder\Section $section
   *   Section.
   * @param array $settings
   *   Settings for the block.
   * @param string $region
   *   Region.
   *
   * @return \Drupal\layout_builder\SectionComponent
   *   Added component.
   */
  protected function addInlineBlockToSection(
    BlockContentInterface $inline_block,
    Section $section,
    array $settings = [],
    string $region = 'content'
  ): SectionComponent {
    $component = new SectionComponent(\Drupal::service('uuid')->generate(),
      $region, [
        'id' => sprintf('inline_block:%s', $inline_block->bundle()),
        'block_revision_id' => $inline_block->getRevisionId(),
      ] + $settings
    );
    $section->appendComponent($component);
    return $component;
  }

  /**
   * Add a section to a layout.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity.
   * @param \Drupal\layout_builder\Section $section
   *   Section.
   * @param bool $ensureDefaults
   *   TRUE to ensure the entity has default layout copied first.
   */
  protected function addSectionToEntityLayout(
    ContentEntityInterface $entity,
    Section $section,
    bool $ensureDefaults = TRUE,
  ): void {
    $overrides = $this->getOverridesStorageForEntity($entity);
    $section_list = $entity->get(OverridesSectionStorage::FIELD_NAME);
    assert($section_list instanceof LayoutSectionItemList);
    if ($ensureDefaults && !$overrides->isOverridden()) {
      // Copy in defaults, this is normally done by
      // \Drupal\layout_builder\EventSubscriber\PrepareLayout::onPrepareLayout.
      $sections = $overrides->getDefaultSectionStorage()->getSections();
      foreach ($sections as $default_section) {
        $overrides->appendSection($default_section);
      }
    }

    $section_list->appendSection($section);
    $entity->save();
  }

  /**
   * Gets overrides section storage for entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   *
   * @return \Drupal\layout_builder\OverridesSectionStorageInterface
   *   Overrides section storage.
   */
  protected function getOverridesStorageForEntity(ContentEntityInterface $entity): OverridesSectionStorageInterface {
    $overrides = \Drupal::service('plugin.manager.layout_builder.section_storage')->load('overrides', [
      'entity' => EntityContext::fromEntity($entity),
      'view_mode' => new Context(ContextDefinition::create('string'), 'default'),
    ]);
    assert($overrides instanceof OverridesSectionStorageInterface);
    return $overrides;
  }

  /**
   * Ensure layout is overridden.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity.
   */
  protected function ensureEntityLayoutIsOverridden(
    ContentEntityInterface $entity,
  ): void {
    $overrides = \Drupal::service('plugin.manager.layout_builder.section_storage')
      ->load('overrides', [
        'entity' => EntityContext::fromEntity($entity),
        'view_mode' => new Context(ContextDefinition::create('string'), 'default'),
      ]);
    assert($overrides instanceof OverridesSectionStorageInterface);
    $section_list = $entity->get(OverridesSectionStorage::FIELD_NAME);
    assert($section_list instanceof LayoutSectionItemList);
    if (!$overrides->isOverridden()) {
      // Copy in defaults, this is normally done by
      // \Drupal\layout_builder\EventSubscriber\PrepareLayout::onPrepareLayout.
      $sections = $overrides->getDefaultSectionStorage()->getSections();
      foreach ($sections as $default_section) {
        $overrides->appendSection($default_section);
      }
    }
  }

  /**
   * Enables layout builder overrides for a given entity-type.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Bundle ID.
   * @param string $view_mode
   *   View mode.
   */
  protected function enableLayoutBuilderForEntityTypeAndBundle(string $entity_type_id, string $bundle, string $view_mode = 'default'): void {
    $display = \Drupal::service('entity_display.repository')->getViewDisplay($entity_type_id, $bundle, $view_mode);
    assert($display instanceof LayoutBuilderEntityViewDisplay);
    $display->enableLayoutBuilder();
    $display->setOverridable();
    $display->save();
  }

  /**
   * Insert a block into an entity layout.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param string $block_id
   *   Block ID.
   * @param array $config
   *   Block configuration.
   *
   * @return string
   *   Component UUID.
   */
  protected function insertBlockIntoPageLayout(ContentEntityInterface $entity, string $block_id, array $config = []): string {
    $section = new Section('layout_onecol');
    $component = new SectionComponent(\Drupal::service('uuid')
      ->generate(), 'content', [
        'id' => $block_id,
      ] + $config);
    $section->appendComponent($component);
    $this->addSectionToEntityLayout($entity, $section);
    return $component->getUuid();
  }

  /**
   * Gets the UUID of components for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   *
   * @return array
   *   UUIDs.
   */
  protected function getLayoutComponentUuids(ContentEntityInterface $entity): array {
    // Reload the entity from the database.
    $entity = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
    if (!$entity->hasField(OverridesSectionStorage::FIELD_NAME)) {
      return [];
    }
    $section_list = $entity->get(OverridesSectionStorage::FIELD_NAME);
    assert($section_list instanceof LayoutSectionItemList);
    return array_reduce($section_list->getSections(), fn(array $carry, Section $section) => [
      ...$carry,
      ...array_keys($section->getComponents()),
    ], []);
  }

}
