<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Traits;

use cebe\openapi\json\JsonPointer;
use cebe\openapi\Reader;
use cebe\openapi\ReferenceContext;
use cebe\openapi\SpecObjectInterface;
use League\OpenAPIValidation\Schema\Exception\KeywordMismatch;
use League\OpenAPIValidation\Schema\Exception\SchemaMismatch;
use League\OpenAPIValidation\Schema\SchemaValidator;

/**
 * Defines a trait for loading the OpenAPI specification.
 */
trait OpenApiSchemaTrait {

  /**
   * Gets the API specification.
   *
   * @return \cebe\openapi\SpecObjectInterface
   *   Specification.
   */
  protected function getSpecification(): SpecObjectInterface {
    $specification = Reader::readFromYaml(file_get_contents(sprintf('%s/openapi.schema.yml', dirname(__FILE__, 4))));
    $context = new ReferenceContext($specification, "/");
    $context->throwException = FALSE;
    $context->mode = ReferenceContext::RESOLVE_MODE_ALL;
    $specification->resolveReferences($context);
    $specification->setDocumentContext($specification, new JsonPointer(''));
    return $specification;
  }

  /**
   * Assert data complies with open API spec.
   *
   * @param array $data
   *   Data.
   * @param string $schemaType
   *   Schema type reference.
   */
  protected function assertDataCompliesWithApiSpecification(array $data, string $schemaType): void {
    $validator = new SchemaValidator();
    try {
      $validator->validate($data, $this->getSpecification()->components->schemas[$schemaType]);
      $this->addToAssertionCount(1);
    }
    catch (KeywordMismatch $e) {
      $this->fail(sprintf('%s:%s %s', implode('➡', $e->dataBreadCrumb()->buildChain()), $e->keyword(), $e->getMessage()));
    }
    catch (SchemaMismatch $e) {
      $this->fail(sprintf('%s %s', implode('➡', $e->dataBreadCrumb()->buildChain()), $e->getMessage()));
    }
  }

}
