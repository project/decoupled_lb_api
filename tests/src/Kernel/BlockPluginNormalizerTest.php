<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\system\Plugin\Block\SystemMenuBlock;
use Drupal\Tests\block\Traits\BlockCreationTrait;

/**
 * Test normalization of block plugins.
 *
 * @covers \Drupal\decoupled_lb_api\Normalizer\BlockPluginNormalizer
 *
 * @group decoupled_lb_api
 */
final class BlockPluginNormalizerTest extends DecoupledLbApiKernelTestBase {

  use BlockCreationTrait;

  /**
   * Tests the default serializer.
   */
  public function testSerializationDefault(): void {
    $block = $this->placeBlock('system_menu_block:main', [
      'level' => 3,
      'depth' => 1,
      'expand_all_items' => TRUE,
      'label' => 'Main navigation',
      'label_display' => '1',
    ]);
    $output = \Drupal::service('serializer')->normalize($block->getPlugin());
    $this->assertEquals([
      'level' => 3,
      'depth' => 1,
      'expand_all_items' => TRUE,
      'id' => 'system_menu_block:main',
      'label' => 'Main navigation',
      'label_display' => TRUE,
      'context_mapping' => [],
    ], $output);
  }

  /**
   * Tests denormalization.
   */
  public function testDenormalizationDefault(): void {
    $plugin = \Drupal::service('serializer')->denormalize([
      'level' => 3,
      'depth' => 1,
      'expand_all_items' => TRUE,
      'id' => 'system_menu_block:main',
      'label' => 'Main navigation',
      'label_display' => TRUE,
      'context_mapping' => [],
    ], BlockPluginInterface::class);
    $this->assertInstanceOf(SystemMenuBlock::class, $plugin);
    $this->assertEquals('Main navigation', $plugin->getConfiguration()['label']);
  }

}
