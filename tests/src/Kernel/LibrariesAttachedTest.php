<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;

/**
 * Test that libraries are attached during LB rendering
 *
 * @group decoupled_lb_api
 */
final class LibrariesAttachedTest extends DecoupledLbApiKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'decoupled_lb_api_test',
  ];

  public function testLibrariesAttached(): void {
    $this->enableLayoutBuilderForEntityTypeAndBundle('user', 'user');
    $user = $this->createUser();

    // And a block with a library.
    $this->insertBlockIntoPageLayout($user, 'test_attach_library_block');

    $storage = \Drupal::service('plugin.manager.layout_builder.section_storage');
    $section_storage = $storage->load('overrides', [
      'entity' => EntityContext::fromEntity($user),
      'view_mode' => new Context(new ContextDefinition('string'), 'default'),
    ]);

    // Render Layout builder.
    $build = [
      '#type' => 'layout_builder',
      '#section_storage' => $section_storage,
    ];
    \Drupal::service('renderer')->renderRoot($build);
    // Test the library was attached.
    $this->assertContains('core/drupal.dialog.ajax', $build['#attached']['library']);
  }

}
