<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Test field block normalizer.
 *
 * @covers \Drupal\layout_builder\Plugin\Block\FieldBlock
 *
 * @group decoupled_lb_api
 */
final class FieldBlockNormalizerTest extends DecoupledLbApiKernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installConfig(['node']);
    $this->createContentType(['type' => 'basic']);
    $this->enableLayoutBuilderForEntityTypeAndBundle('node', 'basic');
  }

  /**
   * Tests normalizing.
   */
  public function testNormalize(): void {
    $plugin = \Drupal::service('plugin.manager.block')->createInstance('field_block:node:basic:body', [
      'label_display' => FALSE,
      'formatter' => [
        'label' => 'hidden',
        'type' => 'text_default',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'context_mapping' => [
        'entity' => 'layout_builder.entity',
        'view_mode' => 'view_mode',
      ],
    ]);
    $body = $this->getRandomGenerator()->sentences(10);
    $node = $this->createNode([
      'type' => 'basic',
      'title' => $this->randomMachineName(),
      'body' => [
        'value' => $body,
        'format' => 'plain_text',
      ],
    ]);
    $contexts = [
      'layout_builder.entity' => EntityContext::fromEntity($node),
      'view_mode' => new Context(ContextDefinition::create('string'), 'full'),
    ];
    \Drupal::service('context.handler')->applyContextMapping($plugin, $contexts);
    $output = \Drupal::service('serializer')->normalize($plugin);
    $this->assertEquals([
      'label_display' => FALSE,
      'formatter' => [
        'label' => 'hidden',
        'type' => 'text_default',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'id' => 'field_block:node:basic:body',
      'label' => '',
      'field_values' => [
        [
          'value' => $body,
          'format' => 'plain_text',
          'summary' => NULL,
          'processed' => _filter_autop($body),
        ],
      ],
      'context_mapping' => [
        'entity' => 'layout_builder.entity',
        'view_mode' => 'view_mode',
      ],
    ], $output);
  }

}
