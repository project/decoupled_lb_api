<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionListInterface;
use Drupal\Tests\decoupled_lb_api\Traits\OpenApiSchemaTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test serialization of section lists.
 *
 * @covers \Drupal\decoupled_lb_api\Normalizer\SectionComponentNormalizer
 * @covers \Drupal\decoupled_lb_api\Normalizer\SectionListNormalizer
 *
 * @group decoupled_lb_api
 */
final class SectionListNormalizerTest extends DecoupledLbApiKernelTestBase {

  use UserCreationTrait;
  use OpenApiSchemaTrait;

  /**
   * Tests serialization of section lists and components.
   */
  public function testNormalize(): void {
    $this->enableLayoutBuilderForEntityTypeAndBundle('user', 'user');
    $user = $this->createUser();

    $body = $this->getRandomGenerator()->sentences(10);
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $body,
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();

    $label = $this->randomMachineName();
    $section = new Section('layout_onecol', [
      'label' => $label,
    ]);

    $component1 = $this->addInlineBlockToSection($block_content, $section, [
      'view_mode' => 'default',
      'label' => 'A block',
      'label_display' => '0',
    ]);

    // Then add our inline block.
    $this->addSectionToEntityLayout($user, $section, FALSE);

    // And a menu block.
    $component2_uuid = $this->insertBlockIntoPageLayout($user, 'system_menu_block:main', [
      'level' => 3,
      'depth' => 1,
      'expand_all_items' => TRUE,
      'label' => 'Main navigation',
      'label_display' => '1',
    ]);

    $section_list = $user->get(OverridesSectionStorage::FIELD_NAME);
    assert($section_list instanceof SectionListInterface);
    $storage = \Drupal::service('plugin.manager.layout_builder.section_storage');
    $section_storage = $storage->load('overrides', [
      'entity' => EntityContext::fromEntity($user),
      'view_mode' => new Context(new ContextDefinition('string'), 'default'),
    ]);
    $normalized = \Drupal::service('serializer')->normalize($section_storage);
    $full_object = $normalized;
    $third_party_settings = array_column($normalized['sections'], 'third_pary_settings');
    foreach ($third_party_settings as $setting) {
      $this->assertArrayHasKey('decoupled_lb_api', $setting);
      $this->assertNotEmpty($setting['decoupled_lb_api']['uuid']);
      $this->assertNotEmpty($setting['decoupled_lb_api']['region_uuids']);
    }
    $normalized['sections'] = array_map(function (array $section) {
      unset($section['third_party_settings']);
      foreach ($section['components'] as $id => $component) {
        $this->assertArrayHasKey('fallback', $component);
      }
      unset($section['components'][$id]['fallback']);
      return $section;
    }, $normalized['sections']);
    $this->assertEquals([
      'sections' => [
        [
          'layout_id' => 'layout_onecol',
          'layout_settings' => [
            'label' => $label,
          ],
          'components' => [
            $component1->getUuid() => [
              'uuid' => $component1->getUuid(),
              'region' => 'content',
              'configuration' => [
                'id' => 'inline_block:basic',
                'label' => 'A block',
                'label_display' => FALSE,
                // Saving the user when we add blocks triggers a new revision
                // of the inline block, so we load back the latest version of
                // configuration to get the current revision ID.
                'block_revision_id' => $section_list->getSection(0)->getComponent($component1->getUuid())->get('configuration')['block_revision_id'],
                'view_mode' => 'default',
                'view_mode_uuid' => EntityViewDisplay::collectRenderDisplay($block_content, 'default')->uuid(),
                'form_mode_uuid' => EntityFormDisplay::collectRenderDisplay($block_content, 'edit')->uuid(),
                'changed' => [],
                'block_serialized' => [
                  'body' => [
                    [
                      'value' => $body,
                      'format' => 'plain_text',
                      'processed' => _filter_autop($body),
                      'summary' => NULL,
                    ],
                  ],
                  'langcode' => [
                    ['value' => $block_content->language()->getId()],
                  ],
                ],
                'context_mapping' => [],
              ],
              'weight' => 0,
              'additional' => [],
            ],
          ],
        ],
        [
          'layout_id' => 'layout_onecol',
          'layout_settings' => [
            'label' => '',
          ],
          'components' => [
            $component2_uuid => [
              'uuid' => $component2_uuid,
              'region' => 'content',
              'configuration' => [
                'level' => 3,
                'depth' => 1,
                'expand_all_items' => TRUE,
                'id' => 'system_menu_block:main',
                'label' => 'Main navigation',
                'label_display' => TRUE,
                'context_mapping' => [],
              ],
              'weight' => 0,
              'additional' => [],
            ],
          ],
        ],
      ],
    ], $normalized);
    foreach ($full_object['sections'] as $section) {
      $this->assertDataCompliesWithApiSpecification($section, 'Section');
    }
  }

  /**
   * Tests denormalization of section lists and components.
   */
  public function testDenormalize(): void {
    $label = $this->randomMachineName();
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $this->randomMachineName(),
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $data = [
      [
        'layout_id' => 'layout_onecol',
        'layout_settings' => [
          'label' => $label,
        ],
        'components' => [
          '3fe8d2d5-e53f-48fc-b62a-b2df36332966' => [
            'uuid' => '3fe8d2d5-e53f-48fc-b62a-b2df36332966',
            'region' => 'content',
            'configuration' => [
              'id' => 'inline_block:basic',
              'label' => 'A block',
              'label_display' => FALSE,
              'block_revision_id' => $block_content->getRevisionId(),
              'view_mode' => 'default',
              'changed' => ['body'],
              'block_serialized' => [
                'body' => [
                  [
                    'value' => 'faff',
                    'format' => 'plain_text',
                    'summary' => NULL,
                  ],
                ],
              ],
              'context_mapping' => [],
            ],
            'weight' => 0,
            'additional' => [],
          ],
          '8288e2a9-1907-451d-954e-6fa3cf00bc07' => [
            'uuid' => '8288e2a9-1907-451d-954e-6fa3cf00bc07',
            'region' => 'content',
            'configuration' => [
              'id' => 'inline_block:basic',
              'label' => 'A block',
              'label_display' => FALSE,
              'block_revision_id' => NULL,
              'view_mode' => 'default',
              'changed' => ['body'],
              'block_serialized' => [
                'body' => [
                  [
                    'value' => 'faff',
                    'format' => 'plain_text',
                    'summary' => NULL,
                  ],
                ],
              ],
              'context_mapping' => [],
            ],
            'weight' => 0,
            'additional' => [],
          ],
        ],
      ],
      [
        'layout_id' => 'layout_onecol',
        'layout_settings' => [
          'label' => '',
        ],
        'components' => [
          '3fe8d2d5-e53f-48fc-b62a-b2df36332966' => [
            'uuid' => '3fe8d2d5-e53f-48fc-b62a-b2df36332966',
            'region' => 'content',
            'configuration' => [
              'level' => 3,
              'depth' => 1,
              'expand_all_items' => TRUE,
              'id' => 'system_menu_block:main',
              'label' => 'Main navigation',
              'label_display' => TRUE,
              'context_mapping' => [],
            ],
            'weight' => 0,
            'additional' => [],
          ],
        ],
      ],
    ];
    $list = \Drupal::service('serializer')->denormalize($data, SectionListInterface::class);
    $this->assertCount(2, $list);
    [$section1, $section2] = $list;
    $this->assertInstanceOf(Section::class, $section1);
    $this->assertInstanceOf(Section::class, $section2);
    $this->assertCount(2, $section1->getComponents());
    $this->assertCount(1, $section2->getComponents());
    $this->assertEquals('system_menu_block:main', $section2->getComponent('3fe8d2d5-e53f-48fc-b62a-b2df36332966')->get('configuration')['id']);
    $this->assertEquals($label, $section1->getLayoutSettings()['label']);
    $this->assertEquals('layout_onecol', $section2->getLayoutId());
  }

}
