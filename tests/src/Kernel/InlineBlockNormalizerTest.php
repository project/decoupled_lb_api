<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\decoupled_lb_api\Controller\SelectionController;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\Tests\block\Traits\BlockCreationTrait;

/**
 * Test normalization of inline block plugins.
 *
 * @covers \Drupal\decoupled_lb_api\Normalizer\InlineBlockNormalizer
 *
 * @group decoupled_lb_api
 */
final class InlineBlockNormalizerTest extends DecoupledLbApiKernelTestBase {

  use BlockCreationTrait;

  /**
   * Tests denormalization.
   */
  public function testDenormalization(): void {
    $body = $this->getRandomGenerator()->sentences(10);
    $body2 = $this->getRandomGenerator()->sentences(10);
    $block_content = BlockContent::create([
      'type' => 'basic',
      'body' => [
        'value' => $this->randomMachineName(),
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $block_content_plugin_update = \Drupal::service('serializer')->denormalize([
      'id' => 'inline_block:basic',
      'label' => 'A block',
      'label_display' => FALSE,
      'block_revision_id' => $block_content->getRevisionId(),
      'view_mode' => 'default',
      'changed' => ['body', 'nosuchfield', 'id'],
      'block_serialized' => [
        'body' => [
          [
            'value' => $body,
            'format' => 'plain_text',
            'summary' => NULL,
          ],
          'id' => [
            'value' => 111,
          ],
        ],
      ],
      'context_mapping' => [],
    ], InlineBlock::class);
    $this->assertInstanceOf(InlineBlock::class, $block_content_plugin_update);
    $block_content_update = unserialize($block_content_plugin_update->getConfiguration()['block_serialized'], [
      'allowed_classes' => [BlockContent::class],
    ]);
    $this->assertEquals($body, $block_content_update->get('body')->value);
    $this->assertEquals($block_content->getRevisionId(), $block_content_update->getRevisionId());
    $this->assertEquals($block_content->id(), $block_content_update->id());
    $this->assertEquals($block_content->bundle(), $block_content_update->bundle());

    $block_content_plugin_new = \Drupal::service('serializer')->denormalize([
      'id' => 'inline_block:basic',
      'label' => 'A block',
      'label_display' => FALSE,
      'block_revision_id' => NULL,
      'view_mode' => 'default',
      'changed' => ['body', 'id'],
      'block_serialized' => [
        'body' => [
          [
            'value' => $body2,
            'format' => 'plain_text',
            'summary' => NULL,
          ],
        ],
        'nosuchfield' => [
          'value' => 'whoops',
        ],
        'id' => [
          'value' => 111,
        ],
      ],
      'context_mapping' => [],
    ], InlineBlock::class);
    $this->assertInstanceOf(InlineBlock::class, $block_content_plugin_new);
    $block_content_new = unserialize($block_content_plugin_new->getConfiguration()['block_serialized'], [
      'allowed_classes' => [BlockContent::class],
    ]);
    assert($block_content_new instanceof BlockContentInterface);
    $this->assertEquals($body2, $block_content_new->get('body')->value);
    $this->assertTrue($block_content_new->isNew());
    $this->assertNull($block_content_new->getRevisionId());
    $this->assertNull($block_content_new->id());
    $this->assertEquals('basic', $block_content_new->bundle());
  }

  /**
   * Tests default values.
   */
  public function testNormalizeDefaultValues(): void {
    // Test normalizing an empty block.
    $block_plugin = \Drupal::service('plugin.manager.block')->createInstance('inline_block:basic', []);
    $serializer = \Drupal::service('serializer');
    $output = $serializer->normalize($block_plugin, context: [SelectionController::DEFAULT_VALUES => TRUE]);
    $sample = \Drupal::service('layout_builder.sample_entity_generator')->get('block_content', 'basic');
    assert($sample instanceof BlockContentInterface);
    $this->assertNotEmpty($sample->get('body')->value);
    $this->assertEquals([
      'id' => 'inline_block:basic',
      'label' => '',
      'label_display' => TRUE,
      'block_revision_id' => NULL,
      'view_mode' => 'full',
      'view_mode_uuid' => EntityViewDisplay::collectRenderDisplay($sample, 'full')->uuid(),
      'form_mode_uuid' => EntityFormDisplay::collectRenderDisplay($sample, 'edit')->uuid(),
      'changed' => [],
      'block_serialized' => [
        'body' => $serializer->normalize($sample->get('body')),
        'info' => [
          ['value' => $sample->label()],
        ],
        'langcode' => [
          ['value' => $sample->language()->getId()],
        ],
        'revision_log' => [
          ['value' => $sample->get('revision_log')->value],
        ],
      ],
      'context_mapping' => [],
    ], $output);
    $this->assertNotEmpty($output['block_serialized']['body'][0]['value']);
  }

  /**
   * Tests serialization of inline blocks.
   *
   * @dataProvider providerViewMode
   */
  public function testNormalization(string $view_mode): void {
    $body = $this->getRandomGenerator()->sentences(10);
    $info = $this->randomMachineName();
    $block_content = BlockContent::create([
      'type' => 'basic',
      'info' => $info,
      'body' => [
        'value' => $body,
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $block_plugin = \Drupal::service('plugin.manager.block')->createInstance('inline_block:basic', [
      'block_revision_id' => $block_content->getRevisionId(),
      'view_mode' => $view_mode,
      'label' => 'A block',
      'label_display' => '0',
    ]);
    $output = \Drupal::service('serializer')->normalize($block_plugin);
    $this->assertEquals([
      'id' => 'inline_block:basic',
      'label' => 'A block',
      'label_display' => FALSE,
      'block_revision_id' => $block_content->getRevisionId(),
      'view_mode' => $view_mode,
      'view_mode_uuid' => EntityViewDisplay::collectRenderDisplay($block_content, 'full')->uuid(),
      'form_mode_uuid' => EntityFormDisplay::collectRenderDisplay($block_content, 'edit')->uuid(),
      'changed' => [],
      'block_serialized' => [
        'body' => [
          [
            'value' => $body,
            'format' => 'plain_text',
            'processed' => _filter_autop($body),
            'summary' => NULL,
          ],
        ],
        'info' => [
          ['value' => $info],
        ],
        'langcode' => [
          ['value' => 'en'],
        ],
      ],
      'context_mapping' => [],
    ], $output);
  }

  /**
   * Data provider for ::testSerializationInlineBlock.
   *
   * @return array[]
   *   Test cases.
   */
  public function providerViewMode(): array {
    return [
      'default' => ['default'],
      // This should fall back to 'default'.
      'no such view mode' => ['nope'],
      'full' => ['full'],
    ];
  }

}
