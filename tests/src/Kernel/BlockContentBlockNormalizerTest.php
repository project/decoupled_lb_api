<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\block\Traits\BlockCreationTrait;

/**
 * Test normalization of block content block plugins.
 *
 * @covers \Drupal\decoupled_lb_api\Normalizer\BlockContentBlockNormalizer
 *
 * @group decoupled_lb_api
 */
final class BlockContentBlockNormalizerTest extends DecoupledLbApiKernelTestBase {

  use BlockCreationTrait;

  /**
   * Tests serialization of inline blocks.
   *
   * @dataProvider providerViewMode
   */
  public function testNormalization(string $view_mode): void {
    $body = $this->getRandomGenerator()->sentences(10);
    $info = $this->randomMachineName();
    $block_content = BlockContent::create([
      'type' => 'basic',
      'info' => $info,
      'body' => [
        'value' => $body,
        'format' => 'plain_text',
      ],
    ]);
    $block_content->save();
    $plugin_id = 'block_content:' . $block_content->uuid();
    $block_plugin = \Drupal::service('plugin.manager.block')->createInstance($plugin_id, [
      'view_mode' => $view_mode,
      'label' => 'A block',
      'label_display' => '0',
    ]);
    $output = \Drupal::service('serializer')->normalize($block_plugin);
    $this->assertEquals([
      'id' => $plugin_id,
      'label' => 'A block',
      'label_display' => FALSE,
      'uuid' => $block_content->uuid(),
      'view_mode' => $view_mode,
      'view_mode_uuid' => EntityViewDisplay::collectRenderDisplay($block_content, $view_mode)->uuid(),
      'status' => TRUE,
      'info' => '',
      'field_values' => [
        'body' => [
          [
            'value' => $body,
            'format' => 'plain_text',
            'processed' => _filter_autop($body),
            'summary' => NULL,
          ],
        ],
      ],
      'context_mapping' => [],
    ], $output);
  }

  /**
   * Data provider for ::testSerializationInlineBlock.
   *
   * @return array[]
   *   Test cases.
   */
  public function providerViewMode(): array {
    return [
      'default' => ['default'],
      // This should fall back to 'default'.
      'no such view mode' => ['nope'],
      'full' => ['full'],
    ];
  }

}
