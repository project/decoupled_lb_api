<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\decoupled_lb_api\Traits\LayoutBuilderTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a base kernel test class for decoupled LB api module.
 */
abstract class DecoupledLbApiKernelTestBase extends KernelTestBase {

  use UserCreationTrait;
  use LayoutBuilderTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'layout_builder',
    'decoupled_lb_api',
    'block_content',
    'user',
    'system',
    'jsonapi',
    'block',
    'contextual',
    'layout_discovery',
    'text',
    'field',
    'filter',
    'serialization',
    'file',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('block_content');
    $this->installEntitySchema('file');
    $this->installConfig(['user', 'system', 'block_content', 'filter']);
    $this->installSchema('layout_builder', ['inline_block_usage']);
    BlockContentType::create(['id' => 'basic', 'label' => 'Basic'])->save();
    \block_content_add_body_field('basic');
    \Drupal::service('theme_installer')->install(['stark']);
  }

}
