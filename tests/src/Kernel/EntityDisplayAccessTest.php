<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Kernel;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Session\AnonymousUserSession;

/**
 * Tests that layout builder users can view form/view displays in JSONAPI.
 *
 * @group decoupled_lb_api
 */
final class EntityDisplayAccessTest extends DecoupledLbApiKernelTestBase {

  /**
   * Tests access to entity view/form displays.
   */
  public function testAccess(): void {
    $anon = new AnonymousUserSession();
    // Create user 1.
    $this->createUser();
    $editor = $this->createUser(['create and edit custom blocks']);
    $non_editor = $this->createUser();
    $admin = $this->createUser([
      'administer block_content display',
      'administer block_content form display',
      'administer user display',
      'administer user form display',
    ]);

    $entity_display_repository = \Drupal::service('entity_display.repository');
    assert($entity_display_repository instanceof EntityDisplayRepositoryInterface);
    $block_content_entity_view_display = $entity_display_repository->getViewDisplay('block_content', 'basic');
    $block_content_entity_form_display = $entity_display_repository->getFormDisplay('block_content', 'basic');
    $user_entity_view_display = $entity_display_repository->getViewDisplay('user', 'user');
    $user_entity_form_display = $entity_display_repository->getFormDisplay('user', 'user');

    foreach ([
      $block_content_entity_form_display,
      $block_content_entity_view_display,
    ] as $display) {
      if ($display->isNew()) {
        $display->save();
      }
      // Only admin and editor should be able to 'view' the block content
      // displays.
      $this->assertFalse($display->access('view', $anon));
      $this->assertFalse($display->access('view', $non_editor));
      $this->assertTrue($display->access('view', $editor));
      $this->assertTrue($display->access('view', $admin));

      // Only admin should be able to 'update' or 'delete' the block content
      // displays.
      foreach (['update', 'delete'] as $operation) {
        $this->assertFalse($display->access($operation, $anon), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
        $this->assertFalse($display->access($operation, $non_editor), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
        $this->assertFalse($display->access($operation, $editor), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
        $this->assertTrue($display->access($operation, $admin), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
      }
    }
    foreach ([
      $user_entity_form_display,
      $user_entity_view_display,
    ] as $display) {
      if ($display->isNew()) {
        $display->save();
      }
      // Only admin should be able to 'view', 'update' or 'delete' the user
      // displays.
      foreach (['update', 'delete', 'view'] as $operation) {
        $this->assertFalse($display->access($operation, $anon), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
        $this->assertFalse($display->access($operation, $non_editor), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
        $this->assertFalse($display->access($operation, $editor), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
        $this->assertTrue($display->access($operation, $admin), sprintf('Access test for %s on %s', $operation, $display->getEntityTypeId()));
      }
    }
  }

}
