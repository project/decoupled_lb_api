<?php

declare(strict_types=1);

namespace Drupal\Tests\decoupled_lb_api\Unit;

use Drupal\Tests\decoupled_lb_api\Traits\OpenApiSchemaTrait;
use Drupal\Tests\UnitTestCase;
use DrupalFinder\DrupalFinder;
use JsonSchema\Validator;

/**
 * Tests open API schema is valid.
 *
 * @group decoupled_lb_api
 */
final class SchemaValidationTest extends UnitTestCase {

  use OpenApiSchemaTrait;

  /**
   * Path to Open API 3.0 schema definition.
   */
  private ?string $schemaLocation = NULL;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $possible_start_paths = [dirname(DRUPAL_ROOT), DRUPAL_ROOT];
    $tested_paths = [];
    foreach ($possible_start_paths as $path) {
      $finder = new DrupalFinder();
      $finder->locateRoot($path);
      $vendor_directory = $finder->getVendorDir();
      if (!$vendor_directory) {
        continue;
      }
      $schema_location = $vendor_directory . '/cebe/php-openapi/schemas/openapi-v3.0.json';
      if (!file_exists($schema_location)) {
        $tested_paths[] = $schema_location;
        continue;
      }
      $this->schemaLocation = $schema_location;
      break;
    }
    if (!$this->schemaLocation) {
      throw new \Exception(sprintf('Could not open API 3.0 schema at %s.', implode(' or ', $tested_paths)));
    }
  }

  /**
   * Tests schema is valid.
   */
  public function testSchemaIsValid(): void {
    $specification = $this->getSpecification();
    $specification->validate();
    $this->assertEmpty($specification->getErrors());
    $validator = new Validator();
    $open_api_data = $specification->getSerializableData();
    $validator->validate($open_api_data, (object) ['$ref' => 'file://' . $this->schemaLocation]);
    $this->assertTrue($validator->isValid(), implode(array_map(function (array $error) {
      return sprintf('%s:%s%s', $error['property'], $error['message'], \PHP_EOL);
    }, $validator->getErrors())));
  }

}
