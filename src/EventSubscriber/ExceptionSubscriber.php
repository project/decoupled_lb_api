<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Http\Exception\CacheableAccessDeniedHttpException;
use Drupal\Core\Routing\RouteMatch;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines an exception subscriber to convert errors into expected JSON format.
 */
final class ExceptionSubscriber extends HttpExceptionSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function onException(ExceptionEvent $event): void {
    $route_match = RouteMatch::createFromRequest($event->getRequest());
    if (!($route_name = $route_match->getRouteName()) || !str_starts_with($route_name, 'decoupled_lb_api')) {
      parent::onException($event);
      return;
    }
    $exception = $event->getThrowable();
    [$status_code, $identifier] = match($exception::class) {
      CacheableAccessDeniedHttpException::class, AccessDeniedHttpException::class => [
        403,
        'access_denied',
      ],
      NotFoundHttpException::class => [404, 'not_found'],
      BadRequestException::class => [401, 'bad_request'],
      HttpExceptionInterface::class => [$exception->getStatusCode(), 'default'],
      default => [200, 'default'],
    };
    $response = new JsonResponse([
      'errors' => [
        $identifier => [
          'message' => $exception->getMessage(),
          'identifier' => $identifier,
        ],
      ],
    ], $status_code);
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return ['json'];
  }

}
