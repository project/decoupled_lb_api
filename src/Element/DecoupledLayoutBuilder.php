<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Element;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Field\FormatterPluginManager;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\layout_builder\Element\LayoutBuilder;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a replacement class for the 'layout_builder' render element.
 */
final class DecoupledLayoutBuilder extends LayoutBuilder {

  /**
   * Layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected LayoutPluginManagerInterface $layoutPluginManager;

  /**
   * Block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected BlockManagerInterface $blockManager;

  /**
   * Formatter manager.
   *
   * @var \Drupal\Core\Field\FormatterPluginManager
   */
  protected FormatterPluginManager $formatterPluginManager;

  /**
   * Widget manager.
   *
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  protected WidgetPluginManager $widgetPluginManager;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->blockManager = $container->get('plugin.manager.block');
    $instance->formatterPluginManager = $container->get('plugin.manager.field.formatter');
    $instance->widgetPluginManager = $container->get('plugin.manager.field.widget');
    $instance->layoutPluginManager = $container->get('plugin.manager.core.layout');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function layout(SectionStorageInterface $section_storage): array {
    $this->prepareLayout($section_storage);

    $output = [];
    $output['#attached']['drupalSettings']['path']['currentPathIsAdmin'] = TRUE;

    $blockInfo = array_filter(array_map(fn (array $definition) => $definition['decoupled_lb'] ?? FALSE, $this->blockManager->getDefinitions()));
    $formatterInfo = array_filter(array_map(fn (array $definition) => $definition['decoupled_lb'] ?? FALSE, $this->formatterPluginManager->getDefinitions()));
    $widgetInfo = array_filter(array_map(fn (array $definition) => $definition['decoupled_lb'] ?? FALSE, $this->widgetPluginManager->getDefinitions()));
    $layoutInfo = array_filter(array_map(fn (LayoutDefinition $definition) => $definition->get('decoupled_lb') ?? FALSE, $this->layoutPluginManager->getDefinitions()));

    $output['#attached']['library'] = array_filter(array_map(fn (LayoutDefinition $definition) => $definition->getLibrary(), $this->layoutPluginManager->getDefinitions()));

    // Add the path to each component to drupalSettings.
    $output['#attached']['drupalSettings']['decoupledLb'] = [
      'blockPlugins' => array_combine(array_keys($blockInfo), array_column($blockInfo, 'componentPath')),
      'layoutPlugins' => array_combine(array_keys($layoutInfo), array_column($layoutInfo, 'componentPath')),
      'widgetPlugins' => array_combine(array_keys($widgetInfo), array_column($widgetInfo, 'componentPath')),
      'formatterPlugins' => array_combine(array_keys($formatterInfo), array_column($formatterInfo, 'componentPath')),
    ];

    // Attach any additional libraries.
    $output['#attached']['library'][] = 'decoupled_lb_api/decoupled_layout_builder';
    $output['#attached']['library'] = array_merge($output['#attached']['library'] ?? [],
      array_values(array_filter(array_column($blockInfo, 'library'))),
      array_values(array_filter(array_column($layoutInfo, 'library'))),
      array_values(array_filter(array_column($widgetInfo, 'library'))),
      array_values(array_filter(array_column($formatterInfo, 'library'))),
    );

    $output['#type'] = 'container';
    $output['#attributes']['id'] = 'layout-builder-decoupled';
    $output['#attributes']['class'][] = 'layout-builder-decoupled';
    $output['#attributes']['data-section-storage-type'] = $section_storage->getStorageType();
    $output['#attributes']['data-section-storage'] = $section_storage->getStorageId();
    $output['#cache']['max-age'] = 0;
    $context = new RenderContext();
    // Get all libraries attached to the sections in the storage.
    foreach ($section_storage->getSections() as $section) {
      $render = $section->toRenderArray($this->getPopulatedContexts($section_storage));
      $this->renderer->executeInRenderContext($context, fn() => $this->renderer->render($render));
    }
    // Attach the collated libraries.
    $output['#attached']['library'] = array_merge(
      $output['#attached']['library'],
      array_unique($context->pop()->getAttachments()['library']) ?? []
    );
    return $output;
  }

}
