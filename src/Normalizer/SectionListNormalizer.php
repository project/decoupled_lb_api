<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Normalizer;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionListInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Defines a normalizer for section lists.
 */
final class SectionListNormalizer extends NormalizerBase implements DenormalizerInterface {

  use LayoutBuilderContextTrait;

  public const CONTEXTS = 'contexts';

  /**
   * Constructs a new SectionListNormalizer.
   *
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   Uuid service.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layoutPluginManager
   *   Layout plugin manager.
   */
  public function __construct(
    protected readonly UuidInterface $uuid,
    protected readonly LayoutPluginManagerInterface $layoutPluginManager) {
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []): array {
    assert($object instanceof SectionStorageInterface);
    $context[self::CONTEXTS] = $this->getPopulatedContexts($object);
    $return = ['sections' => []];
    foreach ($object->getSections() as $section) {
      // Ensure we have uuids for sections and regions.
      $identifiers = $section->getThirdPartySettings('decoupled_lb_api');
      if (!array_key_exists('uuid', $identifiers)) {
        $section->setThirdPartySetting('decoupled_lb_api', 'uuid', $this->uuid->generate());
        $identifiers['uuid'] = $this->uuid->generate();
      }
      $regions = array_keys($this->layoutPluginManager->getDefinition($section->getLayoutId())->getRegions());
      $region_uuids = $identifiers['region_uuids'] ?? [];
      foreach ($regions as $region_id) {
        if (!array_key_exists($region_id, $region_uuids)) {
          $region_uuids[$region_id] = $this->uuid->generate();
        }
      }
      $section->setThirdPartySetting('decoupled_lb_api', 'region_uuids', $region_uuids);
      $providers = $section->getThirdPartyProviders();
      $normalized_section = [
        'layout_id' => $section->getLayoutId(),
        'layout_settings' => $section->getLayoutSettings(),
        'components' => [],
        'third_party_settings' => array_combine(
          $providers,
          array_map(fn(string $provider) => $section->getThirdPartySettings($provider), $providers)
        ),
      ];
      foreach ($section->getComponents() as $component) {
        $normalized_section['components'][$component->getUuid()] = $this->serializer->normalize($component, context: $context);
      }
      $return['sections'][] = $normalized_section;
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [SectionListInterface::class => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(mixed $data, string $type, string $format = NULL, array $context = []): array {
    if (!is_array($data)) {
      throw new UnexpectedValueException('Expected an array of sections');
    }
    $sections = [];
    foreach ($data as $delta => $section_data) {
      if (!array_key_exists('layout_id', $section_data)) {
        throw new UnexpectedValueException(sprintf('Section %d is missing a layout_id', $delta + 1));
      }
      $section = new Section($section_data['layout_id'], $section_data['layout_settings'] ?? []);
      foreach ($section_data['components'] as $uuid => $component_data) {
        $component = $this->serializer->denormalize($component_data, SectionComponent::class, context: ['uuid' => $uuid]);
        $section->appendComponent($component);
      }
      $sections[] = $section;
    }
    return $sections;
  }

}
