<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Normalizer;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\Plugin\Block\BlockContentBlock;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a normalizer for block content blocks.
 */
final class BlockContentBlockNormalizer extends BlockPluginNormalizer {

  /**
   * Constructs a new InlineBlockNormalizer.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   Entity display repository.
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block manager.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityRepositoryInterface $entityRepository,
    protected readonly BlockManagerInterface $blockManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    $data = parent::normalize($object, $format, $context);
    assert($object instanceof BlockContentBlock);
    [, $uuid] = explode(PluginBase::DERIVATIVE_SEPARATOR, $object->getPluginId());
    $data['uuid'] = $uuid;
    if ($block_content = $this->entityRepository->loadEntityByUuid('block_content', $uuid)) {
      assert($block_content instanceof BlockContentInterface);
      $entityViewDisplay = EntityViewDisplay::collectRenderDisplay($block_content, $configuration['view_mode'] ?? 'default');
      $data['field_values'] = array_filter(array_intersect_key(
        $this->serializer->normalize($block_content, $format, $context),
        $entityViewDisplay->getComponents(),
      ));
      $data['view_mode_uuid'] = $entityViewDisplay->uuid();
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [BlockContentBlock::class => TRUE];
  }

}
