<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Normalizer;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\layout_builder\Entity\SampleEntityGeneratorInterface;
use Drupal\layout_builder\Plugin\Block\FieldBlock;

/**
 * Defines a normalizer for field blocks.
 */
final class FieldBlockNormalizer extends BlockPluginNormalizer {

  /**
   * Constructs a new FieldBlockNormalizer.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block manager.
   * @param \Drupal\layout_builder\Entity\SampleEntityGeneratorInterface $sampleEntityGenerator
   *   Sample entity generator.
   */
  public function __construct(
    protected readonly BlockManagerInterface $blockManager,
    protected readonly SampleEntityGeneratorInterface $sampleEntityGenerator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    $normalized = parent::normalize($object, $format, $context);
    assert($object instanceof FieldBlock);
    [, $entity_type_id, $bundle, $field_name] = explode(PluginBase::DERIVATIVE_SEPARATOR, $object->getPluginId());

    try {
      $entity = $object->getContextValue('entity');
    }
    catch (ContextException) {
      // Default value.
      $entity = $this->sampleEntityGenerator->get($entity_type_id, $bundle);
    }
    if (!$entity) {
      $entity = $this->sampleEntityGenerator->get($entity_type_id, $bundle);
    }
    assert($entity instanceof ContentEntityInterface);
    if (!$entity->hasField($field_name)) {
      return $normalized + ['field_values' => []];
    }
    return $normalized + ['field_values' => $this->serializer->normalize($entity->get($field_name))];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [FieldBlock::class => TRUE];
  }

}
