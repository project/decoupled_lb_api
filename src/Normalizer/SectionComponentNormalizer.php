<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Normalizer;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\layout_builder\SectionComponent;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Defines a normalizer for component sections.
 */
final class SectionComponentNormalizer extends NormalizerBase implements DenormalizerInterface {

  /**
   * Constructs a new SectionComponentNormalizer.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer.
   */
  public function __construct(
    protected readonly BlockManagerInterface $blockManager,
    protected readonly RendererInterface $renderer,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []): array {
    assert($object instanceof SectionComponent);
    $render_context = new RenderContext();
    $render_array = $object->toRenderArray($context[SectionListNormalizer::CONTEXTS]);
    $fallback = $this->renderer->executeInRenderContext($render_context, fn() => $this->renderer->render($render_array));
    if (!$render_context->isEmpty() && array_key_exists('cache', $context) && $context['cache'] instanceof CacheableMetadata) {
      $context['cache']->addCacheableDependency($render_context->pop());
    }
    return [
      'uuid' => $object->getUuid(),
      'configuration' => $this->serializer->normalize($object->getPlugin($context[SectionListNormalizer::CONTEXTS])),
      'weight' => $object->getWeight(),
      'region' => $object->getRegion(),
      'additional' => $object->get('additional'),
      'fallback' => (string) $fallback,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [SectionComponent::class => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(mixed $data, string $type, string $format = NULL, array $context = []): SectionComponent {
    if (!array_key_exists('region', $data)) {
      throw new UnexpectedValueException(sprintf('Component %s is missing a region', $context['uuid']));
    }
    if (!array_key_exists('configuration', $data) || !array_key_exists('id', $data['configuration'])) {
      throw new UnexpectedValueException(sprintf('Component %s is missing a component ID', $context['uuid']));
    }
    $plugin_id = $data['configuration']['id'];
    if (!$this->blockManager->hasDefinition($plugin_id)) {
      throw new UnexpectedValueException(sprintf('Component %s uses an invalid block plugin ID %s', $context['uuid'], $plugin_id));
    }
    $definition = $this->blockManager->getDefinition($plugin_id);
    $plugin = $this->serializer->denormalize($data['configuration'], $definition['class'], context: $context);
    assert($plugin instanceof BlockPluginInterface);
    return new SectionComponent($context['uuid'], $data['region'], $plugin->getConfiguration(), $data['additional']);
  }

}
