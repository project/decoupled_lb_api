<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Normalizer;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Defines a class for normalizing block plugins.
 */
class BlockPluginNormalizer extends NormalizerBase implements DenormalizerInterface {

  /**
   * Constructs a new BlockPluginNormalizer.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block manager.
   */
  public function __construct(protected readonly BlockManagerInterface $blockManager) {
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    assert($object instanceof BlockPluginInterface);
    $configuration = $object->getConfiguration() + ['context_mapping' => []];
    // Cast label display to a boolean.
    if (array_key_exists('label_display', $configuration)) {
      $configuration['label_display'] = (bool) $configuration['label_display'];
    }
    // Remove redundant information.
    unset($configuration['provider']);
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      BlockPluginInterface::class => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(mixed $data, string $type, string $format = NULL, array $context = []): BlockPluginInterface {
    if (!is_array($data)) {
      throw new UnexpectedValueException(sprintf('Block configuration should be an array'));
    }
    if (!array_key_exists('id', $data)) {
      throw new UnexpectedValueException(sprintf('Block is missing a plugin ID'));
    }
    if (!$this->blockManager->hasDefinition($data['id'])) {
      throw new UnexpectedValueException(sprintf('No such block plugin %s', $data['id']));
    }
    $instance = $this->blockManager->createInstance($data['id'], array_diff_key($data, array_flip(['id'])));
    assert($instance instanceof BlockPluginInterface);
    return $instance;
  }

}
