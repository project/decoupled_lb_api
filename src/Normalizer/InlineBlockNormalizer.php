<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Normalizer;

use Drupal\block_content\BlockContentInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\decoupled_lb_api\Controller\SelectionController;
use Drupal\layout_builder\Entity\SampleEntityGeneratorInterface;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Defines a normalizer for inline blocks.
 */
final class InlineBlockNormalizer extends BlockPluginNormalizer implements DenormalizerInterface {

  /**
   * Constructs a new InlineBlockNormalizer.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   Entity display repository.
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block manager.
   * @param \Drupal\layout_builder\Entity\SampleEntityGeneratorInterface $sampleEntityGenerator
   *   Sample entity generator.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityRepositoryInterface $entityRepository,
    protected readonly BlockManagerInterface $blockManager,
    protected readonly SampleEntityGeneratorInterface $sampleEntityGenerator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    assert($object instanceof InlineBlock);
    [, $block_content_type] = explode(PluginBase::DERIVATIVE_SEPARATOR, $object->getPluginId());
    $configuration = parent::normalize($object, $format, $context) + ['changed' => []];
    if ($block_content = $this->getBlockContentEntity($configuration, $block_content_type, $context)) {
      assert($block_content instanceof BlockContentInterface);
      $entityViewDisplay = EntityViewDisplay::collectRenderDisplay($block_content, $configuration['view_mode'] ?? 'default');
      $visible_components = array_keys($entityViewDisplay->getComponents());
      // See \Drupal\layout_builder\Plugin\Block\InlineBlock::processBlockForm.
      $entityFormDisplay = EntityFormDisplay::collectRenderDisplay($block_content, 'edit');
      $editable_components = array_keys($entityFormDisplay->getComponents());
      $configuration['block_serialized'] = array_filter(array_intersect_key(
        $this->serializer->normalize($block_content, $format, $context),
        array_flip(array_unique([
          ...$visible_components,
          ...$editable_components,
        ])),
      ));
      $configuration['view_mode_uuid'] = $entityViewDisplay->uuid();
      $configuration['form_mode_uuid'] = $entityFormDisplay->uuid();
    }
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      InlineBlock::class => TRUE,
    ];
  }

  /**
   * Gets the block content entity for this inline block from the configuration.
   *
   * @param array $configuration
   *   Inline block configuration.
   * @param string $block_content_type
   *   Block content type.
   * @param array $context
   *   Normalization context.
   *
   * @return \Drupal\block_content\BlockContentInterface|null
   *   The matching block content entity if it exists.
   */
  protected function getBlockContentEntity(array $configuration, string $block_content_type, array $context): ?BlockContentInterface {
    $storage = $this->entityTypeManager->getStorage('block_content');
    if (!is_null($configuration['block_serialized'])) {
      // A block content entity has update that have not yet been saved and are
      // only in the tempstore.
      $block_content = unserialize($configuration['block_serialized'], ['allowed_classes' => [$this->entityTypeManager->getDefinition('block_content')->getClass()]]);
      if ($block_content instanceof BlockContentInterface) {
        $translation = $this->entityRepository->getTranslationFromContext($block_content);
        assert($translation instanceof BlockContentInterface);
        return $translation;
      }
      // Failed to unserialize.
      return NULL;
    }
    if (is_null($configuration['block_revision_id']) && ($context[SelectionController::DEFAULT_VALUES] ?? FALSE)) {
      // Default values.
      $default_entity = $this->sampleEntityGenerator->get('block_content', $block_content_type);
      assert($default_entity instanceof BlockContentInterface);
      return $default_entity;
    }
    assert($storage instanceof RevisionableStorageInterface);
    $block_content = $storage->loadRevision($configuration['block_revision_id']);
    if (!$block_content) {
      return NULL;
    }
    $translation = $this->entityRepository->getTranslationFromContext($block_content);
    assert($translation instanceof BlockContentInterface);
    return $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(mixed $data, string $type, string $format = NULL, array $context = []): InlineBlock {
    if (!is_array($data)) {
      throw new UnexpectedValueException(sprintf('Block configuration should be an array'));
    }
    if (!array_key_exists('id', $data)) {
      throw new UnexpectedValueException(sprintf('Block is missing a plugin ID'));
    }
    if (!str_contains($data['id'], PluginBase::DERIVATIVE_SEPARATOR)) {
      throw new UnexpectedValueException(sprintf('Missing derivative information in block plugin %s', $data['id']));
    }
    if (!$this->blockManager->hasDefinition($data['id'])) {
      throw new UnexpectedValueException(sprintf('No such block plugin %s', $data['id']));
    }
    [, $bundle] = explode(PluginBase::DERIVATIVE_SEPARATOR, $data['id'], 2);
    $storage = $this->entityTypeManager->getStorage('block_content');
    assert($storage instanceof RevisionableStorageInterface);
    if (!empty($data['block_revision_id'])) {
      $block_content = $storage->loadRevision($data['block_revision_id']);
      if ($block_content instanceof BlockContentInterface) {
        foreach ($data['changed'] ?? [] as $changed_field) {
          if (!$block_content->hasField($changed_field)) {
            continue;
          }
          if (!$block_content->get($changed_field)->access('edit')) {
            continue;
          }
          if (array_key_exists($changed_field, $data['block_serialized'] ?? [])) {
            $block_content->set($changed_field, $data['block_serialized'][$changed_field]);
          }
        }
        $data['block_serialized'] = serialize($block_content);
        $instance = $this->blockManager->createInstance($data['id'], array_diff_key($data, array_flip([
          'id',
          'view_mode_uuid',
          'form_mode_uuid',
        ])));
        assert($instance instanceof InlineBlock);
        return $instance;
      }
    }
    // New block.
    $block_content = $storage->create([
      'type' => $bundle,
    ]);
    $definition = $this->entityTypeManager->getDefinition('block_content');
    $protected_fields = [
      $definition->getKey('id'),
      $definition->getKey('bundle'),
      $definition->getKey('revision'),
      $definition->getKey('uuid'),
      $definition->getKey('langcode'),
    ];
    foreach ($data['block_serialized'] ?? [] as $field => $field_data) {
      if (in_array($field, $protected_fields, TRUE)) {
        continue;
      }
      if (!$block_content->hasField($field)) {
        continue;
      }
      if (!$block_content->get($field)->access('edit')) {
        continue;
      }
      if (array_key_exists($field, $data['block_serialized'] ?? [])) {
        $block_content->set($field, $field_data);
      }
    }
    $data['block_serialized'] = serialize($block_content);
    $instance = $this->blockManager->createInstance($data['id'], array_diff_key($data, array_flip(['id'])));
    assert($instance instanceof InlineBlock);
    return $instance;
  }

}
