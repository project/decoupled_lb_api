<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Controller;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\MissingValueContextException;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Defines controller for selecting blocks and sections (layouts).
 */
final class SelectionController implements ContainerInjectionInterface {

  use LayoutBuilderContextTrait;

  public const DEFAULT_VALUES = 'default_values';

  /**
   * Constructs a new LayoutController.
   *
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layoutManager
   *   Layout plugin manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer.
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme manager.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   Serializer.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $contextHandler
   *   Context handler.
   */
  public function __construct(
    protected readonly LayoutPluginManagerInterface $layoutManager,
    protected readonly RendererInterface $renderer,
    protected readonly BlockManagerInterface $blockManager,
    protected readonly ModuleHandlerInterface $moduleHandler,
    protected readonly ThemeManagerInterface $themeManager,
    protected readonly Serializer $serializer,
    protected readonly ContextHandlerInterface $contextHandler,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.core.layout'),
      $container->get('renderer'),
      $container->get('plugin.manager.block'),
      $container->get('module_handler'),
      $container->get('theme.manager'),
      $container->get('serializer'),
      $container->get('context.handler'),
    );
  }

  /**
   * Gets the jSON representation of available sections for a given layout.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section storage.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   Response.
   */
  public function getSections(SectionStorageInterface $section_storage): CacheableJsonResponse {
    $cacheability = CacheableMetadata::createFromObject($section_storage);
    $context = new RenderContext();
    $sections = $this->renderer->executeInRenderContext($context, function () use ($section_storage) {
      $definitions = $this->layoutManager->getFilteredDefinitions('layout_builder', $this->getPopulatedContexts($section_storage), ['section_storage' => $section_storage]);
      $sections = [];
      foreach ($definitions as $plugin_id => $definition) {
        assert($definition instanceof LayoutDefinition);
        $icon = $definition->getIcon(60, 80, 1, 3);
        $sections[$plugin_id] = [
          'id' => $plugin_id,
          'label' => $definition->getLabel(),
          'icon' => $this->renderer->render($icon),
          'icon_is_url' => (bool) $definition->getIconPath(),
          'region_labels' => array_map(fn(array $region) => $region['label'], $definition->getRegions()),
        ];
      }
      return $sections;
    });
    if (!$context->isEmpty()) {
      $cacheability->addCacheableDependency($context->pop());
    }

    return (new CacheableJsonResponse(['sections' => $sections]))->addCacheableDependency($cacheability);
  }

  /**
   * Gets the jSON representation of a available sections for a given layout.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section storage.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   Response.
   */
  public function getBlocks(SectionStorageInterface $section_storage): CacheableJsonResponse {
    $consumer = 'layout_builder';
    $contexts = $this->getPopulatedContexts($section_storage);
    $sections = $this->layoutManager->getFilteredDefinitions($consumer, $contexts, ['section_storage' => $section_storage]);
    $cacheability = CacheableMetadata::createFromObject($section_storage);
    $blocks = [];
    $allowed = [];
    $all_definitions = $this->blockManager->getDefinitionsForContexts($contexts);
    $inline_definitions = array_filter($all_definitions, fn($key) => str_starts_with($key, 'inline_block:'), ARRAY_FILTER_USE_KEY);
    $other_definitions = array_diff_key($all_definitions, $inline_definitions);
    // Remove broken.
    unset($other_definitions['broken']);
    $section_storage_dummy = clone $section_storage;
    $section_storage_dummy->removeAllSections();
    $hooks = [];
    $hooks[] = "plugin_filter_block";
    $hooks[] = "plugin_filter_block__layout_builder";
    foreach ($sections as $section_plugin_id => $definition) {
      assert($definition instanceof LayoutDefinition);
      $section_storage_dummy->appendSection(new Section($section_plugin_id));
      $last_delta = $section_storage_dummy->count() - 1;
      foreach (array_keys($definition->getRegions()) as $region) {
        $inline_extra = [
          'section_storage' => $section_storage,
          'delta' => $last_delta,
          'list' => 'inline_blocks',
        ];
        $default_extra = [
          'section_storage' => $section_storage,
          'delta' => $last_delta,
          'region' => $region,
        ];
        $filtered_definitions = $other_definitions;
        $filtered_inline = $inline_definitions;
        $this->moduleHandler->alter($hooks, $filtered_definitions, $default_extra, $consumer);
        $this->themeManager->alter($hooks, $filtered_definitions, $default_extra, $consumer);
        $this->moduleHandler->alter($hooks, $filtered_inline, $inline_extra, $consumer);
        $this->themeManager->alter($hooks, $filtered_inline, $inline_extra, $consumer);
        $allowed[$section_plugin_id][$region] = array_keys(array_merge($filtered_definitions, $filtered_inline));
      }
    }
    // array_filter over $allowed for restrictions.
    foreach ($all_definitions as $plugin_id => $definition) {
      if ($plugin_id === 'broken') {
        continue;
      }
      $instance = $this->blockManager->createInstance($plugin_id);
      if ($instance instanceof ContextAwarePluginInterface) {
        $mapping = [];
        foreach ($instance->getContextDefinitions() as $context_slot => $context_definition) {
          $valid_contexts = $this->contextHandler->getMatchingContexts($contexts, $context_definition);
          if (array_key_exists('layout_builder.entity', $valid_contexts)) {
            // When there are multiple matching context, prefer the layout
            // builder entity.
            $mapping[$context_slot] = 'layout_builder.entity';
            continue;
          }
          // We're selecting the first one here. Without user intervention we
          // probably can't do this.
          // @todo add multiple blocks in this case and make this configurable
          // from the UI?
          $mapping[$context_slot] = key($valid_contexts);
        }
        $instance->setContextMapping($mapping);
        try {
          $this->contextHandler->applyContextMapping($instance, $contexts);
        }
        catch (MissingValueContextException) {
          // We carry on here.
        }
        catch (ContextException) {
          // We carry on here.
        }
      }
      $blocks[$plugin_id] = [
        'id' => $plugin_id,
        'label' => $definition['admin_label'],
        'category' => $definition['category'],
        'icon' => NULL,
        'icon_is_url' => FALSE,
        'default_configuration' => $this->serializer->normalize($instance, context: [self::DEFAULT_VALUES => TRUE]),
        'default_layout' => $definition['decoupled_lb_api']['default_layout'] ?? 'layout_onecol',
        'default_region' => $definition['decoupled_lb_api']['default_region'] ?? 'content',
        'section_restrictions' => array_reduce(array_keys($allowed), fn(array $carry, string $layout_id) => [
          ...$carry,
          [
            'layout_id' => $layout_id,
            'regions' => array_keys(array_filter($allowed[$layout_id], fn($definitions) => in_array($plugin_id, $definitions, TRUE))),
          ],
        ], []),
      ];
    }
    return (new CacheableJsonResponse(['blocks' => $blocks]))->addCacheableDependency($cacheability);
  }

}
