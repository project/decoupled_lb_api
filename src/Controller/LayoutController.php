<?php

declare(strict_types=1);

namespace Drupal\decoupled_lb_api\Controller;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\exceptions\UnresolvableReferenceException;
use cebe\openapi\json\JsonPointer;
use cebe\openapi\Reader;
use cebe\openapi\ReferenceContext;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\CsrfRequestHeaderAccessCheck;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Routing\RouteMatch;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\Event\PrepareLayoutEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\OverridesSectionStorageInterface;
use Drupal\layout_builder\SectionListInterface;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Drupal\layout_builder\SectionStorageInterface;
use League\OpenAPIValidation\Schema\Exception\KeywordMismatch;
use League\OpenAPIValidation\Schema\Exception\SchemaMismatch;
use League\OpenAPIValidation\Schema\SchemaValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Defines controller for layout information.
 */
final class LayoutController implements ContainerInjectionInterface {

  use LayoutBuilderContextTrait;

  /**
   * Constructs a new LayoutController.
   *
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface&\Symfony\Component\Serializer\SerializerInterface&\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $serializer
   *   Serializer.
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layoutTempstoreRepository
   *   Layout builder tempstore.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrfTokenGenerator
   *   CSRF token generator.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface $sectionStorageManager
   *   Section storage manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   Module extension list.
   * @param string $appRoot
   *   App root.
   */
  public function __construct(
    protected readonly SerializerInterface & NormalizerInterface & DenormalizerInterface $serializer,
    protected readonly LayoutTempstoreRepositoryInterface $layoutTempstoreRepository,
    protected readonly CsrfTokenGenerator $csrfTokenGenerator,
    protected readonly EventDispatcherInterface $eventDispatcher,
    protected readonly SectionStorageManagerInterface $sectionStorageManager,
    protected readonly ModuleExtensionList $moduleExtensionList,
    protected readonly string $appRoot,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('serializer'),
      $container->get('layout_builder.tempstore_repository'),
      $container->get('csrf_token'),
      $container->get('event_dispatcher'),
      $container->get('plugin.manager.layout_builder.section_storage'),
      $container->get('extension.list.module'),
      $container->getParameter('app.root'),
    );
  }

  /**
   * Gets the jSON representation of a layout.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section storage.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response.
   */
  public function getLayout(SectionStorageInterface $section_storage): JsonResponse {
    $cache = new CacheableMetadata();
    if ($section_storage instanceof OverridesSectionStorageInterface) {
      if (!$section_storage->isOverridden()) {
        return new JsonResponse(['data' => $this->serializer->normalize($section_storage->getDefaultSectionStorage(), context: ['cache' => $cache])]);
      }
    }
    return new JsonResponse(['data' => $this->serializer->normalize($section_storage, context: ['cache' => $cache])]);
  }

  /**
   * Updates the layout in Drupal's tempstore.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section storage.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response.
   */
  public function putLayout(SectionStorageInterface $section_storage, Request $request): JsonResponse {
    $this->checkCsrfHeader($request);
    $data = Json::decode($request->getContent());
    if (!is_array($data)) {
      return new JsonResponse([
        'errors' => [
          'request' => [
            'identifier' => 'request',
            'message' => 'Invalid JSON',
          ],
        ],
      ], Response::HTTP_BAD_REQUEST);
    }
    if (!array_key_exists('data', $data) || !array_key_exists('sections', $data['data'])) {
      return new JsonResponse([
        'errors' => [
          'request' => [
            'identifier' => 'request',
            'message' => "Invalid data structure in JSON, missing 'data' and 'data.sections'",
          ],
        ],
      ], Response::HTTP_BAD_REQUEST);
    }
    try {
      $specification_file = sprintf('%s/%s/openapi.schema.yml', $this->appRoot, $this->moduleExtensionList->getPath('decoupled_lb_api'));
      $specification = Reader::readFromYaml(file_get_contents($specification_file));
      $context = new ReferenceContext($specification, "/");
      $context->throwException = FALSE;
      $context->mode = ReferenceContext::RESOLVE_MODE_ALL;
      $specification->resolveReferences($context);
      $specification->setDocumentContext($specification, new JsonPointer(''));
      (new SchemaValidator())->validate($data, $specification->components->schemas['LayoutResponse']);
    }
    catch (KeywordMismatch | SchemaMismatch $e) {
      $fields_chain = $e->dataBreadCrumb()->buildChain();
      $field = array_pop($fields_chain);
      return new JsonResponse([
        'errors' => [
          $field => [
            'identifier' => $field,
            'message' => sprintf('%s: %s', implode('.', $fields_chain), $e->getMessage()),
          ],
        ],
      ], Response::HTTP_BAD_REQUEST);
    }
    catch (UnresolvableReferenceException | TypeErrorException $e) {
      return new JsonResponse([
        'errors' => [
          'request' => [
            'identifier' => 'request',
            'message' => $e->getMessage(),
          ],
        ],
      ], Response::HTTP_BAD_REQUEST);
    }
    try {
      $sections = $this->serializer->denormalize($data['data']['sections'], SectionListInterface::class);
    }
    catch (UnexpectedValueException $e) {
      return new JsonResponse([
        'errors' => [
          'request' => [
            'field' => 'request',
            'message' => $e->getMessage(),
          ],
        ],
      ], Response::HTTP_BAD_REQUEST);
    }
    $section_storage->removeAllSections();
    foreach ($sections as $section) {
      $section_storage->appendSection($section);
    }
    $this->layoutTempstoreRepository->set($section_storage);
    return $this->getLayout($section_storage);
  }

  /**
   * Reverts section to defaults.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Default layout.
   */
  public function revert(SectionStorageInterface $section_storage, Request $request): JsonResponse {
    $this->checkCsrfHeader($request);
    $section_storage
      ->removeAllSections()
      ->save();
    $this->layoutTempstoreRepository->delete($section_storage);
    return $this->getLayout($section_storage);
  }

  /**
   * Discards tempstore changes.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Default layout.
   */
  public function discard(SectionStorageInterface $section_storage, Request $request): JsonResponse {
    $this->checkCsrfHeader($request);

    // Discard changes.
    $this->layoutTempstoreRepository->delete($section_storage);

    // Create a fresh section storage object by mimicking the logic from
    // \Drupal\layout_builder\Routing\LayoutSectionStorageParamConverter::convert
    // This isn't needed for core's normal editing because the discard form
    // workflow triggers a redirect which reruns routing parameter conversion.
    $type = $section_storage->getStorageType();
    $route_match = RouteMatch::createFromRequest($request);
    $route = $route_match->getRouteObject();
    $contexts = $this->sectionStorageManager->loadEmpty($type)->deriveContextsFromRoute($section_storage->getStorageId(), $route->getOption('parameters')['section_storage'] ?? [], 'section_storage', []);
    $section_storage = $this->sectionStorageManager->load($section_storage->getStorageType(), $contexts);

    // Trigger the prepare layout event so that anyone using Drupal core's
    // layout editing UI at the same time doesn't find their AJAX requests fail
    // because the tempstore is empty. Again this isn't an issue for editing via
    // the core UI because this is handled by the Layout Builder render
    // element's preRender callback.
    $event = new PrepareLayoutEvent($section_storage);
    $this->eventDispatcher->dispatch($event, LayoutBuilderEvents::PREPARE_LAYOUT);

    // Return the default state of the layout now the changes have been
    // discarded.
    return $this->getLayout($this->layoutTempstoreRepository->get($section_storage));
  }

  /**
   * Saves tempstore changes.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   Section.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Default layout.
   */
  public function save(SectionStorageInterface $section_storage, Request $request): JsonResponse {
    $this->checkCsrfHeader($request);

    // Discard changes.
    $section_storage->save();
    $this->layoutTempstoreRepository->delete($section_storage);
    // Trigger the prepare layout event so that anyone using Drupal core's
    // layout editing UI at the same time doesn't find their AJAX requests fail
    // because the tempstore is empty. Again this isn't an issue for editing via
    // the core UI because this is handled by the Layout Builder render
    // element's preRender callback.
    $event = new PrepareLayoutEvent($section_storage);
    $this->eventDispatcher->dispatch($event, LayoutBuilderEvents::PREPARE_LAYOUT);

    // Return the default state of the layout now the changes have been
    // saved.
    return $this->getLayout($section_storage);
  }

  /**
   * Checks CSRF header.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  protected function checkCsrfHeader(Request $request): void {
    $token = $request->headers->get('X-CSRF-Token');
    if ($token === NULL) {
      throw new AccessDeniedHttpException('Missing X-CSRF-Token header');
    }
    if (!$this->csrfTokenGenerator->validate($token, CsrfRequestHeaderAccessCheck::TOKEN_KEY)) {
      throw new AccessDeniedHttpException('Invalid X-CSRF-Token header');
    }
  }

}
